package ports

import (
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.ProviderRequest)
	Start()
}

// IncomingQueryPort ...
type IncomingQueryPort interface {
	AsyncPort
	Get(id common.ID) (service.ProviderModel, error)
	List() ([]service.ProviderModel, error)
}

// IncomingEventPort ...
type IncomingEventPort interface {
	AsyncPort
	Create(request service.Provider, transactionID common.TransactionID) error
	Update(request service.Provider, updateFieldNames []string, transactionID common.TransactionID) error
	Delete(id common.ID, transactionID common.TransactionID) error
}

// OutgoingEventPort ...
type OutgoingEventPort interface {
	Port
	Created(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error
	CreateFailed(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error
	Updated(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error
	UpdateFailed(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error
	Deleted(provider service.Provider, status common.HTTPStatus, transactionID common.TransactionID) error
	DeleteFailed(id common.ID, status common.HTTPStatus, transactionID common.TransactionID) error
}

// ProviderStoragePort ...
type ProviderStoragePort interface {
	Init(*types.Config)
	Create(service.ProviderModel) error
	Get(common.ID) (service.ProviderModel, error)
	List() ([]service.ProviderModel, error)
	Update(service.ProviderModel, []string) error
	Delete(common.ID) error
}

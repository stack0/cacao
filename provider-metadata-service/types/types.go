package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderRequest is a bundle for domain data and an event/query operation on that data.
type ProviderRequest struct {
	service.Provider
	Operation        string
	UpdateFieldNames []string // list field names to update, this is used only when operation is Update
	TransactionID    common.TransactionID
	Response         chan ProviderResponse // channel created by adapter(or other caller) and will receive the response
}

// ProviderResponse ...
type ProviderResponse struct {
	Data  []service.ProviderModel
	Error error
}

package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection messaging.QueryEventService
	WaitGroup  sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.WaitGroup = sync.WaitGroup{}
	adapter.WaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.WaitGroup.Done()
}

func (adapter *QueryAdapter) getQueryHandlerMapping() []messaging.QueryEventHandlerMapping {
	return []messaging.QueryEventHandlerMapping{
		{
			Subject:      service.ProviderGetQueryOp,
			EventHandler: adapter.handleProviderGetQuery,
		},
		{
			Subject:      service.ProviderListQueryOp,
			EventHandler: adapter.handleProviderListQuery,
		},
		{
			Subject:      common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	natsConn, err := messaging.ConnectNatsForService(&natsConfig, adapter.getQueryHandlerMapping())
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.WaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	natsConn, err := messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, adapter.getQueryHandlerMapping())
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.WaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleProviderGetQuery(subject common.QueryOp, transactionID common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.handleProviderGetQuery",
	})

	var getRequest service.ProviderModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderModel"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	provider, err := adapter.IncomingPort.Get(getRequest.GetID())
	var providerGetResponse common.ServiceRequestResult

	if err == nil {
		providerGetResponse = common.ServiceRequestResult{
			Data: provider,
			Status: common.HTTPStatus{
				Message: "got a provider successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		if types.IsProviderNotFoundError(err) {
			providerGetResponse = common.ServiceRequestResult{
				Status: common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusNotFound,
				},
			}
		} else if types.IsProviderNotAuthorizedError(err) {
			providerGetResponse = common.ServiceRequestResult{
				Status: common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusUnauthorized,
				},
			}
		} else {
			logger.Error(err)

			providerGetResponse = common.ServiceRequestResult{
				Status: common.HTTPStatus{
					Message: err.Error(),
					Code:    http.StatusInternalServerError,
				},
			}
		}
	}

	resBytes, err := json.Marshal(providerGetResponse)
	if err != nil {
		errorMessage := "unable to marshal ServiceRequestResult into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleProviderListQuery(subject common.QueryOp, transactionID common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.handleProviderListQuery",
	})

	var result common.ServiceRequestResult
	providers, err := adapter.IncomingPort.List()
	if err == nil {
		result = common.ServiceRequestResult{
			Data: providers,
			Status: common.HTTPStatus{
				Message: "listed providers successfully",
				Code:    http.StatusOK,
			},
		}
	} else {
		logger.Error(err)
		result = common.ServiceRequestResult{
			Status: common.HTTPStatus{
				Message: err.Error(),
				Code:    http.StatusInternalServerError,
			},
		}
	}
	j, err := json.Marshal(result)
	if err != nil {
		errorMessage := "unable to marshal ServiceRequestResult into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return j, nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject common.QueryOp, transactionID common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "provider-metadata-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})
	logger.Tracef("received an unhandled query %s, TransactionID %s", subject, transactionID)

	errResponse := common.ServiceRequestResult{
		Status: common.HTTPStatus{
			Message: fmt.Sprintf("request %s is not handled", subject),
			Code:    http.StatusNotImplemented,
		},
	}
	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		errorMessage := "unable to marshal ServiceRequestResult into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

package domain

import (
	"fmt"

	"gitlab.com/cyverse/cacao/provider-metadata-service/types"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.ProviderRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.ProviderRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {
}

// Get retrieves the Provider
func (impl *QueryPortImpl) Get(id common.ID) (service.ProviderModel, error) {
	if len(id) == 0 {
		return service.ProviderModel{}, fmt.Errorf("Get input validation error: ProviderID is empty")
	}
	p, err := impl.DomainImpl.Storage.Get(id)
	if err != nil {
		return service.ProviderModel{}, err
	}
	return p, nil
}

// List retrieves all providers of the user
func (impl *QueryPortImpl) List() ([]service.ProviderModel, error) {
	providers, err := impl.DomainImpl.Storage.List()
	if err != nil {
		return []service.ProviderModel{}, err
	}
	return providers, nil
}

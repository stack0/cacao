package domain

import (
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	conf      *types.Config
	QueriesIn ports.IncomingQueryPort
	EventsIn  ports.IncomingEventPort
	EventsOut ports.OutgoingEventPort
	Storage   ports.ProviderStoragePort
}

// Init ...
func (d *Domain) Init(conf *types.Config) {
	d.conf = conf
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start ...
func (d Domain) Start() {
}

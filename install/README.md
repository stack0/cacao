# Install

### Table of Contents
[[_TOC_]]

## Getting Started

This directory is reserved for ansible and other resources needed to deploy the CACAO platform and user clusters.

### Supported Operating Systems:

Ubuntu 18.04 or 20.04 

## CACAO Deployment

CACAO deployment is done using the `deploy.sh` script in this directory.  Currently, there are two supported environments that are passed as arguments to this script: `local` and `prod`.

### Local development

If you pass `local` as the argument to `deploy.sh`, the script will first install gettext-base, python3-pip, ansible-base, ansible-core, jq, and curl if they are not already present.  Next, the script will prompt for the following:

```
$ ./deploy.sh local
Enter your cluster type, either k3s or k3d [k3d]: 
Enter the path to the cacao-edge-deployment source code directory [/home/ubuntu/cacao-edge-deployment]: 
Enter the path to the Cacao source code directory [/home/ubuntu/cacao]: 
Enter the Git branch you want to use [master]:
Enter the URL to the Git remote repo [https://gitlab.com/cyverse/cacao.git]: 
```

The script will then generate a configuration file called `config.local.yml`, symbolically link it to `config.yml`, configure an AWM key, install necessary roles from ansible-galaxy, and finally run the `playbook-cacao.yml` Ansible playbook if ran as root.  If not root, it will add your user to the docker group and instruct you to "please log out, log back in, and re-run this script."  At that point it will run `playbook-cacao.yml`.

playbook-cacao.yml will install docker and k3s or k3d based on your response to the prompt above and provision two Kubernetes clusters, one for running the application itself (the "service cluster") and one for running the analyses (the "user cluster").  It will then deploy the CACAO application itself and any required dependencies to these clusters, including Vault, Keycloak, and MongoDB.

Once the script has finished, move to the project's root to run `skaffold dev --status-check=false` and deploy CACAO to your local Kubernetes cluster.

An entry will be added to your `/etc/hosts` file pointing the value of `API_DOMAIN` at the IP address of the NGINX ingress gateway.

### Production deployment

Before you deploy to production, you must:

0. Verify that you have access to [Stache](https://stache.arizona.edu/).
1. Copy the production `hosts.yml` file secret to a file named `hosts.yml` in this directory (ask in Slack if you have trouble finding it).

Now run `./deploy.sh prod`.  You will be prompted for the Git repo and branch you wish to deploy (these will default to `https://gitlab.com/cyverse/cacao.git` and `master` respectively, unlike `local` which defaults to the remote and branch you have checked out).  Next, you will be prompted for the Stache API endpoint for the production secrets.  Ask for the name of the secret in Slack and look it up in Stache.  Once you've found it, click the "Options" tab on the right side of the page.  Copy the path in the "API Endpoint" text area under "READ DATA" and paste it into your terminal.  Then you'll be prompted for the `X-STACHE-KEY`.  Click the greyed out text area entitled "X-STACHE-KEY" to view the value and copy it into your terminal.  The script will now generate a file called `config.prod.yml`, symbolically link it to `config.yml`, and then run Ansible to do a production deployment.

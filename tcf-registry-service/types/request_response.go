package types

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
)

// TCFRegistryChannelRequest is a request struct used between adapters and domain
type TCFRegistryChannelRequest struct {
	Actor            string
	Emulator         string
	Data             interface{}
	Operation        string
	UpdateFieldNames []string // list field names to update, this is used only when operation is Update
	QueryParams      map[string]string
	TransactionID    cacao_common.TransactionID
	Response         chan TCFRegistryChannelResponse // channel created by adapter(or other caller) and will receive the response
}

// TCFRegistryChannelResponse is a response struct used between adapters and domain
type TCFRegistryChannelResponse struct {
	Data  interface{}
	Error error
}

package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertFromModel converts TCFRegistryEntryModel to TCFRegistryEntry
func ConvertFromModel(model cacao_common_service.TCFRegistryEntryModel) TCFRegistryEntry {
	entry := TCFRegistryEntry{
		Name:                      model.Name,
		Description:               model.Description,
		Type:                      model.Type,
		QueryTarget:               model.QueryTarget,
		QueryData:                 model.QueryData,
		QueryResultJSONPathFilter: model.QueryResultJSONPathFilter,
	}

	return entry
}

// ConvertToModel converts TCFRegistryEntry to TCFRegistryEntryModel
func ConvertToModel(session cacao_common_service.Session, entry TCFRegistryEntry) cacao_common_service.TCFRegistryEntryModel {
	return cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Name:                      entry.Name,
		Description:               entry.Description,
		Type:                      entry.Type,
		QueryTarget:               entry.QueryTarget,
		QueryData:                 entry.QueryData,
		QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
	}
}

// ConvertToQueryResultModel converts TCFRegistryQueryResult to TCFRegistryQueryResultModel
func ConvertToQueryResultModel(session cacao_common_service.Session, result TCFRegistryQueryResult) cacao_common_service.TCFRegistryQueryResultModel {
	return cacao_common_service.TCFRegistryQueryResultModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		EntryName: result.EntryName,
		DataType:  result.DataType,
		Value:     result.Value,
	}
}

// ConvertToListItemModel converts TCFRegistryEntry to TCFRegistryEntryListItemModel
func ConvertToListItemModel(entry TCFRegistryEntry) cacao_common_service.TCFRegistryEntryListItemModel {
	return cacao_common_service.TCFRegistryEntryListItemModel{
		Name:                      entry.Name,
		Description:               entry.Description,
		Type:                      entry.Type,
		QueryTarget:               entry.QueryTarget,
		QueryData:                 entry.QueryData,
		QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
	}
}

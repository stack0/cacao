package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// TCFRegistryQueryResult is a struct for storing tcf registry query result
type TCFRegistryQueryResult struct {
	EntryName string      `json:"entry_name"`
	DataType  string      `json:"data_type"`
	Value     interface{} `json:"value"`
}

// TCFRegistryEntry is a struct for storing tcf registry entry information
type TCFRegistryEntry struct {
	Name                      string                                    `bson:"_id" json:"name,omitempty"`
	Description               string                                    `bson:"description" json:"description,omitempty"`
	Type                      cacao_common_service.TCFRegistryEntryType `bson:"type" json:"type,omitempty"`
	QueryTarget               string                                    `bson:"query_target" json:"query_target,omitempty"`
	QueryData                 string                                    `bson:"query_data" json:"query_data,omitempty"`
	QueryResultJSONPathFilter string                                    `bson:"query_result_jsonpath_filter" json:"query_result_jsonpath_filter,omitempty"`
}

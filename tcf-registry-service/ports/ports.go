package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.TCFRegistryChannelRequest)
	Start()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	AsyncPort
	List(actor string, emulator string) ([]types.TCFRegistryEntry, error)
	Get(actor string, emulator string, name string) (types.TCFRegistryEntry, error)
	Query(actor string, emulator string, name string, queryParams map[string]string) (types.TCFRegistryQueryResult, error)
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	Create(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error
	Update(actor string, emulator string, entry types.TCFRegistryEntry, updateFieldNames []string, transactionID cacao_common.TransactionID) error
	Delete(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error
}

// OutgoingEventPort is an outgoing event port
type OutgoingEventPort interface {
	Port
	Created(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error
	CreateFailed(actor string, emulator string, entry types.TCFRegistryEntry, creationError error, transactionID cacao_common.TransactionID) error
	Updated(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error
	UpdateFailed(actor string, emulator string, entry types.TCFRegistryEntry, updateError error, transactionID cacao_common.TransactionID) error
	Deleted(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error
	DeleteFailed(actor string, emulator string, entry types.TCFRegistryEntry, deletionError error, transactionID cacao_common.TransactionID) error
}

// PersistentStoragePort is an interface for Perstistent Storage.
type PersistentStoragePort interface {
	Port
	List() ([]types.TCFRegistryEntry, error)
	Get(name string) (types.TCFRegistryEntry, error)
	Create(entry types.TCFRegistryEntry) error
	Update(entry types.TCFRegistryEntry, updateFieldNames []string) error
	Delete(name string) error
}

// ExternalQueryPort is an interface for an d
type ExternalQueryPort interface {
	Port
	Query(actor string, emulator string, entry types.TCFRegistryEntry, queryParams map[string]string) (types.TCFRegistryQueryResult, error)
}

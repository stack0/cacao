package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// CreatedHandler is a handler for Created event
type CreatedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CreateFailedHandler is a handler for CreateFailed event
type CreateFailedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdatedHandler is a handler for Updated event
type UpdatedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// UpdateFailedHandler is a handler for UpdateFailed event
type UpdateFailedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeletedHandler is a handler for Deleted event
type DeletedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// DeleteFailedHandler is a handler for DeleteFailed event
type DeleteFailedHandler func(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// MockOutgoingEventPort is a mock implementation of OutgoingEventPort
type MockOutgoingEventPort struct {
	Config              *types.Config
	CreatedHandler      CreatedHandler
	CreateFailedHandler CreateFailedHandler
	UpdatedHandler      UpdatedHandler
	UpdateFailedHandler UpdateFailedHandler
	DeletedHandler      DeletedHandler
	DeleteFailedHandler DeleteFailedHandler
}

// Init inits the port
func (port *MockOutgoingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockOutgoingEventPort) Finalize() {}

// SetCreatedHandler sets a handler for Created event
func (port *MockOutgoingEventPort) SetCreatedHandler(createdHandler CreatedHandler) {
	port.CreatedHandler = createdHandler
}

// SetCreateFailedHandler sets a handler for CreateFailed event
func (port *MockOutgoingEventPort) SetCreateFailedHandler(createFailedHandler CreateFailedHandler) {
	port.CreateFailedHandler = createFailedHandler
}

// SetUpdatedHandler sets a handler for Updated event
func (port *MockOutgoingEventPort) SetUpdatedHandler(updatedHandler UpdatedHandler) {
	port.UpdatedHandler = updatedHandler
}

// SetUpdateFailedHandler sets a handler for UpdateFailed event
func (port *MockOutgoingEventPort) SetUpdateFailedHandler(updateFailedHandler UpdateFailedHandler) {
	port.UpdateFailedHandler = updateFailedHandler
}

// SetDeletedHandler sets a handler for Deleted event
func (port *MockOutgoingEventPort) SetDeletedHandler(deletedHandler DeletedHandler) {
	port.DeletedHandler = deletedHandler
}

// SetDeleteFailedHandler sets a handler for DeleteFailed event
func (port *MockOutgoingEventPort) SetDeleteFailedHandler(deleteFailedHandler DeleteFailedHandler) {
	port.DeleteFailedHandler = deleteFailedHandler
}

// Created creates an event for tcf registry entry creation
func (port *MockOutgoingEventPort) Created(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CreatedHandler(actor, emulator, entry, status, transactionID)
}

// CreateFailed creates an event for tcf registry entry creation failure
func (port *MockOutgoingEventPort) CreateFailed(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CreateFailedHandler(actor, emulator, entry, status, transactionID)
}

// Updated creates an event for tcf registry entry update
func (port *MockOutgoingEventPort) Updated(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdatedHandler(actor, emulator, entry, status, transactionID)
}

// UpdateFailed creates an event for tcf registry entry update failure
func (port *MockOutgoingEventPort) UpdateFailed(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.UpdateFailedHandler(actor, emulator, entry, status, transactionID)
}

// Deleted creates an event for tcf registry entry deletion
func (port *MockOutgoingEventPort) Deleted(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeletedHandler(actor, emulator, entry, status, transactionID)
}

// DeleteFailed creates an event for tcf registry entry deletion failure
func (port *MockOutgoingEventPort) DeleteFailed(actor string, emulator string, entry types.TCFRegistryEntry, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.DeleteFailedHandler(actor, emulator, entry, status, transactionID)
}

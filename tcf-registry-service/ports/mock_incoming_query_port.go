package ports

import (
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// ListHandler is a handler for List query
type ListHandler func(actor string, emulator string) ([]types.TCFRegistryEntry, error)

// GetHandler is a handler for Get query
type GetHandler func(actor string, emulator string, name string) (types.TCFRegistryEntry, error)

// QueryHandler is a handler for Query query
type QueryHandler func(actor string, emulator string, name string, queryParams map[string]string) (types.TCFRegistryQueryResult, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config       *types.Config
	ListHandler  ListHandler
	GetHandler   GetHandler
	QueryHandler QueryHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.TCFRegistryChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

// SetListHandler sets a handler for List query
func (port *MockIncomingQueryPort) SetListHandler(listHandler ListHandler) {
	port.ListHandler = listHandler
}

// SetGetHandler sets a handler for Get query
func (port *MockIncomingQueryPort) SetGetHandler(getHandler GetHandler) {
	port.GetHandler = getHandler
}

// List lists tcf registry entries
func (port *MockIncomingQueryPort) List(actor string, emulator string) ([]types.TCFRegistryEntry, error) {
	return port.ListHandler(actor, emulator)
}

// Get returns a tcf registry entry
func (port *MockIncomingQueryPort) Get(actor string, emulator string, name string) (types.TCFRegistryEntry, error) {
	return port.GetHandler(actor, emulator, name)
}

// Query returns results of a query to a tcf registry entry
func (port *MockIncomingQueryPort) Query(actor string, emulator string, name string, queryParams map[string]string) (types.TCFRegistryQueryResult, error) {
	return port.QueryHandler(actor, emulator, name, queryParams)
}

package fixtures

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/tcf-registry-service/ports"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"

	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
)

const (
	// TCFRegistryEntryFixturesFilename is the name of a file containing fixtures
	TCFRegistryEntryFixturesFilename string = "tcf_registry_entry_fixtures.json"
)

// DataFixtures communicates to IncomingEventPort to add fixtures
type DataFixtures struct {
	Config            *types.Config
	IncomingEventPort ports.IncomingEventPort
	IncomingQueryPort ports.IncomingQueryPort
}

// Init initializes the fixtures
func (fixtures *DataFixtures) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.fixtures",
		"function": "DataFixtures.Init",
	})

	logger.Info("initializing DataFixtures")

	fixtures.Config = config
}

// Start starts adding the fixtures
func (fixtures *DataFixtures) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.fixtures",
		"function": "DataFixtures.Start",
	})

	logger.Info("starting DataFixtures")

	actor := "cacao-admin"
	emulator := actor

	fixtures.addTCFRegistryEntries(actor, emulator)
}

func (fixtures *DataFixtures) getTCFRegistryEntryFixtures() ([]types.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.fixtures",
		"function": "DataFixtures.getTCFRegistryEntryFixtures",
	})

	// read tcf_registry_entry_fixtures.json
	absFixturesFilename, err := filepath.Abs(TCFRegistryEntryFixturesFilename)
	if err != nil {
		errorMessage := fmt.Sprintf("Could not access the fixtures file %s", TCFRegistryEntryFixturesFilename)
		logger.WithError(err).Errorf(errorMessage)
		return []types.TCFRegistryEntry{}, fmt.Errorf(errorMessage)
	}

	_, err = os.Stat(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("the fixture file (%s) error", absFixturesFilename)
		return []types.TCFRegistryEntry{}, err
	}

	jsonBytes, err := ioutil.ReadFile(absFixturesFilename)
	if err != nil {
		logger.WithError(err).Errorf("Could not read the fixture file %s", absFixturesFilename)
		return []types.TCFRegistryEntry{}, err
	}

	tcfRegistryEntryFixtures := []types.TCFRegistryEntry{}

	err = json.Unmarshal(jsonBytes, &tcfRegistryEntryFixtures)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into []TCFRegistryEntry"
		logger.WithError(err).Error(errorMessage)
		return []types.TCFRegistryEntry{}, fmt.Errorf(errorMessage)
	}

	return tcfRegistryEntryFixtures, nil
}

func (fixtures *DataFixtures) addTCFRegistryEntries(actor string, emulator string) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.fixtures",
		"function": "DataFixtures.addTCFRegistryEntries",
	})

	tcfRegistryEntries, err := fixtures.IncomingQueryPort.List(actor, emulator)
	if err != nil {
		logger.WithError(err).Fatal("unable to list tcf registry entries")
	}

	if len(tcfRegistryEntries) == 0 {
		// add fixtures only when there is no tcf registry entries registered
		logger.Info("adding tcf registry entries")

		tcfRegistryEntries, err := fixtures.getTCFRegistryEntryFixtures()
		if err != nil {
			logger.WithError(err).Fatal("unable to get tcf registry entry fixtures")
		}

		for _, tcfRegistryEntry := range tcfRegistryEntries {
			logger.Infof(`adding tcf registry entry "%s"`, tcfRegistryEntry.Name)

			transactionID := cacao_common_messaging.NewTransactionID()
			err := fixtures.IncomingEventPort.Create(actor, emulator, tcfRegistryEntry, transactionID)
			if err != nil {
				logger.WithError(err).Errorf("failed to add tcf registry entry %s", tcfRegistryEntry.Name)
			}
		}

		logger.Infof("added %d tcf registry entries", len(tcfRegistryEntries))
	} else {
		logger.Infof("there are %d tcf registry entries already - skip", len(tcfRegistryEntries))
	}
}

# Template Custom Field (TCF) Registry Service

The Template Custom Field (TCF) Registry Service is responsible for maintaining template custom fields.

# Configuration
configurations are set via environment variables.

| env name                           | description                           | default                   |
|------------------------------------|---------------------------------------|---------------------------|
| `TCFRegistryMongoDBCollectionName` | -                                     | tcfregistry               |
| `CACAO_API_URL`                    | base url for REST API                 |
| `NATS_URL`                         | -                                     | nats://nats:4222          |
| `NATS_QUEUE_GROUP`                 | -                                     |                           |
| `NATS_WILDCARD_SUBJECT`            | Should be set to  `cacao.openstack.>` | cyverse.>                 |
| `NATS_CLIENT_ID`                   | -                                     |                           |
| `NATS_MAX_RECONNECTS`              | -                                     | -1                        |
| `NATS_RECONNECT_WAIT`              | -                                     | -1                        |
| `NATS_REQUEST_TIMEOUT`             | -                                     | -1                        |
| `MONGODB_URL`                      | -                                     | mongodb://localhost:27017 |
| `MONGODB_DB_NAME`                  | -                                     |                           |

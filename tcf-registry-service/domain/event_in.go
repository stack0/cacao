package domain

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Config     *types.Config
	Channel    chan types.TCFRegistryChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *EventPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *EventPortImpl) Finalize() {
}

// InitChannel ...
func (impl *EventPortImpl) InitChannel(channel chan types.TCFRegistryChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *EventPortImpl) Start() {

}

// Create creates a tcf registry entry
func (impl *EventPortImpl) Create(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(entry.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: tcf registry entry name is empty")
	}

	if len(entry.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: tcf registry entry type is empty")
	}

	if len(entry.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: tcf registry entry query target is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	tcfRegistryEntryRequest := types.TCFRegistryChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TCFRegistryEntry{
			Name:                      entry.Name,
			Description:               entry.Description,
			Type:                      entry.Type,
			QueryTarget:               entry.QueryTarget,
			QueryData:                 entry.QueryData,
			QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
		},
		Operation:     string(cacao_common_service.TCFRegistryEntryCreateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- tcfRegistryEntryRequest

	// receive response
	tcfRegistryEntryResponse := <-responseChannel
	return tcfRegistryEntryResponse.Error
}

// Update updates the tcf registry entry
func (impl *EventPortImpl) Update(actor string, emulator string, entry types.TCFRegistryEntry, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(entry.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: tcf registry entry name is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	tcfRegistryEntryRequest := types.TCFRegistryChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TCFRegistryEntry{
			Name:                      entry.Name,
			Description:               entry.Description,
			Type:                      entry.Type,
			QueryTarget:               entry.QueryTarget,
			QueryData:                 entry.QueryData,
			QueryResultJSONPathFilter: entry.QueryResultJSONPathFilter,
		},
		Operation:        string(cacao_common_service.TCFRegistryEntryUpdateRequestedEvent),
		UpdateFieldNames: append(updateFieldNames, "updated_at"),
		TransactionID:    transactionID,
		Response:         responseChannel,
	}

	// request to model
	impl.Channel <- tcfRegistryEntryRequest

	// receive response
	tcfRegistryEntryResponse := <-responseChannel
	return tcfRegistryEntryResponse.Error
}

// Delete deletes the tcf registry entry
func (impl *EventPortImpl) Delete(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(entry.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: tcf registry entry name is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	tcfRegistryEntryRequest := types.TCFRegistryChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TCFRegistryEntry{
			Name: entry.Name,
		},
		Operation:     string(cacao_common_service.TCFRegistryEntryDeleteRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- tcfRegistryEntryRequest

	// receive response
	tcfRegistryEntryResponse := <-responseChannel
	return tcfRegistryEntryResponse.Error
}

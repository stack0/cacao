package domain

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/ports"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	Config        *types.Config
	Storage       ports.PersistentStoragePort
	QueryIn       ports.IncomingQueryPort
	EventIn       ports.IncomingEventPort
	EventOut      ports.OutgoingEventPort
	ExternalQuery ports.ExternalQueryPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.Config = config
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	qchannel := make(chan types.TCFRegistryChannelRequest, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go func() {
		defer wg.Done()
		d.QueryIn.Start()
	}()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, &wg)

	wg.Add(1)
	echannel := make(chan types.TCFRegistryChannelRequest, types.DefaultChannelBufferSize)
	d.EventIn.InitChannel(echannel)
	go func() {
		defer wg.Done()
		d.EventIn.Start()
	}()

	// start the domain's event worker
	wg.Add(1)
	go d.processEventWorker(echannel, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.TCFRegistryChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "domain.processQueryWorker",
	})

	defer wg.Done()

	for request := range qchannel {
		logger.Debugf("received a query - %s", request.Operation)

		tcfRegistryEntryRequest, ok := request.Data.(types.TCFRegistryEntry)
		if !ok {
			request.Response <- types.TCFRegistryChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into TCFRegistryEntry"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.TCFRegistryEntryListQueryOp):
			entries, err := d.ListTCFRegistryEntries(request.Actor, request.Emulator)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  entries,
					Error: err,
				}
			}
		case string(cacao_common_service.TCFRegistryEntryGetQueryOp):
			entry, err := d.GetTCFRegistryEntry(request.Actor, request.Emulator, tcfRegistryEntryRequest.Name)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  entry,
					Error: err,
				}
			}
		case string(cacao_common_service.TCFRegistryQueryQueryOp):
			queryResult, err := d.QueryTCFRegistryEntry(request.Actor, request.Emulator, tcfRegistryEntryRequest.Name, request.QueryParams)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  queryResult,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

func (d *Domain) processEventWorker(echannel chan types.TCFRegistryChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "domain.processEventWorker",
	})

	defer wg.Done()

	for request := range echannel {
		logger.Debugf("received an event - %s", request.Operation)

		tcfRegistryEntryRequest, ok := request.Data.(types.TCFRegistryEntry)
		if !ok {
			request.Response <- types.TCFRegistryChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into tcf registry entry"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.TCFRegistryEntryCreateRequestedEvent):
			err := d.CreateTCFRegistryEntry(request.Actor, request.Emulator, tcfRegistryEntryRequest, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TCFRegistryEntryUpdateRequestedEvent):
			err := d.UpdateTCFRegistryEntry(request.Actor, request.Emulator, tcfRegistryEntryRequest, request.UpdateFieldNames, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.TCFRegistryEntryDeleteRequestedEvent):
			err := d.DeleteTCFRegistryEntry(request.Actor, request.Emulator, tcfRegistryEntryRequest.Name, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.TCFRegistryChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

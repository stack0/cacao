package domain

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.TCFRegistryChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.TCFRegistryChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {

}

// List retrieves all tcf registry entries
func (impl *QueryPortImpl) List(actor string, emulator string) ([]types.TCFRegistryEntry, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	// request to model
	impl.Channel <- types.TCFRegistryChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.TCFRegistryEntry{},
		Operation: string(cacao_common_service.TCFRegistryEntryListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	tcfRegistryEntryResponse := <-responseChannel

	if tcfRegistryEntryResponse.Error != nil {
		return nil, tcfRegistryEntryResponse.Error
	}

	entries, ok := tcfRegistryEntryResponse.Data.([]types.TCFRegistryEntry)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TCFRegistryEntry array")
	}

	return entries, nil
}

// Get retrieves the tcf registry entry
func (impl *QueryPortImpl) Get(actor string, emulator string, name string) (types.TCFRegistryEntry, error) {
	if len(actor) == 0 {
		return types.TCFRegistryEntry{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(name) == 0 {
		return types.TCFRegistryEntry{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: entry name is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	// request to model
	impl.Channel <- types.TCFRegistryChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TCFRegistryEntry{
			Name: name,
		},
		Operation: string(cacao_common_service.TCFRegistryEntryGetQueryOp),
		Response:  responseChannel,
	}

	// receive response
	tcfRegistryEntryResponse := <-responseChannel

	if tcfRegistryEntryResponse.Error != nil {
		return types.TCFRegistryEntry{}, tcfRegistryEntryResponse.Error
	}

	entry, ok := tcfRegistryEntryResponse.Data.(types.TCFRegistryEntry)
	if !ok {
		return types.TCFRegistryEntry{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TCFRegistryEntry")
	}

	return entry, nil
}

// Query retrieves the tcf registry entry and query to the target defined in the entry
func (impl *QueryPortImpl) Query(actor string, emulator string, name string, queryParams map[string]string) (types.TCFRegistryQueryResult, error) {
	if len(actor) == 0 {
		return types.TCFRegistryQueryResult{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(name) == 0 {
		return types.TCFRegistryQueryResult{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: entry name is empty")
	}

	responseChannel := make(chan types.TCFRegistryChannelResponse)

	// request to model
	impl.Channel <- types.TCFRegistryChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.TCFRegistryEntry{
			Name: name,
		},
		Operation:   string(cacao_common_service.TCFRegistryQueryQueryOp),
		QueryParams: queryParams,
		Response:    responseChannel,
	}

	// receive response
	tcfRegistryQueryResultResponse := <-responseChannel

	if tcfRegistryQueryResultResponse.Error != nil {
		return types.TCFRegistryQueryResult{}, tcfRegistryQueryResultResponse.Error
	}

	queryResult, ok := tcfRegistryQueryResultResponse.Data.(types.TCFRegistryQueryResult)
	if !ok {
		return types.TCFRegistryQueryResult{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TCFRegistryQueryResult")
	}

	return queryResult, nil
}

package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// ListTCFRegistryEntries returns tcf registry entries
func (d *Domain) ListTCFRegistryEntries(actor string, emulator string) ([]types.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "Domain.ListTCFRegistryEntries",
	})

	logger.Info("List tcf registry entries")

	entries, err := d.Storage.List()
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return entries, nil
}

// GetTCFRegistryEntry returns the tcf registry entry with the name
func (d *Domain) GetTCFRegistryEntry(actor string, emulator string, name string) (types.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "Domain.GetTCFRegistryEntry",
	})

	logger.Infof("Get tcf registry entry %s", name)

	entry, err := d.Storage.Get(name)
	if err != nil {
		logger.Error(err)
		return types.TCFRegistryEntry{}, err
	}

	return entry, nil
}

// QueryTCFRegistryEntry returns the query result to the tcf registry entry with the name
func (d *Domain) QueryTCFRegistryEntry(actor string, emulator string, name string, queryParams map[string]string) (types.TCFRegistryQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "Domain.QueryTCFRegistryEntry",
	})

	logger.Infof("Get tcf registry entry '%s' to query", name)

	entry, err := d.Storage.Get(name)
	if err != nil {
		logger.Error(err)
		return types.TCFRegistryQueryResult{}, err
	}

	logger.Infof("Query tcf registry entry '%s'", name)

	queryResult, err := d.ExternalQuery.Query(actor, emulator, entry, queryParams)
	if err != nil {
		logger.Error(err)
		return types.TCFRegistryQueryResult{}, err
	}

	return queryResult, nil
}

// CreateTCFRegistryEntry creates a tcf registry entry
func (d *Domain) CreateTCFRegistryEntry(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "domain.CreateTCFRegistryEntry",
	})

	logger.Infof("Create tcf registry entry %s", entry.Name)

	err := d.Storage.Create(entry)
	if err != nil {
		createFailedEvent := types.TCFRegistryEntry{
			Name: entry.Name,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	createdEvent := types.TCFRegistryEntry{
		Name: entry.Name,
	}

	err = d.EventOut.Created(actor, emulator, createdEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Created event")
	}

	return nil
}

// UpdateTCFRegistryEntry updates the tcf registry entry
func (d *Domain) UpdateTCFRegistryEntry(actor string, emulator string, entry types.TCFRegistryEntry, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "domain.UpdateTCFRegistryEntry",
	})

	logger.Infof("Update tcf registry entry %s", entry.Name)

	err := d.Storage.Update(entry, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.TCFRegistryEntry{
			Name: entry.Name,
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// get the final result
	updatedEntry, err := d.Storage.Get(entry.Name)
	if err != nil {
		// update failed somehow - updated but not exist
		updateFailedEvent := types.TCFRegistryEntry{
			Name: entry.Name,
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	updatedEvent := types.TCFRegistryEntry{
		Name: updatedEntry.Name,
	}

	err = d.EventOut.Updated(actor, emulator, updatedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteTCFRegistryEntry deletes the tcf registry entry
func (d *Domain) DeleteTCFRegistryEntry(actor string, emulator string, name string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.domain",
		"function": "domain.DeleteTCFRegistryEntry",
	})

	logger.Infof("Delete tcf registry entry %s", name)

	err := d.Storage.Delete(name)
	if err != nil {
		deleteFailedEvent := types.TCFRegistryEntry{
			Name: name,
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	deletedEvent := types.TCFRegistryEntry{
		Name: name,
	}

	err = d.EventOut.Deleted(actor, emulator, deletedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}

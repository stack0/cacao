package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	mongoAdapter := &MongoAdapter{}
	mongoAdapter.InitMock(&config)

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.Store)

	mongoAdapter.Finalize()
}

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	expectedResults := []types.TCFRegistryEntry{
		{
			Name:                      "test_registry_entry1",
			Description:               "test_description1",
			Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
			QueryTarget:               "test_query_target1",
			QueryData:                 "test_query_data1",
			QueryResultJSONPathFilter: "test/filter1",
		},
		{
			Name:                      "test_registry_entry2",
			Description:               "test_description2",
			Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
			QueryTarget:               "test_query_target2",
			QueryData:                 "test_query_data2",
			QueryResultJSONPathFilter: "test/filter2",
		},
	}
	err := mongoAdapter.MockList(expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.List()
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Finalize()
}

func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testEntryName := "test_entry1"

	expectedResult := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}
	err := mongoAdapter.MockGet(testEntryName, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.Get(testEntryName)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testEntryName := "test_entry1"

	testEntry := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	err := mongoAdapter.MockCreate(testEntry, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Create(testEntry)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testEntryName := "test_entry1"

	testExistingEntry := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	testUpdateEntry := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description2",
		QueryTarget:               "test_query_target2",
		QueryData:                 "test_query_data2",
		QueryResultJSONPathFilter: "test/filter2",
	}

	err := mongoAdapter.MockUpdate(testExistingEntry, testUpdateEntry, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Update(testUpdateEntry, []string{"description", "query_target", "query_data", "query_result_jsonpath_filter"})
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testEntryName := "test_entry1"

	testEntry := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	err := mongoAdapter.MockDelete(testEntryName, testEntry, nil)
	assert.NoError(t, err)

	err = mongoAdapter.Delete(testEntryName)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

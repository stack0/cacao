package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/ports"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

func createTestQueryPort() *ports.MockIncomingQueryPort {
	var config types.Config
	config.ProcessDefaults()

	queryInImpl := &ports.MockIncomingQueryPort{}
	queryInImpl.Init(&config)

	return queryInImpl
}

func createTestQueryAdapter(queryInImpl ports.IncomingQueryPort) *QueryAdapter {
	var config types.Config
	config.ProcessDefaults()

	queryAdapter := &QueryAdapter{}
	queryAdapter.Init(&config)

	queryAdapter.IncomingPort = queryInImpl

	go queryAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return queryAdapter
}

func TestInitQueryAdapter(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)
	assert.NotNil(t, queryAdapter)
	assert.NotEmpty(t, queryAdapter.Connection)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTCFRegistryEntryListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	expectedResults := []types.TCFRegistryEntry{
		{
			Name:                      "test_registry_entry1",
			Description:               "test_description1",
			Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
			QueryTarget:               "test_query_target1",
			QueryData:                 "test_query_data1",
			QueryResultJSONPathFilter: "test/filter1",
		},
		{
			Name:                      "test_registry_entry2",
			Description:               "test_description2",
			Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
			QueryTarget:               "test_query_target2",
			QueryData:                 "test_query_data2",
			QueryResultJSONPathFilter: "test/filter2",
		},
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.TCFRegistryEntry, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TCFRegistryEntryListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TCFRegistryEntryListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetEntries()))

	firstTCFRegistryEntry := result.GetEntries()[0]
	assert.EqualValues(t, expectedResults[0].Name, firstTCFRegistryEntry.GetName())
	assert.EqualValues(t, expectedResults[0].Description, firstTCFRegistryEntry.GetDescription())
	assert.EqualValues(t, expectedResults[0].Type, firstTCFRegistryEntry.GetType())
	assert.EqualValues(t, expectedResults[0].QueryTarget, firstTCFRegistryEntry.GetQueryTarget())
	assert.EqualValues(t, expectedResults[0].QueryData, firstTCFRegistryEntry.GetQueryData())
	assert.EqualValues(t, expectedResults[0].QueryResultJSONPathFilter, firstTCFRegistryEntry.GetQueryResultJSONPathFilter())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTCFRegistryEntryListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.TCFRegistryEntry, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list tcf registry entries")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TCFRegistryEntryListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TCFRegistryEntryListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Entries)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTCFRegistryEntryGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	queryData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	expectedResult := types.TCFRegistryEntry{
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, name string) (types.TCFRegistryEntry, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, name)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TCFRegistryEntryGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TCFRegistryEntryModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.Name)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.Name, result.Name)
	assert.Equal(t, expectedResult.Description, result.Description)
	assert.Equal(t, expectedResult.Type, result.Type)
	assert.Equal(t, expectedResult.QueryTarget, result.QueryTarget)
	assert.Equal(t, expectedResult.QueryData, result.QueryData)
	assert.Equal(t, expectedResult.QueryResultJSONPathFilter, result.QueryResultJSONPathFilter)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestTCFRegistryEntryGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	queryData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, name string) (types.TCFRegistryEntry, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, name)
		return types.TCFRegistryEntry{}, fmt.Errorf("unable to get a tcf registry entry")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.TCFRegistryEntryGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.TCFRegistryEntryModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Name)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

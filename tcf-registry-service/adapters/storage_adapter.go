package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// List returns tcf registry entries
func (adapter *MongoAdapter) List() ([]types.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.TCFRegistryEntry{}

	err := adapter.Store.List(adapter.Config.TCFRegistryMongoDBCollectionName, &results)
	if err != nil {
		errorMessage := "unable to list tcf registries"
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(expectedEntries []types.TCFRegistryEntry, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("List", adapter.Config.TCFRegistryMongoDBCollectionName).Return(expectedEntries, expectedError)
	return nil
}

// Get returns the tcf registry entry with the name
func (adapter *MongoAdapter) Get(name string) (types.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.TCFRegistryEntry{}

	err := adapter.Store.Get(adapter.Config.TCFRegistryMongoDBCollectionName, name, &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the tcf registry entry for name %s", name)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(name string, expectedEntry types.TCFRegistryEntry, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TCFRegistryMongoDBCollectionName, name).Return(expectedEntry, expectedError)
	return nil
}

// Create inserts a tcf registry entry
func (adapter *MongoAdapter) Create(entry types.TCFRegistryEntry) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.TCFRegistryMongoDBCollectionName, entry)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a tcf registry entry because name %s conflicts", entry.Name)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a tcf registry entry with name %s", entry.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(entry types.TCFRegistryEntry, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TCFRegistryMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits a tcf registry entry
func (adapter *MongoAdapter) Update(entry types.TCFRegistryEntry, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.Update",
	})

	updated, err := adapter.Store.Update(adapter.Config.TCFRegistryMongoDBCollectionName, entry.Name, entry, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the tcf registry entry for name %s", entry.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the tcf registry entry for name %s", entry.Name)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingEntry types.TCFRegistryEntry, newEntry types.TCFRegistryEntry, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Update", adapter.Config.TCFRegistryMongoDBCollectionName, newEntry.Name).Return(expectedResult, expectedError)
	return nil
}

// Delete deletes a tcf registry entry
func (adapter *MongoAdapter) Delete(name string) error {
	logger := log.WithFields(log.Fields{
		"package":  "tcf-registry-service.adapters",
		"function": "MongoAdapter.Delete",
	})

	// delete
	deleted, err := adapter.Store.Delete(adapter.Config.TCFRegistryMongoDBCollectionName, name)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the tcf registry entry for name %s", name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the tcf registry entry for name %s", name)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDelete sets expected results for Delete
func (adapter *MongoAdapter) MockDelete(name string, existingEntry types.TCFRegistryEntry, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Delete", adapter.Config.TCFRegistryMongoDBCollectionName, name).Return(expectedResult, expectedError)
	return nil
}

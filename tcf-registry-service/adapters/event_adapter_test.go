package adapters

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/tcf-registry-service/ports"
	"gitlab.com/cyverse/cacao/tcf-registry-service/types"
)

func createTestEventInPort() *ports.MockIncomingEventPort {
	var config types.Config
	config.ProcessDefaults()

	eventInImpl := &ports.MockIncomingEventPort{}
	eventInImpl.Init(&config)

	return eventInImpl
}

func createTestEventAdapter(eventInImpl ports.IncomingEventPort) *EventAdapter {
	var config types.Config
	config.ProcessDefaults()

	eventAdapter := &EventAdapter{}
	eventAdapter.Init(&config)

	eventAdapter.IncomingPort = eventInImpl

	go eventAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return eventAdapter
}

func TestInitEventAdapter(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)
	assert.NotNil(t, eventAdapter)
	assert.NotEmpty(t, eventAdapter.Connection)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryCreateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryCreateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryCreateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return fmt.Errorf("unable to create a tcf registry entry")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryCreateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryUpdateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name:                      testEntryName,
		Description:               "test_description1",
		Type:                      cacao_common_service.TCFRegistryEntryTypeRESTAPIEndpoint,
		QueryTarget:               "test_query_target1",
		QueryData:                 "test_query_data1",
		QueryResultJSONPathFilter: "test/filter1",
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryUpdateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryUpdateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	eventInImpl.SetUpdateHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return fmt.Errorf("unable to update a tcf registry entry")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryUpdateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryDeleteEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryDeleteRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryDeleteEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := cacao_common_service.TCFRegistryEntryModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		Name: testEntryName,
	}

	eventInImpl.SetDeleteHandler(func(actor string, emulator string, entry types.TCFRegistryEntry, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testEntryName, entry.Name)
		return fmt.Errorf("unable to delete a tcf registry entry")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.TCFRegistryEntryDeleteRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestTCFRegistryEntryCreatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryCreatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryCreatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		return nil
	})

	err := eventAdapter.Created(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTCFRegistryEntryCreateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testErrorMessage := "tcf registry entry is not created"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryCreateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryCreateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		t.Log(response.GetServiceError())
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.CreateFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTCFRegistryEntryUpdatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryUpdatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryUpdatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		return nil
	})

	err := eventAdapter.Updated(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTCFRegistryEntryUpdateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testErrorMessage := "tcf registry entry is not updated"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryUpdateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryUpdateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.UpdateFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTCFRegistryEntryDeletedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryDeletedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryDeletedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		return nil
	})

	err := eventAdapter.Deleted(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestTCFRegistryEntryDeleteFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testEntryName := "test_entry1"

	eventData := types.TCFRegistryEntry{
		Name: testEntryName,
	}

	testErrorMessage := "tcf registry entry is not deleted"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.TCFRegistryEntryDeleteFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.TCFRegistryEntryDeleteFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.TCFRegistryEntryModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testEntryName, response.Name)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.DeleteFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

package authentication

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_globusProfileExtractor_ExtractUserProfile(t *testing.T) {
	type fields struct {
		requiredIdentityProvider string
	}
	type args struct {
		accessToken string
		claims      map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *UserProfile
		wantErr bool
	}{
		{
			name: "use default identity",
			fields: fields{
				requiredIdentityProvider: "",
			},
			args: args{
				accessToken: "",
				claims:      exampleGlobusClaims,
			},
			want: &UserProfile{
				Username:   "foobar@example1.com",
				FirstName:  "Foo",
				LastName:   "Bar",
				Email:      "foobar@example1.com",
				Attributes: exampleGlobusClaims,
			},
			wantErr: false,
		},
		{
			name: "empty username",
			fields: fields{
				requiredIdentityProvider: "",
			},
			args: args{
				accessToken: "",
				claims: func() map[string]interface{} {
					var claims = cloneClaims(exampleGlobusClaims)
					claims["username"] = ""
					return claims
				}(),
			},
			want:    nil,
			wantErr: true,
		},
		{
			// profile extractor only extracts profile, it does not check "active" claim
			name: "claim inactive",
			fields: fields{
				requiredIdentityProvider: "",
			},
			args: args{
				accessToken: "",
				claims: func() map[string]interface{} {
					var claims = cloneClaims(exampleGlobusClaims)
					claims["active"] = false
					return claims
				}(),
			},
			want: &UserProfile{
				Username:  "foobar@example1.com",
				FirstName: "Foo",
				LastName:  "Bar",
				Email:     "foobar@example1.com",
				Attributes: func() map[string]interface{} {
					var claims = cloneClaims(exampleGlobusClaims)
					claims["active"] = false
					return claims
				}(),
			},
			wantErr: false,
		},
		{
			name: "use required identity - 1 from total of 2",
			fields: fields{
				requiredIdentityProvider: "78aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			},
			args: args{
				accessToken: "",
				claims:      exampleGlobusClaims,
			},
			want: &UserProfile{
				Username:   "foobar@example2.com",
				FirstName:  "Foo",
				LastName:   "Bar",
				Email:      "foobar@example1.com", // still use the primary email, even if the required identity is not the primary.
				Attributes: exampleGlobusClaims,
			},
			wantErr: false,
		},
		{
			name: "missing required identity",
			fields: fields{
				requiredIdentityProvider: "abcdef12-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			},
			args: args{
				accessToken: "",
				claims:      exampleGlobusClaims,
			},
			want:    nil,
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := NewGlobusProfileExtractor(tt.fields.requiredIdentityProvider)

			got, err := e.ExtractUserProfile(tt.args.accessToken, tt.args.claims)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractUserProfile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !assert.Equal(t, tt.want, got) {
				return
			}
		})
	}
}

func cloneClaims(src map[string]interface{}) map[string]interface{} {
	var dest = make(map[string]interface{})
	for k, v := range src {
		dest[k] = v
	}
	return dest
}

var exampleGlobusClaims = map[string]interface{}{
	"active": true,
	"aud": []string{
		"auth.globus.org",
		"aaaaaaaa-aaaa-aaaa-aaaa-bbbbbbbbbbbb",
	},
	"client_id":                 "aaaaaaaa-aaaa-aaaa-aaaa-bbbbbbbbbbbb",
	"dependent_tokens_cache_id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
	"email":                     "foobar@example1.com",
	"exp":                       1663000000,
	"iat":                       1661000000,
	"identity_set": []string{
		"e8aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
		"6faaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
	},
	"identity_set_detail": []map[string]interface{}{
		{
			"email":                          "foobar@example1.com",
			"identity_provider":              "23aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			"identity_provider_display_name": "Example 1 idp",
			"last_authentication":            1661000000,
			"name":                           "Foo Bar",
			"organization":                   "Example Org 1",
			"sub":                            "e8aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			"username":                       "foobar@example1.com",
		},
		{
			"email":                          "foobar@example2.com",
			"identity_provider":              "78aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			"identity_provider_display_name": "Example 2 idp",
			"last_authentication":            1621000000,
			"name":                           "Foo Bar",
			"organization":                   "Example Org 2",
			"sub":                            "6faaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
			"username":                       "foobar@example2.com",
		},
	},
	"iss":        "https://auth.globus.org",
	"name":       "Foo Bar",
	"nbf":        1662000000,
	"scope":      "openid email profile",
	"sub":        "e8aaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
	"token_type": "Bearer",
	"username":   "foobar@example1.com",
}

func Test_ciLogonProfileExtractor_userinfoToProfile(t *testing.T) {
	type fields struct {
		requiredIdentityProviderID string
	}
	type args struct {
		userinfo ciLogonUserInfo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *UserProfile
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				requiredIdentityProviderID: "",
			},
			args: args{
				userinfo: ciLogonUserInfo{
					Subject:              "foobar@access-ci.org",
					Issuer:               "https://cilogon.org",
					Audience:             "<aud-example>",
					PreferredUsername:    "foobar",
					Email:                "foobar@example.com",
					FirstName:            "Foo",
					LastName:             "Bar",
					FullName:             "Foo Bar",
					IdentityProviderID:   "https://access-ci.org/idp",
					IdentityProviderName: "ACCESS",
					Organization:         "FooBar Org",
					EPPN:                 "foobar@access-ci.org",
					EPTID:                "<tptid-example>",
					CertSubjectDN:        "<cert_subject_dn-example>",
					JWTID:                "<jti-example>",
				},
			},
			want: &UserProfile{
				Username:   "foobar@access-ci.org",
				FirstName:  "Foo",
				LastName:   "Bar",
				Email:      "foobar@example.com",
				Attributes: nil,
			},
			wantErr: false,
		},
		{
			name: "empty username",
			fields: fields{
				requiredIdentityProviderID: "",
			},
			args: args{
				userinfo: ciLogonUserInfo{
					Subject:              "",
					Issuer:               "https://cilogon.org",
					Audience:             "<aud-example>",
					PreferredUsername:    "",
					Email:                "foobar@example.com",
					FirstName:            "Foo",
					LastName:             "Bar",
					FullName:             "Foo Bar",
					IdentityProviderID:   "https://access-ci.org/idp",
					IdentityProviderName: "ACCESS",
					Organization:         "FooBar Org",
					EPPN:                 "",
					EPTID:                "<tptid-example>",
					CertSubjectDN:        "<cert_subject_dn-example>",
					JWTID:                "<jti-example>",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			// ACCESS-CI specific
			name: "username not access-ci",
			fields: fields{
				requiredIdentityProviderID: "",
			},
			args: args{
				userinfo: ciLogonUserInfo{
					Subject:              "foobar@not-access-ci.org",
					Issuer:               "https://cilogon.org",
					Audience:             "<aud-example>",
					PreferredUsername:    "foobar",
					Email:                "foobar@example.com",
					FirstName:            "Foo",
					LastName:             "Bar",
					FullName:             "Foo Bar",
					IdentityProviderID:   "https://access-ci.org/idp",
					IdentityProviderName: "ACCESS",
					Organization:         "FooBar Org",
					EPPN:                 "foobar@not-access-ci.org",
					EPTID:                "<tptid-example>",
					CertSubjectDN:        "<cert_subject_dn-example>",
					JWTID:                "<jti-example>",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "require identity provider",
			fields: fields{
				requiredIdentityProviderID: "https://access-ci.org/idp",
			},
			args: args{
				userinfo: ciLogonUserInfo{
					Subject:              "foobar@access-ci.org",
					Issuer:               "https://cilogon.org",
					Audience:             "<aud-example>",
					PreferredUsername:    "foobar",
					Email:                "foobar@example.com",
					FirstName:            "Foo",
					LastName:             "Bar",
					FullName:             "Foo Bar",
					IdentityProviderID:   "https://access-ci.org/idp",
					IdentityProviderName: "ACCESS",
					Organization:         "FooBar Org",
					EPPN:                 "foobar@access-ci.org",
					EPTID:                "<tptid-example>",
					CertSubjectDN:        "<cert_subject_dn-example>",
					JWTID:                "<jti-example>",
				},
			},
			want: &UserProfile{
				Username:   "foobar@access-ci.org",
				FirstName:  "Foo",
				LastName:   "Bar",
				Email:      "foobar@example.com",
				Attributes: nil,
			},
			wantErr: false,
		},
		{
			name: "missing required identity provider",
			fields: fields{
				requiredIdentityProviderID: "https://example.com/idp",
			},
			args: args{
				userinfo: ciLogonUserInfo{
					Subject:              "foobar@access-ci.org",
					Issuer:               "https://cilogon.org",
					Audience:             "<aud-example>",
					PreferredUsername:    "foobar",
					Email:                "foobar@example.com",
					FirstName:            "Foo",
					LastName:             "Bar",
					FullName:             "Foo Bar",
					IdentityProviderID:   "https://access-ci.org/idp",
					IdentityProviderName: "ACCESS",
					Organization:         "FooBar Org",
					EPPN:                 "foobar@access-ci.org",
					EPTID:                "<tptid-example>",
					CertSubjectDN:        "<cert_subject_dn-example>",
					JWTID:                "<jti-example>",
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := ciLogonProfileExtractor{
				requiredIdentityProviderID: tt.fields.requiredIdentityProviderID,
			}
			got, err := e.userinfoToProfile(tt.args.userinfo)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractUserProfile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !assert.Equal(t, tt.want, got) {
				return
			}
		})
	}
}

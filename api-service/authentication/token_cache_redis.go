package authentication

import (
	"context"
	"encoding/json"
	"github.com/eko/gocache/store"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/api-service/config"
	"strings"
	"time"
)

// NewRedisTokenCache creates an access token cache based on Redis.
func NewRedisTokenCache(conf config.Config) TokenCache {
	log.Info("using redis token cache")
	return &redisTokenCache{
		store: store.NewRedis(newRedisClient(conf), &store.Options{
			Expiration: time.Second * time.Duration(conf.TokenCacheTTLSec),
		}),
	}
}

func newRedisClient(conf config.Config) *redis.Client {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     conf.RedisAddress,
		Password: conf.RedisPassword,
		DB:       conf.RedisDB,
	})

	// ping redis to check connection
	ctx, cancel := redisCtx()
	ping := redisClient.Ping(ctx)
	cancel()
	if err := ping.Err(); err != nil {
		log.WithError(err).Panicf("fail to ping redis")
	}
	log.Info("connected to redis")

	return redisClient
}

type redisTokenCache struct {
	store *store.RedisStore
}

const redisOperationTimeout = time.Millisecond * 500

// LookupToken ...
func (c redisTokenCache) LookupToken(token string) (profile UserProfile, found bool) {
	ctx, cancel := redisCtx()
	value, err := c.store.Get(ctx, token)
	cancel()
	if err != nil {
		log.WithError(err).Error("fail to get token from redis")
		return UserProfile{}, false
	}

	serialized, ok := value.(string)
	if !ok {
		log.WithError(err).Error("value from redis token cache is not string")
		return UserProfile{}, false
	}
	profile, err = c.deserializeProfile(serialized)
	if err != nil {
		log.WithError(err).Error("fail to de-serialize profile from redis")
		return UserProfile{}, false
	}
	return profile, true
}

// SaveToken ...
func (c redisTokenCache) SaveToken(token string, profile UserProfile) {
	serialized, err := c.serializeProfile(profile)
	if err != nil {
		log.WithError(err).Error("fail to serialize profile")
		return
	}
	ctx, cancel := redisCtx()
	err = c.store.Set(ctx, token, serialized, &store.Options{
		Tags: []string{"user-" + profile.Username},
	})
	if err != nil {
		// log error and return
		log.WithError(err).Error("fail to save token to redis")
	}
	cancel()
}

func (c redisTokenCache) serializeProfile(profile UserProfile) (string, error) {
	var serializedProfile strings.Builder
	encoder := json.NewEncoder(&serializedProfile)
	err := encoder.Encode(profile)
	if err != nil {
		return "", err
	}
	return serializedProfile.String(), nil
}

func (c redisTokenCache) deserializeProfile(data string) (profile UserProfile, err error) {
	err = json.Unmarshal([]byte(data), &profile)
	if err != nil {
		return UserProfile{}, err
	}
	return
}

// InvalidateUser invalidate token cache for a specific user.
func (c redisTokenCache) InvalidateUser(username string) {
	ctx, cancel := redisCtx()
	err := c.store.Invalidate(ctx, store.InvalidateOptions{Tags: []string{"user-" + username}})
	if err != nil {
		log.WithError(err).WithField("username", username).Error("fail to invalidate token cache for user")
	}
	cancel()
}

func redisCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), redisOperationTimeout)
}

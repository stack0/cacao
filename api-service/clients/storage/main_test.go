package storage

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	dmocks "gitlab.com/cyverse/cacao/api-service/clients/deployments/mocks"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
	tmocks "gitlab.com/cyverse/cacao/api-service/clients/templates/mocks"
)

// newTestSession creates a new storage client session for testing.
func newTestSession(dsession deployments.Session, tsession templates.Session) Session {
	return &storageSession{
		actor:              "test_username",
		emulator:           "",
		isAdmin:            false,
		deploymentsSession: dsession,
		templatesSession:   tsession,
	}
}

// newTemplate returns a new template for testing.
func newTemplate(purpose service.TemplatePurpose) hm.Template {
	return hm.Template{
		ID:        cacaocommon.NewID("template"),
		Name:      "Some Test Template Name",
		CreatedAt: time.Now().Add(-time.Hour * 2).Truncate(time.Second),
		UpdatedAt: time.Now().Add(-time.Hour).Truncate(time.Second),
		Owner:     "test_username",
		Metadata: service.TemplateMetadata{
			Name:        "Some Test Template Name",
			Author:      "Test User",
			AuthorEmail: "testuser@example.com",
			Purpose:     purpose,
		},
	}
}

// newDeployment returns a new deployment for testing.
func newDeployment(templateID string) hm.Deployment {
	return hm.Deployment{
		ID:                cacaocommon.NewID("deployment").String(),
		Owner:             "test_username",
		Name:              "Some Test Deployment Name",
		Description:       "Some Test Deployment Description",
		WorkspaceID:       cacaocommon.NewID("workspace").String(),
		TemplateID:        templateID,
		PrimaryProviderID: cacaocommon.NewID("provider").String(),
		CreatedAt:         time.Now().Add(-time.Hour * 2).Truncate(time.Second),
		UpdatedAt:         time.Now().Add(-time.Hour).Truncate(time.Second),
		CurrentStatus:     service.DeploymentStatusActive.String(),
	}
}

// newDeploymentUpdate returns a deployment update for testing.
func newDeploymentUpdate(templateID *string) hm.DeploymentUpdate {
	return hm.DeploymentUpdate{
		TemplateID: templateID,
	}
}

// newDeploymentRun returns a deployment run for testing.
func newDeploymentRun(status string, includeInstanceID, includeAttach bool) hm.DeploymentRun {

	// Build the list of parameters.
	params := []hm.KeyValue{
		{
			Key:   "name",
			Value: "storage",
		},
		{
			Key:   "size",
			Value: "really big",
		},
	}
	if includeInstanceID {
		params = append(params, hm.KeyValue{
			Key:   instanceIDParamName,
			Value: "bogus-instance-id",
		})
	}
	if includeAttach {
		params = append(params, hm.KeyValue{
			Key:   attachParamName,
			Value: "false",
		})
	}

	return hm.DeploymentRun{
		ID:         cacaocommon.NewID("run").String(),
		Owner:      "test_username",
		Status:     status,
		Parameters: params,
	}
}

// storageTemplateListingTest is a structure for defining storage template listing tests.
type storageTemplateListingTest struct {
	name                 string
	availableTemplates   []hm.Template
	templateListingError error
	expectedTemplateIDs  []cacaocommon.ID
}

// getStorageTemplateListingTests returns the template listing test cases.
func getStorageTemplateListingTests() []storageTemplateListingTest {
	testCases := make([]storageTemplateListingTest, 5)

	// The base test case.
	testCases[0] = storageTemplateListingTest{
		name:                "no templates defined",
		availableTemplates:  []hm.Template{},
		expectedTemplateIDs: []cacaocommon.ID{},
	}

	// The case when there aren't any storage templates defined.
	testCases[1] = storageTemplateListingTest{
		name: "no storage templates defined",
		availableTemplates: []hm.Template{
			newTemplate(service.TemplatePurposeCompute),
			newTemplate(service.TemplatePurposeGeneral),
		},
		expectedTemplateIDs: []cacaocommon.ID{},
	}

	// Some storage templates for testing.
	storageTemplate1 := newTemplate(service.TemplatePurposeStorage)
	storageTemplate2 := newTemplate(service.TemplatePurposeStorage)

	// The case where there are some storage templates defined.
	testCases[2] = storageTemplateListingTest{
		name: "some storage templates defined",
		availableTemplates: []hm.Template{
			newTemplate(service.TemplatePurposeCompute),
			newTemplate(service.TemplatePurposeGeneral),
			storageTemplate1,
			newTemplate(service.TemplatePurposeGeneral),
			storageTemplate2,
			newTemplate(service.TemplatePurposeCompute),
		},
		expectedTemplateIDs: []cacaocommon.ID{storageTemplate1.ID, storageTemplate2.ID},
	}

	// The case where there are only storage templates defined.
	testCases[3] = storageTemplateListingTest{
		name:                "only storage templates defined",
		availableTemplates:  []hm.Template{storageTemplate1, storageTemplate2},
		expectedTemplateIDs: []cacaocommon.ID{storageTemplate1.ID, storageTemplate2.ID},
	}

	// An error occurs when listing templates.
	testCases[4] = storageTemplateListingTest{
		name:                 "template listing error occurs",
		templateListingError: fmt.Errorf("something bad happened"),
	}

	return testCases
}

// TestListStoragetTemplates runs the unit tests for listing storage templates.
func TestListStorageTemplateIDs(t *testing.T) {
	for _, tc := range getStorageTemplateListingTests() {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the deployments session.
			dsession := new(dmocks.Session)

			// Create the templates session.
			tsession := new(tmocks.Session)
			tsession.On("ListTemplates").
				Return(tc.availableTemplates, tc.templateListingError)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the list of templates IDs.
			actualTemplateIDs, err := session.ListStorageTemplateIDs()

			// Verify that all expectations were met.
			dsession.AssertExpectations(t)
			tsession.AssertExpectations(t)

			// Verify that we got back the expected result.
			if tc.templateListingError != nil {
				assert.Equal(err, tc.templateListingError)
				assert.Nil(actualTemplateIDs)
			} else {
				assert.NoError(err)
				assert.Equal(tc.expectedTemplateIDs, actualTemplateIDs)
			}
		})
	}
}

// storageDeploymentListingTest is a structure used to define test cases for listing storage deployments.
type storageDeploymentListingTest struct {
	name                   string
	availableTemplates     []hm.Template
	storageTemplates       []hm.Template
	templateListingError   error
	deploymentListingError error
	expectedResult         []hm.Deployment
}

// getExpectedListOptions returns the list options expected for the given list of storage templates.
func getExpectedListOptions(storageTemplates []hm.Template) service.DeploymentListOption {

	// Get the list of templateIDs to search for.
	templateIDs := make([]cacaocommon.ID, len(storageTemplates))
	for i, template := range storageTemplates {
		templateIDs[i] = template.ID
	}

	// Build the expected list options.
	return service.DeploymentListOption{
		SortBy:        service.SortByID,
		SortDirection: service.DescendingSort,
		Filter: service.DeploymentFilter{
			User:        "test_username",
			TemplateSet: templateIDs,
		},
	}
}

// getStorageDeploymentListingTests returns the list of test cases for storage deployment listings.
func getStorageDeploymentListingTests() []storageDeploymentListingTest {
	testCases := make([]storageDeploymentListingTest, 5)

	// Some storage templates for testing.
	storageTemplate1 := newTemplate(service.TemplatePurposeStorage)
	storageTemplate2 := newTemplate(service.TemplatePurposeStorage)

	// Some compute templates for testing.
	computeTemplate1 := newTemplate(service.TemplatePurposeCompute)
	computeTemplate2 := newTemplate(service.TemplatePurposeCompute)

	// Some general purpose templates for testing.
	generalTemplate1 := newTemplate(service.TemplatePurposeGeneral)
	generalTemplate2 := newTemplate(service.TemplatePurposeGeneral)

	// The list of available templates used for most tests.
	defaultAvailableTemplates := []hm.Template{
		storageTemplate1, computeTemplate1, generalTemplate1,
		storageTemplate2, computeTemplate2, generalTemplate2,
	}

	// The list of available storage templates used for most tests.
	defaultAvailableStorageTemplates := []hm.Template{storageTemplate1, storageTemplate2}

	// The case where there are no storage templates available, and thus no storage deployments either.
	testCases[0] = storageDeploymentListingTest{
		name:               "no storage templates available",
		availableTemplates: []hm.Template{computeTemplate1, generalTemplate1},
		storageTemplates:   []hm.Template{},
		expectedResult:     []hm.Deployment{},
	}

	// The case where there are storage templates, but not storage deployments.
	testCases[1] = storageDeploymentListingTest{
		name:               "no storage deployments available",
		availableTemplates: []hm.Template{computeTemplate1, generalTemplate1, storageTemplate1},
		storageTemplates:   []hm.Template{storageTemplate1},
		expectedResult:     []hm.Deployment{},
	}

	// The case where there are some storage deployments.
	testCases[2] = storageDeploymentListingTest{
		name:               "some storage deployments available",
		availableTemplates: defaultAvailableTemplates,
		storageTemplates:   defaultAvailableStorageTemplates,
		expectedResult:     []hm.Deployment{newDeployment(storageTemplate1.ID.String())},
	}

	// The case where a template listing error occurs.
	testCases[3] = storageDeploymentListingTest{
		name:                 "a template listing error occurs",
		availableTemplates:   defaultAvailableTemplates,
		templateListingError: fmt.Errorf("something bad happened"),
	}

	// The case when a deployment listing error occurs.
	testCases[4] = storageDeploymentListingTest{
		name:                   "a deployment listing error occurs",
		availableTemplates:     defaultAvailableTemplates,
		storageTemplates:       defaultAvailableStorageTemplates,
		deploymentListingError: fmt.Errorf("something bad happened"),
	}

	return testCases
}

// TestListStorageDeployments runs the unit tests for listing storage deployments.
func TestListStorageDeployments(t *testing.T) {
	for _, tc := range getStorageDeploymentListingTests() {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			if len(tc.storageTemplates) != 0 {
				dsession.On("SearchDeployments", getExpectedListOptions(tc.storageTemplates)).
					Return(tc.expectedResult, tc.deploymentListingError)
			}

			// Create the templates session.
			tsession := new(tmocks.Session)
			tsession.On("ListTemplates").
				Return(tc.availableTemplates, tc.templateListingError)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			actualResult, err := session.ListStorageDeployments()

			// Verify that all expectations were met.
			tsession.AssertExpectations(t)
			dsession.AssertExpectations(t)

			// Verify that we got back the expected results.
			if tc.templateListingError != nil {
				assert.Nil(actualResult)
				assert.Equal(tc.templateListingError, err)
			} else if tc.deploymentListingError != nil {
				assert.Nil(actualResult)
				assert.Equal(tc.deploymentListingError, err)
			} else {
				assert.Equal(tc.expectedResult, actualResult)
				assert.Nil(err)
			}
		})
	}
}

// storageDeploymentCreationRequestValidationTestCase represents a single test case for
// ValidateStorageDeploymentCreationRequest.
type storageDeploymentCreationRequestValidationTestCase struct {
	name                      string
	creationRequest           *hm.Deployment
	template                  hm.Template
	templateRetrievalError    error
	deploymentValidationError error
}

// ExpectValidationCall returns true if we should expect a call to the deployment clients validation method.
func (tc storageDeploymentCreationRequestValidationTestCase) ExpectValidationCall() bool {
	if tc.templateRetrievalError != nil {
		return false
	} else if tc.template.Metadata.Purpose != service.TemplatePurposeStorage {
		return false
	}
	return true
}

// IncorrectPurpose returns true if the template purpose is not `storage`.
func (tc storageDeploymentCreationRequestValidationTestCase) IncorrectPurpose() bool {
	return tc.template.Metadata.Purpose != service.TemplatePurposeStorage
}

// ExpectedTemplatePurposeError returns the expected error if the template for the test case has an incorrect
// purpose.
func (tc storageDeploymentCreationRequestValidationTestCase) ExpectedTemplatePurposeError() error {
	return getIncorrectTemplatePurposeError(tc.template.Metadata.Purpose)
}

// getStorageDeploymentCreationRequestValidationTestCases returns the list of test cases for
// ValidateStorageDeploymentCreationRequest.
func getStorageDeploymentCreationRequestValidationTestCases() []storageDeploymentCreationRequestValidationTestCase {
	testCases := make([]storageDeploymentCreationRequestValidationTestCase, 4)

	// Some templates for testing.
	generalTemplate := newTemplate(service.TemplatePurposeGeneral)
	storageTemplate := newTemplate(service.TemplatePurposeStorage)

	// The case where the corresponding template can't be retrieved.
	creationRequest0 := newDeployment(string(cacaocommon.NewID("template")))
	testCases[0] = storageDeploymentCreationRequestValidationTestCase{
		name:                   "template retrieval error",
		creationRequest:        &creationRequest0,
		templateRetrievalError: fmt.Errorf("something bad happened"),
	}

	// The case where the template type is incorrect.
	creationRequest1 := newDeployment(string(generalTemplate.ID))
	testCases[1] = storageDeploymentCreationRequestValidationTestCase{
		name:            "incorrect template type",
		creationRequest: &creationRequest1,
		template:        generalTemplate,
	}

	// The case where the deployment creation request validation fails.
	creationRequest2 := newDeployment(string(storageTemplate.ID))
	testCases[2] = storageDeploymentCreationRequestValidationTestCase{
		name:                      "downstream validation failure",
		creationRequest:           &creationRequest2,
		template:                  storageTemplate,
		deploymentValidationError: fmt.Errorf("something bad happened"),
	}

	// The case where the deployment request validation succeeds.
	creationRequest3 := newDeployment(string(storageTemplate.ID))
	testCases[3] = storageDeploymentCreationRequestValidationTestCase{
		name:            "validation success",
		creationRequest: &creationRequest3,
		template:        storageTemplate,
	}

	return testCases
}

// TestValidateStorageDeploymentCreationRequest runs all of the test cases for ValidateStorageDeploymentCreationRequest.
func TestValidateStorageDeploymentCreationRequest(t *testing.T) {
	for _, tc := range getStorageDeploymentCreationRequestValidationTestCases() {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			templateID := cacaocommon.ID(tc.creationRequest.TemplateID)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			if tc.ExpectValidationCall() {
				dsession.On("ValidateDeploymentCreationRequest", tc.creationRequest).
					Return(tc.deploymentValidationError)
			}

			// Create the templates session.
			tsession := new(tmocks.Session)
			tsession.On("GetTemplate", templateID).
				Return(tc.template, tc.templateRetrievalError)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			err := session.ValidateStorageDeploymentCreationRequest(tc.creationRequest)

			// Verify that we got back the expecte result.
			if tc.templateRetrievalError != nil {
				assert.Equal(tc.templateRetrievalError, err)
			} else if tc.IncorrectPurpose() {
				assert.Equal(tc.ExpectedTemplatePurposeError(), err)
			} else {
				assert.Equal(tc.deploymentValidationError, err)
			}
		})
	}
}

// storageDeploymentCreationTestCase represents a single test case for AddStorageDeployment.
type storageDeploymentCreationTestCase struct {
	name                    string
	creationRequest         *hm.Deployment
	template                hm.Template
	deploymentCreationError error
}

// getStorageDeploymentCreationTests returns the list of test cases for AddStorageDeployment.
func getStorageDeploymentCreationTestCases() []storageDeploymentCreationTestCase {
	testCases := make([]storageDeploymentCreationTestCase, 2)

	// Some templates for testing.
	storageTemplate := newTemplate(service.TemplatePurposeStorage)

	// The case where a deployment creation error occurs.
	creationRequest0 := newDeployment(string(storageTemplate.ID))
	testCases[0] = storageDeploymentCreationTestCase{
		name:                    "deployment creation error",
		creationRequest:         &creationRequest0,
		template:                storageTemplate,
		deploymentCreationError: fmt.Errorf("something bad happened"),
	}

	// The case where the deployment is created successfully.
	creationRequest1 := newDeployment(string(storageTemplate.ID))
	testCases[1] = storageDeploymentCreationTestCase{
		name:            "deployment creation success",
		creationRequest: &creationRequest1,
		template:        storageTemplate,
	}
	return testCases
}

// TestAddStorageDeployment runs the test cases for AddStorageDeployment
func TestAddStorageDeployment(t *testing.T) {
	for _, tc := range getStorageDeploymentCreationTestCases() {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			deploymentID := cacaocommon.ID(tc.creationRequest.ID)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			var returnedID cacaocommon.ID
			if tc.deploymentCreationError == nil {
				returnedID = deploymentID
			}
			dsession.On("AddDeployment", tc.creationRequest).
				Return(returnedID, tc.deploymentCreationError)

			// Cteate the templates session.
			tsession := new(tmocks.Session)

			// Create the new storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			id, err := session.AddStorageDeployment(tc.creationRequest)

			// Verify that all expectations were met.
			dsession.AssertExpectations(t)
			tsession.AssertExpectations(t)

			// Verify that we got the expected result.
			if tc.deploymentCreationError != nil {
				assert.Empty(id)
				assert.Equal(tc.deploymentCreationError, err)
			} else {
				assert.Equal(deploymentID, id)
				assert.Nil(err)
			}
		})
	}
}

// storageDeploymentRunTestCase represents a single test case for RunStorageDeployment.
type storageDeploymentRunTestCase struct {
	name               string
	deploymentID       cacaocommon.ID
	runRequest         *hm.DeploymentRun
	runID              cacaocommon.ID
	deploymentRunError error
}

// TestRunStorageDeployment runs the test cases for RunStorageDeployment.
func TestRunStorageDeployment(t *testing.T) {
	testCases := make([]storageDeploymentRunTestCase, 2)

	// A deployment run request for testing.
	runRequest := hm.DeploymentRun{
		Parameters: []hm.KeyValue{
			{
				Key:   "instance_id",
				Value: "some_instance",
			},
			{
				Key:   "attach",
				Value: "true",
			},
		},
	}

	// The case where a deployment run error occurs
	testCases[0] = storageDeploymentRunTestCase{
		name:               "deployment run error",
		deploymentID:       cacaocommon.NewID("deployment"),
		runRequest:         &runRequest,
		deploymentRunError: fmt.Errorf("something bad happened"),
	}

	// The case where the deployment run succeeds.
	testCases[1] = storageDeploymentRunTestCase{
		name:         "deployment run success",
		deploymentID: cacaocommon.NewID("deployment"),
		runRequest:   &runRequest,
		runID:        cacaocommon.NewID("run"),
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			dsession.On("RunDeployment", tc.deploymentID, tc.runRequest).
				Return(tc.runID, tc.deploymentRunError)

			// Create the templates session.
			tsession := new(tmocks.Session)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			id, err := session.RunStorageDeployment(tc.deploymentID, tc.runRequest)

			// Verify that all expectations were met.
			dsession.AssertExpectations(t)
			tsession.AssertExpectations(t)

			// Verify that we got the expected result.
			assert.Equal(tc.runID, id)
			assert.Equal(tc.deploymentRunError, err)
		})
	}
}

// storageDeploymentRetrievalTestCase represents a single test case for GetStorageDeployment.
type storageDeploymentRetrievalTestCase struct {
	name                     string
	deploymentRetrievalError error
	templateRetrievalError   error
	template                 hm.Template
	deployment               hm.Deployment
}

// ExpectedTemplatePurposeError returns the expected error if the template purpose is incorrect. It returns nil if the
// template purpose is correct.
func (tc storageDeploymentRetrievalTestCase) ExpectedTemplatePurposeError() error {
	return getIncorrectTemplatePurposeError(tc.template.Metadata.Purpose)
}

// ExpectedError returns the error that we expect to be returned for a test case.
func (tc storageDeploymentRetrievalTestCase) ExpectedError() error {
	switch {
	case tc.deploymentRetrievalError != nil:
		return tc.deploymentRetrievalError
	case tc.templateRetrievalError != nil:
		return tc.templateRetrievalError
	default:
		return tc.ExpectedTemplatePurposeError()
	}
}

// getStorageDeploymentRetrievalTestCases returns the list of test cases for GetStorageDeployment.
func getStorageDeploymentRetrievalTestCases() []storageDeploymentRetrievalTestCase {
	testCases := make([]storageDeploymentRetrievalTestCase, 4)

	// Some templates for testing.
	storageTemplate := newTemplate(service.TemplatePurposeStorage)
	computeTemplate := newTemplate(service.TemplatePurposeCompute)

	// Some deployments for testing.
	storageDeployment := newDeployment(storageTemplate.ID.String())
	computeDeployment := newDeployment(computeTemplate.ID.String())

	// The case where an error occurs during deployment retrieval.
	testCases[0] = storageDeploymentRetrievalTestCase{
		name:                     "deployment retrieval error",
		deploymentRetrievalError: fmt.Errorf("something bad happened"),
		template:                 storageTemplate,
		deployment:               storageDeployment,
	}

	// The case where an error occurs during template retrieval.
	testCases[1] = storageDeploymentRetrievalTestCase{
		name:                   "template retrieval error",
		templateRetrievalError: fmt.Errorf("something bad happened"),
		template:               storageTemplate,
		deployment:             storageDeployment,
	}

	// The case where the template purpose is invalid.
	testCases[2] = storageDeploymentRetrievalTestCase{
		name:       "incorrect template purpose",
		template:   computeTemplate,
		deployment: computeDeployment,
	}

	// The case where the lookup succeeds.
	testCases[3] = storageDeploymentRetrievalTestCase{
		name:       "success",
		template:   storageTemplate,
		deployment: storageDeployment,
	}

	return testCases
}

// TestGetStorageDeployment runs the test cases for GetStorageDeployment
func TestGetStorageDeployment(t *testing.T) {
	for _, tc := range getStorageDeploymentRetrievalTestCases() {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			deploymentID := cacaocommon.ID(tc.deployment.ID)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			var deploymentToReturn hm.Deployment
			if tc.deploymentRetrievalError == nil {
				deploymentToReturn = tc.deployment
			}
			dsession.On("GetDeployment", deploymentID).
				Return(deploymentToReturn, tc.deploymentRetrievalError)

			// Create the templates session.
			tsession := new(tmocks.Session)
			if tc.deploymentRetrievalError == nil {
				tsession.On("GetTemplate", tc.template.ID).
					Return(tc.template, tc.templateRetrievalError)
			}

			// Create the new storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			deployment, err := session.GetStorageDeployment(deploymentID)

			// Verify that all expectaionts were met.
			dsession.AssertExpectations(t)
			tsession.AssertExpectations(t)

			// Verify that we got the expected results.
			expectedError := tc.ExpectedError()
			if expectedError != nil {
				assert.Empty(deployment)
				assert.Equal(expectedError, err)
			} else {
				assert.Equal(tc.deployment, deployment)
				assert.NoError(err)
			}
		})
	}
}

// storageDeploymentUpdateRequestValidationTestCase represents a single test case for
// ValidateStorageDeploymentUpdateRequest.
type storageDeploymentUpdateRequestValidationTestCase struct {
	name                     string
	validationError          error
	deploymentID             string
	template                 hm.Template
	templateRetrievalError   error
	deployment               hm.Deployment
	deploymentRetrievalError error
	update                   hm.DeploymentUpdate
}

// ExpectedError returns the error that we expect ValidateStorageDeploymentUpdateRequest to return for the test case.
func (tc storageDeploymentUpdateRequestValidationTestCase) ExpectedError() error {
	switch {
	case tc.validationError != nil:
		return tc.validationError
	case tc.templateRetrievalError != nil:
		return tc.templateRetrievalError
	case tc.deploymentRetrievalError != nil:
		return tc.deploymentRetrievalError
	default:
		return getIncorrectTemplatePurposeError(tc.template.Metadata.Purpose)
	}
}

// ExpectGetTemplateCall returns true if we expect GetTemplate to be called for the test case.
func (tc storageDeploymentUpdateRequestValidationTestCase) ExpectGetTemplateCall() bool {
	return tc.validationError == nil && tc.deploymentRetrievalError == nil
}

// ExpectGetDeploymentCall returns true if we expect GetDeployment to be called for the test case.
func (tc storageDeploymentUpdateRequestValidationTestCase) ExpectGetDeploymentCall() bool {
	return tc.validationError == nil && tc.update.TemplateID == nil
}

// TestValidateStorageDeploymentUpdateRequest runs the test cases for ValidateStorageDeploymentUpdateRequest.
func TestValidateStorageDeploymentUpdateRequest(t *testing.T) {
	testCases := make([]storageDeploymentUpdateRequestValidationTestCase, 5)

	// Some templates for testing.
	storageTemplate := newTemplate(service.TemplatePurposeStorage)
	computeTemplate := newTemplate(service.TemplatePurposeCompute)

	// Some deployments for testing.
	storageDeployment := newDeployment(storageTemplate.ID.String())
	computeDeployment := newDeployment(computeTemplate.ID.String())

	// Some updates for testing.
	storageDeploymentUpdate := newDeploymentUpdate(&storageDeployment.TemplateID)
	computeDeploymentUpdate := newDeploymentUpdate(&computeDeployment.TemplateID)

	// The lower level validation fails.
	testCases[0] = storageDeploymentUpdateRequestValidationTestCase{
		name:            "lower level validation failure",
		deploymentID:    storageDeployment.ID,
		update:          storageDeploymentUpdate,
		validationError: fmt.Errorf("something bad happened"),
	}

	// An error occurs while trying to retrieve the template for validation.
	testCases[1] = storageDeploymentUpdateRequestValidationTestCase{
		name:                   "error while looking up new template ID",
		deploymentID:           storageDeployment.ID,
		template:               storageTemplate,
		update:                 storageDeploymentUpdate,
		templateRetrievalError: fmt.Errorf("something bad happened"),
	}

	// An error occurs while trying to look up an existing template ID.
	testCases[2] = storageDeploymentUpdateRequestValidationTestCase{
		name:                     "error while looking up the current template ID",
		deploymentID:             storageDeployment.ID,
		template:                 storageTemplate,
		deployment:               storageDeployment,
		update:                   newDeploymentUpdate(nil),
		deploymentRetrievalError: fmt.Errorf("something bad happened"),
	}

	// The new template has the wrong purpose.
	testCases[3] = storageDeploymentUpdateRequestValidationTestCase{
		name:         "the new template has the wrong purpose",
		deploymentID: computeDeployment.ID,
		template:     computeTemplate,
		update:       computeDeploymentUpdate,
	}

	// The existing template has the wrong purpose.
	testCases[4] = storageDeploymentUpdateRequestValidationTestCase{
		name:         "the existing template has the wrong purpose",
		deploymentID: computeDeployment.ID,
		deployment:   computeDeployment,
		template:     computeTemplate,
		update:       newDeploymentUpdate(nil),
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the templates session.
			tsession := new(tmocks.Session)
			if tc.ExpectGetTemplateCall() {
				tsession.On("GetTemplate", tc.template.ID).
					Return(tc.template, tc.templateRetrievalError)
			}

			// Create the deployments session.
			dsession := new(dmocks.Session)
			deploymentID := cacaocommon.ID(tc.deploymentID)
			update := tc.update
			dsession.On("ValidateDeploymentUpdateRequest", deploymentID, &update).
				Return(tc.validationError)
			if tc.ExpectGetDeploymentCall() {
				dsession.On("GetDeployment", deploymentID).
					Return(tc.deployment, tc.deploymentRetrievalError)
			}

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			err := session.ValidateStorageDeploymentUpdateRequest(deploymentID, &update)

			// Verify that all expectations were met.
			tsession.AssertExpectations(t)
			dsession.AssertExpectations(t)

			// Verify that we got the expected result.
			assert.Equal(tc.ExpectedError(), err)
		})
	}
}

// storageDeploymentUpdateTestCase represents a single test case for UpdateStorageDeployment.
type storageDeploymentUpdateTestCase struct {
	name          string
	deploymentID  cacaocommon.ID
	update        hm.DeploymentUpdate
	transactionID cacaocommon.ID
	updateError   error
}

// TestUpdateStorageDeployment runs the test cases for UpdateStorageDeployment
func TestUpdateStorageDeployment(t *testing.T) {
	testCases := make([]storageDeploymentUpdateTestCase, 2)

	// A deployment ID for testing.
	deploymentID := cacaocommon.NewID("deployment")

	// A deployment update for testing.
	update := newDeploymentUpdate(nil)

	// An update error occurs.
	testCases[0] = storageDeploymentUpdateTestCase{
		name:         "an update error occurs",
		deploymentID: deploymentID,
		update:       update,
		updateError:  fmt.Errorf("something bad happened"),
	}

	// The update request is submitted successfully.
	testCases[1] = storageDeploymentUpdateTestCase{
		name:          "successful submission",
		deploymentID:  deploymentID,
		update:        update,
		transactionID: cacaocommon.NewID("tid"),
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the templates session.
			tsession := new(tmocks.Session)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			dsession.On("UpdateDeployment", tc.deploymentID, &tc.update).
				Return(tc.transactionID, tc.updateError)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual results.
			tid, err := session.UpdateStorageDeployment(tc.deploymentID, &tc.update)

			// Verify that all expectations were met.
			tsession.AssertExpectations(t)
			dsession.AssertExpectations(t)

			// Verify that we got the expected results.
			assert.Equal(tc.transactionID, tid)
			assert.Equal(tc.updateError, err)
		})
	}
}

// storageDeploymentDeletionRequestValidationTestCase represents a single test case for
// validateStorageDeploymentDeletionRequest
type storageDeploymentDeletionRequestValidationTestCase struct {
	name                     string
	deploymentID             string
	validationError          error
	deployment               hm.Deployment
	deploymentRetrievalError error
	template                 hm.Template
	templateRetrievalError   error
}

// ExpectGetDeploymentCall returns ture if we expect GetDeployment to be called for the test case.
func (tc storageDeploymentDeletionRequestValidationTestCase) ExpectGetDeploymentCall() bool {
	return tc.validationError == nil
}

// ExpectGetTemplateCall returns true if we expect GetTemplate to be called for the test case.
func (tc storageDeploymentDeletionRequestValidationTestCase) ExpectGetTemplateCall() bool {
	return tc.ExpectGetDeploymentCall() && tc.deploymentRetrievalError == nil
}

// ExpectedError returns the error that we expect to be returned for the test case.
func (tc storageDeploymentDeletionRequestValidationTestCase) ExpectedError() error {
	switch {
	case tc.validationError != nil:
		return tc.validationError
	case tc.deploymentRetrievalError != nil:
		return tc.deploymentRetrievalError
	case tc.templateRetrievalError != nil:
		return tc.templateRetrievalError
	default:
		return getIncorrectTemplatePurposeError(tc.template.Metadata.Purpose)
	}
}

// TestValidateStorageDeploymentDeletionRequest runs the test cases for ValidateStorageDeploymentDeletionRequest.
func TestValidateStorageDeploymentDeletionRequest(t *testing.T) {
	testCases := make([]storageDeploymentDeletionRequestValidationTestCase, 5)

	// Some templates for testing.
	storageTemplate := newTemplate(service.TemplatePurposeStorage)
	generalTemplate := newTemplate(service.TemplatePurposeGeneral)

	// Some deployments for testing.
	storageDeployment := newDeployment(storageTemplate.ID.String())
	generalDeployment := newDeployment(generalTemplate.ID.String())

	// The lower level validation fails.
	testCases[0] = storageDeploymentDeletionRequestValidationTestCase{
		name:            "lower level validation failure",
		deploymentID:    storageDeployment.ID,
		validationError: fmt.Errorf("something bad happened"),
	}

	// An error occurs while trying to fetch the deployment.
	testCases[1] = storageDeploymentDeletionRequestValidationTestCase{
		name:                     "storage deployment retrieval error",
		deploymentID:             storageDeployment.ID,
		deploymentRetrievalError: fmt.Errorf("something bad happened"),
	}

	// An error occurs while trying to fetch the template.
	testCases[2] = storageDeploymentDeletionRequestValidationTestCase{
		name:                   "template retrieval error",
		deploymentID:           storageDeployment.ID,
		deployment:             storageDeployment,
		template:               storageTemplate,
		templateRetrievalError: fmt.Errorf("something bad happened"),
	}

	// The template purpose is incorrect.
	testCases[3] = storageDeploymentDeletionRequestValidationTestCase{
		name:         "incorrect template purpose",
		deploymentID: generalDeployment.ID,
		deployment:   generalDeployment,
		template:     generalTemplate,
	}

	// The validation succeeds.
	testCases[4] = storageDeploymentDeletionRequestValidationTestCase{
		name:         "validation success",
		deploymentID: storageDeployment.ID,
		deployment:   storageDeployment,
		template:     storageTemplate,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			deploymentID := cacaocommon.ID(tc.deploymentID)

			// Create the templates session.
			tsession := new(tmocks.Session)
			if tc.ExpectGetTemplateCall() {
				tsession.On("GetTemplate", tc.template.ID).
					Return(tc.template, tc.templateRetrievalError)
			}

			// Create the deployments session.
			dsession := new(dmocks.Session)
			dsession.On("ValidateDeploymentDeletionRequest", deploymentID).
				Return(tc.validationError)
			if tc.ExpectGetDeploymentCall() {
				dsession.On("GetDeployment", deploymentID).
					Return(tc.deployment, tc.deploymentRetrievalError)
			}

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual results.
			err := session.ValidateStorageDeploymentDeletionRequest(deploymentID)

			// Verify that all expectations were met.
			tsession.AssertExpectations(t)
			dsession.AssertExpectations(t)

			// Verify that we got the expected results.
			assert.Equal(tc.ExpectedError(), err)
		})
	}
}

// storageDeploymentDeletionTestCase represents a single test case for DeleteStorageDeployment.
type storageDeploymentDeletionTestCase struct {
	name          string
	deploymentID  string
	transactionID cacaocommon.ID
	deleted       bool
	deletionError error
}

// TestDeleteStorageDeployment runs the test cases for DeleteStorageDeployment.
func TestDeleteStorageDeployment(t *testing.T) {
	testCases := make([]storageDeploymentDeletionTestCase, 3)

	// A deployment ID for convenience.
	deploymentID := cacaocommon.NewID("deployment").String()

	// The deletion request submission fails.
	testCases[0] = storageDeploymentDeletionTestCase{
		name:          "submission failure",
		deploymentID:  deploymentID,
		deletionError: fmt.Errorf("something bad happened"),
	}

	// The deletion request submission succceeds, but the storage deployment isn't deleted.
	testCases[1] = storageDeploymentDeletionTestCase{
		name:          "storage deployment not deleted",
		deploymentID:  deploymentID,
		transactionID: cacaocommon.NewID("tid"),
	}

	// The storage deployment is successfully deleted.
	testCases[2] = storageDeploymentDeletionTestCase{
		name:          "storage deployment deleted",
		deploymentID:  deploymentID,
		transactionID: cacaocommon.NewID("tid"),
		deleted:       true,
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)
			deploymentID := cacaocommon.ID(tc.deploymentID)

			// Create the templates session.
			tsession := new(tmocks.Session)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			dsession.On("DeleteDeployment", deploymentID).
				Return(tc.transactionID, tc.deleted, tc.deletionError)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual results.
			tid, deleted, err := session.DeleteStorageDeployment(deploymentID)

			// Verify that all expectations were met.
			tsession.AssertExpectations(t)
			dsession.AssertExpectations(t)

			// Verify that we got the expected results.
			assert.Equal(tc.transactionID, tid)
			assert.Equal(tc.deleted, deleted)
			assert.Equal(tc.deletionError, err)
		})
	}
}

// storageDeploymentActionTestCase represents a single test case for PerformStorageDeploymentAction.
type storageDeploymentActionTestCase struct {
	name            string
	deploymentID    cacaocommon.ID
	actionRequest   *hm.StorageActionRequest
	runs            []hm.DeploymentRun
	runListingError error
	expectedID      cacaocommon.ID
	runError        error
}

// expectListDeploymentRunsCall returns true if LilstDeploymentRuns should be called for the test case.
func (tc storageDeploymentActionTestCase) expectListDeploymentRunsCall() bool {
	return tc.actionRequest.InstanceID != ""
}

// mostRecentSuccessfulRun returns the most recent successful run for the test case.
func (tc storageDeploymentActionTestCase) mostRecentSuccessfulRun() *hm.DeploymentRun {
	returnIndex := -1
	for i, run := range tc.runs {
		if run.Status != "errored" {
			returnIndex = i
		}
	}
	if returnIndex >= 0 {
		return &tc.runs[returnIndex]
	}
	return nil
}

// getNewRunRequest returns the request that should be used for the new deployment run. This method should only
// be called if there's at least one successful run in the test case.
func (tc storageDeploymentActionTestCase) getNewRunRequest() *hm.DeploymentRun {
	runRequest := copyDeploymentRun(tc.mostRecentSuccessfulRun())

	// Update existing parameters if they're present.
	var instanceIDParamFound, attachParamFound bool
	for i, param := range runRequest.Parameters {
		if param.Key == instanceIDParamName {
			instanceIDParamFound = true
			runRequest.Parameters[i].Value = tc.actionRequest.InstanceID
		} else if param.Key == attachParamName {
			attachParamFound = true
			runRequest.Parameters[i].Value = tc.actionRequest.Action.AttachParamValue()
		}
	}

	// Add parameters if we need to.
	if !instanceIDParamFound {
		runRequest.Parameters = append(runRequest.Parameters, hm.KeyValue{
			Key:   instanceIDParamName,
			Value: tc.actionRequest.InstanceID,
		})
	}
	if !attachParamFound {
		runRequest.Parameters = append(runRequest.Parameters, hm.KeyValue{
			Key:   attachParamName,
			Value: tc.actionRequest.Action.AttachParamValue(),
		})
	}

	return runRequest
}

// expectRunDeploymentCall returns true if RunDeployment should be called for the test case.
func (tc storageDeploymentActionTestCase) expectRunDeploymentCall() bool {
	switch {
	case !tc.expectListDeploymentRunsCall():
		return false
	case tc.runListingError != nil:
		return false
	case tc.mostRecentSuccessfulRun() == nil:
		return false
	default:
		return true
	}
}

// expectedError returns the error that should be returned for the test case.
func (tc storageDeploymentActionTestCase) expectedError() error {
	switch {
	case tc.actionRequest.InstanceID == "":
		return service.NewCacaoInvalidParameterError("no instance ID specified")
	case tc.runListingError != nil:
		return tc.runListingError
	case tc.mostRecentSuccessfulRun() == nil:
		return service.NewCacaoInvalidParameterError("no successful deployment run found")
	case tc.runError != nil:
		return tc.runError
	default:
		return nil
	}
}

// TestPerformStorageDeploymentAction runs the test cases for PerformStorageDeploymentAction.
func TestPerformStorageDeploymentAction(t *testing.T) {
	testCases := make([]storageDeploymentActionTestCase, 8)

	// Action requests for testing.
	invalidActionRequest := hm.StorageActionRequest{
		Action: hm.StorageActionAttach,
	}
	attachActionRequest := hm.StorageActionRequest{
		Action:     hm.StorageActionAttach,
		InstanceID: "attach-instance",
	}
	detachActionRequest := hm.StorageActionRequest{
		Action:     hm.StorageActionDetach,
		InstanceID: "detach-instance",
	}

	// Some deployment runs for testing.
	successfulDeploymentRun := newDeploymentRun("active", true, true)
	failedDeploymentRun := newDeploymentRun("errored", true, true)

	// A generic error for testing.
	genericError := fmt.Errorf("something bad happened")

	// The case where the action request is invalid.
	testCases[0] = storageDeploymentActionTestCase{
		name:          "invalid action request",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &invalidActionRequest,
	}

	// The case where the deployment run listing fails.
	testCases[1] = storageDeploymentActionTestCase{
		name:            "deployment run listing failure",
		deploymentID:    cacaocommon.NewID("deployment"),
		actionRequest:   &attachActionRequest,
		runListingError: genericError,
	}

	// The case where there are no successful deployment runs.
	testCases[2] = storageDeploymentActionTestCase{
		name:          "no successful deployment runs",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &attachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, failedDeploymentRun},
	}

	// The case where the deployment run request fails.
	testCases[3] = storageDeploymentActionTestCase{
		name:          "the new deployment run fails",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &attachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, successfulDeploymentRun},
		runError:      genericError,
	}

	// The case where the deployment run request succeeds.
	testCases[4] = storageDeploymentActionTestCase{
		name:          "the new deployment run succeeds",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &detachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, successfulDeploymentRun},
		expectedID:    cacaocommon.NewID("run"),
	}

	// The case where the attach parameter wasn't included in the last successful run.
	testCases[5] = storageDeploymentActionTestCase{
		name:          "the attach parameter wasn't included in the last successful run",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &attachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, newDeploymentRun("active", true, false)},
		expectedID:    cacaocommon.NewID("run"),
	}

	// The case where the instanceID parameter wasn't included in the last successful run.
	testCases[6] = storageDeploymentActionTestCase{
		name:          "the instance ID parameter wasn't included in the last successful run",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &attachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, newDeploymentRun("active", false, true)},
		expectedID:    cacaocommon.NewID("run"),
	}

	// The case where the instanceID and attach parameters weren't included in the last succcessful run.
	testCases[7] = storageDeploymentActionTestCase{
		name:          "the instance ID and attach parameters weren't included in the last successful run",
		deploymentID:  cacaocommon.NewID("deployment"),
		actionRequest: &attachActionRequest,
		runs:          []hm.DeploymentRun{failedDeploymentRun, newDeploymentRun("active", false, false)},
		expectedID:    cacaocommon.NewID("run"),
	}

	// Run the test cases.
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert := assert.New(t)

			// Create the deployments session.
			dsession := new(dmocks.Session)
			if tc.expectListDeploymentRunsCall() {
				dsession.On("ListDeploymentRuns", tc.deploymentID).
					Return(tc.runs, tc.runListingError)
			}
			if tc.expectRunDeploymentCall() {
				dsession.On("RunDeployment", tc.deploymentID, tc.getNewRunRequest()).
					Return(tc.expectedID, tc.runError)
			}

			// Create the templates session.
			tsession := new(tmocks.Session)

			// Create the storage session.
			session := newTestSession(dsession, tsession)

			// Get the actual result.
			actualID, actualError := session.PerformStorageDeploymentAction(tc.deploymentID, tc.actionRequest)

			// Verify that all expectations were met.
			dsession.AssertExpectations(t)
			tsession.AssertExpectations(t)

			// Verify that we got back the expected results.
			assert.Equal(tc.expectedID, actualID)
			assert.Equal(tc.expectedError(), actualError)
		})
	}
}

package awsprovider

import (
	"context"
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// Client is an interface for interacting with the provider openstack microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(ctx context.Context, actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the provider openstack microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error)
	ImageList(providerID string, region string, credential providers.CredentialOption) ([]providers.AWSImage, error)
	GetImage(providerID string, region string, credential providers.CredentialOption, imageID string) (*providers.AWSImage, error)
	FlavorList(providerID string, region string, credential providers.CredentialOption) ([]providers.Flavor, error)
	GetFlavor(providerID string, region string, credential providers.CredentialOption, flavorID string) (*providers.Flavor, error)
}

// primary implementation of Client interface.
type providerClient struct {
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
}

// New creates a new provider openstack microservice client.
func New(natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) Client {
	return &providerClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session ...
func (c providerClient) Session(ctx context.Context, actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, errors.New("no actor specified")
	}
	if ctx == nil {
		log.WithFields(log.Fields{
			"package":  "awsprovider",
			"function": "awsprovider.Session",
		}).Error("context is nil")
		return nil, errors.New("context is nil")
	}
	return providerSession{
		natsConfig: &c.natsConfig,
		stanConfig: &c.stanConfig,
		ctx:        ctx,
		actor:      actor,
		emulator:   emulator,
	}, nil
}

// primary implementation of Session interface.
type providerSession struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
	ctx        context.Context
	actor      string
	emulator   string
}

// RegionList ...
func (s providerSession) RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error) {
	svcClient := s.getSvcClient(providerID)
	regionList, err := svcClient.RegionList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credential)
	if err != nil {
		return nil, err
	}
	return regionList, nil
}

// ImageList ...
func (s providerSession) ImageList(providerID string, region string, credential providers.CredentialOption) ([]providers.AWSImage, error) {
	svcClient := s.getSvcClient(providerID)
	imageList, err := svcClient.ImageList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, credential)
	if err != nil {
		return nil, err
	}
	return imageList, nil
}

// GetImage ...
func (s providerSession) GetImage(providerID string, region string, credential providers.CredentialOption, imageID string) (*providers.AWSImage, error) {
	svcClient := s.getSvcClient(providerID)
	image, err := svcClient.GetImage(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, imageID, credential)
	if err != nil {
		return nil, err
	}
	return image, nil
}

// FlavorList ...
func (s providerSession) FlavorList(providerID string, region string, credential providers.CredentialOption) ([]providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavorList, err := svcClient.FlavorList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, credential)
	if err != nil {
		return nil, err
	}
	return flavorList, nil
}

// GetFlavor ...
func (s providerSession) GetFlavor(providerID string, region string, credential providers.CredentialOption, flavorID string) (*providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavor, err := svcClient.GetFlavor(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, flavorID, credential)
	if err != nil {
		return nil, err
	}
	return flavor, nil
}

func (s providerSession) getSvcClient(providerID string) providers.AWSProvider {
	// new client ID for each construction
	newNatsConfig := s.natsConfig
	newNatsConfig.ClientID = common.NewID(newNatsConfig.ClientID).String()
	return providers.NewAWSProvider(common.ID(providerID), newNatsConfig, s.stanConfig)
}

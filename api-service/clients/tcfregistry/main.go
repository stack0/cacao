package tcfregistry

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the TCF Registry microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the TCF Registry microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListTCFRegistryEntries() ([]cacao_common_http.TCFRegistryEntry, error)
	GetTCFRegistryEntry(name string) (cacao_common_http.TCFRegistryEntry, error)
	QueryTCFRegistryEntry(name string, queryParams map[string]string) (cacao_common_http.TCFRegistryQueryResult, error)

	CreateTCFRegistryEntry(creationRequest cacao_common_http.TCFRegistryEntry) (string, error)
	ValidateTCFRegistryEntryCreationRequest(creationRequest cacao_common_http.TCFRegistryEntry) error
	UpdateTCFRegistryEntry(name string, updateRequest cacao_common_http.TCFRegistryEntry) (string, error)
	ValidateTCFRegistryEntryUpdateRequest(name string, updateRequest cacao_common_http.TCFRegistryEntry) error
	UpdateTCFRegistryEntryFields(name string, updateRequest cacao_common_http.TCFRegistryEntry, updateFields []string) (string, error)
	ValidateTCFRegistryEntryUpdateFieldsRequest(name string, updateRequest cacao_common_http.TCFRegistryEntry, updateFields []string) error
	DeleteTCFRegistryEntry(name string) (string, error)
	ValidateTCFRegistryEntryDeletionRequest(name string) error
}

// tcfRegistryClient is the primary Client implementation.
type tcfRegistryClient struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
}

// New creates a new TCF Registry microservice client.
func New(natsConfig cacao_common_messaging.NatsConfig, stanConfig cacao_common_messaging.StanConfig) Client {
	return &tcfRegistryClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session returns a new TCF Registry microservice client session.
func (c *tcfRegistryClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := tcfRegistrySession{
		natsConfig: c.natsConfig,
		stanConfig: c.stanConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
		context:    context.Background(),
	}
	return &session, nil
}

// tcfRegistrySession is the primary TCFRegistrySession implementation.
type tcfRegistrySession struct {
	natsConfig cacao_common_messaging.NatsConfig
	stanConfig cacao_common_messaging.StanConfig
	actor      string
	emulator   string
	isAdmin    bool
	context    context.Context
}

func (t *tcfRegistrySession) convertToHTTPObject(obj cacao_common_service.TCFRegistryEntry) cacao_common_http.TCFRegistryEntry {
	return cacao_common_http.TCFRegistryEntry{
		Name:                      obj.GetName(),
		Description:               obj.GetDescription(),
		Type:                      obj.GetType(),
		QueryTarget:               obj.GetQueryTarget(),
		QueryData:                 obj.GetQueryData(),
		QueryResultJSONPathFilter: obj.GetQueryResultJSONPathFilter(),
	}
}

func (t *tcfRegistrySession) convertToQueryResultHTTPObject(obj cacao_common_service.TCFRegistryQueryResult) cacao_common_http.TCFRegistryQueryResult {
	return cacao_common_http.TCFRegistryQueryResult{
		EntryName: obj.GetEntryName(),
		DataType:  obj.GetDataType(),
		Value:     obj.GetValue(),
	}
}

func (t *tcfRegistrySession) convertToServiceObject(obj cacao_common_http.TCFRegistryEntry) cacao_common_service.TCFRegistryEntry {
	return &cacao_common_service.TCFRegistryEntryModel{
		Name:                      obj.Name,
		Description:               obj.Description,
		Type:                      obj.Type,
		QueryTarget:               obj.QueryTarget,
		QueryData:                 obj.QueryData,
		QueryResultJSONPathFilter: obj.QueryResultJSONPathFilter,
	}
}

// ListTCFRegistryEntries obtains a list of tcf registry entries.
func (t *tcfRegistrySession) ListTCFRegistryEntries() ([]cacao_common_http.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTCFRegistryEntries",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	tcfRegistryEntries, err := serviceClient.List()
	if err != nil {
		msg := "failed to list tcf registry entries"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TCFRegistryEntry, 0, len(tcfRegistryEntries))
	for _, entries := range tcfRegistryEntries {
		httpObject := t.convertToHTTPObject(entries)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTCFRegistryEntry returns the tcf registry entry with the name if it exists.
func (t *tcfRegistrySession) GetTCFRegistryEntry(name string) (cacao_common_http.TCFRegistryEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTCFRegistryEntry",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TCFRegistryEntry{}, err
	}

	tcfRegistryEntry, err := serviceClient.Get(name)
	if err != nil {
		msg := fmt.Sprintf("failed to get the tcf registry entry for name %s", name)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TCFRegistryEntry{}, err
	}

	// convert to http object
	httpObject := t.convertToHTTPObject(tcfRegistryEntry)
	return httpObject, nil
}

// QueryTCFRegistryEntry returns the query result with the tcf registry entry if it exists.
func (t *tcfRegistrySession) QueryTCFRegistryEntry(name string, queryParams map[string]string) (cacao_common_http.TCFRegistryQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "QueryTCFRegistryEntry",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TCFRegistryQueryResult{}, err
	}

	queryResult, err := serviceClient.Query(name, queryParams)
	if err != nil {
		msg := fmt.Sprintf("failed to query to the tcf registry entry for name %s", name)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TCFRegistryQueryResult{}, err
	}

	// convert to http object
	httpObject := t.convertToQueryResultHTTPObject(queryResult)
	return httpObject, nil
}

// CreateTCFRegistryEntry creates a new tcf registry entry.
func (t *tcfRegistrySession) CreateTCFRegistryEntry(creationRequest cacao_common_http.TCFRegistryEntry) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateTCFRegistryEntry",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := t.convertToServiceObject(creationRequest)

	err = serviceClient.Create(serviceObject)
	if err != nil {
		msg := "failed to create a tcf registry entry"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return creationRequest.Name, nil
}

// ValidateTCFRegistryEntryCreationRequest checks a tcf registry entry creation request to ensure that it's valid.
func (t *tcfRegistrySession) ValidateTCFRegistryEntryCreationRequest(creationRequest cacao_common_http.TCFRegistryEntry) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry name is not valid")
	}

	if len(creationRequest.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry type is not valid")
	}

	if len(creationRequest.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry query target is not valid")
	}

	return nil
}

// UpdateTCFRegistryEntry updates the tcf registry entry with the given name.
func (t *tcfRegistrySession) UpdateTCFRegistryEntry(name string, updateRequest cacao_common_http.TCFRegistryEntry) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTCFRegistryEntry",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := t.convertToServiceObject(updateRequest)
	serviceObject.SetName(name)

	err = serviceClient.Update(serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the tcf registry entry for name %s", name)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return name, nil
}

// ValidateTCFRegistryEntryUpdateRequest checks a tcf registry entry update request to ensure that it's valid.
func (t *tcfRegistrySession) ValidateTCFRegistryEntryUpdateRequest(name string, updateRequest cacao_common_http.TCFRegistryEntry) error {
	if len(name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry name is not valid")
	}

	if len(updateRequest.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry type is not valid")
	}

	if len(updateRequest.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry query target is not valid")
	}

	return nil
}

// UpdateTCFRegistryEntryFields updates fields of the tcf registry entry with the given name.
func (t *tcfRegistrySession) UpdateTCFRegistryEntryFields(name string, updateRequest cacao_common_http.TCFRegistryEntry, updateFields []string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTCFRegistryEntryFields",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	// convert to service object
	serviceObject := t.convertToServiceObject(updateRequest)
	serviceObject.SetName(name)

	err = serviceClient.UpdateFields(serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the tcf registry entry for name %s", name)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return name, nil
}

// ValidateTCFRegistryEntryUpdateFieldsRequest checks a tcf registry entry update request to ensure that it's valid.
func (t *tcfRegistrySession) ValidateTCFRegistryEntryUpdateFieldsRequest(name string, updateRequest cacao_common_http.TCFRegistryEntry, updateFields []string) error {
	if len(name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry name is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "type":
			if len(updateRequest.Type) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry type is not valid")
			}
		case "query_target":
			if len(updateRequest.QueryTarget) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("tcf registry entry query target is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTCFRegistryEntry deletes an existing tcf registry entry.
func (t *tcfRegistrySession) DeleteTCFRegistryEntry(name string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTCFRegistryEntry",
	})

	serviceClient, err := cacao_common_service.NewNatsTCFRegistryClient(t.context, t.actor, t.emulator, t.natsConfig, t.stanConfig)
	if err != nil {
		msg := "failed to create a tcf registry client"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	err = serviceClient.Delete(name)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the tcf registry entry for name %s", name)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return name, nil
}

// ValidateTCFRegistryEntryDeletionRequest checks a tcf registry entry delete request to ensure that it's valid.
func (t *tcfRegistrySession) ValidateTCFRegistryEntryDeletionRequest(name string) error {
	if len(name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("tcf registry name is not valid")
	}
	return nil
}

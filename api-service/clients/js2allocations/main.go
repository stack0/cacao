package js2allocations

import (
	"context"

	log "github.com/sirupsen/logrus"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the JS2Allocation microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the JS2Allocation microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	GetUser(nocache bool) (cacao_common_http.JS2User, error)
	ListProjects(nocache bool) ([]cacao_common_http.JS2Project, error)
}

// js2allocationsClient is the primary Client implementation.
type js2allocationsClient struct {
	natsConfig cacao_common_messaging.NatsConfig
}

// New creates a new JS2Allocation microservice client.
func New(natsConfig cacao_common_messaging.NatsConfig) Client {
	return &js2allocationsClient{
		natsConfig: natsConfig,
	}
}

// Session returns a new JS2Allocation microservice client session.
func (c *js2allocationsClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}

	// Define and return the session.
	session := js2allocationSession{
		natsConfig: c.natsConfig,
		actor:      actor,
		emulator:   emulator,
		isAdmin:    isAdmin,
		context:    context.Background(),
	}
	return &session, nil
}

// js2allocationSession is the primary Session implementation.
type js2allocationSession struct {
	natsConfig cacao_common_messaging.NatsConfig
	actor      string
	emulator   string
	isAdmin    bool
	context    context.Context
}

func (s *js2allocationSession) convertUserToHTTPObject(obj cacao_common_service.JS2User) cacao_common_http.JS2User {
	return cacao_common_http.JS2User{
		Owner:        obj.GetOwner(),
		TACCUsername: obj.GetTACCUsername(),
		RetrievedAt:  obj.GetRetrievedAt(),
	}
}

func (s *js2allocationSession) convertProjectToHTTPObject(obj cacao_common_service.JS2Project) cacao_common_http.JS2Project {
	return cacao_common_http.JS2Project{
		ID:          obj.GetID(),
		Owner:       obj.GetOwner(),
		Title:       obj.GetTitle(),
		Description: obj.GetDescription(),
		PI:          obj.GetPI(),
		Allocations: obj.GetAllocations(),
		RetrievedAt: obj.GetRetrievedAt(),
	}
}

// GetUser obtains a JetStream2 user.
func (s *js2allocationSession) GetUser(nocache bool) (cacao_common_http.JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetUser",
	})

	serviceClient, err := cacao_common_service.NewNatsJS2AllocationClient(s.context, s.actor, s.emulator, s.natsConfig)
	if err != nil {
		msg := "failed to create a js2allocation client"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.JS2User{}, err
	}

	js2User, err := serviceClient.GetUser(nocache)
	if err != nil {
		msg := "failed to get JS2 User"
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.JS2User{}, err
	}

	// convert to http object
	httpObject := s.convertUserToHTTPObject(js2User)

	return httpObject, nil
}

// ListProjects obtains a list of JetStream2 projects.
func (s *js2allocationSession) ListProjects(nocache bool) ([]cacao_common_http.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListProjects",
	})

	serviceClient, err := cacao_common_service.NewNatsJS2AllocationClient(s.context, s.actor, s.emulator, s.natsConfig)
	if err != nil {
		msg := "failed to create a js2allocation client"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	js2Projects, err := serviceClient.List(nocache)
	if err != nil {
		msg := "failed to list JS2 Projects"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.JS2Project, 0, len(js2Projects))
	for _, js2Project := range js2Projects {
		httpObject := s.convertProjectToHTTPObject(js2Project)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

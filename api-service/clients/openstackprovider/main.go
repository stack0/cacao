package openstackprovider

import (
	"context"
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// Client is an interface for interacting with the provider openstack microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(ctx context.Context, actor, emulator string) (Session, error)
}

// Session is an interface for interacting with the provider openstack microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	CreateApplicationCredential(providerID string, credential providers.CredentialOption, namePostfix string, scope providers.ProjectScope) (string, error)
	ApplicationCredentialList(providerID string, credential providers.CredentialOption) ([]providers.ApplicationCredential, error)
	GetApplicationCredential(providerID string, credential providers.CredentialOption, applicationCredentialID string) (*providers.ApplicationCredential, error)
	DeleteApplicationCredential(providerID string, credentialID string) error
	AuthenticationTest(providerID string, variables map[string]string) error
	RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error)
	ImageList(providerID string, credential providers.CredentialOption, region string) ([]providers.OpenStackImage, error)
	GetImage(providerID string, credential providers.CredentialOption, region string, imageID string) (*providers.OpenStackImage, error)
	FlavorList(providerID string, credential providers.CredentialOption, region string) ([]providers.Flavor, error)
	GetFlavor(providerID string, credential providers.CredentialOption, region string, flavorID string) (*providers.Flavor, error)
	ProjectList(providerID string, credential providers.CredentialOption) ([]providers.Project, error)
	GetProject(providerID string, credential providers.CredentialOption, projectID string) (*providers.Project, error)
}

// primary implementation of Client interface.
type providerClient struct {
	natsConfig messaging.NatsConfig
	stanConfig messaging.StanConfig
}

// New creates a new provider openstack microservice client.
func New(natsConfig messaging.NatsConfig, stanConfig messaging.StanConfig) Client {
	return &providerClient{
		natsConfig: natsConfig,
		stanConfig: stanConfig,
	}
}

// Session ...
func (c providerClient) Session(ctx context.Context, actor, emulator string) (Session, error) {
	if actor == "" {
		return nil, errors.New("no actor specified")
	}
	if ctx == nil {
		log.WithFields(log.Fields{
			"package":  "openstackprovider",
			"function": "providerClient.Session",
		}).Error("context is nil")
		return nil, errors.New("context is nil")
	}
	return providerSession{
		natsConfig: &c.natsConfig,
		stanConfig: &c.stanConfig,
		ctx:        ctx,
		actor:      actor,
		emulator:   emulator,
	}, nil
}

// primary implementation of Session interface.
type providerSession struct {
	natsConfig *messaging.NatsConfig
	stanConfig *messaging.StanConfig
	ctx        context.Context
	actor      string
	emulator   string
}

// CreateApplicationCredential ...
func (s providerSession) CreateApplicationCredential(providerID string, credential providers.CredentialOption, namePostfix string, scope providers.ProjectScope) (credID string, err error) {
	svcClient := s.getSvcClient(providerID)
	credID, err = svcClient.CreateApplicationCredential(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, scope, namePostfix, credential)
	if err != nil {
		return "", err
	}
	return credID, nil
}

// ApplicationCredentialList ...
func (s providerSession) ApplicationCredentialList(providerID string, credential providers.CredentialOption) ([]providers.ApplicationCredential, error) {
	svcClient := s.getSvcClient(providerID)
	applicationCredentialList, err := svcClient.ApplicationCredentialList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credential)
	if err != nil {
		return nil, err
	}
	return applicationCredentialList, nil
}

// GetApplicationCredential ...
func (s providerSession) GetApplicationCredential(providerID string, credential providers.CredentialOption, applicationCredentialID string) (*providers.ApplicationCredential, error) {
	svcClient := s.getSvcClient(providerID)
	applicationCredential, err := svcClient.GetApplicationCredential(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, applicationCredentialID, credential)
	if err != nil {
		return nil, err
	}
	return applicationCredential, nil
}

// DeleteApplicationCredential ...
func (s providerSession) DeleteApplicationCredential(providerID string, credentialID string) error {
	svcClient := s.getSvcClient(providerID)
	err := svcClient.DeleteApplicationCredential(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credentialID)
	if err != nil {
		return err
	}
	return nil
}

// AuthenticationTest ...
func (s providerSession) AuthenticationTest(providerID string, variables map[string]string) error {
	svcClient := s.getSvcClient(providerID)
	err := svcClient.AuthenticationTest(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, variables)
	if err != nil {
		return err
	}
	return nil
}

// RegionList ...
func (s providerSession) RegionList(providerID string, credential providers.CredentialOption) ([]providers.Region, error) {
	svcClient := s.getSvcClient(providerID)
	regionList, err := svcClient.RegionList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credential)
	if err != nil {
		return nil, err
	}
	return regionList, nil
}

// ImageList ...
func (s providerSession) ImageList(providerID string, credential providers.CredentialOption, region string) ([]providers.OpenStackImage, error) {
	svcClient := s.getSvcClient(providerID)
	imageList, err := svcClient.ImageList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, credential)
	if err != nil {
		return nil, err
	}
	return imageList, nil
}

// GetImage ...
func (s providerSession) GetImage(providerID string, credential providers.CredentialOption, region string, imageID string) (*providers.OpenStackImage, error) {
	svcClient := s.getSvcClient(providerID)
	image, err := svcClient.GetImage(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, imageID, credential)
	if err != nil {
		return nil, err
	}
	return image, nil
}

// FlavorList ...
func (s providerSession) FlavorList(providerID string, credential providers.CredentialOption, region string) ([]providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavorList, err := svcClient.FlavorList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, credential)
	if err != nil {
		return nil, err
	}
	return flavorList, nil
}

// GetFlavor ...
func (s providerSession) GetFlavor(providerID string, credential providers.CredentialOption, region string, flavorID string) (*providers.Flavor, error) {
	svcClient := s.getSvcClient(providerID)
	flavor, err := svcClient.GetFlavor(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, region, flavorID, credential)
	if err != nil {
		return nil, err
	}
	return flavor, nil
}

// ProjectList ...
func (s providerSession) ProjectList(providerID string, credential providers.CredentialOption) ([]providers.Project, error) {
	svcClient := s.getSvcClient(providerID)
	projectList, err := svcClient.ProjectList(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, credential)
	if err != nil {
		return nil, err
	}
	return projectList, nil
}

// GetProject ...
func (s providerSession) GetProject(providerID string, credential providers.CredentialOption, projectID string) (*providers.Project, error) {
	svcClient := s.getSvcClient(providerID)
	project, err := svcClient.GetProject(s.ctx, &service.Session{
		SessionActor:    s.actor,
		SessionEmulator: s.emulator,
	}, projectID, credential)
	if err != nil {
		return nil, err
	}
	return project, nil
}

func (s providerSession) getSvcClient(providerID string) providers.OpenStackProvider {
	// new client ID for each construction
	newNatsConfig := s.natsConfig
	newNatsConfig.ClientID = common.NewID(newNatsConfig.ClientID).String()
	return providers.NewOpenStackProvider(common.ID(providerID), newNatsConfig, s.stanConfig)
}

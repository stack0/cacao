package credentials

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"strings"
	"time"
)

// Client is client for credential service
type Client interface {
	Session(actor, emulator string) (Session, error)
}

// Session is a session for an actor to perform operations on credentials.
type Session interface {
	ListCredentials() ([]hm.Credential, error)
	GetCredential(id string) (hm.Credential, error)
	CreateCredential(credential hm.Credential) (Result, error)
	DeleteCredential(id string) (Result, error)
}

// Result is the result for operations like create and delete.
// This is to maintain the same schema for the REST api.
type Result struct {
	CredentialID string               `json:"credential"`
	ErrMsg       string               `json:"errMsg"`
	Tid          common.TransactionID `json:"tid"`
	Timestamp    time.Time            `json:"timestamp"`
}

type credentialClient struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
	timeSrc  func() time.Time
}

// New creates a new client
func New(natsConf messaging.NatsConfig, stanConf messaging.StanConfig, timeSrc func() time.Time) Client {
	return credentialClient{
		natsConf: natsConf,
		stanConf: stanConf,
		timeSrc:  timeSrc,
	}
}

// Session create a new session
func (c credentialClient) Session(actor, emulator string) (Session, error) {
	return credentialSession{
		natsConf: c.natsConf,
		stanConf: c.stanConf,
		timeSrc:  c.timeSrc,
		actor:    actor,
		emulator: emulator,
	}, nil
}

type credentialSession struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
	timeSrc  func() time.Time

	actor    string
	emulator string
}

// ListCredentials returns a list of all credential owned by actor.
func (session credentialSession) ListCredentials() ([]hm.Credential, error) {
	client, err := session.newNatsClient()
	if err != nil {
		return nil, err
	}
	list, err := client.List()
	if err != nil {
		return nil, err
	}
	var result []hm.Credential
	for _, cred := range list {
		credModel := cred.(*service.CredentialModel) // FIXME
		result = append(result, serviceCredentialToHTTP(credModel))
	}
	return result, nil
}

// GetCredential fetches the credential of given ID that belonged to actor.
func (session credentialSession) GetCredential(id string) (hm.Credential, error) {
	client, err := session.newNatsClient()
	if err != nil {
		return hm.Credential{}, err
	}
	cred, err := client.Get(id)
	if err != nil {
		return hm.Credential{}, err
	}
	credModel := cred.(*service.CredentialModel) // FIXME
	return serviceCredentialToHTTP(credModel), nil
}

// CreateCredential creates a credential at the given ID for actor, if credential already exists, it will be overwritten
func (session credentialSession) CreateCredential(credential hm.Credential) (Result, error) {
	client, err := session.newNatsClient()
	if err != nil {
		return Result{
			CredentialID: credential.ID,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.timeSrc(),
		}, err
	}
	_, err = client.Add(httpCredentialToService(credential))
	if err != nil {
		return Result{
			CredentialID: credential.ID,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.utcTime(),
		}, err
	}
	return Result{
		CredentialID: credential.ID,
		ErrMsg:       "",
		Tid:          "",
		Timestamp:    session.utcTime(),
	}, nil
}

// DeleteCredential deletes a single credential of given ID.
func (session credentialSession) DeleteCredential(id string) (Result, error) {
	client, err := session.newNatsClient()
	if err != nil {
		return Result{
			CredentialID: id,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.utcTime(),
		}, err
	}
	_, err = client.Delete(id)
	if err != nil {
		return Result{
			CredentialID: id,
			ErrMsg:       err.Error(),
			Tid:          "",
			Timestamp:    session.utcTime(),
		}, err
	}
	return Result{
		CredentialID: id,
		ErrMsg:       "",
		Tid:          "",
		Timestamp:    session.utcTime(),
	}, nil
}

func (session credentialSession) utcTime() time.Time {
	return session.timeSrc().UTC()
}

func (session credentialSession) newNatsClient() (service.CredentialClient, error) {
	return service.NewNatsCredentialClient(context.TODO(), session.actor, session.emulator, session.natsConf, session.stanConf)
}

// convert service representation of credential(NATS) to http representation(REST).
func serviceCredentialToHTTP(cred *service.CredentialModel) hm.Credential {
	if cred == nil {
		return hm.Credential{}
	}
	return hm.Credential{
		Username:          cred.Username,
		Value:             cred.Value,
		Type:              string(cred.Type),
		ID:                cred.ID,
		Name:              cred.Name,
		Description:       cred.Description,
		IsSystem:          cred.IsSystem,
		IsHidden:          cred.IsHidden,
		Visibility:        string(cred.Visibility),
		Tags:              cred.GetTags(),
		CreatedAt:         cred.CreatedAt,
		UpdatedAt:         cred.UpdatedAt,
		UpdatedBy:         cred.UpdatedBy,
		UpdatedEmulatorBy: cred.UpdatedEmulatorBy,
	}
}

// convert credential from http representation (REST) to service representation(NATS).
func httpCredentialToService(cred hm.Credential) *service.CredentialModel {
	credName := cred.Name
	if credName == "" {
		// use ID from http as Name. TODO changes this when frontend make use of Name field in REST api
		credName = cred.ID
	}
	return &service.CredentialModel{
		ID:                cred.ID,
		Name:              credName,
		Username:          cred.Username,
		Type:              service.CredentialType(cred.Type),
		Value:             cred.Value,
		Description:       cred.Description,
		IsSystem:          cred.IsSystem,
		IsHidden:          cred.IsHidden,
		Visibility:        service.VisibilityType(cred.Visibility),
		Tags:              toKeyValueTags(cred.Tags),
		CreatedAt:         cred.CreatedAt,
		UpdatedAt:         cred.UpdatedAt,
		UpdatedBy:         cred.UpdatedBy,
		UpdatedEmulatorBy: cred.UpdatedEmulatorBy,
	}
}

func toKeyValueTags(tags []string) map[string]string {
	var tagKeyValPairs = make(map[string]string, len(tags))
	for _, tag := range tags {
		if strings.HasPrefix(tag, "cacao_") {
			// ignore tag start with "cacao_", this prohibits REST API to create tag key with "cacao_" prefix
			continue
		}
		tagKeyValPairs[tag] = ""
	}
	return tagKeyValPairs
}

// Convert the key-value tags to string-array tags for REST API.
// To keep the expected result the same (e.g. for openstack application credential created by CACAO), there is some filtering on tags.
// e.g.
// { "provider": "provider-aaaaaaaaaaaaaaaaaaaa", "OS_AUTH_TYPE": "v3applicationcredential", "cacao_managed": "true" }
// =>
// ["provider-aaaaaaaaaaaaaaaaaaaa", "v3applicationcredential", "cacao_managed"]
// TODO remove this when REST API is changed to key-value tags as well.
func toLegacyTags(tags map[string]string) []string {
	// insert into a set to de-duplicate
	tagSet := make(map[string]struct{})
	for key, val := range tags {
		// use value by default
		switch key {
		case "":
			// ignore empty key
		case "os_auth_type": // for openstack app cred managed by CACAO
			fallthrough
		case "cacao_openstack_project_id": // for openstack app cred managed by CACAO
			fallthrough
		case "cacao_openstack_project_name": // for openstack app cred managed by CACAO
			fallthrough
		case "cacao_provider": // for openstack app cred managed by CACAO
			tagSet[val] = struct{}{} // use value
		default:
			tagSet[key] = struct{}{}
		}
	}
	var result = make([]string, 0)
	for tagName := range tagSet {
		result = append(result, tagName)
	}
	return result
}

package config

import (
	"context"
	"gitlab.com/cyverse/cacao/api-service/constants"

	"gitlab.com/cyverse/cacao-common/messaging"
)

// Config - holds the configurable constants for the microservice
type Config struct {
	Nats       messaging.NatsConfig
	Stan       messaging.StanConfig
	AppContext context.Context `ignored:"true"`
	LogLevel   string          `default:"debug" split_words:"true"`

	OAuth2ClientID     string `envconfig:"OAUTH2_CLIENT_ID"`
	OAuth2ClientSecret string `envconfig:"OAUTH2_CLIENT_SECRET"`
	OAuth2RedirectURL  string `envconfig:"OAUTH2_REDIRECT_URL" default:"http://api:8080/user/login/callback"`
	OAuth2HmacSecret   string `envconfig:"OAUTH2_HMAC_SECRET" default:"secret"`

	KeycloakURL string `default:"http://keycloak:8080/auth/realms/cacao" split_words:"true"`

	GlobusScopes []string `default:"profile,email" split_words:"true"`
	// if true, then only use username from the primary identity, and ignore linked identities
	GlobusIgnoreLinkedIdentities bool `split_words:"true"`
	// for Globus, only used if GlobusIgnoreLinkedIdentities is false, cannot be empty if GlobusIgnoreLinkedIdentities is false.
	RequiredIdentityProvider string `split_words:"true"`

	// If enabled, will activate the autocreation of users upon login; default = false
	AutoCreateUser bool `default:"false" split_words:"true"`

	// AuthDriver how to auth, choices are "keycloak", "globus", "cilogon" or "simpletoken"
	AuthDriver constants.AuthDriverType `default:"simpletoken" split_words:"true"`

	// SimpleTokenUsername is the username to use, if using the SimpleToken authentication driver
	SimpleTokenUsername string `default:"" split_words:"true"`

	// SimpleTokenToken is the token to associate with a given username
	SimpleTokenToken string `default:"cacaosimpletoken" split_words:"true"`

	// SimpleTokenStandalone if true will not require nats, mongo, or users microservice to authentication/validate
	SimpleTokenStandalone bool `default:"false" split_words:"true"`

	//	CacaoDirectory path to where cacao was cloned by GIT, currently only used by Version Check API
	CacaoDirectory string `default:"/opt/cacao"`

	EnableRedisCache bool   `envconfig:"ENABLE_REDIS" default:"false"`
	RedisAddress     string `envconfig:"REDIS_ADDRESS" default:"redis:6379"`
	RedisPassword    string `envconfig:"REDIS_PASSWORD"`
	RedisDB          int    `envconfig:"REDIS_DB" default:"1"`           // different DB number than the DB number used by provider-openstack
	TokenCacheTTLSec uint   `envconfig:"TOKEN_CACHE_TTL" default:"7200"` // TTL in seconds
}

// GlobalConfig conains the configuration that can be shared across calls
// It should be cautioned that this should be initialized once, upon start of api service and not changed
// this is not thread-safe approach for mutable fields
var GlobalConfig Config

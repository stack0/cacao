package utils

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"k8s.io/apimachinery/pkg/util/json"
)

// HTTPError is an error that includes an HTTP response code.
type HTTPError struct {
	code    int
	message string
}

// Code returns the HTTP response code corresponding to the error.
func (h *HTTPError) Code() int {
	return h.code
}

// Error returns the message corresponding to the error.
func (h *HTTPError) Error() string {
	return h.message
}

// NewHTTPError returns a new HTTP error with the given status code and (optionally formatted) message.
func NewHTTPError(code int, format string, args ...interface{}) *HTTPError {
	return &HTTPError{
		code:    code,
		message: fmt.Sprintf(format, args...),
	}
}

// ErrorStatus is the struct/json object that is marshalled for every error response per CACAO's openapi spec
type ErrorStatus struct {
	Actor     string `json:"actor"`
	Timestamp string `json:"timestamp"`
	ErrorType string `json:"error"`
	Message   string `json:"message"`
	Path      string `json:"path"`
	Verb      string `json:"verb"`
}

// JSONError passthrough
// TODO: replace references near the end of MR
func JSONError(w http.ResponseWriter, r *http.Request, errorType string, errorMsg string, code int) {
	errorObj := new(ErrorStatus)

	actor := r.Header.Get(constants.RequestHeaderCacaoUser)
	if actor != service.ReservedCacaoSystemActor { // we don't want to expose this reserved actor
		errorObj.Actor = r.Header.Get(constants.RequestHeaderCacaoUser)
	}
	errorObj.Timestamp = time.Now().UTC().String()
	errorObj.ErrorType = errorType
	errorObj.Message = errorMsg
	errorObj.Path = r.URL.Path
	errorObj.Verb = r.Method

	w.Header().Add("Content-Type", "application/json")
	b, err := json.Marshal(errorObj)
	if err == nil {
		w.WriteHeader(code)
		w.Write(b)
	} else { // something very bad happened and we shouldn't hit this point
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("could not fulfill request and return a result; please notify site admin"))
	}
}

package main

import (
	"context"
	"gitlab.com/cyverse/cacao/api-service/clients/awsprovider"
	"gitlab.com/cyverse/cacao/api-service/clients/credentials"
	"gitlab.com/cyverse/cacao/api-service/clients/openstackprovider"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/api-service/api"
	"gitlab.com/cyverse/cacao/api-service/clients/deployments"
	"gitlab.com/cyverse/cacao/api-service/clients/interactivesessions"
	"gitlab.com/cyverse/cacao/api-service/clients/js2allocations"
	"gitlab.com/cyverse/cacao/api-service/clients/storage"
	"gitlab.com/cyverse/cacao/api-service/clients/tcfregistry"
	"gitlab.com/cyverse/cacao/api-service/clients/templates"
	"gitlab.com/cyverse/cacao/api-service/clients/workspaces"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"gitlab.com/cyverse/cacao/api-service/utils"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// GitCommit is the commit hash of the cacao repo which api-service is a part of.
// This is meant to be set at build time.
// e.g.
// go build -ldflags="-X 'main.GitCommit=aaaaaaaa'"
var GitCommit string

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)

	err := envconfig.Process(constants.ConfigVarPrefix, &config.GlobalConfig)
	if err != nil {
		log.Fatal(err.Error())
	}

	level, err := log.ParseLevel(config.GlobalConfig.LogLevel)
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	})

	// initialize the AppContext
	if config.GlobalConfig.AppContext == nil {
		config.GlobalConfig.AppContext = context.Background()
	}

	log.Trace("initializing NATS connections variables")
	api.InitNATSConnectionVars(&config.GlobalConfig)

	err = api.InitAuthenticationDriver(&config.GlobalConfig)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Panic("unable to initialize authentication driver")
		panic(err)
	}
}

func main() {
	log.Info("Auto create user = " + strconv.FormatBool(config.GlobalConfig.AutoCreateUser))
	log.Info("Log level set to: " + config.GlobalConfig.LogLevel)
	log.Info("Nats URL = " + config.GlobalConfig.Nats.URL)

	// Create the microservice clients.
	deploymentsClient := deployments.New(&config.GlobalConfig.Nats, &config.GlobalConfig.Stan)
	credentialClient := credentials.New(config.GlobalConfig.Nats, config.GlobalConfig.Stan, time.Now)
	tcfRegistryClient := tcfregistry.New(config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	templatesClient := templates.New(config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	workspacesClient := workspaces.New(config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	storageClient := storage.New(&config.GlobalConfig.Nats, &config.GlobalConfig.Stan)
	openstackProviderClient := openstackprovider.New(natsConfigWithTimeout(config.GlobalConfig.Nats, 30), config.GlobalConfig.Stan)
	awsProviderClient := awsprovider.New(natsConfigWithTimeout(config.GlobalConfig.Nats, 30), config.GlobalConfig.Stan)

	// js2allocationsClient requires longer deadline than other microservices because it communicates with external XCEDE servers
	js2allocationsClient := js2allocations.New(natsConfigWithTimeout(config.GlobalConfig.Nats, 30))

	// interactivesessionsClient requires longer deadline than other microservices because it uses ansible to configure instances
	interactivesessionsClient := interactivesessions.New(natsConfigWithTimeout(config.GlobalConfig.Nats, 60), config.GlobalConfig.Stan)

	// Router is only used for User things since they won't require authentication
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)
	api.DocsAPIRouter(router)
	api.HealthCheckAPIRouter(router)

	// Require Admin permission to access
	//api.AdminAPIRouter(router)

	// Subrouter will be used for all paths other than login since it will
	// require headers to authenticate with AuthenticateMiddleware
	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)
	api.UserAPIRouter(subRouter)
	api.DeploymentAPIRouter(deploymentsClient, subRouter)
	api.CredentialAPIRouter(credentialClient, subRouter)
	api.VersionAPIRouter(subRouter, GitCommit)
	api.ProviderAPIRouter(subRouter)
	api.ProviderSpecificAPIRouter(api.NewOpenStackProviderAPI(openstackProviderClient), api.NewAWSProviderAPI(awsProviderClient), subRouter)
	api.StorageAPIRouter(storageClient, subRouter)
	// api.ClusterAPIRouter(subRouter)
	// api.WorkflowDefinitionAPIRouter(subRouter)
	// api.BuildAPIRouter(subRouter)
	// api.AnchoreAPIRouter(subRouter)
	api.TCFRegistryAPIRouter(tcfRegistryClient, subRouter)
	api.TemplateAPIRouter(templatesClient, subRouter)
	api.WorkspaceAPIRouter(workspacesClient, deploymentsClient, subRouter)
	api.JS2AllocationAPIRouter(js2allocationsClient, subRouter)
	api.InteractiveSessionAPIRouter(interactivesessionsClient, subRouter)
	//Put new routes above this line
	//RunAPIRouter defines a "catchall" endpoint for HTTP scaling
	// api.RunAPIRouter(subRouter)

	// Setting this is useful for triaging errant requests while tracing
	subRouter.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Trace("http path not found: path = " + r.URL.Path + ", method = " + r.Method)
		utils.JSONError(w, r, "path not found", "path not found", http.StatusNotFound)
	})

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8080")
	http.ListenAndServe(":8080",
		handlers.CORS(
			handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
			handlers.AllowedMethods([]string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"}),
			handlers.AllowedOrigins([]string{"*"}))(router))
}

// generate a different NATS config with a different timeout
func natsConfigWithTimeout(natsConfig messaging.NatsConfig, timeoutSec uint) messaging.NatsConfig {
	newConf := natsConfig
	newConf.RequestTimeout = int(timeoutSec)
	return newConf
}

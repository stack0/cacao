package api

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/js2allocations"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// js2allocationAPI is a single instance of the js2allocation API implementation.
type js2allocationAPI struct {
	js2allocationsClient js2allocations.Client
}

// JS2AllocationAPIRouter creates routes for operations related to JS2 Allocations.
func JS2AllocationAPIRouter(js2allocationsClient js2allocations.Client, router *mux.Router) {
	js2aapi := &js2allocationAPI{
		js2allocationsClient: js2allocationsClient,
	}

	router.HandleFunc("/js2user", js2aapi.getUser).Methods("GET")
	router.HandleFunc("/js2projects", js2aapi.listProjects).Methods("GET")
}

func (js2aapi *js2allocationAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	var status int
	switch err.(type) {
	case *cacao_common_service.CacaoInvalidParameterError, *cacao_common_service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *cacao_common_service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *cacao_common_service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *cacao_common_service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *cacao_common_service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *cacao_common_service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// getJS2AllocationsClientSession obtains the js2allocation client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (js2aapi *js2allocationAPI) getJS2AllocationsClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) js2allocations.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := js2aapi.js2allocationsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the js2allocation microservice client")
		js2aapi.JSONError(logger, w, r, err)
		return nil
	}
	return session
}

// getUser implements the GET /js2user endpoint.
func (js2aapi *js2allocationAPI) getUser(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getUser",
	})
	logger.Info("api.getUser(): start")
	var err error

	// handle nocache parameter
	nocache := false
	nocacheString := r.URL.Query().Get("nocache")
	nocacheBool, err := strconv.ParseBool(nocacheString)
	if err == nil {
		nocache = nocacheBool
	}

	// Create the JS2 Allocation client session.
	session := js2aapi.getJS2AllocationsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get JS2 User
	js2User, err := session.GetUser(nocache)
	if err != nil {
		errorMessage := "error obtaining JS2 User"
		logger.WithField("error", err).Error(errorMessage)
		js2aapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListProjects returning nil.
	if len(js2User.Owner) == 0 && len(js2User.TACCUsername) == 0 {
		errorMessage := "JS2 User not found"
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		js2aapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, js2User, http.StatusOK)
}

// listProjects implements the GET /js2projects endpoint.
func (js2aapi *js2allocationAPI) listProjects(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listProjects",
	})
	logger.Info("api.listProjects(): start")
	var err error

	// handle nocache parameter
	nocache := false
	nocacheString := r.URL.Query().Get("nocache")
	nocacheBool, err := strconv.ParseBool(nocacheString)
	if err == nil {
		nocache = nocacheBool
	}

	// Create the JS2 Allocation client session.
	session := js2aapi.getJS2AllocationsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of JS2 Projects
	js2Projects, err := session.ListProjects(nocache)
	if err != nil {
		errorMessage := "error listing JS2 Projects"
		logger.WithField("error", err).Error(errorMessage)
		js2aapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListProjects returning nil.
	if js2Projects == nil {
		js2Projects = make([]cacao_common_http.JS2Project, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, js2Projects, http.StatusOK)
}

package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"net/http"
)

// ProviderSpecificAPIRouter ...
func ProviderSpecificAPIRouter(openstackAPI OpenStackProviderAPI, awsAPI AWSProviderAPI, router *mux.Router) {

	router.HandleFunc("/providers/{providerid}/regions",
		multiProvider().OpenStack(openstackAPI.RegionList).AWS(awsAPI.RegionList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images",
		multiProvider().OpenStack(openstackAPI.ImageList).AWS(awsAPI.ImageList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/images/{imageid}",
		multiProvider().OpenStack(openstackAPI.GetImage).AWS(awsAPI.GetImage).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors",
		multiProvider().OpenStack(openstackAPI.FlavorList).AWS(awsAPI.FlavorList).Handler,
	).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/flavors/{flavorid}",
		multiProvider().OpenStack(openstackAPI.GetFlavor).AWS(awsAPI.GetFlavor).Handler,
	).Methods(http.MethodGet)

	// OpenStack only
	router.HandleFunc("/providers/{providerid}/applicationCredentials", openstackOnly(openstackAPI.CreateApplicationCredential).Handler).Methods(http.MethodPost)
	router.HandleFunc("/providers/{providerid}/applicationCredentials", openstackOnly(openstackAPI.ApplicationCredentialList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/applicationCredentials/{applicationcredentialid}", openstackOnly(openstackAPI.GetApplicationCredential).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/applicationCredentials/{credentialid}", openstackOnly(openstackAPI.DeleteApplicationCredential).Handler).Methods(http.MethodDelete)
	router.HandleFunc("/providers/{providerid}/authenticationTest", openstackOnly(openstackAPI.AuthenticationTest).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects", openstackOnly(openstackAPI.ProjectList).Handler).Methods(http.MethodGet)
	router.HandleFunc("/providers/{providerid}/projects/{projectid}", openstackOnly(openstackAPI.GetProject).Handler).Methods(http.MethodGet)
}

type baseProviderSpecificHandler struct {
	openstackHandler http.HandlerFunc
	awsHandler       http.HandlerFunc
}

func (mph *baseProviderSpecificHandler) OpenStack(handler http.HandlerFunc) *baseProviderSpecificHandler {
	mph.openstackHandler = handler
	return mph
}

func (mph *baseProviderSpecificHandler) AWS(handler http.HandlerFunc) *baseProviderSpecificHandler {
	mph.awsHandler = handler
	return mph
}

func (mph *baseProviderSpecificHandler) Handler(w http.ResponseWriter, r *http.Request) {

	providerID := common.ID(mux.Vars(r)["providerid"])
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "baseProviderSpecificHandler.Handler",
		"providerID": providerID,
	})

	if providerID.PrimaryType() != "provider" {
		JSONError(w, "bad provider ID", http.StatusBadRequest)
	}
	providerType := providerID.SubType()
	if providerType == "" {
		// if no subtype, query provider svc for type. This is to maintain compatibility with existing provider ID w/o subtype
		logger.Debug("query for provider type")
		var err error
		providerType, err = mph.queryForProviderType(r, providerID)
		if err != nil {
			JSONError(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	switch providerType {
	case "openstack":
		if mph.openstackHandler == nil {
			logger.WithField("endpoint", r.URL).Info("provider does not support this endpoint")
			JSONError(w, "provider does not support this endpoint", http.StatusNotImplemented)
			return
		}
		mph.openstackHandler(w, r)
	case "aws":
		if mph.awsHandler == nil {
			logger.WithField("endpoint", r.URL).Info("provider does not support this endpoint")
			JSONError(w, "provider does not support this endpoint", http.StatusNotImplemented)
			return
		}
		mph.awsHandler(w, r)
	default:
		logger.WithField("providerType", providerType).Info("unknown provider type")
		JSONError(w, "unsupported provider type", http.StatusBadRequest)
	}
}

// Get provider type by query for provider by its ID via provider metadata service.
// TODO refactor this, extract out the creation of provider client.
func (mph *baseProviderSpecificHandler) queryForProviderType(r *http.Request, providerID common.ID) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":    "api",
		"function":   "baseProviderSpecificHandler.queryForProviderType",
		"providerID": providerID,
	})
	actor, emulator, _ := utils.GetCacaoHeaders(r)
	client, err := service.NewNatsProviderClient(config.GlobalConfig.AppContext, actor, emulator, config.GlobalConfig.Nats, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats provider client")
		return "", err
	}

	var provider service.Provider
	provider, err = client.Get(providerID)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("error when trying to get providerid")
		return "", err
	}
	return provider.GetType(), nil
}

func multiProvider() *baseProviderSpecificHandler {
	return &baseProviderSpecificHandler{}
}

func openstackOnly(handler http.HandlerFunc) *baseProviderSpecificHandler {
	return &baseProviderSpecificHandler{openstackHandler: handler}
}

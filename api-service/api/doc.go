package api

import (
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// DocsAPIRouter maps documentation functions to endpoints.
func DocsAPIRouter(router *mux.Router) {
	router.HandleFunc("/openapi.yaml", sendSpec).Methods("GET")
	router.HandleFunc("/docs", serveDocs).Methods("GET")
}

func sendSpec(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadFile("openapi.yaml")
	if err != nil {
		utils.JSONError(w, r, "OpenAPI spec not found", err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(b)
}

func serveDocs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf8")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(docPage))
}

const docPage = `<!DOCTYPE html>
<html>
  <head>
    <title>API Documentation</title>
    <!-- needed for adaptive design -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
    ReDoc doesn't change outer page styles
    -->
    <style>
      body {
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <redoc spec-url='/openapi.yaml'></redoc>
    <script src='https://cdn.jsdelivr.net/npm/redoc@2.0.0-alpha.17/bundles/redoc.standalone.js'> </script>
  </body>
</html>
`

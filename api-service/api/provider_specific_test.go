package api

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/api-service/constants"
	"net/http"
	"net/http/httptest"
	"runtime"
	"strings"
	"testing"
)

// testing if the correct handler from the correct provider API is being invoked.
// e.g. /providers/provider-aws-aaaaaaaaaaaaaaaaaaaa/images should invoke ImageList() from AWS api.
func TestProviderSpecificAPIRouter_Get(t *testing.T) {
	type args struct {
		providerID               string
		pathFormat               string
		providerSupportOperation bool
		expectedMethodName       string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "aws region list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/regions",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.RegionList",
			},
		},
		{
			name: "aws image list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/images",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.ImageList",
			},
		},
		{
			name: "aws flavor list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/flavors",
				providerSupportOperation: true,
				expectedMethodName:       "mockAWSProviderAPI.FlavorList",
			},
		},
		{
			name: "aws project list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/projects",
				providerSupportOperation: false, // aws does not support this operation
				expectedMethodName:       "",
			},
		},
		{
			name: "aws app cred list",
			args: args{
				providerID:               "provider-aws-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/applicationCredentials",
				providerSupportOperation: false, // aws does not support this operation
				expectedMethodName:       "",
			},
		},
		{
			name: "openstack region list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/regions",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.RegionList",
			},
		},
		{
			name: "openstack image list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/images",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ImageList",
			},
		},
		{
			name: "openstack flavor list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/flavors",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.FlavorList",
			},
		},
		{
			name: "openstack project list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/projects",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ProjectList",
			},
		},
		{
			name: "openstack auth test",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/authenticationTest",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.AuthenticationTest",
			},
		},
		{
			name: "openstack app cred list",
			args: args{
				providerID:               "provider-openstack-aaaaaaaaaaaaaaaaaaaa",
				pathFormat:               "/providers/%s/applicationCredentials",
				providerSupportOperation: true,
				expectedMethodName:       "mockOpenStackAPI.ApplicationCredentialList",
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockAWS := &mockAWSProviderAPI{}
			mockOS := &mockOpenStackAPI{}

			path := fmt.Sprintf(tt.args.pathFormat, tt.args.providerID)
			request := httptest.NewRequest(http.MethodGet, path, nil)
			request.Header.Set(constants.RequestHeaderCacaoUser, "test_username")
			request.Header.Set(constants.RequestHeaderCacaoEmulator, "")
			recorder := createRecorderForProviderSpecificAPI(mockOS, mockAWS, request)

			if tt.args.providerSupportOperation {
				// the mock method from below should be called (e.g. mockAWSProviderAPI.RegionList)
				assert.Equal(t, 200, recorder.Code)
				assert.Equal(t, tt.args.expectedMethodName, recorder.Body.String())
			} else {
				assert.Equal(t, http.StatusNotImplemented, recorder.Code)
				assert.Equal(t, "application/json", recorder.Header().Get("Content-Type"))
			}
		})
	}
}

func createRecorderForProviderSpecificAPI(osAPI OpenStackProviderAPI, awsAPI AWSProviderAPI, r *http.Request) *httptest.ResponseRecorder {
	router := mux.NewRouter()
	ProviderSpecificAPIRouter(osAPI, awsAPI, router)
	recorder := httptest.NewRecorder()
	router.ServeHTTP(recorder, r)
	return recorder
}

type mockAWSProviderAPI struct{}

func (m mockAWSProviderAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockAWSProviderAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

type mockOpenStackAPI struct{}

func (m mockOpenStackAPI) CreateApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ApplicationCredentialList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) DeleteApplicationCredential(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) AuthenticationTest(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) ProjectList(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

func (m mockOpenStackAPI) GetProject(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(currentMethodName()))
}

// this struct is for testing currentMethodName()
type Foo struct{}

func (f Foo) Bar() string {
	return currentMethodName()
}

func Test_currentMethodName(t *testing.T) {
	var foo Foo
	assert.Equal(t, "Foo.Bar", foo.Bar())
}

// currentMethodName returns the method name of the caller, e.g. "mockOpenStackAPI.GetProject"
func currentMethodName() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	splits := strings.Split(frame.Function, ".")
	return strings.Join(splits[len(splits)-2:], ".")
}

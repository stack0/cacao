package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
)

// HealthCheckAPIRouter adds a health check endpoint, the endpoint is used to check if api service is health (still responding to request).
func HealthCheckAPIRouter(router *mux.Router) {
	router.HandleFunc("/health-check", healthCheck).Methods(http.MethodGet, http.MethodHead)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if r.Method == http.MethodGet {
		// TODO may consider return some useful metric information here.
		// Note that this endpoint is public, no auth required, so any information published here will be open to anyone on the internet.
		_, err := w.Write([]byte("{}"))
		if err != nil {
			log.WithField("function", "healthCheck").WithError(err).Error("fail to write response body")
		}
	}
	log.Info("/health-check")
}

// Package api is used to create routing and handler functions providing an API
// gateway for interacting with Cacao services over HTTP
package api

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
)

// JSONError will create and write a nice JSON error response
// TODO this function duplicates the ones in utils package, consider merging them
func JSONError(w http.ResponseWriter, msg string, code int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, code, msg)))
}

// writeError is a utility function for logging and sending back an error
// to the client.
// TODO this function duplicates the ones in utils package, consider merging them
func writeError(w http.ResponseWriter, code int, err error) {
	log.WithError(err).Error()
	http.Error(w, fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, code, err.Error()), code)
}

// writeJSON is a utility function for responding with JSON to the caller.
// TODO this function duplicates the ones in utils package, consider merging them
func writeJSON(w http.ResponseWriter, data interface{}) {
	var err error
	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(data); err != nil {
		writeError(w, http.StatusInternalServerError, err)
	}
}

// TODO this function duplicates the ones in utils package, consider merging them
func logAndWriteError(logger *log.Entry, w http.ResponseWriter, statusCode int, err error) {
	logger.WithError(err).Error(statusCode)
	writeError(w, statusCode, err)
}

package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/storage"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// storageAPI is a single instance of the storage API implementation.
type storageAPI struct {
	client storage.Client
}

// StorageAPIRouter creates routes for operations related to storage.
func StorageAPIRouter(client storage.Client, router *mux.Router) {
	d := &storageAPI{
		client: client,
	}

	router.HandleFunc("/storage", d.listStorageDeployments).Methods("GET")
	router.HandleFunc("/storage", d.addStorageDeployment).Methods("POST")
	router.HandleFunc("/storage/{storage_id}", d.getStorageDeployment).Methods("GET")
	router.HandleFunc("/storage/{storage_id}", d.updateStorageDeployment).Methods("PATCH")
	router.HandleFunc("/storage/{storage_id}", d.deleteStorageDeployment).Methods("DELETE")
	router.HandleFunc("/storage/{storage_id}/action", d.performStorageDeploymentAction).Methods("POST")
}

// getClientSessions obtains the deployments and templates client sessions for a request. If either session can't
// be created, it writes an error response and returns nil for both return values.
func (s *storageAPI) getClientSession(
	logger *log.Entry, w http.ResponseWriter, r *http.Request,
) storage.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := s.client.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the storage client session")
		utils.JSONError(w, r, "unexpected error", err.Error(), http.StatusInternalServerError)
		return nil
	}
	return session
}

// JSONError produces a JSON response body and returns the appropriate return code for the given error.
func (s *storageAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	var status int
	switch err.(type) {
	case *service.CacaoInvalidParameterError, *service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// unmarshalRequest reads the request body and unmarshals it.
func (s *storageAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := service.NewCacaoCommunicationError(errorMessage)
		s.JSONError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := service.NewCacaoMarshalError(errorMessage)
		s.JSONError(logger, w, r, cerr)
		return err
	}

	return nil
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (s *storageAPI) extractID(
	logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder, idType string,
) (cacaocommon.ID, error) {
	deploymentID := cacaocommon.ID(mux.Vars(r)[placeholder])
	if !deploymentID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, deploymentID)
		cerr := service.NewCacaoInvalidParameterError(errorMessage)
		s.JSONError(logger, w, r, cerr)
		return cacaocommon.ID(""), cerr
	}
	return deploymentID, nil
}

// listStorageDeployments is the handler for the GET /storage endpoint.
func (s *storageAPI) listStorageDeployments(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listStorageDeployments",
	})
	logger.Info("api.listStorageDeployments(): start")

	// Create the client session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// List the deployments.
	deployments, err := session.ListStorageDeployments()
	if err != nil {
		logger.WithField("error", err).Error("unable to list storage deployments")
		utils.JSONError(w, r, "unexpected error", err.Error(), http.StatusInternalServerError)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, deployments, http.StatusOK)
}

// addStorageDeployment is the handler for the PUT /storage endpoint.
func (s *storageAPI) addStorageDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "addStorageDeployment",
	})
	logger.Info("api.addStorageDeployment(): start")

	// Unmarshal the request body.
	var incomingRequest hm.Deployment
	err := s.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the client session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Validate the request.
	err = session.ValidateStorageDeploymentCreationRequest(&incomingRequest)
	if err != nil {
		errorMessage := "storage deployment creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Create the deployment.
	deploymentID, err := session.AddStorageDeployment(&incomingRequest)
	if err != nil {
		errorMessage := "storage deployment creation failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Create the deployment run.
	runRequest := &hm.DeploymentRun{Parameters: incomingRequest.Parameters}
	runID, err := session.RunStorageDeployment(deploymentID, runRequest)
	if err != nil {
		logger.WithField("error", err).Error("storage deployment run failed")
		_, _, deletionError := session.DeleteStorageDeployment(deploymentID)
		if deletionError != nil {
			logger.WithField("error", deletionError).Error("storage deployment deletion failed after run error")
		}
		s.JSONError(logger, w, r, err)
		return
	}
	logger.WithField("run_id", runID).Info("storage deployment run created")

	// Format the response body.
	body := utils.NewAcceptedResponse(deploymentID.String(), "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// getStorageDeployment is the handler for the GET /storage/{storage_id} endpoint.
func (s *storageAPI) getStorageDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "addStorageDeployment",
	})
	logger.Info("api.addStorageDeployment(): start")

	// Extract the storage deployment ID.
	deploymentID, err := s.extractID(logger, w, r, "storage_id", "storage")
	if err != nil {
		return
	}

	// Create the client session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Look up the storage deployment.
	deployment, err := session.GetStorageDeployment(deploymentID)
	if err != nil {
		errorMessage := fmt.Sprintf("error looking up storage deployment : %s", deploymentID)
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, deployment, http.StatusOK)
}

// updateStorageDeployment is the handler for the PATCH /storage/{storage_id} endpoint.
func (s *storageAPI) updateStorageDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateStorageDeployment",
	})
	logger.Info("api.updateStorageDeployment(): start")

	// Extract the storage deployment ID.
	deploymentID, err := s.extractID(logger, w, r, "storage_id", "storage")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest hm.DeploymentUpdate
	err = s.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Validate the update request.
	err = session.ValidateStorageDeploymentUpdateRequest(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "storage deployment update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateStorageDeployment(deploymentID, &incomingRequest)
	if err != nil {
		errorMessage := "storage deployment update submission failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid.String())
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deleteStorageDeployment is the handler for the DELETE /storage/{storage_id} endpoint.
func (s *storageAPI) deleteStorageDeployment(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteStorageDeployment",
	})
	logger.Info("api.deleteStorageDeployment(): start")

	// Extract the storage deployment ID.
	deploymentID, err := s.extractID(logger, w, r, "storage_id", "storage")
	if err != nil {
		return
	}

	// Create the session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Validate the deletion request.
	err = session.ValidateStorageDeploymentDeletionRequest(deploymentID)
	if err != nil {
		errorMessage := "storage deployment deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tid, deleted, err := session.DeleteStorageDeployment(deploymentID)
	if err != nil {
		errorMesage := "storage deployment deletion request submission failed"
		logger.WithField("error", err).Error(errorMesage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(deploymentID.String(), tid.String())
	if deleted {
		utils.ReturnStatus(w, body, http.StatusOK)
	} else {
		utils.ReturnStatus(w, body, http.StatusAccepted)
	}
}

// performStorageDeploymentAction is the handler for the POST /storage/{storage_id}/action endpoint.
func (s *storageAPI) performStorageDeploymentAction(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "performStorageDeploymentAction",
	})
	logger.Info("api.performStorageDeploymentAction(): start")

	// Extract the storage deployment ID.
	storageID, err := s.extractID(logger, w, r, "storage_id", "storage")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var actionRequest hm.StorageActionRequest
	err = s.unmarshalRequest(logger, w, r, &actionRequest)
	if err != nil {
		return
	}

	// Create the session.
	session := s.getClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Perform the action.
	runID, err := session.PerformStorageDeploymentAction(storageID, &actionRequest)
	if err != nil {
		errorMessage := "storage deployment action failed"
		logger.WithField("error", err).Error(errorMessage)
		s.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(runID.String(), "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

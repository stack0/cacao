package api

import (
	"fmt"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao/api-service/clients/credentials"
	"io/ioutil"
	"net/http"
	"time"
	"unicode"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

type credentialAPI struct {
	client credentials.Client
}

// CredentialAPIRouter creates routes for credential-related operations such as creating and
// updating credentials and logging in
func CredentialAPIRouter(client credentials.Client, router *mux.Router) {
	api := &credentialAPI{client: client}
	router.HandleFunc("/credentials", api.listCredentials).Methods("GET")
	router.HandleFunc("/credentials", api.createCredential).Methods("POST")
	router.HandleFunc("/credentials/{credentialname}", api.getCredential).Methods("GET")
	router.HandleFunc("/credentials/{credentialname}", api.patchCredential).Methods("PATCH")
	router.HandleFunc("/credentials/{credentialname}", api.deleteCredential).Methods("DELETE")
	router.HandleFunc("/credentials_types", api.getCredentialTypes).Methods("GET")
}

var credentialTypeList = []string{
	"openstack",
	"github",
	"gitlab",
	"dockerhub",
	"kubernetes",
	"ssh",
	"aws",
	"gcp",
	"azure",
}

func (api credentialAPI) getCredentialTypes(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "credentials_types",
	})
	logger.Info("api.getCredentialTypes(): start")
	utils.ReturnStatus(writer, credentialTypeList, http.StatusOK)
}

// delete credential via dependency mediator
func (api credentialAPI) deleteCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteCredential",
	})
	logger.Info("called")

	id := mux.Vars(request)["credentialname"]

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	natsConfig := config.GlobalConfig.Nats
	natsConfig.ClientID = cacao_common.NewID(config.GlobalConfig.Nats.ClientID).String()

	logger.Trace("creating DependencyMediatorClient")
	svcClient, err := service.NewDependencyMediatorClient(natsConfig, config.GlobalConfig.Stan)
	if err != nil {
		logger.WithError(err).Error("fail to create svc client for dependency mediator")
		utils.JSONError(writer, request, "internal error", err.Error(), http.StatusInternalServerError)
		return
	}

	deletionResult, err := svcClient.DeleteCredential(service.Session{SessionActor: actor, SessionEmulator: emulator}, id)
	if err != nil {
		logger.WithError(err).Error("fail to delete credential with dependency check")
		utils.JSONError(writer, request, "internal error", err.Error(), http.StatusInternalServerError)
		return
	}
	logger.WithField("result", deletionResult).Info("deletion result")

	err = deletionResult.ToError("credential", id)
	if err != nil {
		logger.WithError(err).Error("error from deletion result")
		utils.JSONError(writer, request, "dependency conflict", err.Error(), http.StatusConflict)
		return
	}
	msg := map[string]string{"credential": id, "timestamp": time.Now().String()}
	utils.ReturnStatus(writer, msg, http.StatusOK)
}

// TODO Currently patch credential is just an alias to creating credential, this may change later on.
func (api credentialAPI) patchCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "patchCredential",
	})
	logger.Info("called")

	//TODO special logic here if we are changing the ID of a credential
	logger.Info("calling createCredential")
	api.createCredential(writer, request)
}

func (api credentialAPI) getCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getCredential",
	})
	logger.Info("called")

	id := mux.Vars(request)["credentialname"]

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)
	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	cred, err := session.GetCredential(id)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	utils.ReturnStatus(writer, cred, http.StatusOK)
}

func (api credentialAPI) createCredential(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createCredential",
	})
	logger.Debug("called")

	credential, err := api.parseCreationRequest(logger, request)
	if err != nil {
		utils.JSONError(writer, request, "bad creation request", err.Error(), http.StatusBadRequest)
		return
	}
	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)
	credential.Username = actor // set owner

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create nats credential client")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	result, err := session.CreateCredential(credential)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not create credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	utils.ReturnStatus(writer, result, http.StatusCreated)
}

func (api credentialAPI) parseCreationRequest(logger *log.Entry, request *http.Request) (hm.Credential, error) {
	reqBody, err := ioutil.ReadAll(request.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		return hm.Credential{}, fmt.Errorf("unable to read request body, %w", err)
	}
	var credential hm.Credential
	err = utils.StrictJSONDecode(reqBody, &credential)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("json request body is invalid")
		return hm.Credential{}, fmt.Errorf("json request body is invalid, %w", err)
	}
	err = validateCredential(credential)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("credential is invalid")
		return hm.Credential{}, fmt.Errorf("bad credential, %w", err)
	}
	return credential, nil
}

func (api credentialAPI) listCredentials(writer http.ResponseWriter, request *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listCredentials",
	})
	logger.Debug("called")

	//TODO Admin credential should be able to get all secrets of all credentials?
	actor, emulator, _ := utils.GetCacaoHeaders(request)

	session, err := api.client.Session(actor, emulator)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the deployments microservice client")
		utils.JSONError(writer, request, "unexpected error", err.Error(), http.StatusInternalServerError)
		return
	}

	credList, err := session.ListCredentials()
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not list credentials")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("could not get credential")
		utils.JSONError(writer, request, fmt.Sprintf("unable to get credential: %v", err), err.Error(), http.StatusInternalServerError)
		return
	}

	if len(credList) == 0 {
		writer.Header().Add("Content-Type", "application/json")
		writer.WriteHeader(200)
		writer.Write([]byte("[]"))
		return
	}

	utils.ReturnStatus(writer, credList, http.StatusOK)
}

func validateCredential(cred hm.Credential) error {
	err := validateCredentialID(cred.ID)
	if err != nil {
		return err
	}
	err = validateCredentialType(cred.Type)
	if err != nil {
		return err
	}
	if len(cred.Description) > 300 {
		return fmt.Errorf("description too long")
	}
	return nil
}

func validateCredentialID(credID string) error {
	if credID == "" {
		return fmt.Errorf("credential ID cannot be empty")
	}
	if len(credID) > 80 {
		return fmt.Errorf("credential ID too long")
	}
	for i, r := range credID {
		if r > unicode.MaxASCII {
			// only allow ASCII
			return fmt.Errorf("invalid character in credential ID at index %d", i)
		}
		if r < ' ' || r > '~' {
			// check if it is outside of range of normal ASCII characters (not digit, not letter, not punct, not space)
			return fmt.Errorf("invalid character in credential ID at index %d", i)
		}
	}
	return nil
}

func validateCredentialType(credType string) error {
	allowedType := false
	for _, typeName := range credentialTypeList {
		if credType == typeName {
			allowedType = true
			break
		}
	}
	if !allowedType {
		return fmt.Errorf("unknown credential type")
	}
	return nil
}

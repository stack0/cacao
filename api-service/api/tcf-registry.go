package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/tcfregistry"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// tcfRegistryAPI is a single instance of the tcf registry API implementation.
type tcfRegistryAPI struct {
	tcfRegistryClient tcfregistry.Client
}

// TCFRegistryAPIRouter creates routes for operations related to tcf registry.
func TCFRegistryAPIRouter(tcfRegistryClient tcfregistry.Client, router *mux.Router) {
	tapi := &tcfRegistryAPI{
		tcfRegistryClient: tcfRegistryClient,
	}

	router.HandleFunc("/tcfregistry", tapi.listTCFRegistryEntries).Methods("GET")
	router.HandleFunc("/tcfregistry", tapi.createTCFRegistryEntry).Methods("POST")
	router.HandleFunc("/tcfregistry/{entry_name}", tapi.getTCFRegistryEntry).Methods("GET")
	router.HandleFunc("/tcfregistry/query/{entry_name}", tapi.queryTCFRegistryEntry).Methods("GET")
	router.HandleFunc("/tcfregistry/{entry_name}", tapi.updateTCFRegistryEntry).Methods("PUT")
	router.HandleFunc("/tcfregistry/{entry_name}", tapi.updateTCFRegistryEntryFields).Methods("PATCH")
	router.HandleFunc("/tcfregistry/{entry_name}", tapi.deleteTCFRegistryEntry).Methods("DELETE")
}

func (tapi *tcfRegistryAPI) JSONError(logger *log.Entry, w http.ResponseWriter, r *http.Request, err error) {
	logger.Error(err)

	var status int
	switch err.(type) {
	case *cacao_common_service.CacaoInvalidParameterError, *cacao_common_service.CacaoMarshalError:
		status = http.StatusBadRequest
	case *cacao_common_service.CacaoNotImplementedError:
		status = http.StatusNotImplemented
	case *cacao_common_service.CacaoNotFoundError:
		status = http.StatusNotFound
	case *cacao_common_service.CacaoTimeoutError:
		status = http.StatusRequestTimeout
	case *cacao_common_service.CacaoAlreadyExistError:
		status = http.StatusConflict
	case *cacao_common_service.CacaoUnauthorizedError:
		status = http.StatusUnauthorized
	default:
		status = http.StatusInternalServerError
	}

	if cerr, ok := err.(cacao_common_service.CacaoError); ok {
		utils.JSONError(w, r, string(cerr.StandardError()), cerr.ContextualError(), status)
	} else {
		utils.JSONError(w, r, "internal service error", err.Error(), status)
	}
}

// unmarshalRequest reads the request body and unmarshals it.
func (tapi *tcfRegistryAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return err
	}

	return nil
}

// unmarshalRequestAndGetFieldNames reads the request body and unmarshals it. Also returns field names.
func (tapi *tcfRegistryAPI) unmarshalRequestAndGetFieldNames(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) ([]string, error) {
	// Read the request body.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	requestMap := map[string]interface{}{}
	// Unmarshal the request body to Map.
	err = json.Unmarshal(requestBody, &requestMap)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return nil, err
	}

	fields := []string{}
	for k := range requestMap {
		fields = append(fields, k)
	}

	return fields, nil
}

// getTCFRegistryClientSession obtains the tcf registry client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (tapi *tcfRegistryAPI) getTCFRegistryClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) tcfregistry.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := tapi.tcfRegistryClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the tcf registry microservice client")
		tapi.JSONError(logger, w, r, err)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (tapi *tcfRegistryAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string) (string, error) {
	tcfRegistryEntryName := mux.Vars(r)[placeholder]
	if len(tcfRegistryEntryName) == 0 {
		errorMessage := fmt.Sprintf("empty name: %s", tcfRegistryEntryName)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return "", cerr
	}
	return tcfRegistryEntryName, nil
}

// listTCFRegistryEntries implements the GET /tcfregistry endpoint.
func (tapi *tcfRegistryAPI) listTCFRegistryEntries(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listTCFRegistryEntries",
	})
	logger.Info("api.listTCFRegistryEntries(): start")
	var err error

	// Create the tcf registry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of tcf registry entries
	tcfRegistryEntries, err := session.ListTCFRegistryEntries()
	if err != nil {
		errorMessage := "error listing tcf registry entries"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Guard against ListTCFRegistryEntries returning nil.
	if tcfRegistryEntries == nil {
		tcfRegistryEntries = make([]cacao_common_http.TCFRegistryEntry, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, tcfRegistryEntries, http.StatusOK)
}

// getTCFRegistryEntry implements the GET /tcfregistry/{entry_name} endpoint.
// getTCFRegistryEntry returns the tcf registry entry with the specified name if it exists.
func (tapi *tcfRegistryAPI) getTCFRegistryEntry(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getTCFRegistryEntryByName",
	})
	logger.Info("api.getTCFRegistryEntryByName(): start")
	var err error

	// Extract the entry name from the request and validate it.
	entryName, err := tapi.extractID(logger, w, r, "entry_name")
	if err != nil {
		return
	}

	// Create the tcf registry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the tcf registry tcfRegistryEntry.
	tcfRegistryEntry, err := session.GetTCFRegistryEntry(entryName)
	if err != nil {
		errorMessage := "error obtaining tcf registry entry information"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	if len(tcfRegistryEntry.Name) == 0 {
		errorMessage := fmt.Sprintf("tcf registry entry not found: %s", entryName)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, tcfRegistryEntry, http.StatusOK)
}

// queryTCFRegistryEntry implements the GET /tcfregistry/query/{entry_name} endpoint.
// queryTCFRegistryEntry returns the tcf registry query result with the specified entry name if it exists.
func (tapi *tcfRegistryAPI) queryTCFRegistryEntry(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "queryTCFRegistryEntryByName",
	})
	logger.Info("api.queryTCFRegistryEntryByName(): start")
	var err error

	// Extract the entry name from the request and validate it.
	entryName, err := tapi.extractID(logger, w, r, "entry_name")
	if err != nil {
		return
	}

	// Extract QueryParams
	queryParams := map[string]string{}
	queryValues := r.URL.Query()
	for key, qvalues := range queryValues {
		if len(qvalues) > 0 {
			queryParams[key] = qvalues[0]
		}
	}

	// Extract auth info
	authToken := r.Header.Get("Authorization")
	queryParams["auth_token"] = authToken

	// Create the tcf registry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Query to the tcf registry and returns tcfRegistryQueryResult.
	tcfRegistryQueryResult, err := session.QueryTCFRegistryEntry(entryName, queryParams)
	if err != nil {
		errorMessage := "error querying to tcf registry entry"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	if len(tcfRegistryQueryResult.EntryName) == 0 {
		errorMessage := fmt.Sprintf("tcf registry query result not found: %s", entryName)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		tapi.JSONError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, tcfRegistryQueryResult, http.StatusOK)
}

// createTCFRegistryEntry implements the POST /tcfregistry endpoint.
func (tapi *tcfRegistryAPI) createTCFRegistryEntry(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createTCFRegistryEntry",
	})
	logger.Info("api.createTCFRegistryEntry(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TCFRegistryEntry
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the tcf registry session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the tcf registry entry creation request.
	err = session.ValidateTCFRegistryEntryCreationRequest(incomingRequest)
	if err != nil {
		errorMessage := "tcf registry entry creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	tid, err := session.CreateTCFRegistryEntry(incomingRequest)
	if err != nil {
		errorMessage := "tcf registry entry creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTCFRegistryEntry implements the PUT /tcfregistry/{entry_name} endpoint.
// updateTCFRegistryEntry updates (replaces) a tcf registry entry information.
func (tapi *tcfRegistryAPI) updateTCFRegistryEntry(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTCFRegistryEntry",
	})
	logger.Info("api.updateTCFRegistryEntry(): start")

	// Extract the entry name from the request and validate it.
	entryName, err := tapi.extractID(logger, w, r, "entry_name")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TCFRegistryEntry
	err = tapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the tcf registry entry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the tcf registry entry update request.
	err = session.ValidateTCFRegistryEntryUpdateRequest(entryName, incomingRequest)
	if err != nil {
		errorMessage := "tcf registry entry update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateTCFRegistryEntry(entryName, incomingRequest)
	if err != nil {
		errorMessage := "tcf registry entry update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// updateTCFRegistryEntryFields implements the PATCH /tcfregistry/{entry_name} endpoint.
// updateTCFRegistryEntryFields updates fields of a tcf registry entry information.
func (tapi *tcfRegistryAPI) updateTCFRegistryEntryFields(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "updateTCFRegistryEntryFields",
	})
	logger.Info("api.updateTCFRegistryEntryFields(): start")

	// Extract the entry name from the request and validate it.
	entryName, err := tapi.extractID(logger, w, r, "entry_name")
	if err != nil {
		return
	}

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.TCFRegistryEntry
	updateFields, err := tapi.unmarshalRequestAndGetFieldNames(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the tcf registry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the tcf registry entry update request.
	err = session.ValidateTCFRegistryEntryUpdateFieldsRequest(entryName, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "tcf registry entry update request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the update request.
	tid, err := session.UpdateTCFRegistryEntryFields(entryName, incomingRequest, updateFields)
	if err != nil {
		errorMessage := "tcf registry entry update request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse("", tid)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deleteTCFRegistryEntry implements the DELETE /tcfregistry/{entry_name} endpoint.
func (tapi *tcfRegistryAPI) deleteTCFRegistryEntry(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deleteTCFRegistryEntry",
	})
	logger.Info("api.deleteTCFRegistryEntry(): start")

	// Extract the entry name from the request and validate it.
	entryName, err := tapi.extractID(logger, w, r, "entry_name")
	if err != nil {
		return
	}

	// Create the tcf registry client session.
	session := tapi.getTCFRegistryClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the tcf registry entry deletion request.
	err = session.ValidateTCFRegistryEntryDeletionRequest(entryName)
	if err != nil {
		errorMessage := "tcf registry entry deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	tid, err := session.DeleteTCFRegistryEntry(entryName)
	if err != nil {
		errorMessage := "tcf registry entry deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		tapi.JSONError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(entryName, tid)
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

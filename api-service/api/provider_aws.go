package api

import (
	"context"
	"errors"
	"gitlab.com/cyverse/cacao/api-service/clients/awsprovider"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// AWSProviderAPI ...
type AWSProviderAPI interface {
	RegionList(w http.ResponseWriter, r *http.Request)
	ImageList(w http.ResponseWriter, r *http.Request)
	GetImage(w http.ResponseWriter, r *http.Request)
	FlavorList(w http.ResponseWriter, r *http.Request)
	GetFlavor(w http.ResponseWriter, r *http.Request)
}

// awsProviderAPI implements the HTTP API for the OpenStack Provider stuff.
type awsProviderAPI struct {
	client awsprovider.Client
}

// NewAWSProviderAPI ...
func NewAWSProviderAPI(client awsprovider.Client) AWSProviderAPI {
	return &awsProviderAPI{client}
}

// RegionList is an HTTP handler for listing all the available regions available
func (p *awsProviderAPI) RegionList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "awsProviderAPI.RegionList",
	})
	logger.Info("request received")

	providerID, _, credential, err := p.extractSharedFromRequest(r, false)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	regionList, err := session.RegionList(providerID, credential)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	if regionList != nil {
		logger.WithField("length", len(regionList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, regionList)
}

// ImageList is an HTTP handler listing all the available images available
// to the user.
func (p *awsProviderAPI) ImageList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "awsProviderAPI.ImageList",
	})
	logger.Info("request received")

	providerID, region, credential, err := p.extractSharedFromRequest(r, true)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"region":     region,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	imageList, err := session.ImageList(providerID, region, credential)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	if imageList != nil {
		logger.WithField("length", len(imageList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, imageList)
}

// GetImage is an HTTP handler for getting a single image description.
func (p *awsProviderAPI) GetImage(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "awsProviderAPI.GetImage",
	})
	logger.Info("request received")

	providerID, region, credential, err := p.extractSharedFromRequest(r, true)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	imageID, err := p.extractImageIDFromRequest(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"region":     region,
		"credential": credential(),
		"image":      imageID,
	})
	session1, cancel, err := p.newSession(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	image, err := session1.GetImage(providerID, region, credential, imageID)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, image)
}

// FlavorList is an HTTP handler for listing all of the available flavors.
func (p *awsProviderAPI) FlavorList(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "awsProviderAPI.FlavorList",
	})
	logger.Info("request received")

	providerID, region, credential, err := p.extractSharedFromRequest(r, true)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"region":     region,
		"credential": credential(),
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	flavorList, err := session.FlavorList(providerID, region, credential)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	if flavorList != nil {
		logger.WithField("length", len(flavorList)).Info("responded")
	} else {
		logger.WithField("length", nil).Info("responded")
	}
	writeJSON(w, flavorList)
}

// GetFlavor is an HTTP handler for getting a single flavor.
func (p *awsProviderAPI) GetFlavor(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "awsProviderAPI.GetFlavor",
	})
	logger.Info("request received")

	providerID, region, credential, err := p.extractSharedFromRequest(r, true)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	flavorID, err := p.extractFlavorIDFromRequest(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusBadRequest, err)
		return
	}
	logger = logger.WithFields(log.Fields{
		"provider":   providerID,
		"region":     region,
		"credential": credential(),
		"flavor":     flavorID,
	})
	session, cancel, err := p.newSession(r)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	defer cancel()
	flavor, err := session.GetFlavor(providerID, region, credential, flavorID)
	if err != nil {
		logAndWriteError(logger, w, http.StatusInternalServerError, err)
		return
	}
	logger.Info("responded")
	writeJSON(w, flavor)
}

// newSession creates a new AWS session.
func (p awsProviderAPI) newSession(r *http.Request) (awsprovider.Session, context.CancelFunc, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*8)

	actor, emulator, _ := utils.GetCacaoHeaders(r)
	session, err := p.client.Session(ctx, actor, emulator)
	return session, cancel, err
}

// extractSharedFromRequest extract values from an HTTP request that is shared by all requests to provider openstack service
func (p awsProviderAPI) extractSharedFromRequest(r *http.Request, regionRequired bool) (providerID string, region string, credential providers.CredentialOption, err error) {
	providerID, ok := mux.Vars(r)["providerid"]
	if !ok {
		err = errors.New("missing providerid variable")
		return "", "", nil, err
	}
	if regionRequired {
		region = r.URL.Query().Get("region")
		if region == "" {
			err = errors.New("missing region query parameter")
			return "", "", nil, err
		}
	}

	return providerID, region, p.extractCredentialOptionFromRequest(r), nil
}

// extractApplicationCredentialIDFromRequest extracts the application credential ID from an HTTP request.
func (p awsProviderAPI) extractApplicationCredentialIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["applicationcredentialid"]
	if !ok {
		err := errors.New("missing applicationcredentialid variable")
		return "", err
	}
	return id, nil
}

// extractImageIDFromRequest extracts the image ID from an HTTP request.
func (p awsProviderAPI) extractImageIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["imageid"]
	if !ok {
		err := errors.New("missing imageid variable")
		return "", err
	}
	return id, nil
}

func (p awsProviderAPI) extractFlavorIDFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["flavorid"]
	if !ok {
		err := errors.New("missing flavorid variable")
		return "", err
	}
	return id, nil
}

// AWS only will support one credential option for now, which is With Credential ID
func (p *awsProviderAPI) extractCredentialOptionFromRequest(r *http.Request) providers.CredentialOption {
	return providers.WithCredentialID(r.URL.Query().Get("credential"))
}

package api

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/api-service/authentication"
	"gitlab.com/cyverse/cacao/api-service/config"
	"gitlab.com/cyverse/cacao/api-service/constants"
)

// InitAuthenticationDriver initializes the authentication driver
func InitAuthenticationDriver(c *config.Config) error {
	log.Trace("InitAuthentication(): start with driver = " + c.AuthDriver)
	var err error = nil

	switch c.AuthDriver {
	case constants.AuthDriverCILogon:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("CILogon client ID and secret should not be empty")
		}

		Authenticator, err = authentication.NewCILogonOAuth2Driver(*c)
	case constants.AuthDriverGlobus:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("Globus client ID and secret should not be empty")
		}

		Authenticator, err = authentication.NewGlobusOAuth2Driver(*c)
	case constants.AuthDriverKeycloak:
		if c.OAuth2ClientID == "" || c.OAuth2ClientSecret == "" {
			log.Panic("Keycloak ID and client secret should not be empty")
		}

		Authenticator, err = authentication.NewKeycloakAuth(*c)
	default:
		Authenticator = authentication.NewSimpleTokenAuth(*c)
	}

	return err
}

// InitNATSConnectionVars is used to define the NATS connection variables
func InitNATSConnectionVars(c *config.Config) {
	if c.Nats.URL == "" {
		c.Nats.URL = messaging.DefaultNatsURL
	}
	if c.Nats.ClientID == "" {
		c.Nats.ClientID = constants.DefaultNatsClientID
	}

}

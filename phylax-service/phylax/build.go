package phylax

import (
	"encoding/json"
	"log"
)

// BuildStart persists a WorkflowDefinition Build in the database
func BuildStart(reqBytes []byte, natsInfo map[string]string) {
	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		log.Printf("Error reading request to create Runner: %v\n", err)
		return
	}

	log.Printf("Storing new Build for %s\n", request.GetBuildID())
	build, err := NewBuild(request)
	if err != nil {
		log.Printf("Error creating new Build: %v\n", err)
		return
	}
	db.Save(build)
}

// BuildJob adds an Informer to watch a BuildJob on the user cluster
func BuildJob(reqBytes []byte, natsInfo map[string]string) {
	var request Request
	err := json.Unmarshal(reqBytes, &request)
	if err != nil {
		log.Printf("Error reading request to create Runner: %v\n", err)
		return
	}

	createBuildInformer(request, natsInfo)
}

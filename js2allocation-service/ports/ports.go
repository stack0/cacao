package ports

import (
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.JS2AllocationChannelRequest)
	Start()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	AsyncPort
	GetUser(actor string, emulator string, nocache bool) (types.JS2User, error)
	ListProjects(actor string, emulator string, nocache bool) ([]types.JS2Project, error)
}

// PersistentStoragePort is an interface for for Perstistent Storage.
type PersistentStoragePort interface {
	Port
	GetUser(user string) (types.JS2User, error)
	InsertUser(user string, js2User types.JS2User) error
	DeleteUser(user string) error
	DeleteAndInsertUser(user string, js2User types.JS2User) error

	ListProjects(user string) ([]types.JS2Project, error)
	InsertAllProjects(user string, js2Projects []types.JS2Project) error
	DeleteAllProjects(user string) error
	DeleteAndInsertAllProjects(user string, js2Projects []types.JS2Project) error
}

// JS2APIPort is an interface for a JS2API port.
type JS2APIPort interface {
	Port
	GetUser(user string) (types.JS2User, error)
	ListProjects(user string) ([]types.JS2Project, error)
}

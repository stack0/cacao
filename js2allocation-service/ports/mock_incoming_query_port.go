package ports

import (
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// GetUserHandler is a handler for User Get query
type GetUserHandler func(actor string, emulator string, nocache bool) (types.JS2User, error)

// ListProjectsHandler is a handler for Project List query
type ListProjectsHandler func(actor string, emulator string, nocache bool) ([]types.JS2Project, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config              *types.Config
	GetUserHandler      GetUserHandler
	ListProjectsHandler ListProjectsHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.JS2AllocationChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

// SetGetUserHandler sets a handler for User Get query
func (port *MockIncomingQueryPort) SetGetUserHandler(getUserHandler GetUserHandler) {
	port.GetUserHandler = getUserHandler
}

// SetListProjectsHandler sets a handler for Project List query
func (port *MockIncomingQueryPort) SetListProjectsHandler(listProjectsHandler ListProjectsHandler) {
	port.ListProjectsHandler = listProjectsHandler
}

// GetUser returns user
func (port *MockIncomingQueryPort) GetUser(actor string, emulator string, nocache bool) (types.JS2User, error) {
	return port.GetUserHandler(actor, emulator, nocache)
}

// ListProjects lists projects
func (port *MockIncomingQueryPort) ListProjects(actor string, emulator string, nocache bool) ([]types.JS2Project, error) {
	return port.ListProjectsHandler(actor, emulator, nocache)
}

package adapters

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

const (
	// TASApiURLGetUserPrefix is a URL prefix for getting TAS username
	TASApiURLGetUserPrefix string = "https://tas.tacc.utexas.edu/api/v1/users/xsede"
	// TASApiURLGetProjectsPrefix is a URL prefix for getting JS2 Projects
	TASApiURLGetProjectsPrefix string = "https://tas.tacc.utexas.edu/api/v1/projects/username"
)

// TASAdapter implements JS2APIPort
// Deprecated: this is for TAS API for JS1
type TASAdapter struct {
	Config *types.Config
}

// Init initialize TAS adapter
func (adapter *TASAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.Init",
	})

	logger.Info("Initializing Git Adapter")

	adapter.Config = config
}

// Finalize finalizes TASadapter
func (adapter *TASAdapter) Finalize() {
}

// GetUser gets JS2 User
func (adapter *TASAdapter) GetUser(user string) (types.JS2User, error) {
	js2user, err := adapter.getUser(user)
	if err != nil {
		return types.JS2User{}, err
	}

	return js2user, nil
}

// ListProjects lists all JS2 Projects and allocations
func (adapter *TASAdapter) ListProjects(user string) ([]types.JS2Project, error) {
	js2user, err := adapter.getUser(user)
	if err != nil {
		return nil, err
	}

	js2projects, err := adapter.listProjects(js2user)
	if err != nil {
		return nil, err
	}

	return js2projects, nil
}

// getUser returns JS2 User
func (adapter *TASAdapter) getUser(user string) (types.JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.getUser",
	})

	logger.Infof("Getting TAS username for user %s", user)

	// if username is email format, take only username part.
	userNoEmail := strings.Split(user, "@")[0]

	requestURL := fmt.Sprintf("%s/%s", TASApiURLGetUserPrefix, userNoEmail)

	logger.Infof("Request to TAS - %s", requestURL)

	req, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access TAS API at url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	req.SetBasicAuth(adapter.Config.TASApiUsername, adapter.Config.TASApiPassword)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access TAS API at url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		errorMessage := fmt.Sprintf("unexpected response from TAS API for url '%s' - status: %s", requestURL, response.Status)
		logger.Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	jsonBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to read data from TAS API result for url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	tasUserResponse := types.TASResponse{}
	err = json.Unmarshal(jsonBytes, &tasUserResponse)
	if err != nil {
		errorMessage := "failed to unmarshal TAS API result in JSON into TASResponse"
		logger.WithError(err).Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	tasUsername := ""
	if strings.ToLower(tasUserResponse.Status) == "success" {
		if tasUsernameResponse, ok := tasUserResponse.Result.(string); ok {
			tasUsername = tasUsernameResponse
		} else {
			errorMessage := "failed to convert TAS API Result field to string"
			logger.Error(errorMessage)
			return types.JS2User{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
		}
	} else {
		errorMessage := fmt.Sprintf("failed to get TAS username for user %s", user)
		logger.Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if len(tasUsername) == 0 {
		errorMessage := fmt.Sprintf("failed to get TAS username for user %s", user)
		logger.Error(errorMessage)
		return types.JS2User{}, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	userID := fmt.Sprintf("js2user-%s", user)
	retrievedTime := time.Now().UTC()
	return types.JS2User{
		ID:           common.ID(userID),
		Owner:        user,
		TACCUsername: tasUsername,
		RetrivedAt:   retrievedTime,
	}, nil
}

// listProjects returns JS2 Projects
func (adapter *TASAdapter) listProjects(user types.JS2User) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.listProjects",
	})

	logger.Infof("Getting JS2 Projects for tas user %s", user.TACCUsername)

	requestURL := fmt.Sprintf("%s/%s", TASApiURLGetProjectsPrefix, user.TACCUsername)

	logger.Infof("Request to TAS - %s", requestURL)

	req, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access TAS API at url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	req.SetBasicAuth(adapter.Config.TASApiUsername, adapter.Config.TASApiPassword)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to access TAS API at url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		errorMessage := fmt.Sprintf("unexpected response from TAS API for url '%s' - status: %s", requestURL, response.Status)
		logger.Error(errorMessage)
		return nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	jsonBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to read data from TAS API result for url '%s'", requestURL)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoCommunicationError(errorMessage)
	}

	tasProjectResponse := types.TASResponse{}
	err = json.Unmarshal(jsonBytes, &tasProjectResponse)
	if err != nil {
		errorMessage := "failed to unmarshal TAS API result in JSON into TASResponse"
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	js2ProjectMap := map[string]types.JS2Project{}

	retrievedTime := time.Now().UTC()
	if strings.ToLower(tasProjectResponse.Status) == "success" {
		if tasProjectsArray, ok := tasProjectResponse.Result.([]interface{}); ok {
			for _, tasProject := range tasProjectsArray {
				js2Project, err := adapter.convertTasProjectToJS2Project(user.Owner, tasProject)
				if err != nil {
					errorMessage := "failed to convert TAS Project to JS2 Project"
					logger.WithError(err).Error(errorMessage)
					return nil, err
				}

				// update retrieved time
				js2Project.RetrivedAt = retrievedTime

				// add to existing or add new
				if existingJS2Project, exist := js2ProjectMap[string(js2Project.ID)]; exist {
					// append
					existingJS2Project.Allocations = append(existingJS2Project.Allocations, js2Project.Allocations...)
					js2ProjectMap[string(js2Project.ID)] = existingJS2Project
				} else {
					// add new
					js2ProjectMap[string(js2Project.ID)] = js2Project
				}
			}
		} else {
			errorMessage := "failed to convert TAS API Result field to project array"
			logger.Error(errorMessage)
			return nil, cacao_common_service.NewCacaoMarshalError(errorMessage)
		}
	} else {
		errorMessage := fmt.Sprintf("failed to get JS2 Projects for TAS username %s", user.TACCUsername)
		logger.Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	// conv map to array
	js2Projects := []types.JS2Project{}
	for _, js2project := range js2ProjectMap {
		if len(js2project.Allocations) > 0 {
			js2Projects = append(js2Projects, js2project)
		}
	}

	return js2Projects, nil
}

// convertTasProjectToJS2Project converts TAS Project response into JS2 Project
func (adapter *TASAdapter) convertTasProjectToJS2Project(user string, tasProject interface{}) (types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.convertTasProjectToJS2Project",
	})

	// convert to map
	tasProjectMap, ok := tasProject.(map[string]interface{})
	if !ok {
		errorMessage := "failed to convert TAS Project into map"
		logger.Error(errorMessage)
		return types.JS2Project{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	projectIDInt, ok := tasProjectMap["id"].(float64)
	if !ok {
		errorMessage := "failed to access 'id' field in a TAS Project"
		logger.Error(errorMessage)
		return types.JS2Project{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	projectID := fmt.Sprintf("js2project-%s-%d", user, int64(projectIDInt))

	tasPI, ok := tasProjectMap["pi"].(map[string]interface{})
	if !ok {
		errorMessage := "failed to access 'pi' field in a TAS Project"
		logger.Error(errorMessage)
		return types.JS2Project{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	pi := types.JS2PI{
		Username:  tasPI["username"].(string),
		FirstName: tasPI["firstName"].(string),
		LastName:  tasPI["lastName"].(string),
	}

	tasAllocations, ok := tasProjectMap["allocations"].([]interface{})
	if !ok {
		errorMessage := "failed to access 'allocations' field in a TAS Project"
		logger.Error(errorMessage)
		return types.JS2Project{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	allocations := []types.JS2Allocation{}
	for _, tasAllocation := range tasAllocations {
		accept, _ := adapter.acceptTasAllocation(tasAllocation)
		if accept {
			js2Allocation, err := adapter.convertTasAllocationToJS2Allocation(tasAllocation)
			if err != nil {
				errorMessage := "failed to convert TAS Allocation to JS2 Allocation"
				logger.WithError(err).Error(errorMessage)
				return types.JS2Project{}, err
			}

			allocations = append(allocations, js2Allocation)
		}
	}

	js2Project := types.JS2Project{
		ID:          common.ID(projectID),
		Title:       tasProjectMap["title"].(string),
		Description: tasProjectMap["description"].(string),
		PI:          pi,
		Allocations: allocations,
	}
	return js2Project, nil
}

// convertTasAllocationToJS2Allocation converts TAS Allocation response into JS2 Allocation
func (adapter *TASAdapter) convertTasAllocationToJS2Allocation(tasAllocation interface{}) (types.JS2Allocation, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.convertTasAllocationToJS2Allocation",
	})

	// convert to map
	tasAllocationMap, ok := tasAllocation.(map[string]interface{})
	if !ok {
		errorMessage := "failed to convert TAS Allocation into map"
		logger.Error(errorMessage)
		return types.JS2Allocation{}, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	// id
	idInt := int64(tasAllocationMap["id"].(float64))
	idString := fmt.Sprintf("%d", idInt)

	// current date must be between start and end time
	start := tasAllocationMap["start"].(string)
	end := tasAllocationMap["end"].(string)

	layout := "2006-01-02T15:04:05Z"
	startTime, err := time.Parse(layout, start)
	if err != nil {
		errorMessage := "failed to parse TAS Allocation start time"
		logger.WithError(err).Error(errorMessage)
		return types.JS2Allocation{}, err
	}

	endTime, err := time.Parse(layout, end)
	if err != nil {
		errorMessage := "failed to parse TAS Allocation start time"
		logger.WithError(err).Error(errorMessage)
		return types.JS2Allocation{}, err
	}

	js2Allocations := types.JS2Allocation{
		ID:               idString,
		Start:            startTime,
		End:              endTime,
		ProjectCode:      tasAllocationMap["project"].(string),
		Resource:         tasAllocationMap["resource"].(string),
		ComputeAllocated: tasAllocationMap["computeAllocated"].(float64),
		StorageAllocated: tasAllocationMap["storageAllocated"].(float64),
		ComputeUsed:      tasAllocationMap["computeUsed"].(float64),
	}

	return js2Allocations, nil
}

// acceptTasAllocation checks if the TAS Allocation is acceptable
func (adapter *TASAdapter) acceptTasAllocation(tasAllocation interface{}) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "TASAdapter.acceptTasAllocation",
	})

	// convert to map
	tasAllocationMap, ok := tasAllocation.(map[string]interface{})
	if !ok {
		errorMessage := "failed to convert TAS Allocation into map"
		logger.Error(errorMessage)
		return false, cacao_common_service.NewCacaoMarshalError(errorMessage)
	}

	// status must be active
	status := tasAllocationMap["status"].(string)
	if strings.ToLower(status) != "active" {
		return false, nil
	}

	// resource must be jeststream or jetstream2
	resource := tasAllocationMap["resource"].(string)
	switch strings.ToLower(resource) {
	case "jetstream", "jetstream2":
		// pass
	default:
		return false, nil
	}

	// current date must be between start and end time
	start := tasAllocationMap["start"].(string)
	end := tasAllocationMap["end"].(string)

	layout := "2006-01-02T15:04:05Z"
	startTime, err := time.Parse(layout, start)
	if err != nil {
		errorMessage := "failed to parse TAS Allocation start time"
		logger.WithError(err).Error(errorMessage)
		return false, err
	}

	endTime, err := time.Parse(layout, end)
	if err != nil {
		errorMessage := "failed to parse TAS Allocation start time"
		logger.WithError(err).Error(errorMessage)
		return false, err
	}

	now := time.Now().UTC()
	if endTime.Before(now) || startTime.After(now) {
		return false, nil
	}
	return true, nil
}

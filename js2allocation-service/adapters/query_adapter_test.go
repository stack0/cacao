package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

func createTestQueryPort() *ports.MockIncomingQueryPort {
	var config types.Config
	config.ProcessDefaults()

	queryInImpl := &ports.MockIncomingQueryPort{}
	queryInImpl.Init(&config)

	return queryInImpl
}

func createTestQueryAdapter(queryInImpl ports.IncomingQueryPort) *QueryAdapter {
	var config types.Config
	config.ProcessDefaults()

	queryAdapter := &QueryAdapter{}
	queryAdapter.Init(&config)

	queryAdapter.IncomingPort = queryInImpl

	go queryAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return queryAdapter
}

func TestInitQueryAdapter(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)
	assert.NotNil(t, queryAdapter)
	assert.NotEmpty(t, queryAdapter.Connection)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestUserGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.JS2UserModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		NoCache: false,
	}

	testTime := time.Now().UTC()

	expectedResult := types.JS2User{
		ID:           "0001",
		Owner:        testUser,
		TACCUsername: "test_tacc_user1",
		RetrivedAt:   testTime,
	}

	queryInImpl.SetGetUserHandler(func(actor string, emulator string, nocache bool) (types.JS2User, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, false, nocache)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.JS2UserGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.JS2UserModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.Equal(t, expectedResult.Owner, result.Owner)
	assert.Equal(t, expectedResult.TACCUsername, result.TACCUsername)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestUserGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.JS2UserModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		NoCache: false,
	}

	queryInImpl.SetGetUserHandler(func(actor string, emulator string, nocache bool) (types.JS2User, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, false, nocache)
		return types.JS2User{}, fmt.Errorf("unable to get JS2 User")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.JS2UserGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.JS2UserModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestProjectListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.JS2ProjectListModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		NoCache: false,
	}

	testTime := time.Now().UTC()

	testPI := types.JS2PI{
		Username:  "test_pi1",
		FirstName: "test_pi1_firstname",
		LastName:  "test_pi1_lastname",
	}

	expectedResults := []types.JS2Project{
		{
			ID:          "0001",
			Owner:       testUser,
			Title:       "test_project1",
			Description: "test_description1",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Title:       "test_project2",
			Description: "test_description2",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
	}

	queryInImpl.SetListProjectsHandler(func(actor string, emulator string, nocache bool) ([]types.JS2Project, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, false, nocache)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.JS2ProjectListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.JS2ProjectListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetProjects()))

	firstProject := result.GetProjects()[0]
	assert.EqualValues(t, expectedResults[0].ID, firstProject.GetID())
	assert.EqualValues(t, expectedResults[0].Title, firstProject.GetTitle())
	assert.EqualValues(t, expectedResults[0].Description, firstProject.GetDescription())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestProjectListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.JS2ProjectListModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		NoCache: false,
	}

	queryInImpl.SetListProjectsHandler(func(actor string, emulator string, nocache bool) ([]types.JS2Project, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, false, nocache)
		return nil, fmt.Errorf("unable to list JS2 Projects")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.JS2ProjectListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.JS2ProjectListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.Projects)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

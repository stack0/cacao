package adapters

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

func createTestTASAdapter() *TASAdapter {
	var config types.Config
	config.ProcessDefaults()

	tasAdapter := &TASAdapter{}
	tasAdapter.Init(&config)

	return tasAdapter
}

func TestInitTASAdapter(t *testing.T) {
	tasAdapter := createTestTASAdapter()
	assert.NotNil(t, tasAdapter)

	tasAdapter.Finalize()
}

func TestGetUser(t *testing.T) {
	tasAdapter := createTestTASAdapter()

	defer tasAdapter.Finalize()

	// we will not test below if TAS API account is not configured
	if len(tasAdapter.Config.TASApiUsername) > 0 && len(tasAdapter.Config.TASApiPassword) > 0 {
		js2User, err := tasAdapter.GetUser("iychoi")
		assert.NoError(t, err)

		assert.NotEmpty(t, js2User.ID)
		assert.NotEmpty(t, js2User.Owner)
		assert.NotEmpty(t, js2User.TACCUsername)
	} else {
		t.SkipNow()
	}
}

func TestListProjects(t *testing.T) {
	tasAdapter := createTestTASAdapter()

	defer tasAdapter.Finalize()

	// we will not test below if TAS API account is not configured
	if len(tasAdapter.Config.TASApiUsername) > 0 && len(tasAdapter.Config.TASApiPassword) > 0 {
		js2Projects, err := tasAdapter.ListProjects("iychoi")
		assert.NoError(t, err)

		for _, js2Project := range js2Projects {
			assert.NotEmpty(t, js2Project.ID)
			assert.NotEmpty(t, js2Project.Title)
			assert.NotEmpty(t, js2Project.PI.Username)

			for _, js2Allocation := range js2Project.Allocations {
				assert.NotEmpty(t, js2Allocation.ID)
				assert.NotEmpty(t, js2Allocation.Start)
				assert.NotEmpty(t, js2Allocation.End)
			}
		}
	} else {
		t.SkipNow()
	}
}

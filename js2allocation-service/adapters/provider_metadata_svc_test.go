package adapters

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"testing"
	"time"
)

func TestProviderMetadataSvc_FindJS2Provider(t *testing.T) {

	type mockObjs struct {
		mockSvcClient *mocks.MockProviderMetadataSvcClient
	}
	type args struct {
		session types.Session
	}
	type fields struct {
		natsConf messaging.NatsConfig
		stanConf messaging.StanConfig
	}

	var clientConstructor = func(t *testing.T, objs mockObjs, args args) providerMetadataSvcClientConstructor {
		return func(session types.Session) (service.ProviderClient, error) {
			assert.Equal(t, args.session, session)
			return objs.mockSvcClient, nil
		}
	}
	var testSession = types.Session{
		TID:             messaging.NewTransactionID(),
		SessionActor:    "actor-123",
		SessionEmulator: "emulator-123",
	}

	tests := []struct {
		name     string
		mockObjs mockObjs
		fields   fields
		args     args
		want     []service.Provider
		wantErr  bool
	}{
		{
			name: "one-openstack-js2",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args: args{session: testSession},
			want: []service.Provider{
				&service.ProviderModel{
					ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "one-openstack-js2, one-other",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "other",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args: args{session: testSession},
			want: []service.Provider{
				&service.ProviderModel{
					ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "js2-non-standard-port",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args: args{session: testSession},
			want: []service.Provider{
				&service.ProviderModel{
					ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "js2-non-standard-port-with-path",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024/some/path",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args: args{session: testSession},
			want: []service.Provider{
				&service.ProviderModel{
					ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
					Name:      "",
					Type:      "openstack",
					URL:       "",
					CreatedAt: time.Time{},
					UpdatedAt: time.Time{},
					Metadata: map[string]interface{}{
						"OS_AUTH_URL": "https://js2.jetstream-cloud.org:1024/some/path",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "openstack-diff-domain",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.cyverse.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args:    args{session: testSession},
			want:    []service.Provider{},
			wantErr: false,
		},
		{
			name: "openstack-diff-sub-domain",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.jetstream-cloud.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args:    args{session: testSession},
			want:    []service.Provider{},
			wantErr: false,
		},
		{
			name: "no-js2",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-bbbbbbbbbbbbbbbbbbbb",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata: map[string]interface{}{
								"OS_AUTH_URL": "https://not-js2.cyverse.org",
							},
						},
					}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args:    args{session: testSession},
			want:    []service.Provider{},
			wantErr: false,
		},
		{
			name: "no-provider",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return([]service.Provider{}, nil)
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args:    args{session: testSession},
			want:    []service.Provider{},
			wantErr: false,
		},
		{
			name: "fail-to-list-provider",
			mockObjs: mockObjs{
				mockSvcClient: func() *mocks.MockProviderMetadataSvcClient {
					client := &mocks.MockProviderMetadataSvcClient{}
					client.On("List").Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			fields: fields{
				natsConf: messaging.NatsConfig{},
				stanConf: messaging.StanConfig{},
			},
			args:    args{session: testSession},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := ProviderMetadataSvc{
				natsConf:          tt.fields.natsConf,
				stanConf:          tt.fields.stanConf,
				clientConstructor: clientConstructor(t, tt.mockObjs, tt.args),
			}
			got, err := svc.FindJS2Provider(tt.args.session)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "FindJS2Provider(%v)", tt.args.session)
			tt.mockObjs.mockSvcClient.AssertExpectations(t)
		})
	}
}

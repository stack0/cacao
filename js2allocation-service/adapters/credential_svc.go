package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"sync"
)

// CredentialService provides access to credential service
type CredentialService interface {
	ListOpenStackCredentials(session types.Session) ([]service.Credential, error)
}

// CredentialSvc is a client for accessing credential service, it implements CredentialService
type CredentialSvc struct {
	natsConf             messaging.NatsConfig
	stanConf             messaging.StanConfig
	svcClientConstructor credentialSvcClientConstructor
}

// NewCredentialService ...
func NewCredentialService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) CredentialService {
	natsConf.ClientID = common.NewID(natsConf.ClientID).String()
	client := CredentialSvc{
		natsConf: natsConf,
		stanConf: stanConf,
	}
	client.svcClientConstructor = client.getClient
	return client
}

// ListOpenStackCredentials returns all credential user has of type "openstack"
func (svc CredentialSvc) ListOpenStackCredentials(session types.Session) ([]service.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialSvc.ListOpenStackCredentials",
	})
	client, err := svc.svcClientConstructor(session)
	if err != nil {
		logger.WithError(err).Error("fail to construct credential svc client")
		return []service.Credential{}, err
	}
	credentials, err := svc.listOpenStackCredentials(client)
	if err != nil {
		return []service.Credential{}, err
	}
	logger.WithField("len", len(credentials)).Debug("credential listed")
	// populate value, since list operation does not include value
	err = svc.populateCredentialValues(client, credentials)
	if err != nil {
		return []service.Credential{}, err
	}
	return credentials, nil
}

func (svc CredentialSvc) listOpenStackCredentials(client service.CredentialClient) ([]service.Credential, error) {
	var result = make([]service.Credential, 0)
	credentials, err := client.List()
	if err != nil {
		return nil, err
	}
	for _, cred := range credentials {
		if cred.GetType() == "openstack" {
			// only include "openstack" type in result
			result = append(result, cred)
		}
	}
	return result, nil
}

// populate value for each credential in the array, and do so in a concurrent manner
func (svc CredentialSvc) populateCredentialValues(client service.CredentialClient, credentials []service.Credential) error {
	var errors = make([]error, len(credentials))
	var wg sync.WaitGroup

	for i := 0; i < len(credentials); i++ {
		wg.Add(1)
		var counter = i // explicitly capture the counter instead of using the loop iter
		go func() {
			defer wg.Done()
			err := svc.populateValue(client, credentials[counter])
			if err != nil {
				errors[counter] = err
			}
		}()
	}
	wg.Wait()

	// check error
	for i, err := range errors {
		if err != nil {
			log.WithFields(log.Fields{
				"function": "CredentialSvc.populateCredentialValues",
				"credID":   credentials[i].GetID(),
			}).WithError(err).Error("fail to fetch credential")
			return err
		}
	}
	return nil
}

// fetch credential based on ID, and populate the value
func (svc CredentialSvc) populateValue(client service.CredentialClient, cred service.Credential) error {
	result, err := client.Get(cred.GetID())
	if err != nil {
		return err
	}
	cred.SetValue(result.GetValue())
	return nil
}

// this implements credentialSvcClientConstructor
func (svc CredentialSvc) getClient(session types.Session) (service.CredentialClient, error) {
	client, err := service.NewNatsCredentialClient(context.TODO(), session.SessionActor, session.SessionEmulator, svc.natsConf, svc.stanConf)
	if err != nil {
		return nil, err
	}
	return client, nil
}

// this is used to abstract away the creation of svc client for credential service, this makes writing tests easier
type credentialSvcClientConstructor func(session types.Session) (service.CredentialClient, error)

// MockCredentialSvcClient is used to generate mocks for service.CredentialClient, this interface is needed to generate the mocks locally in this repo
type MockCredentialSvcClient interface {
	service.CredentialClient
}

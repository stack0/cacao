// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	providers "gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// AccountingRESTAPI is an autogenerated mock type for the accountingRESTAPI type
type AccountingRESTAPI struct {
	mock.Mock
}

// ListAllocation provides a mock function with given fields: accountingAPI, token
func (_m *AccountingRESTAPI) ListAllocation(accountingAPI string, token providers.Token) ([]types.NewJS2Allocation, error) {
	ret := _m.Called(accountingAPI, token)

	var r0 []types.NewJS2Allocation
	if rf, ok := ret.Get(0).(func(string, providers.Token) []types.NewJS2Allocation); ok {
		r0 = rf(accountingAPI, token)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.NewJS2Allocation)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, providers.Token) error); ok {
		r1 = rf(accountingAPI, token)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

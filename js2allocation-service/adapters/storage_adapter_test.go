package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	mongoAdapter := &MongoAdapter{}
	mongoAdapter.InitMock(&config)

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.Store)

	mongoAdapter.Finalize()
}

func TestMongoAdapterGetUser(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTaccUsername := "test_tacc_username1"

	expectedResult := types.JS2User{
		ID:           "0001",
		Owner:        testUser,
		TACCUsername: testTaccUsername,
		RetrivedAt:   testTime,
	}
	err := mongoAdapter.MockGetUser(testUser, expectedResult, nil)
	assert.NoError(t, err)

	result, err := mongoAdapter.GetUser(testUser)
	assert.NoError(t, err)
	assert.Equal(t, result, expectedResult)

	mongoAdapter.Finalize()
}

func TestMongoAdapterInsertUser(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTaccUsername := "test_tacc_username1"

	testJS2User := types.JS2User{
		ID:           common.ID("test_js2_user_id"),
		Owner:        testUser,
		TACCUsername: testTaccUsername,
		RetrivedAt:   testTime,
	}

	err := mongoAdapter.MockInsertUser(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.InsertUser(testUser, testJS2User)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDeleteUser(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testUser := "test_owner1"

	err := mongoAdapter.MockDeleteUser(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteUser(testUser)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDeleteAndInsertUser(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testTaccUsername := "test_tacc_username1"

	testJS2User := types.JS2User{
		ID:           common.ID("test_js2_user_id"),
		Owner:        testUser,
		TACCUsername: testTaccUsername,
		RetrivedAt:   testTime,
	}

	err := mongoAdapter.MockDeleteAndInsertUser(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteAndInsertUser(testUser, testJS2User)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterListProjects(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testPI := types.JS2PI{
		Username:  "test_pi1",
		FirstName: "test_pi1_firstname",
		LastName:  "test_pi1_lastname",
	}

	expectedResults := []types.JS2Project{
		{
			ID:          "0001",
			Owner:       testUser,
			Title:       "test_project1",
			Description: "test_description1",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
		{
			ID:          "0002",
			Owner:       testUser,
			Title:       "test_project2",
			Description: "test_description2",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
	}
	err := mongoAdapter.MockListProjects(testUser, expectedResults, nil)
	assert.NoError(t, err)

	results, err := mongoAdapter.ListProjects(testUser)
	assert.NoError(t, err)
	assert.ElementsMatch(t, results, expectedResults)

	mongoAdapter.Finalize()
}

func TestMongoAdapterInsertAllProjects(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testPI := types.JS2PI{
		Username:  "test_pi1",
		FirstName: "test_pi1_firstname",
		LastName:  "test_pi1_lastname",
	}

	testJS2Projects := []types.JS2Project{
		{
			ID:          cacao_common_service.NewJS2ProjectID(),
			Owner:       testUser,
			Title:       "test_project1",
			Description: "test_description1",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
		{
			ID:          cacao_common_service.NewJS2ProjectID(),
			Owner:       testUser,
			Title:       "test_project2",
			Description: "test_description2",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
	}

	err := mongoAdapter.MockInsertAllProjects(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.InsertAllProjects(testUser, testJS2Projects)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDeleteAllProjects(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testUser := "test_owner1"

	err := mongoAdapter.MockDeleteAllProjects(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteAllProjects(testUser)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

func TestMongoAdapterDeleteAndInsertAllProjects(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()

	testTime := time.Now().UTC()
	testUser := "test_owner1"
	testPI := types.JS2PI{
		Username:  "test_pi1",
		FirstName: "test_pi1_firstname",
		LastName:  "test_pi1_lastname",
	}

	testJS2Projects := []types.JS2Project{
		{
			ID:          cacao_common_service.NewJS2ProjectID(),
			Owner:       testUser,
			Title:       "test_project1",
			Description: "test_description1",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
		{
			ID:          cacao_common_service.NewJS2ProjectID(),
			Owner:       testUser,
			Title:       "test_project2",
			Description: "test_description2",
			PI:          testPI,
			Allocations: []types.JS2Allocation{},
			RetrivedAt:  testTime,
		},
	}

	err := mongoAdapter.MockDeleteAndInsertAllProjects(testUser, nil)
	assert.NoError(t, err)

	err = mongoAdapter.DeleteAndInsertAllProjects(testUser, testJS2Projects)
	assert.NoError(t, err)

	mongoAdapter.Finalize()
}

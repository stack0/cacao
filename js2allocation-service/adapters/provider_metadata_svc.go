package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"net/url"
)

// ProviderMetadataService provides access to provider metadata service
type ProviderMetadataService interface {
	FindJS2Provider(session types.Session) ([]service.Provider, error)
}

// ProviderMetadataSvc is a client to access provider metadata service, it implements ProviderMetadataService
type ProviderMetadataSvc struct {
	natsConf          messaging.NatsConfig
	stanConf          messaging.StanConfig
	clientConstructor providerMetadataSvcClientConstructor
}

// NewProviderMetadataService ...
func NewProviderMetadataService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ProviderMetadataService {
	natsConf.ClientID = common.NewID(natsConf.ClientID).String()
	svc := ProviderMetadataSvc{
		natsConf:          natsConf,
		stanConf:          stanConf,
		clientConstructor: nil,
	}
	svc.clientConstructor = svc.getClient
	return svc
}

// FindJS2Provider finds the Jetstream 2 provider, there should just be at most one provider for Jetstream 2, but return an array just in case.
func (svc ProviderMetadataSvc) FindJS2Provider(session types.Session) ([]service.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMetadataSvc.FindJS2Provider",
	})
	providerList, err := svc.listProviders(session)
	if err != nil {
		logger.WithError(err).Error("fail to list providers")
		return nil, err
	}
	return svc.filterJS2Provider(logger, providerList), nil
}

func (svc ProviderMetadataSvc) filterJS2Provider(logger *log.Entry, providerList []service.Provider) []service.Provider {
	var result = make([]service.Provider, 0)
	for _, prov := range providerList {
		if !svc.isOpenStackProvider(prov) {
			// skip non-openstack type
			continue
		}
		if svc.isJS2Provider(prov) {
			result = append(result, prov)
		}
	}
	logger.WithFields(log.Fields{
		"before": len(providerList),
		"after":  len(result),
	}).Info("filter js2 provider")
	return result
}

// JS2 provider must have OS_AUTH_URL on hostname js2.jetstream-cloud.org
func (svc ProviderMetadataSvc) isJS2Provider(provider service.Provider) bool {
	metadata := provider.GetMetadata()
	if metadata == nil {
		return false
	}
	authURLRaw, ok := metadata["OS_AUTH_URL"]
	if !ok {
		return false
	}
	authURL, ok := authURLRaw.(string)
	if !ok {
		return false
	}
	parsed, err := url.ParseRequestURI(authURL)
	if err != nil {
		return false
	}
	return parsed.Hostname() == "js2.jetstream-cloud.org"
}

func (svc ProviderMetadataSvc) isOpenStackProvider(provider service.Provider) bool {
	return provider.GetType() == "openstack"
}

func (svc ProviderMetadataSvc) listProviders(session types.Session) ([]service.Provider, error) {
	client, err := svc.clientConstructor(session)
	if err != nil {
		return nil, err
	}
	return client.List()
}

// the function signature of this method fits type providerMetadataSvcClientConstructor
func (svc ProviderMetadataSvc) getClient(session types.Session) (service.ProviderClient, error) {
	return service.NewNatsProviderClient(context.TODO(), session.SessionActor, session.SessionEmulator, svc.natsConf, svc.stanConf)
}

// this is used to abstract away the creation of svc client for provider metadata service, this makes writing tests easier
type providerMetadataSvcClientConstructor func(session types.Session) (service.ProviderClient, error)

// MockProviderMetadataSvcClient is used to generate mocks for service.ProviderClient, this interface is needed to generate the mocks locally in this repo
type MockProviderMetadataSvcClient interface {
	service.ProviderClient
}

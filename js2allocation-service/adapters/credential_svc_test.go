package adapters

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"testing"
	"time"
)

func TestCredentialSvc_ListOpenStackCredentials(t *testing.T) {
	type mockObjs struct {
		mockSvcClient *mocks.MockCredentialSvcClient
	}
	type fields struct {
		natsConf messaging.NatsConfig
		stanConf messaging.StanConfig
	}
	type args struct {
		session types.Session
	}

	var clientConstructor = func(t *testing.T, objs mockObjs, args args) credentialSvcClientConstructor {
		return func(session types.Session) (service.CredentialClient, error) {
			assert.Equal(t, args.session, session)
			return objs.mockSvcClient, nil
		}
	}
	var testSession = types.Session{
		TID:             messaging.NewTransactionID(),
		SessionActor:    "actor-123",
		SessionEmulator: "emulator-123",
	}

	tests := []struct {
		name        string
		mockObjects mockObjs
		fields      fields
		args        args
		want        []service.Credential
		wantErr     bool
	}{
		{
			name: "one-openstack",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{
						&service.CredentialModel{
							Session:           service.Session{},
							Username:          "",
							Value:             "",
							Type:              "openstack",
							ID:                "cred-id-123",
							Description:       "",
							IsSystem:          false,
							IsHidden:          false,
							Visibility:        "",
							Tags:              nil,
							CreatedAt:         time.Time{},
							UpdatedAt:         time.Time{},
							UpdatedBy:         "",
							UpdatedEmulatorBy: "",
						},
					}, nil)
					client.On("Get", "cred-id-123").Return(&service.CredentialModel{
						Session:           service.Session{},
						Username:          "",
						Value:             "cred-value-123",
						Type:              "openstack",
						ID:                "cred-id-123",
						Description:       "",
						IsSystem:          false,
						IsHidden:          false,
						Visibility:        "",
						Tags:              nil,
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, nil)
					return client
				}(),
			},
			fields: fields{},
			args:   args{testSession},
			want: []service.Credential{
				&service.CredentialModel{
					Session:           service.Session{},
					Username:          "",
					Value:             "cred-value-123",
					Type:              "openstack",
					ID:                "cred-id-123",
					Description:       "",
					IsSystem:          false,
					IsHidden:          false,
					Visibility:        "",
					Tags:              nil,
					CreatedAt:         time.Time{},
					UpdatedAt:         time.Time{},
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				},
			},
			wantErr: false,
		},
		{
			name: "two-openstack one-other",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{
						&service.CredentialModel{
							Value: "",
							Type:  "openstack",
							ID:    "cred-id-123",
						},
						&service.CredentialModel{
							Value: "",
							Type:  "other-type",
							ID:    "cred-id-12345",
						},
						&service.CredentialModel{
							Value: "",
							Type:  "openstack",
							ID:    "cred-id-1234567",
						},
					}, nil)
					client.On("Get", "cred-id-123").Return(&service.CredentialModel{
						Value: "cred-value-123",
						Type:  "openstack",
						ID:    "cred-id-123",
					}, nil)
					client.On("Get", "cred-id-1234567").Return(&service.CredentialModel{
						Value: "cred-value-1234567",
						Type:  "openstack",
						ID:    "cred-id-1234567",
					}, nil)
					return client
				}(),
			},
			fields: fields{},
			args:   args{testSession},
			want: []service.Credential{
				&service.CredentialModel{
					Value: "cred-value-123",
					Type:  "openstack",
					ID:    "cred-id-123",
				},
				&service.CredentialModel{
					Value: "cred-value-1234567",
					Type:  "openstack",
					ID:    "cred-id-1234567",
				},
			},
			wantErr: false,
		},
		{
			name: "one-other",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{
						&service.CredentialModel{
							Value: "",
							Type:  "other-type",
							ID:    "cred-id-123",
						},
					}, nil)
					return client
				}(),
			},
			fields:  fields{},
			args:    args{testSession},
			want:    []service.Credential{},
			wantErr: false,
		},
		{
			name: "empty",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{}, nil)
					return client
				}(),
			},
			fields:  fields{},
			args:    args{testSession},
			want:    []service.Credential{},
			wantErr: false,
		},
		{
			name: "fail to list credentials",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			fields:  fields{},
			args:    args{testSession},
			want:    []service.Credential{},
			wantErr: true,
		},
		{
			name: "one-openstack fail to get credential",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{
						&service.CredentialModel{
							Value: "",
							Type:  "openstack",
							ID:    "cred-id-123",
						},
					}, nil)
					client.On("Get", "cred-id-123").Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			fields:  fields{},
			args:    args{testSession},
			want:    []service.Credential{},
			wantErr: true,
		},
		{
			name: "two-openstack fail to get one credential",
			mockObjects: mockObjs{
				mockSvcClient: func() *mocks.MockCredentialSvcClient {
					client := &mocks.MockCredentialSvcClient{}
					client.On("List").Return([]service.Credential{
						&service.CredentialModel{
							Value: "",
							Type:  "openstack",
							ID:    "cred-id-123",
						},
						&service.CredentialModel{
							Value: "",
							Type:  "openstack",
							ID:    "cred-id-12345",
						},
					}, nil)
					client.On("Get", "cred-id-123").Return(&service.CredentialModel{
						Value: "cred-value-123",
						Type:  "openstack",
						ID:    "cred-id-123",
					}, nil)
					// only fail to get one cred out of two
					client.On("Get", "cred-id-12345").Return(nil, fmt.Errorf("failed"))
					return client
				}(),
			},
			fields:  fields{},
			args:    args{testSession},
			want:    []service.Credential{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := CredentialSvc{
				natsConf:             tt.fields.natsConf,
				stanConf:             tt.fields.stanConf,
				svcClientConstructor: clientConstructor(t, tt.mockObjects, tt.args),
			}
			got, err := svc.ListOpenStackCredentials(tt.args.session)
			if tt.wantErr {
				assert.Errorf(t, err, "ListOpenStackCredentials(%v)", tt.args.session)
			} else {
				assert.NoErrorf(t, err, "ListOpenStackCredentials(%v)", tt.args.session)
			}
			assert.Equalf(t, tt.want, got, "ListOpenStackCredentials(%v)", tt.args.session)
		})
	}
}

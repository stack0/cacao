package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection     cacao_common_messaging.QueryEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *QueryAdapter) getEventHandlerMapping() []cacao_common_messaging.QueryEventHandlerMapping {
	return []cacao_common_messaging.QueryEventHandlerMapping{
		{
			Subject:      cacao_common_service.JS2UserGetQueryOp,
			EventHandler: adapter.handleUserGetQuery,
		},
		{
			Subject:      cacao_common_service.JS2ProjectListQueryOp,
			EventHandler: adapter.handleProjectListQuery,
		},
		{
			Subject:      cacao_common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.ConnectNatsForService(&natsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleUserGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.handleUserGetQuery",
	})

	var getRequest cacao_common_service.JS2UserModel
	var getResponse cacao_common_service.JS2UserModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		getResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getResponse)
		if err2 != nil {
			errorMessage := "unable to marshal JS2UserModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.GetUser(getRequest.SessionActor, getRequest.SessionEmulator, getRequest.NoCache)
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.SessionActor,
			SessionEmulator: getRequest.SessionEmulator,
		}

		getResponse = types.ConvertUserToModel(session, getResult)
	} else {
		logger.Error(err)

		getResponse.Session = cacao_common_service.Session{
			SessionActor:    getRequest.SessionActor,
			SessionEmulator: getRequest.SessionEmulator,
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getResponse.ServiceError = cerr.GetBase()
		} else {
			getResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getResponse)
	if err != nil {
		errorMessage := "unable to marshal JS2UserModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleProjectListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.handleProjectListQuery",
	})

	var listRequest cacao_common_service.JS2ProjectListModel
	var listResponse cacao_common_service.JS2ProjectListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal JS2ProjectListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	listResult, err := adapter.IncomingPort.ListProjects(listRequest.SessionActor, listRequest.SessionEmulator, listRequest.NoCache)
	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.SessionActor,
			SessionEmulator: listRequest.SessionEmulator,
		}

		models := []cacao_common_service.JS2ProjectListItemModel{}
		for _, js2Project := range listResult {
			model := types.ConvertProjectToListItemModel(js2Project)
			models = append(models, model)
		}

		listResponse.Projects = models
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.SessionActor,
			SessionEmulator: listRequest.SessionEmulator,
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal JS2ProjectListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)

	errResponse := cacao_common_service.JS2ProjectModel{}
	errMessage := fmt.Sprintf("request %s is not handled", subject)
	errResponse.ServiceError = cacao_common_service.NewCacaoNotImplementedError(errMessage).GetBase()

	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		errorMessage := "unable to marshal JS2ProjectModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

package adapters

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// JS2AccountingAPI uses the accounting API for Jetstream 2 to provide allocation information for user. It implements JS2APIPort.
type JS2AccountingAPI struct {
	providerSvc           ProviderMetadataService
	openstackSvc          ProviderOpenStackService
	credSvc               CredentialService
	timeSrc               TimeSource
	accountingRESTAPI     accountingRESTAPI
	projectIDGenerator    func() common.ID // use func ptr abstract away ID generation, this way unit test can override this
	allocationIDGenerator func() common.ID
}

// NewJS2AccountingAPI ...
func NewJS2AccountingAPI(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) *JS2AccountingAPI {
	api := &JS2AccountingAPI{
		providerSvc:           NewProviderMetadataService(natsConf, stanConf),
		openstackSvc:          NewProviderOpenStackService(natsConf, stanConf),
		credSvc:               NewCredentialService(natsConf, stanConf),
		timeSrc:               UTCTime{},
		accountingRESTAPI:     js2AccountingRESTAPI{},
		projectIDGenerator:    nil,
		allocationIDGenerator: nil,
	}
	api.projectIDGenerator = api.generateNewProjectID
	api.allocationIDGenerator = api.generateNewAllocationID
	return api
}

// Init ...
func (api JS2AccountingAPI) Init(config *types.Config) {
}

// Finalize ...
func (api JS2AccountingAPI) Finalize() {
}

// GetUser return the same username.
func (api JS2AccountingAPI) GetUser(user string) (types.JS2User, error) {
	if user == "" {
		return types.JS2User{}, fmt.Errorf("username is empty")
	}
	return types.JS2User{
		ID:           common.ID(user), // use username for ID, since this is primary key for mongodb
		Owner:        user,
		TACCUsername: user,
		RetrivedAt:   api.timeSrc.Now(),
	}, nil
}

// ListProjects ...
func (api JS2AccountingAPI) ListProjects(user string) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "JS2AccountingAPI.ListProjects",
		"user":     user,
	})
	session := types.Session{
		TID:             "",
		SessionActor:    user,
		SessionEmulator: "",
	}
	logger.Info()

	js2Providers, err := api.providerSvc.FindJS2Provider(session)
	if err != nil {
		logger.WithError(err).Errorf("fail to find js2 provider")
		return nil, err
	}
	if len(js2Providers) == 0 {
		logger.Error("no js2 provider found")
		return []types.JS2Project{}, nil
	} else if len(js2Providers) > 1 {
		logger.WithField("len", len(js2Providers)).Warning("there are more than 1 js2 providers")
	}
	credentials, err := api.listCredentials(session, js2Providers)
	if err != nil {
		logger.WithError(err).Errorf("fail to list openstack credentials")
		return nil, err
	}
	if len(credentials) == 0 {
		logger.Error("no credentials found")
		return []types.JS2Project{}, nil
	}
	endpoint, err := api.findAccountingAPIEndpoint(session, js2Providers[0], credentials[0])
	if err != nil {
		logger.WithError(err).Errorf("fail to find endpoint for accounting api")
		return nil, err
	}
	logger.WithField("endpoint", endpoint).Info("accounting endpoint found")
	tokens, err := api.obtainTokenForAllCred(session, js2Providers[0], credentials)
	if err != nil {
		logger.WithError(err).Errorf("fail to obtain token for all credentials")
		return nil, err
	}
	allocations, err := api.listAllocationsForAllTokens(endpoint, tokens)
	if err != nil {
		logger.WithError(err).Errorf("fail to list allocations")
		return nil, err
	}
	logger.WithField("len", len(allocations)).Info("allocations list fetched")
	allocations = api.filterOutInactiveAllocations(allocations)
	logger.WithField("len", len(allocations)).Info("inactive and expired allocation filtered out")
	return api.convertAllocations(allocations, user), nil
}

func (api JS2AccountingAPI) listCredentials(session types.Session, js2Providers []service.Provider) ([]service.Credential, error) {
	credentials, err := api.credSvc.ListOpenStackCredentials(session)
	if err != nil {
		return nil, err
	}
	return api.filterCredentialByProviders(js2Providers, credentials)
}

// only keep credentials that has tag the same as provider ID in the list of providers
func (api JS2AccountingAPI) filterCredentialByProviders(providers []service.Provider, credentials []service.Credential) ([]service.Credential, error) {
	var providerIDs = make(map[string]struct{})
	for _, provider := range providers {
		providerIDs[provider.GetID().String()] = struct{}{}
	}
	var result []service.Credential
	for _, cred := range credentials {
		var isJS2Cred bool
		for _, tag := range cred.GetTags() {
			_, isJS2Cred = providerIDs[tag]
			if isJS2Cred {
				break
			}
		}
		if isJS2Cred {
			result = append(result, cred)
		}
	}
	return result, nil
}

func (api JS2AccountingAPI) findAccountingAPIEndpoint(session types.Session, js2Provider service.Provider, credential service.Credential) (endpoint string, err error) {
	catalog, err := api.openstackSvc.FindAccountCatalogEntry(session, js2Provider.GetID(), credential.GetID())
	if err != nil {
		return "", err
	}
	if len(catalog.Endpoints) == 0 {
		return "", fmt.Errorf("catalog %s (%s) has no endpoints", catalog.Name, catalog.Type)
	}
	// TODO accounting api might have non-public interface (currently there isn't, just a public one)
	return catalog.Endpoints[0].URL, nil
}

func (api JS2AccountingAPI) obtainTokenForAllCred(session types.Session, js2Provider service.Provider, credentials []service.Credential) ([]providers.Token, error) {
	var result []providers.Token
	for _, cred := range credentials {
		token, err := api.openstackSvc.ObtainToken(session, js2Provider.GetID(), cred.GetID())
		if err != nil {
			return nil, err
		}
		result = append(result, token)
	}
	return api.deduplicateTokens(result), nil
}

// de-duplicate tokens, make sure only 1 token for each openstack project, or if there is an unscoped token (albeit unlikely for js2), use the unscoped token instead.
func (api JS2AccountingAPI) deduplicateTokens(tokens []providers.Token) []providers.Token {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "JS2AccountingAPI.deduplicateTokens",
	})
	var openstackProjectIDSet = make(map[string]struct{})
	var result = make([]providers.Token, 0)
	for _, token := range tokens {
		if token.ProjectID == "" {
			// unscoped token
			logger.Info("there is an unscoped token in the list, will use the unscoped token exclusively")
			return []providers.Token{token}
		}
		_, tokenExistsForProject := openstackProjectIDSet[token.ProjectID]
		if tokenExistsForProject {
			continue
		}
		result = append(result, token)
		openstackProjectIDSet[token.ProjectID] = struct{}{}
	}
	logger.WithFields(log.Fields{
		"before": len(tokens),
		"after":  len(result),
	}).Info("tokens deduplicated")
	return result
}

func (api JS2AccountingAPI) listAllocationsForAllTokens(accountingAPI string, tokens []providers.Token) ([]types.NewJS2Allocation, error) {
	var result []types.NewJS2Allocation
	for _, token := range tokens {
		allocations, err := api.accountingRESTAPI.ListAllocation(accountingAPI, token)
		if err != nil {
			return nil, err
		}
		result = append(result, allocations...)
	}
	return result, nil
}

func (api JS2AccountingAPI) filterOutInactiveAllocations(allocations []types.NewJS2Allocation) []types.NewJS2Allocation {
	var result []types.NewJS2Allocation
	now := api.timeSrc.Now()
	for _, alloc := range allocations {
		if alloc.IsInactiveOrExpired(now) {
			// skip inactive or expired allocation
			continue
		}
		result = append(result, alloc)
	}
	return result
}

func (api JS2AccountingAPI) convertAllocations(allAllocations []types.NewJS2Allocation, username string) []types.JS2Project {
	// TODO this needs to be revisited. The format between js1 and js2 are quite different, we might need to change the external API to better fit the js2 format.
	var result []types.JS2Project
	now := api.timeSrc.Now()
	groupedAllocation := api.groupAllocationByTitle(allAllocations)
	for title, allocations := range groupedAllocation {
		var serviceAllocations []types.JS2Allocation
		for _, alloc := range allocations {
			alloc2 := types.JS2Allocation{
				ID:               api.allocationIDGenerator().String(),
				Start:            alloc.StartDate,
				End:              alloc.EndDate,
				ProjectCode:      alloc.Title,
				Resource:         alloc.Resource,
				ComputeAllocated: 0,
				StorageAllocated: 0,
				ComputeUsed:      0,
			}
			if strings.Contains(alloc.Resource, "storage") {
				alloc2.StorageAllocated = float64(alloc.ServiceUnitsAllocated)
			} else {
				alloc2.ComputeAllocated = float64(alloc.ServiceUnitsAllocated)
				alloc2.ComputeUsed = float64(alloc.ServiceUnitsUsed)
			}
			serviceAllocations = append(serviceAllocations, alloc2)
		}
		result = append(result, types.JS2Project{
			ID:          api.projectIDGenerator(),
			Owner:       username,
			Title:       title,
			Description: allocations[0].Description,
			PI:          types.JS2PI{},
			Allocations: serviceAllocations,
			RetrivedAt:  now,
		})
	}
	return result
}

func (api JS2AccountingAPI) groupAllocationByTitle(allocations []types.NewJS2Allocation) map[string][]types.NewJS2Allocation {
	var result = make(map[string][]types.NewJS2Allocation)
	for _, alloc := range allocations {
		_, ok := result[alloc.Title]
		if !ok {
			result[alloc.Title] = []types.NewJS2Allocation{alloc}
		} else {
			result[alloc.Title] = append(result[alloc.Title], alloc)
		}
	}
	return result
}

func (api JS2AccountingAPI) generateNewAllocationID() common.ID {
	return common.NewID("js2alloc")
}

func (api JS2AccountingAPI) generateNewProjectID() common.ID {
	return common.NewID("js2project")
}

// accountingRESTAPI abstract away the actual REST call.
// https://gitlab.com/jetstream-cloud/accounting-api/-/blob/main/openapi.yml
type accountingRESTAPI interface {
	ListAllocation(accountingAPI string, token providers.Token) ([]types.NewJS2Allocation, error)
}

// implements accountingRESTAPI.
// https://gitlab.com/jetstream-cloud/accounting-api/-/blob/main/openapi.yml
type js2AccountingRESTAPI struct{}

// ListAllocation hit the accounting API with a token
func (api js2AccountingRESTAPI) ListAllocation(accountingAPI string, token providers.Token) ([]types.NewJS2Allocation, error) {
	client := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       time.Second * 5,
	}
	request, err := http.NewRequest(http.MethodGet, accountingAPI, nil)
	if err != nil {
		return nil, err
	}
	request.Header.Set("X-Auth-Token", token.ID)
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	var allocations []types.NewJS2Allocation
	err = json.NewDecoder(response.Body).Decode(&allocations)
	if err != nil {
		return nil, err
	}
	return allocations, nil
}

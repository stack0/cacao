package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// ProviderOpenStackService provides access to provider openstack service
type ProviderOpenStackService interface {
	FindAccountCatalogEntry(session types.Session, providerID common.ID, credID string) (providers.CatalogEntry, error)
	ObtainToken(session types.Session, providerID common.ID, credID string) (providers.Token, error)
}

// ProviderOpenStackSvc is a client to access provider openstack service. It implements ProviderOpenStackService
type ProviderOpenStackSvc struct {
	natsConf          messaging.NatsConfig
	stanConf          messaging.StanConfig
	clientConstructor providerOpenStackSvcClientConstructor
}

// NewProviderOpenStackService ...
func NewProviderOpenStackService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ProviderOpenStackService {
	natsConf.ClientID = common.NewID(natsConf.ClientID).String()
	svc := ProviderOpenStackSvc{
		natsConf:          natsConf,
		stanConf:          stanConf,
		clientConstructor: nil,
	}
	svc.clientConstructor = svc.getClient
	return svc
}

// FindAccountCatalogEntry find the catalog entry for service of type "accounting", first one is returned if there are multiple.
func (svc ProviderOpenStackSvc) FindAccountCatalogEntry(session types.Session, providerID common.ID, credID string) (providers.CatalogEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderOpenStackSvc.FindAccountCatalogEntry",
	})
	catalog, err := svc.listCatalog(session, providerID, credID)
	if err != nil {
		logger.WithError(err).Error("fail to list catalog")
		return providers.CatalogEntry{}, err
	}
	logger.Debug("catalog listed")
	for _, entry := range catalog {
		if entry.Type == "accounting" {
			logger.Debug("catalog entry for accounting found")
			return entry, nil
		}
	}
	return providers.CatalogEntry{}, service.NewCacaoNotFoundError("accounting catalog entry not found")
}

func (svc ProviderOpenStackSvc) listCatalog(session types.Session, providerID common.ID, credID string) ([]providers.CatalogEntry, error) {
	return svc.clientConstructor(providerID).ListCatalog(context.TODO(), &service.Session{
		SessionActor:    session.SessionActor,
		SessionEmulator: session.SessionEmulator,
	}, providers.WithCredentialID(credID))
}

// ObtainToken issues an OpenStack token using a credential
func (svc ProviderOpenStackSvc) ObtainToken(session types.Session, providerID common.ID, credID string) (providers.Token, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderOpenStackSvc.ObtainToken",
	})
	token, err := svc.clientConstructor(providerID).GetToken(context.TODO(), &service.Session{
		SessionActor:    session.SessionActor,
		SessionEmulator: session.SessionEmulator,
	}, providers.WithCredentialID(credID))
	if err != nil {
		logger.WithError(err).Error("fail to obtain token")
		return providers.Token{}, err
	}
	return *token, nil
}

// the function signature of this method fits type providerOpenStackSvcClientConstructor
func (svc ProviderOpenStackSvc) getClient(providerID common.ID) providers.OpenStackProvider {
	return providers.NewOpenStackProvider(providerID, &svc.natsConf, &svc.stanConf)
}

// this is used to abstract away the creation of svc client for provider openstack service, this makes writing tests easier
type providerOpenStackSvcClientConstructor func(providerID common.ID) providers.OpenStackProvider

// MockProviderOpenStackSvcClient is used to generate mocks for providers.OpenStackProvider, this interface is needed to generate the mocks locally in this repo
type MockProviderOpenStackSvcClient interface {
	providers.OpenStackProvider
}

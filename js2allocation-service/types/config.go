package types

import (
	"time"

	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	NatsConfig cacao_common_messaging.NatsConfig
	StanConfig cacao_common_messaging.StanConfig

	// MongoDB
	MongoDBConfig                             cacao_common_db.MongoDBConfig
	JS2AllocationMongoDBUserCollectionName    string
	JS2AllocationMongoDBProjectCollectionName string

	// TAS API
	TASApiUsername string `envconfig:"TACC_API_USER"`
	TASApiPassword string `envconfig:"TACC_API_PASS"`

	// Cache
	UserCacheExpirationTime    time.Duration
	ProjectCacheExpirationTime time.Duration
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	// NATS
	if c.NatsConfig.URL == "" {
		c.NatsConfig.URL = DefaultNatsURL
	}

	if c.NatsConfig.QueueGroup == "" {
		c.NatsConfig.QueueGroup = DefaultNatsQueueGroup
	}

	if c.NatsConfig.WildcardSubject == "" {
		c.NatsConfig.WildcardSubject = DefaultNatsWildcardSubject
	}

	if c.NatsConfig.ClientID == "" {
		c.NatsConfig.ClientID = DefaultNatsClientID
	}

	if c.NatsConfig.MaxReconnects <= 0 {
		c.NatsConfig.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.NatsConfig.ReconnectWait <= 0 {
		c.NatsConfig.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.NatsConfig.RequestTimeout <= 0 {
		c.NatsConfig.RequestTimeout = DefaultNatsRequestTimeout
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.JS2AllocationMongoDBUserCollectionName == "" {
		c.JS2AllocationMongoDBUserCollectionName = DefaultJS2AllocationMongoDBUserCollectionName
	}

	if c.JS2AllocationMongoDBProjectCollectionName == "" {
		c.JS2AllocationMongoDBProjectCollectionName = DefaultJS2AllocationMongoDBProjectCollectionName
	}

	// Cache
	if c.UserCacheExpirationTime == 0 {
		c.UserCacheExpirationTime = DefaultUserCacheExpirationTime
	}

	if c.ProjectCacheExpirationTime == 0 {
		c.ProjectCacheExpirationTime = DefaultProjectCacheExpirationTime
	}
}

package types

// JS2AllocationChannelRequest is a request struct used between adapters and domain
type JS2AllocationChannelRequest struct {
	Actor     string
	Emulator  string
	Data      interface{}
	Operation string
	NoCache   bool
	Response  chan JS2AllocationChannelResponse // channel created by adapter(or other caller) and will receive the response
}

// JS2AllocationChannelResponse is a response struct used between adapters and domain
type JS2AllocationChannelResponse struct {
	Data  interface{}
	Error error
}

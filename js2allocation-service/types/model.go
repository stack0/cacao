package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

func convertAllocationToModel(allocation JS2Allocation) cacao_common_service.JS2Allocation {
	return cacao_common_service.JS2Allocation{
		ID:               allocation.ID,
		Start:            allocation.Start,
		End:              allocation.End,
		ProjectCode:      allocation.ProjectCode,
		Resource:         allocation.Resource,
		ComputeAllocated: allocation.ComputeAllocated,
		StorageAllocated: allocation.StorageAllocated,
		ComputeUsed:      allocation.ComputeUsed,
	}
}

// ConvertUserToModel converts JS2User to JS2UserModel
func ConvertUserToModel(session cacao_common_service.Session, user JS2User) cacao_common_service.JS2UserModel {
	return cacao_common_service.JS2UserModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		Owner:        user.Owner,
		TACCUsername: user.TACCUsername,
		RetrievedAt:  user.RetrivedAt,
	}
}

// ConvertProjectToListItemModel converts JS2Project to JS2ProjectListItemModel
func ConvertProjectToListItemModel(project JS2Project) cacao_common_service.JS2ProjectListItemModel {
	allocationModels := []cacao_common_service.JS2Allocation{}
	for _, allocation := range project.Allocations {
		allocationModels = append(allocationModels, convertAllocationToModel(allocation))
	}

	return cacao_common_service.JS2ProjectListItemModel{
		ID:          project.ID,
		Owner:       project.Owner,
		Title:       project.Title,
		Description: project.Description,
		PI: cacao_common_service.JS2PI{
			Username:  project.PI.Username,
			FirstName: project.PI.FirstName,
			LastName:  project.PI.LastName,
		},
		Allocations: allocationModels,
		RetrievedAt: project.RetrivedAt,
	}
}

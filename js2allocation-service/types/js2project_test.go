package types

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestUnmarshalJS2Allocation(t *testing.T) {
	type test struct {
		testCaseName string
		input        string
		expected     NewJS2Allocation
		wantErr      bool
	}
	var tests = []test{
		{
			testCaseName: "active",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": 2,
    "start_date": "2022-04-15",
    "end_date": "2023-04-15",
    "active": 1
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1,
				Description:           "A short description",
				StartDate:             time.Date(2022, 4, 15, 0, 0, 0, 0, time.FixedZone("", 0)),
				EndDate:               time.Date(2023, 4, 15, 23, 59, 59, 0, time.FixedZone("", 0)),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 100,
				ServiceUnitsUsed:      2,
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "inactive",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": 2,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 0
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                0,
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, time.FixedZone("", 0)),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, time.FixedZone("", 0)),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 100,
				ServiceUnitsUsed:      2,
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "SU used null",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": null,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 1
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1,
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, time.FixedZone("", 0)),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, time.FixedZone("", 0)),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 100,
				ServiceUnitsUsed:      0, // use 0 for null
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "SU alloc/used null",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": null,
    "service_units_used": null,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 1
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1,
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, time.FixedZone("", 0)),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, time.FixedZone("", 0)),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 0, // use 0 for null
				ServiceUnitsUsed:      0, // use 0 for null
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "SU alloc negative",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": -100,
    "service_units_used": 0,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 1
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1,
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, utcTimeZone()),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, utcTimeZone()),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 0, // use 0 for negative
				ServiceUnitsUsed:      0,
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "SU used negative",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": -100,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 1
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1,
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, utcTimeZone()),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, utcTimeZone()),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 100,
				ServiceUnitsUsed:      0, // use 0 for negative
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
		{
			testCaseName: "active not 0/1",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": 2,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": 999
  }`,
			expected: NewJS2Allocation{},
			wantErr:  true,
		},
		{
			testCaseName: "active is null",
			input: `{
    "title": "code-name-for-allocation",
    "description": "A short description",
    "abstract": "An abstract",
    "resource": "myresource.cyverse.org",
    "service_units_allocated": 100,
    "service_units_used": 2,
    "start_date": "2021-04-15",
    "end_date": "2022-04-15",
    "active": null
  }`,
			expected: NewJS2Allocation{
				Abstract:              "An abstract",
				Active:                1, // treat `active: null` as 1
				Description:           "A short description",
				StartDate:             time.Date(2021, 4, 15, 0, 0, 0, 0, utcTimeZone()),
				EndDate:               time.Date(2022, 4, 15, 23, 59, 59, 0, utcTimeZone()),
				Resource:              "myresource.cyverse.org",
				ServiceUnitsAllocated: 100,
				ServiceUnitsUsed:      2,
				Title:                 "code-name-for-allocation",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.testCaseName, func(t *testing.T) {
			var result NewJS2Allocation
			err := json.Unmarshal([]byte(tt.input), &result)
			if tt.wantErr {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
			assert.Equal(t, tt.expected, result)
		})
	}
}

// assume that the time in the result from accounting api is UTC timezone, since that is where the accounting api is deployed.
func utcTimeZone() *time.Location {
	return time.FixedZone("", 0)
}

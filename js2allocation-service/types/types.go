package types

import "gitlab.com/cyverse/cacao-common/common"

// Session contains info for a request session
type Session struct {
	TID             common.TransactionID
	SessionActor    string `json:"actor"`
	SessionEmulator string `json:"emulator,omitempty"`
}

package types

// TASResponse is a struct for returning TAS query
type TASResponse struct {
	Status  string      `json:"status,omitempty"`
	Message interface{} `json:"message,omitempty"`
	Result  interface{} `json:"result,omitempty"`
}

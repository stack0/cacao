package domain

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	Config  *types.Config
	Storage ports.PersistentStoragePort
	QueryIn ports.IncomingQueryPort
	JS2API  ports.JS2APIPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.Config = config
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	qchannel := make(chan types.JS2AllocationChannelRequest, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go func() {
		defer wg.Done()
		d.QueryIn.Start()
	}()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.JS2AllocationChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "domain.processQueryWorker",
	})

	defer wg.Done()

	for request := range qchannel {
		logger.Debugf("received a query - %s", request.Operation)

		switch request.Operation {
		case string(cacao_common_service.JS2UserGetQueryOp):
			js2User, err := d.GetUser(request.Actor, request.Emulator, request.NoCache)
			if request.Response != nil {
				request.Response <- types.JS2AllocationChannelResponse{
					Data:  js2User,
					Error: err,
				}
			}
		case string(cacao_common_service.JS2ProjectListQueryOp):
			js2Projects, err := d.ListProjects(request.Actor, request.Emulator, request.NoCache)
			if request.Response != nil {
				request.Response <- types.JS2AllocationChannelResponse{
					Data:  js2Projects,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.JS2AllocationChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

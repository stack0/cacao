package domain

import (
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// GetUser returns JS2 user owned by a user
func (d *Domain) GetUser(actor string, emulator string, nocache bool) (types.JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.GetUser",
	})

	logger.Info("Get JS2 User")

	// 1. Use caches if there’re recent data cached
	// 2. if no cache, or caches are expired (i.e. 1 hour), or nocache flag is set get them from XCEDE
	// 3. If XCEDE returns data, cache them
	// 4. If XCEDE is down, use cached info although it is expired

	if nocache {
		return d.getUserWithNoCache(actor, emulator)
	}

	// check cache
	user, err := d.Storage.GetUser(actor)
	if err != nil {
		logger.Error(err)
		return types.JS2User{}, err
	}

	expired := false
	now := time.Now().UTC()
	if user.RetrivedAt.Add(d.Config.UserCacheExpirationTime).Before(now) {
		expired = true
	}

	if expired {
		logger.Info("Cache expired - retrieving JS2 User from XCEDE")
	}

	if len(user.TACCUsername) > 0 && len(user.Owner) > 0 && !expired {
		// use cache
		logger.Info("Cache exists - returning cached JS2 User")
		return user, nil
	}

	newUser, err := d.JS2API.GetUser(actor)
	if err != nil {
		// XCEDE is down?
		if cerr, ok := err.(*cacao_common_service.CacaoCommunicationError); ok {
			if len(user.TACCUsername) > 0 && len(user.Owner) > 0 {
				// use cache instead
				logger.WithError(cerr).Error("failed to access XCEDE TAS API, API server may be down? Using cache instead")
				return user, nil
			}

			// no cache
			logger.Error(cerr)
			return types.JS2User{}, cerr
		}

		// other types of error
		logger.Error(err)
		return types.JS2User{}, err
	}

	if len(newUser.TACCUsername) > 0 && len(newUser.Owner) > 0 {
		// update cache
		logger.Infof("Caching JS2User for a user %s", actor)
		err = d.Storage.DeleteAndInsertUser(actor, newUser)
		if err != nil {
			logger.Error(err)
			return types.JS2User{}, err
		}
	}

	return newUser, nil
}

// ListProjects returns JS2 projects owned by a user
func (d *Domain) ListProjects(actor string, emulator string, nocache bool) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.ListProjects",
	})

	logger.Info("List JS2 Projects")

	// 1. Use caches if there’re recent data cached
	// 2. if no cache, or caches are expired (i.e. 1 hour), or nocache flag is set get them from XCEDE
	// 3. If XCEDE returns data, cache them
	// 4. If XCEDE is down, use cached info although it is expired

	if nocache {
		return d.listProjectsWithNoCache(actor, emulator)
	}

	// check cache
	projects, err := d.Storage.ListProjects(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	expired := false
	now := time.Now().UTC()
	for _, project := range projects {
		if project.RetrivedAt.Add(d.Config.ProjectCacheExpirationTime).Before(now) {
			expired = true
			break
		}
	}

	if expired {
		logger.Info("Cache expired - retrieving JS2 Projects from XCEDE")
	}

	if len(projects) > 0 && !expired {
		// use cache
		logger.Info("Cache exists - returning cached JS2 Projects")
		return projects, nil
	}

	newProjects, err := d.JS2API.ListProjects(actor)
	if err != nil {
		// XCEDE is down?
		if cerr, ok := err.(*cacao_common_service.CacaoCommunicationError); ok {
			if len(projects) > 0 {
				// use cache instead
				logger.WithError(cerr).Error("failed to access XCEDE TAS API, API server may be down? Using cache instead")
				return projects, nil
			}

			// no cache
			logger.Error(cerr)
			return nil, cerr
		}

		// other types of error
		logger.Error(err)
		return nil, err
	}

	if len(newProjects) > 0 {
		// update cache
		logger.Infof("Caching JS2Projects for a user %s", actor)
		err = d.Storage.DeleteAndInsertAllProjects(actor, newProjects)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
	}

	return newProjects, nil
}

// getUserWithNoCache returns JS2 user owned by a user, forcefully getting the most recent data from XCEDE
func (d *Domain) getUserWithNoCache(actor string, emulator string) (types.JS2User, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.getUserWithNoCache",
	})

	logger.Info("Get JS2 User forcefully")

	// get the most recent data
	user, err := d.JS2API.GetUser(actor)
	if err != nil {
		logger.Error(err)
		return types.JS2User{}, err
	}

	if len(user.TACCUsername) > 0 && len(user.Owner) > 0 {
		// update cache
		err = d.Storage.DeleteAndInsertUser(actor, user)
		if err != nil {
			logger.Error(err)
			return types.JS2User{}, err
		}
	}

	return user, nil
}

// listProjectsWithNoCache returns JS2 projects owned by a user, forcefully getting the most recent data from XCEDE
func (d *Domain) listProjectsWithNoCache(actor string, emulator string) ([]types.JS2Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "js2allocation-service.domain",
		"function": "Domain.listProjectsWithNoCache",
	})

	logger.Info("List JS2 Projects forcefully")

	// get the most recent data
	projects, err := d.JS2API.ListProjects(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if len(projects) > 0 {
		// update cache
		err = d.Storage.DeleteAndInsertAllProjects(actor, projects)
		if err != nil {
			logger.Error(err)
			return nil, err
		}
	}

	return projects, nil
}

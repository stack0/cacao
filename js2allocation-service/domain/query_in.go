package domain

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.JS2AllocationChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.JS2AllocationChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {

}

// GetUser returns JS2 user of the user
func (impl *QueryPortImpl) GetUser(actor string, emulator string, nocache bool) (types.JS2User, error) {
	if len(actor) == 0 {
		return types.JS2User{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.JS2AllocationChannelResponse)

	// request to model
	impl.Channel <- types.JS2AllocationChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.JS2User{},
		Operation: string(cacao_common_service.JS2UserGetQueryOp),
		NoCache:   nocache,
		Response:  responseChannel,
	}

	// receive response
	js2userResponse := <-responseChannel

	if js2userResponse.Error != nil {
		return types.JS2User{}, js2userResponse.Error
	}

	js2User, ok := js2userResponse.Data.(types.JS2User)
	if !ok {
		return types.JS2User{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into JS2User")
	}

	return js2User, nil
}

// ListProjects retrieves all JS2 projects of the user
func (impl *QueryPortImpl) ListProjects(actor string, emulator string, nocache bool) ([]types.JS2Project, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.JS2AllocationChannelResponse)

	// request to model
	impl.Channel <- types.JS2AllocationChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.JS2Project{},
		Operation: string(cacao_common_service.JS2ProjectListQueryOp),
		NoCache:   nocache,
		Response:  responseChannel,
	}

	// receive response
	js2projectResponse := <-responseChannel

	if js2projectResponse.Error != nil {
		return nil, js2projectResponse.Error
	}

	js2Projects, ok := js2projectResponse.Data.([]types.JS2Project)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into JS2Project array")
	}

	return js2Projects, nil
}

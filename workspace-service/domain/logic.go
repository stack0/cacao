package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// ListWorkspaces returns workspaces owned by a user
func (d *Domain) ListWorkspaces(actor string, emulator string) ([]types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "Domain.ListWorkspaces",
	})

	logger.Info("List workspaces")

	workspaces, err := d.Storage.List(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return workspaces, nil
}

// GetWorkspace returns the workspace with the ID
func (d *Domain) GetWorkspace(actor string, emulator string, workspaceID cacao_common.ID) (types.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "Domain.GetWorkspace",
	})

	logger.Infof("Get workspace %s", workspaceID.String())

	workspace, err := d.Storage.Get(actor, workspaceID)
	if err != nil {
		logger.Error(err)
		return types.Workspace{}, err
	}

	return workspace, nil
}

// CreateWorkspace creates a workspace
func (d *Domain) CreateWorkspace(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.CreateWorkspace",
	})

	logger.Infof("Create workspace %s", workspace.ID.String())

	err := d.Storage.Create(workspace)
	if err != nil {
		createFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	createdEvent := types.Workspace{
		ID:        workspace.ID,
		Owner:     actor,
		Name:      workspace.Name,
		CreatedAt: workspace.CreatedAt,
	}

	err = d.EventOut.Created(actor, emulator, createdEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Created event")
	}

	return nil
}

// UpdateWorkspace updates the workspace
func (d *Domain) UpdateWorkspace(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.UpdateWorkspace",
	})

	logger.Infof("Update workspace %s", workspace.ID.String())

	err := d.Storage.Update(workspace, updateFieldNames)
	if err != nil {
		updateFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name, // may be empty
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// get the final result
	updatedWorkspace, err := d.Storage.Get(actor, workspace.ID)
	if err != nil {
		// update failed somehow - updated but not exist
		updateFailedEvent := types.Workspace{
			ID:    workspace.ID,
			Owner: actor,
			Name:  workspace.Name, // may be empty
		}

		err2 := d.EventOut.UpdateFailed(actor, emulator, updateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send an UpdateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	updatedEvent := types.Workspace{
		ID:        workspace.ID,
		Owner:     actor,
		Name:      updatedWorkspace.Name,
		UpdatedAt: updatedWorkspace.UpdatedAt,
	}

	err = d.EventOut.Updated(actor, emulator, updatedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send an Updated event")
	}

	return nil
}

// DeleteWorkspace deletes the workspace
func (d *Domain) DeleteWorkspace(actor string, emulator string, workspaceID cacao_common.ID, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.domain",
		"function": "domain.DeleteWorkspace",
	})

	logger.Infof("Delete workspace %s", workspaceID.String())

	// get the workspace first
	workspace, err := d.Storage.Get(actor, workspaceID)
	if err != nil {
		// not exist
		deleteFailedEvent := types.Workspace{
			ID:    workspaceID,
			Owner: actor,
			Name:  "", // unknown
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	err = d.Storage.Delete(actor, workspaceID)
	if err != nil {
		deleteFailedEvent := types.Workspace{
			ID:    workspaceID,
			Owner: actor,
			Name:  workspace.Name,
		}

		err2 := d.EventOut.DeleteFailed(actor, emulator, deleteFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeleteFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	deletedEvent := types.Workspace{
		ID:    workspaceID,
		Owner: actor,
		Name:  workspace.Name,
	}

	err = d.EventOut.Deleted(actor, emulator, deletedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deleted event")
	}

	return nil
}

package domain

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.WorkspaceChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.WorkspaceChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {

}

// List retrieves all workspaces of the user
func (impl *QueryPortImpl) List(actor string, emulator string) ([]types.Workspace, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.WorkspaceChannelResponse)

	// request to model
	impl.Channel <- types.WorkspaceChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.Workspace{},
		Operation: string(cacao_common_service.WorkspaceListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	workspaceResponse := <-responseChannel

	if workspaceResponse.Error != nil {
		return nil, workspaceResponse.Error
	}

	workspaces, ok := workspaceResponse.Data.([]types.Workspace)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into Workspace array")
	}

	return workspaces, nil
}

// Get retrieves the workspace
func (impl *QueryPortImpl) Get(actor string, emulator string, workspaceID cacao_common.ID) (types.Workspace, error) {
	if len(actor) == 0 {
		return types.Workspace{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(workspaceID) == 0 {
		return types.Workspace{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: workspace ID is empty")
	}

	responseChannel := make(chan types.WorkspaceChannelResponse)

	// request to model
	impl.Channel <- types.WorkspaceChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.Workspace{
			ID: workspaceID,
		},
		Operation: string(cacao_common_service.WorkspaceGetQueryOp),
		Response:  responseChannel,
	}

	// receive response
	workspaceResponse := <-responseChannel

	if workspaceResponse.Error != nil {
		return types.Workspace{}, workspaceResponse.Error
	}

	workspace, ok := workspaceResponse.Data.(types.Workspace)
	if !ok {
		return types.Workspace{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into Workspace")
	}

	return workspace, nil
}

package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/ports"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection     cacao_common_messaging.QueryEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *QueryAdapter) getEventHandlerMapping() []cacao_common_messaging.QueryEventHandlerMapping {
	return []cacao_common_messaging.QueryEventHandlerMapping{
		{
			Subject:      cacao_common_service.WorkspaceListQueryOp,
			EventHandler: adapter.handleWorkspaceListQuery,
		},
		{
			Subject:      cacao_common_service.WorkspaceGetQueryOp,
			EventHandler: adapter.handleWorkspaceGetQuery,
		},
		{
			Subject:      cacao_common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.ConnectNatsForService(&natsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleWorkspaceListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleWorkspaceListQuery",
	})

	var listRequest cacao_common_service.Session
	var listResponse cacao_common_service.WorkspaceListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal WorkspaceListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	listResult, err := adapter.IncomingPort.List(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.WorkspaceListItemModel{}
		for _, workspace := range listResult {
			model := types.ConvertToListItemModel(workspace)
			models = append(models, model)
		}

		listResponse.Workspaces = models
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal WorkspaceListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleWorkspaceGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleWorkspaceGetQuery",
	})

	var getRequest cacao_common_service.WorkspaceModel
	var getResponse cacao_common_service.WorkspaceModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into WorkspaceModel"
		logger.WithError(err).Error(errorMessage)
		getResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getResponse)
		if err2 != nil {
			errorMessage := "unable to marshal WorkspaceModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.Get(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetID())
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		getResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getResponse.Session = cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getResponse.ServiceError = cerr.GetBase()
		} else {
			getResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getResponse)
	if err != nil {
		errorMessage := "unable to marshal WorkspaceModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "workspace-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)

	errResponse := cacao_common_service.WorkspaceModel{}
	errMessage := fmt.Sprintf("request %s is not handled", subject)
	errResponse.ServiceError = cacao_common_service.NewCacaoNotImplementedError(errMessage).GetBase()

	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		errorMessage := "unable to marshal WorkspaceModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

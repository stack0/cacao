package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/workspace-service/types"
)

// CreateHandler is a handler for Create event
type CreateHandler func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error

// UpdateHandler is a handler for Update event
type UpdateHandler func(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error

// DeleteHandler is a handler for Delete event
type DeleteHandler func(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error

// MockIncomingEventPort is a mock implementation of IncomingEventPort
type MockIncomingEventPort struct {
	Config        *types.Config
	CreateHandler CreateHandler
	UpdateHandler UpdateHandler
	DeleteHandler DeleteHandler
}

// Init inits the port
func (port *MockIncomingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingEventPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingEventPort) InitChannel(channel chan types.WorkspaceChannelRequest) {}

// Start starts the port
func (port *MockIncomingEventPort) Start() {}

// SetCreateHandler sets a handler for Create event
func (port *MockIncomingEventPort) SetCreateHandler(createHandler CreateHandler) {
	port.CreateHandler = createHandler
}

// SetUpdateHandler sets a handler for Update event
func (port *MockIncomingEventPort) SetUpdateHandler(updateHandler UpdateHandler) {
	port.UpdateHandler = updateHandler
}

// SetDeleteHandler sets a handler for Delete event
func (port *MockIncomingEventPort) SetDeleteHandler(deleteHandler DeleteHandler) {
	port.DeleteHandler = deleteHandler
}

// Create creates a workspace
func (port *MockIncomingEventPort) Create(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	return port.CreateHandler(actor, emulator, workspace, transactionID)
}

// Update updates a workspace
func (port *MockIncomingEventPort) Update(actor string, emulator string, workspace types.Workspace, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	return port.UpdateHandler(actor, emulator, workspace, updateFieldNames, transactionID)
}

// Delete deletes a workspace
func (port *MockIncomingEventPort) Delete(actor string, emulator string, workspace types.Workspace, transactionID cacao_common.TransactionID) error {
	return port.DeleteHandler(actor, emulator, workspace, transactionID)
}

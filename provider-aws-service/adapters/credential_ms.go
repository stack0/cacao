package adapters

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	NATS messaging.NatsConfig
	STAN messaging.StanConfig
}

// ListCredentialsByTag ...
func (svc CredentialMicroservice) ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentials",
		"actor":    actor,
		"emulator": emulator,
		"tag":      tagName,
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credList, err := client.List()
	if err != nil {
		logger.WithError(err).Error("fail to list credentials")
		return nil, err
	}
	matchedCred := make([]types.Credential, 0)
	for _, cred := range credList {
		if svc.credentialHasTag(cred, tagName) {
			refetchedCred, err := svc.GetCredential(ctx, actor, emulator, cred.GetID())
			if err != nil {
				logger.WithError(err).Error("fail to re-fetch cred for cred value")
				continue
			}
			if refetchedCred == nil {
				logger.Error("GetCredential return nil on success")
				continue
			}
			matchedCred = append(matchedCred, *refetchedCred)
		}
	}
	logger.WithFields(log.Fields{
		"listCount":    len(credList),
		"matchedCount": len(matchedCred),
	}).Info("credential fetched")

	if len(matchedCred) == 0 {
		return []types.Credential{}, nil
	}
	return matchedCred, nil
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(NATS messaging.NatsConfig, STAN messaging.StanConfig) *CredentialMicroservice {
	return &CredentialMicroservice{NATS: NATS, STAN: STAN}
}

// GetCredential ...
func (svc CredentialMicroservice) GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error) {

	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.GetCredential",
		"actor":    actor,
		"emulator": emulator,
		"ID":       ID,
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credential, err := client.Get(ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return nil, err
	}
	converted, err := svc.convertCredential(credential)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cred value")
		return nil, err
	}
	logger.Info("credential fetched")
	return &converted, nil

}

func (svc CredentialMicroservice) convertCredential(cred service.Credential) (types.Credential, error) {
	var awsCred types.AWSAccessKeyCredential
	if err := json.Unmarshal([]byte(cred.GetValue()), &awsCred); err != nil {
		return types.Credential{}, err
	}
	return types.Credential{
		ID:        cred.GetID(),
		CreatedAt: cred.GetCreatedAt(),
		UpdatedAt: cred.GetUpdatedAt(),
		Cred:      awsCred,
	}, nil
}

func (svc CredentialMicroservice) credentialHasTag(cred service.Credential, tagName string) bool {
	for _, tag := range cred.GetTags() {
		if tag == tagName {
			return true
		}
	}
	return false
}

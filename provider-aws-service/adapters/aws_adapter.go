package adapters

import (
	"context"
	"crypto/sha1"
	"fmt"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	ec2types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// awsAdapter is used to access medata from AWS
type awsAdapter struct {
	createClient func(region string) AWSSDK
}

// NewAWSAdapter creates a new AWS adapter
func NewAWSAdapter() ports.AWS {
	return &awsAdapter{
		createClient: createClient,
	}
}

// AWSSDK is an interface that has all the methods that will be used by awsAdapter. ec2.Client implements this interface.
// This interface is created to separate the adapter from the AWS sdk, thus allow mocking the AWS sdk for unit testing.
type AWSSDK interface {
	DescribeImages(ctx context.Context, param *ec2.DescribeImagesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeImagesOutput, error)
	DescribeInstanceTypes(ctx context.Context, param *ec2.DescribeInstanceTypesInput, optFn ...func(*ec2.Options)) (*ec2.DescribeInstanceTypesOutput, error)
	DescribeRegions(ctx context.Context, param *ec2.DescribeRegionsInput, optFn ...func(*ec2.Options)) (*ec2.DescribeRegionsOutput, error)
}

type awsCtxKey int

// ONLY use withAWSCred to inject credential into context.
// the context value awsCtxKey constants should be unexported to avoid collision.
const (
	accessKeyIDCtxKey awsCtxKey = iota
	secretAccessKeyCtxKey
)

// ONLY use withAWSCred to inject credential into context.
func withAWSCred(parent context.Context, credential types.Credential) context.Context {
	ctx := context.WithValue(parent, accessKeyIDCtxKey, credential.Cred.AccessKey)
	return context.WithValue(ctx, secretAccessKeyCtxKey, credential.Cred.SecretKey)
}

func awsCredFromCtx(ctx context.Context) types.AWSAccessKeyCredential {
	accessKey := ctx.Value(accessKeyIDCtxKey)
	secretKey := ctx.Value(secretAccessKeyCtxKey)
	// because credential is injected by withAWSCred(), there is no need to check for nil or
	// the type of the value, we can just do type assertion.
	// if this panics then there is a bug somewhere.
	return types.AWSAccessKeyCredential{
		AccessKey: accessKey.(string),
		SecretKey: secretKey.(string),
	}
}

// retrieve is used to retrieve credentials from the context.
// this has the same function signature as aws.CredentialsProviderFunc.
func retrieve(ctx context.Context) (aws.Credentials, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "retrieve",
	})

	awsKey := awsCredFromCtx(ctx)
	credentials := aws.Credentials{
		AccessKeyID:     awsKey.AccessKey,
		SecretAccessKey: awsKey.SecretKey,
	}
	logger.WithFields(log.Fields{
		"accessKey": hashSecretForLogging(credentials.AccessKeyID),
		"secretKey": hashSecretForLogging(credentials.SecretAccessKey),
	}).Debug("credentials retrieved")
	return credentials, nil
}

func hashSecretForLogging(secret string) string {
	sha1.New()
	h := sha1.New()
	_, _ = io.WriteString(h, secret)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func createClient(region string) AWSSDK {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "createClient",
	})
	const defaultRegion = "us-east-1"

	if region == "" {
		log.WithField("region", region).Warn("region not specified, using default")
		region = defaultRegion
	}
	cfg := aws.Config{
		Credentials: aws.CredentialsProviderFunc(retrieve),
		Region:      region,
	}
	logger.WithField("region", region).Info("creating client")
	return ec2.NewFromConfig(
		cfg,
	)
}

// GetImage attempts to retrieve a specific AWS image by ID.
func (a *awsAdapter) GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.AWSImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.GetImage",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	imageOutput, err := client.DescribeImages(withAWSCred(ctx, credential), &ec2.DescribeImagesInput{
		ImageIds: []string{id},
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch aws image")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to fetch aws image, %s", err.Error()))
	}

	if len(imageOutput.Images) == 0 {
		// check length just in case
		return nil, service.NewCacaoGeneralError("fail to fetch aws image, but no error from AWS")
	}
	image := imageAWS(imageOutput.Images[0]).Convert()
	logger.WithField("image", image.ID).Info("image fetched")
	return &image, err
}

// GetFlavor returns a list of instances types from AWS
func (a *awsAdapter) GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.GetFlavor",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	flavorOutput, err := client.DescribeInstanceTypes(withAWSCred(ctx, credential), &ec2.DescribeInstanceTypesInput{
		InstanceTypes: []ec2types.InstanceType{ec2types.InstanceType(id)},
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch aws instance type")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to fetch aws instance type, %s", err.Error()))
	}
	if len(flavorOutput.InstanceTypes) == 0 {
		// check length just in case
		return nil, service.NewCacaoGeneralError("fail to fetch aws instance types, but no error from AWS")
	}
	flavor := flavorAWS(flavorOutput.InstanceTypes[0]).Convert()
	logger.WithField("flavor", flavor.ID).Info("flavor fetched")
	return &flavor, err
}

const (
	// owner ID of amazon linux images.
	// filter on this as owner will return ~600 images
	amazonLinuxOwnerID = "137112412989"
	// images owned by user
	selfOwner = "self"
)

// ListImages returns a list of images from AWS
func (a *awsAdapter) ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.AWSImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListImages",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	imgOutput, err := client.DescribeImages(withAWSCred(ctx, credential), &ec2.DescribeImagesInput{Owners: []string{selfOwner, amazonLinuxOwnerID}})
	if err != nil {
		logger.WithError(err).Error("fail to list aws images")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws images, %s", err.Error()))
	}

	logger.WithField("length", len(imgOutput.Images)).Info("images listed")
	return a.convertImageList(imgOutput.Images), err
}

// convertImageList converts a list of imageAWS to a list of providers.AWSImage
func (a *awsAdapter) convertImageList(imageList []ec2types.Image) []providers.AWSImage {
	if len(imageList) == 0 {
		return []providers.AWSImage{}
	}
	var result []providers.AWSImage
	for _, image := range imageList {
		result = append(result, imageAWS(image).Convert())
	}
	return result
}

// ListFlavors returns a list of instances types from AWS
func (a *awsAdapter) ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListFlavors",
		"actor":    types.UsernameFromCtx(ctx),
	})
	client := a.createClient(region)

	flavorOutput, err := client.DescribeInstanceTypes(withAWSCred(ctx, credential), &ec2.DescribeInstanceTypesInput{})
	if err != nil {
		logger.WithError(err).Error("fail to list aws instance types")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws instance types, %s", err.Error()))
	}
	logger.WithField("length", len(flavorOutput.InstanceTypes)).Info("flavors listed")
	return a.convertFlavorList(flavorOutput.InstanceTypes), err
}

// convertFlavorList converts a list of flavorAWS to a list of providers.Flavor
func (a *awsAdapter) convertFlavorList(flavorList []ec2types.InstanceTypeInfo) []providers.Flavor {
	var result []providers.Flavor
	for _, flavor := range flavorList {
		result = append(result, flavorAWS(flavor).Convert())

	}
	return result
}

// ListRegions returns a list of regions from AWS
func (a *awsAdapter) ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "awsAdapter.ListRegions",
		"actor":    types.UsernameFromCtx(ctx),
	})

	client := a.createClient("")
	regionsOutput, err := client.DescribeRegions(withAWSCred(ctx, credential), &ec2.DescribeRegionsInput{})
	if err != nil {
		logger.WithError(err).Error("fail to list aws regions")
		return nil, service.NewCacaoGeneralError(fmt.Sprintf("fail to list aws regions, %s", err.Error()))
	}

	logger.WithField("length", len(regionsOutput.Regions)).Info("Regions listed")
	return a.convertRegionList(regionsOutput.Regions), nil
}

// convertRegionList converts a list of regionAWS to a list of providers.Region
func (a *awsAdapter) convertRegionList(regionsList []ec2types.Region) []providers.Region {
	var result []providers.Region
	for _, region := range regionsList {
		result = append(result, regionAWS(region).Convert())
	}
	return result
}

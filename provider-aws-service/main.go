package main

import (
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-aws-service/adapters"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

var config types.Configuration

func init() {
	var err error

	if err = envconfig.Process("", &config); err != nil {
		log.Fatal(err.Error())
	}

	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	config.NATS.QueueGroup = "provider-aws-service"

	if err = config.Validate(); err != nil {
		log.Fatal(err)
	}
	config.NATS.WildcardSubject = "cyverse.providers.aws.>"

}

func main() {
	awsAdapter := adapters.NewAWSAdapter()

	var awsQueryAdapter adapters.QueryAdapter
	awsQueryAdapter.Init(config)

	credMS := adapters.NewCredentialMicroservice(config.NATS, config.STAN)

	providerMS := adapters.NewProviderMetadataClient(config.NATS, config.STAN)

	d := domain.NewAWSDomain(&config, &awsQueryAdapter, awsAdapter, credMS, providerMS)
	d.Start()
}

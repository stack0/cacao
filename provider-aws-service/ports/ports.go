package ports

import (
	"context"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// IncomingQueryPort is an interface for queries.
// Incoming queries are pushed into a channel by the adapter that implement this interface,
// and domain will be popping queries off from the channel to process them.
// Domain will then reply to the query by calling types.CloudEventRequest.Replyer.Reply().
type IncomingQueryPort interface {
	Init(config types.Configuration)
	Start()
	InitChannel(data chan types.CloudEventRequest)
}

// ProviderMetadataMS is an interface to interact with provider metadata service.
type ProviderMetadataMS interface {
	GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error)
}

// CredentialMS ...
type CredentialMS interface {
	GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error)
	ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error)
}

// AWS ...
type AWS interface {
	GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error)
	GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.AWSImage, error)
	ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error)
	ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error)
	ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.AWSImage, error)
}

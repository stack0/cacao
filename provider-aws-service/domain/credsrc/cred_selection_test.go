package credsrc

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-aws-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
	"testing"
	"time"
)

func TestCredentialSelector(t *testing.T) {
	t.Run("cred in request", testCredentialSelectorCredInRequest)
	t.Run("cred in request failed", testCredentialSelectorCredInRequestFailed)
	t.Run("cred matched tag", testCredentialSelectorMatchedTag)
	t.Run("cred matched tag multiple", testCredentialSelectorMultipleMatchedTag)
	t.Run("cred matched tag multiple with updated timestamp", testCredentialSelectorMultipleMatchedTagWithUpdatedTimestamp)
	t.Run("cred matched name", testCredentialSelectorMatchedName)

}

func testCredentialSelectorCredInRequest(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "my-cred-123",
		},
		Args: nil,
	}
	var cred = &types.Credential{
		ID:        request.Credential.Data.(string),
		CreatedAt: time.Now(),
		UpdatedAt: time.Time{},
		Cred: types.AWSAccessKeyCredential{
			AccessKey: "foo",
			SecretKey: "bar",
		},
	}
	credMock.On(
		"GetCredential",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Credential.Data,
	).Return(cred, nil)

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.NoError(t, err)
	assert.Equal(t, cred, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorCredInRequestFailed(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "my-cred-123",
		},
		Args: nil,
	}
	expectedErr := errors.New("something failed")
	credMock.On(
		"GetCredential",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Credential.Data,
	).Return(nil, expectedErr)

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.Error(t, err)
	assert.Equal(t, expectedErr, err)
	assert.Nil(t, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMatchedTag(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "",
		},
		Args: nil,
	}
	var cred = types.Credential{
		ID:        "cred1",
		CreatedAt: time.Now(),
		UpdatedAt: time.Time{},
		Cred: types.AWSAccessKeyCredential{
			AccessKey: "foo",
			SecretKey: "bar",
		},
	}
	var credList = []types.Credential{
		cred,
	}
	credMock.On(
		"ListCredentialsByTag",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.NoError(t, err)
	assert.Equal(t, &cred, selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMultipleMatchedTag(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "",
		},
		Args: nil,
	}
	timeNow := time.Now()
	var credList = []types.Credential{
		{
			ID:        "cred1",
			CreatedAt: timeNow.Add(time.Second * 10),
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo1",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred2",
			CreatedAt: timeNow.Add(time.Minute), // most recent
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo2",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred3",
			CreatedAt: timeNow,
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo3",
				SecretKey: "bar",
			},
		},
	}
	credMock.On(
		"ListCredentialsByTag",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.NoError(t, err)
	assert.Equal(t, &credList[1], selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMultipleMatchedTagWithUpdatedTimestamp(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "",
		},
		Args: nil,
	}
	timeNow := time.Now()
	var credList = []types.Credential{
		{
			ID:        "cred1",
			CreatedAt: timeNow.Add(time.Second * 10),
			UpdatedAt: timeNow.Add(time.Second * 30),
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo1",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred2",
			CreatedAt: timeNow.Add(time.Second * 10),
			UpdatedAt: timeNow.Add(time.Minute + time.Second),
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo2",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred3",
			CreatedAt: timeNow.Add(time.Minute),
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo3",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred4",
			CreatedAt: timeNow,
			UpdatedAt: timeNow.Add(time.Minute * 5), // most recent
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo4",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred5",
			CreatedAt: timeNow,
			UpdatedAt: time.Time{},
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo5",
				SecretKey: "bar",
			},
		},
		{
			ID:        "cred6",
			CreatedAt: timeNow,
			UpdatedAt: timeNow.Add(time.Second * 20),
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo6",
				SecretKey: "bar",
			},
		},
	}
	credMock.On(
		"ListCredentialsByTag",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(credList, nil)

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.NoError(t, err)
	assert.Equal(t, &credList[3], selectedCred)
	credMock.AssertExpectations(t)
}

func testCredentialSelectorMatchedName(t *testing.T) {
	t.Parallel()
	var credMock = &portmocks.CredentialMS{}
	var selector = credentialSelector{credMock}
	var request = providers.ProviderRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "emulator123",
		},
		Operation: "",
		Provider:  common.NewID("provider"),
		Credential: providers.ProviderCredential{
			Type: providers.ProviderCredentialID,
			Data: "",
		},
		Args: nil,
	}
	var cred = &types.Credential{
		ID:        request.Provider.String(),
		CreatedAt: time.Now(),
		UpdatedAt: time.Time{},
		Cred: types.AWSAccessKeyCredential{
			AccessKey: "foo",
			SecretKey: "bar",
		},
	}
	credMock.On(
		"ListCredentialsByTag",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(nil, errors.New("something wrong")) // match by tag failed
	credMock.On(
		"GetCredential",
		testContext(),
		request.GetSessionActor(),
		request.GetSessionEmulator(),
		request.Provider.String(),
	).Return(cred, nil) // match by name

	selectedCred, err := selector.SelectCredential(testContext(), request)
	assert.NoError(t, err)
	assert.Equal(t, cred, selectedCred)
	credMock.AssertExpectations(t)
}

func testContext() context.Context {
	return context.Background()
}

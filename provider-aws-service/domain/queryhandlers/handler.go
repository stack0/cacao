package queryhandlers

import (
	"context"
	"fmt"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
)

// Handler is query handler
type Handler interface {
	Handle(ctx context.Context, request providers.ProviderRequest) Reply
}

// Reply is an interface implemented by all reply structs in cacao-common (e.g. providers.ImageListReply)
type Reply interface {
	ToCloudEvent(source string, providerPrefix common.QueryOp) (cloudevents.Event, error)
}

// RegionListHandler handles getting a list of regions
type RegionListHandler struct {
	AWS     ports.AWS
	CredFac credsrc.CredentialFactory
}

// Handle ...
func (h RegionListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	regionList, err := h.AWS.ListRegions(ctx, cred)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.RegionListReply{
		BaseProviderReply: requestToBaseReply(request),
		Regions:           regionList,
	}
}

func (h RegionListHandler) errorReply(request providers.ProviderRequest, err error) providers.RegionListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.RegionListReply{
		BaseProviderReply: errorReply(request, err),
		Regions:           nil,
	}
}

// ImageListHandler handles getting a list of images
type ImageListHandler struct {
	AWS     ports.AWS
	CredFac credsrc.CredentialFactory
}

// Handle ...
func (h ImageListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.ImageListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	if args.Region == "" {
		return h.errorReply(request, service.NewCacaoInvalidParameterError("region cannot be empty"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	imageList, err := h.AWS.ListImages(ctx, cred, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.ImageListReply[providers.AWSImage]{
		BaseProviderReply: requestToBaseReply(request),
		Images:            imageList,
	}
}

func (h ImageListHandler) errorReply(request providers.ProviderRequest, err error) providers.ImageListReply[providers.AWSImage] {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.ImageListReply[providers.AWSImage]{
		BaseProviderReply: errorReply(request, err),
		Images:            nil,
	}
}

// ImageGetHandler handles getting a single image
type ImageGetHandler struct {
	AWS     ports.AWS
	CredFac credsrc.CredentialFactory
}

// Handle ...
func (h ImageGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.ImageGetInArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	if args.Region == "" {
		return h.errorReply(request, service.NewCacaoInvalidParameterError("region cannot be empty"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	image, err := h.AWS.GetImage(ctx, cred, args.ID, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetImageReply[providers.AWSImage]{
		BaseProviderReply: requestToBaseReply(request),
		Image:             image,
	}
}

func (h ImageGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetImageReply[providers.AWSImage] {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "ImageGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetImageReply[providers.AWSImage]{
		BaseProviderReply: errorReply(request, err),
		Image:             nil,
	}
}

// FlavorListHandler handles getting a list of flavors
type FlavorListHandler struct {
	AWS     ports.AWS
	CredFac credsrc.CredentialFactory
}

// Handle ...
func (h FlavorListHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	fmt.Println("FlavorListHandler.Handle")
	var args providers.FlavorListingArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	if args.Region == "" {
		return h.errorReply(request, service.NewCacaoInvalidParameterError("region cannot be empty"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavorList, err := h.AWS.ListFlavors(ctx, cred, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.FlavorListReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavors:           flavorList,
	}
}

func (h FlavorListHandler) errorReply(request providers.ProviderRequest, err error) providers.FlavorListReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorListHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.FlavorListReply{
		BaseProviderReply: errorReply(request, err),
		Flavors:           nil,
	}
}

// FlavorGetHandler handles getting a single flavor
type FlavorGetHandler struct {
	AWS     ports.AWS
	CredFac credsrc.CredentialFactory
}

// Handle ...
func (h FlavorGetHandler) Handle(ctx context.Context, request providers.ProviderRequest) Reply {
	var args providers.FlavorGetInArgs
	err := mapstructure.Decode(request.Args, &args)
	if err != nil {
		return h.errorReply(request, service.NewCacaoMarshalError(err.Error()))
	}
	if args.Region == "" {
		return h.errorReply(request, service.NewCacaoInvalidParameterError("region cannot be empty"))
	}
	cred, err := h.CredFac.GetCredential(ctx, request)
	if err != nil {
		return h.errorReply(request, err)
	}

	flavor, err := h.AWS.GetFlavor(ctx, cred, args.ID, args.Region)
	if err != nil {
		return h.errorReply(request, err)
	}

	return providers.GetFlavorReply{
		BaseProviderReply: requestToBaseReply(request),
		Flavor:            flavor,
	}
}

func (h FlavorGetHandler) errorReply(request providers.ProviderRequest, err error) providers.GetFlavorReply {
	log.WithFields(log.Fields{
		"package":  "domain",
		"function": "FlavorGetHandler.errorReply",
	}).WithError(err).Error("reply error")
	return providers.GetFlavorReply{
		BaseProviderReply: errorReply(request, err),
		Flavor:            nil,
	}
}

package domain

import (
	"context"
	"encoding/json"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-aws-service/domain/queryhandlers"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-aws-service/ports"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// AWSDomain ...
type AWSDomain struct {
	config     *types.Configuration
	QueryIn    ports.IncomingQueryPort
	AWS        ports.AWS
	CredMS     ports.CredentialMS
	ProviderMS ports.ProviderMetadataMS
}

// NewAWSDomain ...
func NewAWSDomain(c *types.Configuration, queryIn ports.IncomingQueryPort, aws ports.AWS, credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS) *AWSDomain {
	retval := &AWSDomain{
		config: c,

		QueryIn: queryIn,

		AWS:        aws,
		CredMS:     credMS,
		ProviderMS: providerMS,
	}
	return retval
}

// Start ...
func (domain *AWSDomain) Start() {
	var wg sync.WaitGroup
	domain.startQueryWorker(&wg)

	wg.Wait()

}

func (domain *AWSDomain) startQueryWorker(wg *sync.WaitGroup) {
	incomingQueryChan := make(chan types.CloudEventRequest, domain.config.DefaultChannelBufferSize)
	domain.QueryIn.InitChannel(incomingQueryChan)
	go domain.QueryIn.Start()

	for i := 0; i < domain.config.QueryWorkerCount; i++ {
		wg.Add(1)
		go domain.processQueryWorker(incomingQueryChan, wg)
	}
}

// processQueryWorker performs the listing and writes out the response on the ResponsePort.
func (domain *AWSDomain) processQueryWorker(cloudEventReqChan chan types.CloudEventRequest, wg *sync.WaitGroup) {
	defer wg.Done()

	origin := context.Background()

	for cloudEventReq := range cloudEventReqChan {
		ctx, cancel := context.WithTimeout(origin, time.Second*60)
		domain.processQuery(ctx, cloudEventReq)
		cancel()
	}
}

func (domain *AWSDomain) processQuery(ctx context.Context, cloudEventReq types.CloudEventRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "AWSDomain.processQuery",
	})
	request, err := domain.parseRequest(cloudEventReq.CloudEvent)
	if err != nil {
		logger.WithError(err).Error("fail to parse request")
		return
	}
	ctx = types.WithUsername(ctx, request.SessionActor)
	ctx = types.WithTransactionID(ctx, messaging.GetTransactionID(&cloudEventReq.CloudEvent))
	handler := domain.selectQueryHandler(request)
	if handler == nil {
		logger.WithField("op", request.Operation).Error("no handler selected for query")
		return
	}
	response := handler.Handle(ctx, request)
	replyCe, err := response.ToCloudEvent("aws-provider-service", providers.AWSQueryPrefix)
	if err != nil {
		logger.WithError(err).Error("fail to convert reply to cloudevent")
		return
	}
	if err = cloudEventReq.Replyer.Reply(replyCe); err != nil {
		logger.WithError(err).Error("fail to send reply")
	}
}

func (domain *AWSDomain) parseRequest(ce cloudevents.Event) (providers.ProviderRequest, error) {
	var req providers.ProviderRequest
	err := json.Unmarshal(ce.Data(), &req)
	if err != nil {
		return providers.ProviderRequest{}, err
	}
	return req, nil
}

// selects a handler based on the operation
func (domain *AWSDomain) selectQueryHandler(request providers.ProviderRequest) queryhandlers.Handler {

	credFac := credsrc.NewCredentialFactory(domain.CredMS, domain.ProviderMS)
	switch request.Operation {

	case providers.RegionsListOp:
		return queryhandlers.RegionListHandler{AWS: domain.AWS, CredFac: credFac}
	case providers.ImagesGetOp:
		return queryhandlers.ImageGetHandler{AWS: domain.AWS, CredFac: credFac}
	case providers.ImagesListOp:
		return queryhandlers.ImageListHandler{AWS: domain.AWS, CredFac: credFac}
	case providers.FlavorsGetOp:
		return queryhandlers.FlavorGetHandler{AWS: domain.AWS, CredFac: credFac}
	case providers.FlavorsListOp:
		return queryhandlers.FlavorListHandler{AWS: domain.AWS, CredFac: credFac}
	default:
		return nil
	}
}

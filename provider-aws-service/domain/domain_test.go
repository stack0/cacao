package domain

import (
	"context"
	"encoding/json"
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-aws-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

type MockReplyer struct {
	ce *cloudevents.Event
}

func (m *MockReplyer) Reply(ce cloudevents.Event) error {
	m.ce = &ce
	return nil
}

func TestAWSDomain_processQuery_imageList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo",
				SecretKey: "bar",
			},
		}
		region  = "us-east-2"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: "cred-123",
			},
			Args: providers.ImageListingArgs{Region: region},
		}
		subject     = providers.AWSQueryPrefix + providers.ImagesListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		imageList   = []providers.AWSImage{
			{
				Image: providers.Image{
					ID:     "image-1",
					Name:   "image-1-name",
					Status: "available",
				},
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := types.WithUsername(originalCtx, actor)
	expectedCtx = types.WithTransactionID(expectedCtx, messaging.GetTransactionID(&ce))
	awsMock := &portmocks.AWS{}
	awsMock.On("ListImages", expectedCtx, cred, region).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", anyContext(), actor, emulator, credID).Return(&cred, nil)
	domain := &AWSDomain{
		AWS:    awsMock,
		CredMS: credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.ImageListReply[providers.AWSImage]
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Images)
	assert.Equal(t, imageList, reply.Images)
	assert.NoError(t, reply.Session.GetServiceError())
}

func TestAWSDomain_processQuery_flavorList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			Cred: types.AWSAccessKeyCredential{
				AccessKey: "foo",
				SecretKey: "bar",
			},
		}
		region  = "us-east-2"
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: "cred-123",
			},
			Args: providers.FlavorListingArgs{Region: region},
		}
		subject     = providers.AWSQueryPrefix + providers.FlavorsListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		flavorList  = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := types.WithUsername(originalCtx, actor)
	expectedCtx = types.WithTransactionID(expectedCtx, messaging.GetTransactionID(&ce))
	awsMock := &portmocks.AWS{}
	awsMock.On("ListFlavors", expectedCtx, cred, region).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", anyContext(), actor, emulator, credID).Return(&cred, nil)
	domain := &AWSDomain{
		AWS:    awsMock,
		CredMS: credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	awsMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.FlavorListReply
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Flavors)
	assert.Equal(t, flavorList, reply.Flavors)
	assert.NoError(t, reply.Session.GetServiceError())
}

// this return an argument matcher that matches anything of context.Context type
func anyContext() interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool { return true })
}

package types

import (
	"errors"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"

	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// Credential ...
type Credential struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	Cred      AWSAccessKeyCredential
}

// AWSAccessKeyCredential is the format of AWS credential that this service accepts.
// The Value field in the credential we get from credential service should be a JSON string of this struct.
type AWSAccessKeyCredential struct {
	AccessKey string `json:"AWS_ACCESS_KEY_ID"`
	SecretKey string `json:"AWS_SECRET_ACCESS_KEY"`
}

// AWSClient ...
type AWSClient struct {
	*ec2.Client
}

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	NATS                     messaging.NatsConfig
	STAN                     messaging.StanConfig
	DefaultChannelBufferSize int    `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`
	QueryWorkerCount         int    `envconfig:"QUERY_WORKER_COUNT" default:"10"`
	PodName                  string `envconfig:"POD_NAME"`
	LogLevel                 string `envconfig:"LOG_LEVEL" default:"trace"`
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.NATS.ClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.NATS.QueueGroup == "" {
		return errors.New("NATS_QGROUP environment variable must be set")
	}

	if c.QueryWorkerCount <= 0 {
		return errors.New("QUERY_WORKER_COUNT cannot be zero or negative")
	}

	return nil
}

// QueryReply defines a type that can send a response of some sort.
type QueryReply interface {
	Reply(cloudevents.Event) error
}

// CloudEventRequest is what gets comes in as a request
type CloudEventRequest struct {
	CloudEvent event.Event
	Replyer    QueryReply
}

package types

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
)

// the context value key type should be unexported to avoid collision.
type key int

// the context value key constants should be unexported to avoid collision.
const (
	// usernameKey is what you should use as a key for the username in context.Context.
	usernameKey key = iota

	// transactionIDKey is the key for the transaction ID in context.Context.
	transactionIDKey
)

// WithUsername inject username into context
func WithUsername(parent context.Context, username string) context.Context {
	return context.WithValue(parent, usernameKey, username)
}

// WithTransactionID inject transaction ID into context
func WithTransactionID(parent context.Context, tid common.TransactionID) context.Context {
	return context.WithValue(parent, transactionIDKey, tid)
}

// UsernameFromCtx extracts username from context, return "' if key not exists or value not string
func UsernameFromCtx(ctx context.Context) string {
	usernameRaw := ctx.Value(usernameKey)
	if usernameRaw == nil {
		return ""
	}
	username, ok := usernameRaw.(string)
	if !ok {
		return ""
	}
	return username
}

// TransactionIDFromCtx extracts transaction ID from context, return "' if key not exists or value is wrong type
func TransactionIDFromCtx(ctx context.Context) common.TransactionID {
	tidRaw := ctx.Value(transactionIDKey)
	if tidRaw == nil {
		return ""
	}
	tid, ok := tidRaw.(common.TransactionID)
	if !ok {
		return ""
	}
	return tid
}

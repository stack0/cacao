package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.TemplateChannelRequest)
	Start()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	AsyncPort

	// TemplateType
	ListTypes(actor string, emulator string) ([]types.TemplateType, error)
	ListTypesForProviderType(actor string, emulator string, providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error)
	GetType(actor string, emulator string, typeName string) (types.TemplateType, error)

	// TemplateSourceType
	ListSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error)

	// Template
	List(actor string, emulator string) ([]types.Template, error)
	Get(actor string, emulator string, templateID cacao_common.ID) (types.Template, error)
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	AsyncPort

	// TemplateType
	CreateType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error
	UpdateType(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error
	DeleteType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error

	// Template
	Import(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error
	Update(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error
	Delete(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error
	Sync(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error
}

// OutgoingEventPort is an outgoing event port
type OutgoingEventPort interface {
	Port

	// TemplateType
	TypeCreated(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error
	TypeCreateFailed(actor string, emulator string, templateType types.TemplateType, creationError error, transactionID cacao_common.TransactionID) error
	TypeUpdated(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error
	TypeUpdateFailed(actor string, emulator string, templateType types.TemplateType, updateError error, transactionID cacao_common.TransactionID) error
	TypeDeleted(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error
	TypeDeleteFailed(actor string, emulator string, templateType types.TemplateType, deletionError error, transactionID cacao_common.TransactionID) error

	// Template
	Imported(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error
	ImportFailed(actor string, emulator string, template types.Template, importError error, transactionID cacao_common.TransactionID) error
	Updated(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error
	UpdateFailed(actor string, emulator string, template types.Template, updateError error, transactionID cacao_common.TransactionID) error
	Deleted(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error
	DeleteFailed(actor string, emulator string, template types.Template, deletionError error, transactionID cacao_common.TransactionID) error
	Synced(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error
	SyncFailed(actor string, emulator string, template types.Template, syncError error, transactionID cacao_common.TransactionID) error
}

// PersistentStoragePort is an interface for a Perstistent Storage port.
type PersistentStoragePort interface {
	Port

	// TemplateType
	ListTypes() ([]types.TemplateType, error)
	ListTypesForProviderType(providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error)
	GetType(templateTypeName string) (types.TemplateType, error)
	CreateType(templateType types.TemplateType) error
	UpdateType(templateType types.TemplateType, updateFieldNames []string) error
	DeleteType(templateTypeName string) error

	// Template
	List(user string) ([]types.Template, error)
	Get(user string, templateID cacao_common.ID) (types.Template, error)
	Create(template types.Template) error
	Update(template types.Template, updateFieldNames []string) error
	Delete(user string, templateID cacao_common.ID) error
}

// TemplateSourcePort is an interface for a template source port.
type TemplateSourcePort interface {
	Port

	GetTemplateMetadata(source types.TemplateSource, credential cacao_common_service.TemplateSourceCredential) (types.TemplateMetadata, error)
}

// CredentialPort is an interface for a credential service port.
type CredentialPort interface {
	Port

	GetTemplateSourceCredential(actor string, emulator string, credentialID string) (cacao_common_service.TemplateSourceCredential, error)
}

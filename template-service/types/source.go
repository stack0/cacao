package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

const (
	// TemplateSourceTypeGit is for a template source from git
	TemplateSourceTypeGit cacao_common_service.TemplateSourceType = "git"
)

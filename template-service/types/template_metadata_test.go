package types

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

// getInitialTestMeatadata returns a map representing valid test metadata JSON. A map is used here so that it will
// be easy to add and remove keys for testing.
func getInitialTestMetadata() map[string]interface{} {
	return map[string]interface{}{
		"name":          "test_template",
		"author":        "Some Author",
		"author_email":  "sauthor@example.org",
		"description":   "Test Template",
		"template_type": "openstack_terraform",
		"purpose":       "compute",
		"cacao_post_tasks": []map[string]string{
			{
				"type":          "something",
				"location_type": "somewhere",
			},
		},
		"parameters": map[string]interface{}{
			"foo": map[string]string{
				"type":        "string",
				"ui_label":    "Foo",
				"description": "It's a programmer thing.",
			},
		},
	}
}

// validateMetadataMap validates metadata that is stored as a map. The map will be converted to JSON then validated
// against the schema.
func validateMetadataMap(m map[string]interface{}) error {
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	return ValidateTemplateMetadataJSON(b)
}

// TestValidMetadata verifies that valid metadata passes.
func TestValidMetadata(t *testing.T) {
	assert := assert.New(t)
	assert.NoError(validateMetadataMap(getInitialTestMetadata()))
}

// TestMissingPurpose verifies that a missing purpose field will generate an error.
func TestMissingPurpose(t *testing.T) {
	assert := assert.New(t)
	metadata := getInitialTestMetadata()
	delete(metadata, "purpose")
	assert.Error(validateMetadataMap(metadata))
}

// TestInvalidPurpose verifies that an invalid purpose field will generate an error.
func TestInvalidPurpose(t *testing.T) {
	assert := assert.New(t)
	metadata := getInitialTestMetadata()
	metadata["purpose"] = "nihilism"
	assert.Error(validateMetadataMap(metadata))
}

// TestEmptyPurpose verifies that an empty purpose field will generate an error.
func TestEmptyPurpose(t *testing.T) {
	assert := assert.New(t)
	metadata := getInitialTestMetadata()
	metadata["purpose"] = ""
	assert.Error(validateMetadataMap(metadata))
}

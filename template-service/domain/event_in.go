package domain

import (
	"gitlab.com/cyverse/cacao/template-service/types"
)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Config     *types.Config
	Channel    chan types.TemplateChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *EventPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *EventPortImpl) Finalize() {
}

// InitChannel ...
func (impl *EventPortImpl) InitChannel(channel chan types.TemplateChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *EventPortImpl) Start() {

}

package domain

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListSourceTypes retrieves all template source types
func (impl *QueryPortImpl) ListSourceTypes(actor string, emulator string) ([]cacao_common_service.TemplateSourceType, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.TemplateChannelResponse)

	// request to model
	impl.Channel <- types.TemplateChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      nil,
		Operation: string(cacao_common_service.TemplateSourceTypeListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	templateResponse := <-responseChannel

	if templateResponse.Error != nil {
		return nil, templateResponse.Error
	}

	templateSourceTypes, ok := templateResponse.Data.([]cacao_common_service.TemplateSourceType)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into TemplateSourceType array")
	}

	return templateSourceTypes, nil
}

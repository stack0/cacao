package domain

import (
	"gitlab.com/cyverse/cacao/template-service/types"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.TemplateChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.TemplateChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {

}

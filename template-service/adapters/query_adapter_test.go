package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func createTestQueryPort() *ports.MockIncomingQueryPort {
	var config types.Config
	config.ProcessDefaults()

	queryInImpl := &ports.MockIncomingQueryPort{}
	queryInImpl.Init(&config)

	return queryInImpl
}

func createTestQueryAdapter(queryInImpl ports.IncomingQueryPort) *QueryAdapter {
	var config types.Config
	config.ProcessDefaults()

	queryAdapter := &QueryAdapter{}
	queryAdapter.Init(&config)

	queryAdapter.IncomingPort = queryInImpl

	go queryAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return queryAdapter
}

func TestInitQueryAdapter(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)
	assert.NotNil(t, queryAdapter)
	assert.NotEmpty(t, queryAdapter.Connection)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

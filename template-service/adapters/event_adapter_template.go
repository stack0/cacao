package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *EventAdapter) handleTemplateImportRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateImportRequest",
	})

	var importRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &importRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	template := types.ConvertFromModel(importRequest)

	err = adapter.IncomingPort.Import(importRequest.GetSessionActor(), importRequest.GetSessionEmulator(), template, importRequest.CredentialID, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateDeleteRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateDeleteRequest",
	})

	var deleteRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &deleteRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	template := types.ConvertFromModel(deleteRequest)

	err = adapter.IncomingPort.Delete(deleteRequest.GetSessionActor(), deleteRequest.GetSessionEmulator(), template, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateUpdateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateUpdateRequest",
	})

	var updateRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &updateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	template := types.ConvertFromModel(updateRequest)

	if len(updateRequest.UpdateFieldNames) == 0 {
		// update all fields
		updateRequest.UpdateFieldNames = append(updateRequest.UpdateFieldNames, "name", "description", "public", "source")
	}

	err = adapter.IncomingPort.Update(updateRequest.GetSessionActor(), updateRequest.GetSessionEmulator(), template, updateRequest.UpdateFieldNames, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleTemplateSyncRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.handleTemplateSyncRequest",
	})

	var syncRequest cacao_common_service.TemplateModel
	err := json.Unmarshal(jsonData, &syncRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	template := types.ConvertFromModel(syncRequest)

	err = adapter.IncomingPort.Sync(syncRequest.GetSessionActor(), syncRequest.GetSessionEmulator(), template, syncRequest.CredentialID, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// Imported ...
func (adapter *EventAdapter) Imported(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Imported",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateImportedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateImportedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateImportedEvent)
	}

	return nil
}

// ImportFailed ...
func (adapter *EventAdapter) ImportFailed(actor string, emulator string, template types.Template, importError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.ImportFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       importError.Error(),
		ErrorMessage:    importError.Error(),
	}

	if cerr, ok := importError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(importError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateImportFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateImportFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateImportFailedEvent)
	}

	return nil
}

// Updated ...
func (adapter *EventAdapter) Updated(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateUpdatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdatedEvent)
	}

	return nil
}

// UpdateFailed ...
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, template types.Template, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       updateError.Error(),
		ErrorMessage:    updateError.Error(),
	}

	if cerr, ok := updateError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(updateError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateUpdateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateUpdateFailedEvent)
	}

	return nil
}

// Deleted ...
func (adapter *EventAdapter) Deleted(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateDeletedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateDeletedEvent)
	}

	return nil
}

// DeleteFailed ...
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, template types.Template, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deletionError.Error(),
		ErrorMessage:    deletionError.Error(),
	}

	if cerr, ok := deletionError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deletionError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateDeleteFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateDeleteFailedEvent)
	}

	return nil
}

// Synced ...
func (adapter *EventAdapter) Synced(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.Synced",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateSyncedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncedEvent)
	}

	return nil
}

// SyncFailed ...
func (adapter *EventAdapter) SyncFailed(actor string, emulator string, template types.Template, syncError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "EventAdapter.SyncFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       syncError.Error(),
		ErrorMessage:    syncError.Error(),
	}

	if cerr, ok := syncError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(syncError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, template)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.TemplateSyncFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TemplateSyncFailedEvent)
	}

	return nil
}

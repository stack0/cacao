package adapters

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func (adapter *QueryAdapter) handleTemplateTypeListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateTypeListQuery",
	})

	var listRequest cacao_common_service.TemplateTypeModel
	var listResponse cacao_common_service.TemplateTypeListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateTypeModel"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal TemplateTypeListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	// check if the request has provider type filter
	var listResult []types.TemplateType
	if len(listRequest.ProviderTypes) > 0 {
		providerTypeFilter := listRequest.ProviderTypes[0]
		listResult, err = adapter.IncomingPort.ListTypesForProviderType(listRequest.GetSessionActor(), listRequest.SessionEmulator, providerTypeFilter)
	} else {
		listResult, err = adapter.IncomingPort.ListTypes(listRequest.GetSessionActor(), listRequest.SessionEmulator)
	}

	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.TemplateTypeListItemModel{}
		for _, templateType := range listResult {
			model := types.ConvertTypeToListItemModel(templateType)
			models = append(models, model)
		}

		listResponse.TemplateTypes = models
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal TemplateTypeListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleTemplateTypeGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleTemplateTypeGetQuery",
	})

	var getRequest cacao_common_service.TemplateTypeModel
	var getResponse cacao_common_service.TemplateTypeModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into TemplateTypeModel"
		logger.WithError(err).Error(errorMessage)
		getResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getResponse)
		if err2 != nil {
			errorMessage := "unable to marshal TemplateTypeModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.GetType(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetName())
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		getResponse = types.ConvertTypeToModel(session, getResult)
	} else {
		logger.Error(err)

		getResponse.Session = cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getResponse.ServiceError = cerr.GetBase()
		} else {
			getResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getResponse)
	if err != nil {
		errorMessage := "unable to marshal TemplateTypeModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

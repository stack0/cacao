package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection     cacao_common_messaging.QueryEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *QueryAdapter) getEventHandlerMapping() []cacao_common_messaging.QueryEventHandlerMapping {
	return []cacao_common_messaging.QueryEventHandlerMapping{
		{
			Subject:      cacao_common_service.TemplateTypeListQueryOp,
			EventHandler: adapter.handleTemplateTypeListQuery,
		},
		{
			Subject:      cacao_common_service.TemplateTypeGetQueryOp,
			EventHandler: adapter.handleTemplateTypeGetQuery,
		},
		{
			Subject:      cacao_common_service.TemplateSourceTypeListQueryOp,
			EventHandler: adapter.handleTemplateSourceTypeListQuery,
		},
		{
			Subject:      cacao_common_service.TemplateListQueryOp,
			EventHandler: adapter.handleTemplateListQuery,
		},
		{
			Subject:      cacao_common_service.TemplateGetQueryOp,
			EventHandler: adapter.handleTemplateGetQuery,
		},
		{
			Subject:      cacao_common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.ConnectNatsForService(&natsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)

	errResponse := cacao_common_service.Session{}
	errMessage := fmt.Sprintf("request %s is not handled", subject)
	errResponse.ServiceError = cacao_common_service.NewCacaoNotImplementedError(errMessage).GetBase()

	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		errorMessage := "unable to marshal Session into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

# Template Service

The Template Service is responsible for maintaining templates.

Templates are defined in git repositories, and they must have a `metadata.json` that defines various metadata about the template (type, parameters, etc.).

Template needs to be imported into CACAO before using it. During importing, templates are cloned to memory from git repositories,
and `metadata.json` for the template is read to retrieve metadata info about the template; the metadata is stored in database for faster lookup.

> Note: the metadata stored in database does **not** automatically get synced with the `metadata.json`.

# Configuration
configurations are set via environment variables.

| env name                            | description                                      | default                   |
|-------------------------------------|--------------------------------------------------|---------------------------|
| `TemplateMongoDBCollectionName`     | -                                                | template                  |
| `TemplateTypeMongoDBCollectionName` | -                                                | template_type             |
| `GitRAMSizeLimit`                   | max size of the git repo to clone to RAM (bytes) | 0 (unlimited)             |
| `CACAO_API_URL`                     | base url for REST API                            |
| `NATS_URL`                          | -                                                | nats://nats:4222          |
| `NATS_QUEUE_GROUP`                  | -                                                |                           |
| `NATS_WILDCARD_SUBJECT`             | Should be set to  `cacao.openstack.>`            | cyverse.>                 |
| `NATS_CLIENT_ID`                    | -                                                |                           |
| `NATS_MAX_RECONNECTS`               | -                                                | -1                        |
| `NATS_RECONNECT_WAIT`               | -                                                | -1                        |
| `NATS_REQUEST_TIMEOUT`              | -                                                | -1                        |
| `MONGODB_URL`                       | -                                                | mongodb://localhost:27017 |
| `MONGODB_DB_NAME`                   | -                                                |                           |

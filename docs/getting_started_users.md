# Getting Started

This document outlines getting started with CACAO as a user. This assumed you have an account and access to a running CACAO server and an OpenStack credential.


### Table of Contents
[[_TOC_]]


### Prepare CACAO CLI
The CACAO CLI is the simplest way to interact with the CACAO API.

In order to use the CACAO CLI, you will need to build it
1. Clone or download the repository from GitLab
	```bash
	git clone https://gitlab.com/cyverse/cacao.git
	# or download it without git:
	wget https://gitlab.com/cyverse/cacao/-/archive/master/cacao-master.zip
	unzip cacao.zip
	```
2. Build the `cacao` binary:
	```bash
	cd cacao/cmd
	go build -o cacao
	```
3. Move this to somewhere in your `$PATH`, or add this directory to your path:
	```bash
	sudo mv cacao /usr/local/bin/cacao
	# or
	export PATH=$PATH:$(pwd)
	```


### First Steps

In order to use the CACAO CLI, you need to log in:
```bash
$ cacao login
```

If this is your first time logging in, the CACAO CLI will prompt you for the address off the CACAO API.
```bash
Please provide address of CACAO API.
Format Should be: http://<ip address>:<port>      or    <ip address>:<port>
CACAO API address:
```

To access the production deployment of CACAO, enter `http://ca.cyverse.org/api`.  For local development environments, enter `ca.cyverse.local/api` (or `localhost` if you run services manually from cmd line).


### User Actions
Now you have everything you need to interact with your user's resources in CACAO, specifically the OpenStack resources.

```bash
cacao get user ${username}

# list all providers
cacao provider get
# get a specific provider
cacao provider get ${providerID}
# create a new provider
cacao provider create -name ${providerName} -type ${providerType} -url ${providerURL} -metadata ${metadataJSONString}
# create a new provider with metadata from file
cacao provider create -name ${providerName} -type ${providerType} -url ${providerURL} -metadata-file ${metadataJSONFilename}
# create a new provider from JSON file
cacao provider create -f provider.json
# update a provider
cacao provider update -name ${providerName} -type ${providerType} -url ${providerURL} -metadata ${metadataJSONString} ${providerID}
# delete a provider
cacao provider delete ${providerID}

# list all templates
cacao template get
# get a specific template
cacao template get ${templateID}
# create/import a new template
cacao template create -name ${templateName} -public true -source_type git -source_uri ${gitRepoURL} -source_branch ${gitBranch} -source_path ${subPath}
# create/import a new template from JSON file
cacao template create -f template.json
# sync a template with its source
cacao template sync ${templateID}
# delete a template
cacao template delete ${templateID}

# list all workspaces
cacao workspace get
# get a specific workspace
cacao workspace get ${workspaceID}
# create a new workspace
cacao workspace create -name ${workspaceName} -default_provider_id ${defaultProviderID}
# create a new workspace from JSON file
cacao workspace create -f workspace.json
# update a workspace
cacao workspace update -name ${workspaceName} -default_provider_id ${defaultProviderID} ${workspaceID}
# delete a workspace
cacao workspace delete ${workspaceID}

# list all credentials
cacao credential get
# get a specific credential
cacao credential get ${credentialID}
# create a new credential (example types: openstack, git, dockerhub, ssh, etc.)
cacao credential create -type ${type} -name ${credentialName} -value ${secret_string}
# create a new credential from JSON file
cacao credential create -f credential.json
# delete a credential
cacao credential delete ${credentialID}

# list all deployments
cacao deployment get
# get a specific deployment
cacao deployment get ${deploymentID}
# create a new deployment
cacao deployment create ${workspaceID} ${templateID} ${providerID} -cloud-cred ${credentialID}
# delete a deployment
cacao deployment delete ${deploymentID}

# list all runs in a deployment
cacao run get ${deploymentID}
# get a specific run
cacao run get ${deploymentID} ${runID}
# create a new run from JSON file
cacao run create ${deploymentID} -f run.json
```

### Create a prerequisite template
> Performed by Admin

First import a prerequiste template, use this template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/prerequisite/metadata.json .
Prerequiste template is a template that will be exectued before normal template (hence prerequisite), and it is specific to provider.
```bash
cacao template create -name openstack-prerequisite \
	-public true \
	-source_type git \
	-source_uri https://gitlab.com/cyverse/cacao-tf-os-ops.git \
	-source_branch main \
	-source_path prerequisite
```
Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create an OpenStack provider
> Performed by Admin

Create a metadata JSON file, use the one below as an example. Note that you need to populate the fields in the JSON object.

`provider_metadata.json`
```json
{
    "prerequisite_template": {
        "template_id": "<prerequisite-template-ID-from-previous-step>"
    },
    "public_ssh_key": "<your-public-ssh-key>",
    "external_network_name": "<openstack-external-network-name>",
	"external_network_uuid": "<openstack-external-network-uuid>",
	"external_subnet_uuids": [
		"<a-subnet-under-external-network>"
	],
	"project_domain_uuid": "<project_domain_uuid>",
	"OS_AUTH_URL": "<auth-url>",
    "OS_IDENTITY_API_VERSION": "3",
    "OS_INTERFACE": "<interface>",
    "OS_PROJECT_DOMAIN_ID": "<domain-id>",
    "OS_REGION_NAME": "<region-name>",
    "OS_USER_DOMAIN_NAME": "<domain-name>"
}
```

Then, create the provider
```bash
cacao provider create -name my-provider -type openstack -url https://example.com -metadata-file provider_metadata.json
```

Save the provider ID generated from the output (e.g. `provider-xxxxxxxxxxxxxxxxxxxx`)

### Create a template
> Performed by Admin
Import a template. Use this single-image template as an example https://gitlab.com/cyverse/cacao-tf-os-ops/-/blob/main/single-image/metadata.json .
```bash
cacao template create -name openstack-single-image \
	-public true \
	-source_type git \
	-source_uri https://gitlab.com/cyverse/cacao-tf-os-ops.git \
	-source_branch main \
	-source_path single-image
```

Save the template ID generated from the output (e.g. `template-xxxxxxxxxxxxxxxxxxxx`)

### Create a workspace
Create a workspace with the default provider ID set to the one from previous step.
```bash
cacao workspace create -name my-workspace -default_provider_id ${providerID}
```

Save the workspace ID generated from the output (e.g. `workspace-xxxxxxxxxxxxxxxxxxxx`)

### Create a credential
Construct a JSON file that contains your openstack credential, use the example below as a guide. Note that the structure is very similar to the `.openrc` file the openstack generated.

`openstack_credential.json`
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "",
  "OS_USERNAME": "os_username",
  "OS_PASSWORD": "os_password"
}
```

`openstack_credential.json` with OpenStack application credential
```json
{
  "OS_REGION_NAME": "os_region_name",
  "OS_PROJECT_DOMAIN_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_INTERFACE": "os_interface",
  "OS_PROJECT_ID": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
  "OS_USER_DOMAIN_NAME": "os_user_domain_name",
  "OS_PROJECT_NAME": "os_project_name",
  "OS_IDENTITY_API_VERSION": "3",
  "OS_AUTH_URL": "https://os-auth-url",
  "OS_AUTH_TYPE": "v3applicationcredential",
  "OS_APPLICATION_CREDENTIAL_ID": "os_application_credential_id",
  "OS_APPLICATION_CREDENTIAL_SECRET": "os_application_credential_secret"
}
```

Create credential with the JSON file.
```bash
cacao credential create -type openstack -name ${providerID} -value-file openstack_credential.json
```

> In order to the images and flavor endpoints to work, you need to create a credential with the provider ID as the name of the credential. Also the credential value cannot be an application credential, and must be username&password.

> Note: you can create a credential for the images and flavor endpoints, and another credential for the deployment with a different name.

### Create a deployment
Create a deployment with the workspace, template, and provider from previous steps
```bash
cacao deployment create ${workspaceID} ${templateID} ${providerID} -cloud-cred ${credentialID}
```

> If you run the command from previous step, then the credentialID you should use is the providerID from eariler step. `${credentialID}` == `${providerID}`

Save the deployment ID generated from the output (e.g. `deployment-xxxxxxxxxxxxxxxxxxxx`)

### Run the deployment
First, create a JSON file for the request. 

`run.json`
```json
{
    "cloud_credentials": ["<credential-ID-from-above>"],
    "git_credential_id": "",
    "parameters": [
        {
            "key": "flavor",
            "value": "<openstack-instance-size>"
        },
        {
            "key": "image",
            "value": "<openstack-image-uuid>"
        },
        {
            "key": "instance_count",
            "value": "1"
        },
        {
            "key": "instance_name",
            "value": "my-cacao-instane"
        },
        {
            "key": "power_state",
            "value": "active"
        }
    ]
}

```

Run the deployment
```bash
cacao run create ${deploymentID} -f run.json
```
Save the run ID generated from the output (e.g. `run-xxxxxxxxxxxxxxxxxxxx`)

### Check status of the deployment & run

```bash
cacao run create ${deploymentID} ${runID} | jq -r .status
```

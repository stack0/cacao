# Logging and Error Conventions in CACAO
This document will explain some of the decisions made for logging in CACAO and will outline some of the requirements for adding new logs that are consistent with the rest of the project.

We use the [`logrus` package](https://github.com/Sirupsen/logrus) for logging. This package is really useful because it allows for all the regular logging statements that would be used by the built-in `log` package but also adds functionality like log levels, JSON (and other) formatting, and extra fields.


### Table of Contents
[[_TOC_]]


## Conventions/Rules
1. In general, do not output logs in functions that return errors. Instead, return a descriptive error that can be used by the consuming function in its logs

2. When returning errors, use a lower-case letter to start the message. This makes the error message easier to read when it is combined with other text and logged

3. Use `log.WithFields()` when logging and be sure to include: `package`, `function`. And Use `log.WithError()` to log errors.

4. Create a `logger` in functions with multiple logging opportunities to make it easier to log with common fields:
```go
import (
 log "github.com/sirupsen/logrus"
)

logger := log.WithFields(log.Fields{
        "package":  "packageName",
        "function": "functionName",
})

logger.Info("some info")

logger.WithError(err).Error("we had an error")
```

## ElasticSearch, Fluentd, Kibana (EFK)

To help with EFK integration, CACAO containers add a couple of labels:

* `cacao.cyverse.org/app`: This is always set to `cacao`
* `cacao.cyverse.org/type`: This label has three values `core` for core cacao components, `edge` for edge cloud components, and `helper` for cacao components that provide helper functionality (e.g. the wiretap service) 

Other services, such as postgresql or nats, will not have these labels. Developers can use these labels to filter and search for text in logs and other fields.

To access kibana, you can find the base path, username, and password in `install/configs/password.kibana.txt`, if it is not set in the config.yml.

## Resources/Refernces
- https://github.com/Sirupsen/logrus
- https://www.datadoghq.com/blog/go-logging/
- https://blog.maddevs.io/how-to-start-with-logging-in-golang-projects-part-1-3e3a708b75be
- https://blog.golang.org/error-handling-and-go
- https://blog.golang.org/go1.13-errors

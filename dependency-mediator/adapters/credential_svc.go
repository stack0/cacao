package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// CredentialService implements ports.CredentialService
type CredentialService struct {
	credentialSvcClientCreator credentialSvcClientCreator
}

// NewCredentialService ...
func NewCredentialService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ports.CredentialService {
	return CredentialService{
		credentialSvcClientCreator: func(session types.Session) (service.CredentialClient, error) {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return service.NewNatsCredentialClient(context.TODO(), session.Actor, session.Emulator, newNatsConf, stanConf)
		},
	}
}

// Get fetches a credential
func (c CredentialService) Get(session types.Session, id string) (service.Credential, error) {
	client, err := c.credentialSvcClientCreator(session)
	if err != nil {
		return nil, err
	}
	cred, err := client.Get(id)
	if err != nil {
		return nil, err
	}
	return cred, nil
}

// Delete deletes a credential
func (c CredentialService) Delete(session types.Session, id string) error {
	client, err := c.credentialSvcClientCreator(session)
	if err != nil {
		return err
	}
	_, err = client.Delete(id)
	return err
}

// CredentialSvcClient is redefinition of service.CredentialClient, it is to allow mocks to be generated locally in this repo.
type CredentialSvcClient interface {
	service.CredentialClient
}

type credentialSvcClientCreator func(session types.Session) (service.CredentialClient, error)

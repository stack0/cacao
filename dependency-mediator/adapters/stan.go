package adapters

import (
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// StanAdapter ...
type StanAdapter struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
	conn     *messaging.StanConnection
}

// NewStanAdapter ...
func NewStanAdapter(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) *StanAdapter {
	newNatsConf := natsConf
	newNatsConf.ClientID = newNATSClientID()
	return &StanAdapter{
		natsConf: newNatsConf,
		stanConf: stanConf,
		conn:     nil,
	}
}

// Start ...
func (s *StanAdapter) Start(handlers map[common.EventType]types.DeletionEventHandler) error {
	conn, err := messaging.ConnectStanForService(&s.natsConf, &s.stanConf, s.createMappingList(handlers))
	if err != nil {
		return err
	}
	s.conn = conn
	return nil
}

// Close ...
func (s StanAdapter) Close() {
	if s.conn == nil {
		return
	}
	err := s.conn.Disconnect()
	if err != nil {
		log.WithError(err).Error("fail to close connection")
	}
	log.Info("connection closed")
}

func (s *StanAdapter) createMappingList(handlers map[common.EventType]types.DeletionEventHandler) []messaging.StreamingEventHandlerMapping {
	var mappingList []messaging.StreamingEventHandlerMapping
	for eventType, handler := range handlers {
		mappingList = append(mappingList, s.createMapping(eventType, handler))
	}
	return mappingList
}

func (s *StanAdapter) createMapping(eventType common.EventType, handler types.DeletionEventHandler) messaging.StreamingEventHandlerMapping {
	return messaging.StreamingEventHandlerMapping{
		Subject:           eventType,
		CloudEventHandler: deletionHandlerWrapper(s, handler), // s (StanAdapter) is used instead of s.conn because s.conn is not yet initialized here. so cannot be captured by anonymous function.
	}
}

// PublishCloudEvent implements cloudEventPublisher, this is needed for StanAdapter to be captured in the anonymous function returned by deletionHandlerWrapper().
func (s *StanAdapter) PublishCloudEvent(ce *cloudevents.Event) error {
	return s.conn.PublishCloudEvent(ce)
}

// wrap deletion handler into a cloudevent handler
func deletionHandlerWrapper(conn cloudEventPublisher, handler types.DeletionEventHandler) messaging.StreamingCloudEventHandler {
	return func(requestCe *cloudevents.Event) error {
		logger := log.WithFields(log.Fields{
			"package":  "adapters",
			"function": "StanAdapter.deletionHandlerWrapper",
		})
		var request types.DeletionRequest
		err := json.Unmarshal(requestCe.Data(), &request)
		if err != nil {
			return err
		}
		logger = logger.WithFields(log.Fields{
			"requestType": requestCe.Type(),
			"objID":       request.ID,
		})
		logger.Info("request received")

		tid := messaging.GetTransactionID(requestCe)
		response := handler(request, tid)

		responseCe, err := createDeletionCloudEventResponse(requestCe, response)
		if err != nil {
			logger.WithError(err).Error("fail to create cloudevent for deletion response")
			return err
		}
		logger = logger.WithField("responseType", responseCe.Type())
		err = conn.PublishCloudEvent(responseCe)
		if err != nil {
			logger.WithError(err).Error("fail to publish deletion response")
			return err
		}
		logger.Info("response published")
		return nil
	}
}

func createDeletionCloudEventResponse(requestCe *cloudevents.Event, response types.DeletionResponse) (*cloudevents.Event, error) {
	respEventType := getResponseEventType(common.EventType(requestCe.Type()), response)
	responseCe, err := messaging.CreateCloudEventWithTransactionID(response, string(respEventType), "dependency-mediator", messaging.GetTransactionID(requestCe))
	if err != nil {
		return nil, err
	}
	return &responseCe, nil
}

func getResponseEventType(reqEventType common.EventType, response types.DeletionResponse) common.EventType {
	if response.Errored() {
		return responseEventTypeMapping[reqEventType][responseDeleteErrored]
	}
	if response.Deleted {
		return responseEventTypeMapping[reqEventType][responseDeleted]
	}
	if response.HasDependent {
		return responseEventTypeMapping[reqEventType][responseHasDependent]
	}
	return ""
}

type responseType int

const (
	responseDeleted       responseType = 0
	responseDeleteErrored responseType = 1
	responseHasDependent  responseType = 2
)

var responseEventTypeMapping = map[common.EventType]map[responseType]common.EventType{
	service.EventDepMedDeleteProvider: {
		responseDeleted:       service.EventDepMedProviderDeleted,
		responseDeleteErrored: service.EventDepMedProviderDeletionErrored,
		responseHasDependent:  service.EventDepMedProviderHasDependent,
	},
	service.EventDepMedDeleteTemplate: {
		responseDeleted:       service.EventDepMedTemplateDeleted,
		responseDeleteErrored: service.EventDepMedTemplateDeletionErrored,
		responseHasDependent:  service.EventDepMedTemplateHasDependent,
	},
	service.EventDepMedDeleteWorkspace: {
		responseDeleted:       service.EventDepMedWorkspaceDeleted,
		responseDeleteErrored: service.EventDepMedWorkspaceDeletionErrored,
		responseHasDependent:  service.EventDepMedWorkspaceHasDependent,
	},
	service.EventDepMedDeleteCredential: {
		responseDeleted:       service.EventDepMedCredentialDeleted,
		responseDeleteErrored: service.EventDepMedCredentialDeletionErrored,
		responseHasDependent:  service.EventDepMedCredentialHasDependent,
	},
}

type cloudEventPublisher interface {
	PublishCloudEvent(ce *cloudevents.Event) error
}

func newNATSClientID() string {
	return common.NewID("dependency-mediator").String()
}

package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"time"
)

type openstackProviderSvcClientCreator func(types.Session, common.ID) (providers.OpenStackProvider, error)

// ProviderOpenStackService implements ports.ProviderOpenStackService
type ProviderOpenStackService struct {
	svcClientCreator openstackProviderSvcClientCreator
}

// NewProviderOpenStackService ...
func NewProviderOpenStackService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ProviderOpenStackService {
	return ProviderOpenStackService{
		svcClientCreator: func(session types.Session, providerID common.ID) (providers.OpenStackProvider, error) {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return providers.NewOpenStackProvider(providerID, &newNatsConf, &stanConf), nil
		},
	}
}

// DeleteApplicationCredential delete the application credential stored in a credential (specified by credential ID)
// via the openstack provider service.
func (p ProviderOpenStackService) DeleteApplicationCredential(session types.Session, providerID common.ID, credID string) error {
	svcClient, err := p.svcClientCreator(session, providerID)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err = svcClient.DeleteApplicationCredential(
		ctx,
		&service.Session{
			SessionActor:    session.Actor,
			SessionEmulator: session.Emulator,
		},
		credID,
	)
	if err == nil {
		return nil
	}
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		return err
	}
	if svcErr.StandardError() == providers.NotOpenStackCredentialErrorMessage {
		// If the credential is ill-formed. Which means it does not even have the correct schema for any types of openstack credential (password, app cred, token).
		return nil
	} else if svcErr.StandardError() == providers.NotApplicationCredentialErrorMessage {
		// If the credential is ill-formed, but it is at least an OpenStack credential, just not an application credential
		return nil
	}
	// if other CacaoError, return the error as is
	return err
}

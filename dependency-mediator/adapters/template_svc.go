package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type templateSvcClientCreator func(types.Session) (service.TemplateClient, error)

// TemplateService implements ports.TemplateService
type TemplateService struct {
	svcClientCreator templateSvcClientCreator
}

// NewTemplateService ...
func NewTemplateService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ports.TemplateService {
	return TemplateService{
		svcClientCreator: func(session types.Session) (service.TemplateClient, error) {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return service.NewNatsTemplateClient(context.TODO(), session.Actor, session.Emulator, newNatsConf, stanConf)
		},
	}
}

// Get fetches template
func (t TemplateService) Get(session types.Session, template common.ID) (service.Template, error) {
	svcClient, err := t.svcClientCreator(session)
	if err != nil {
		return nil, err
	}
	return svcClient.Get(template)
}

// Delete deletes template
func (t TemplateService) Delete(session types.Session, template common.ID) error {
	svcClient, err := t.svcClientCreator(session)
	if err != nil {
		return err
	}
	return svcClient.Delete(template)
}

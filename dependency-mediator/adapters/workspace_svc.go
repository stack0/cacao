package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type workspaceSvcClientCreator func(types.Session) (service.WorkspaceClient, error)

// WorkspaceService implements ports.ProviderMS
type WorkspaceService struct {
	svcClientCreator workspaceSvcClientCreator
}

// NewWorkspaceService ...
func NewWorkspaceService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ports.WorkspaceService {
	return WorkspaceService{
		svcClientCreator: func(session types.Session) (service.WorkspaceClient, error) {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return service.NewNatsWorkspaceClient(context.TODO(), session.Actor, session.Emulator, newNatsConf, stanConf)
		},
	}
}

// Get fetches provider
func (w WorkspaceService) Get(session types.Session, id common.ID) (service.Workspace, error) {
	svcClient, err := w.svcClientCreator(session)
	if err != nil {
		return nil, err
	}
	return svcClient.Get(id)
}

// Delete deletes the workspace
func (w WorkspaceService) Delete(session types.Session, workspace common.ID) error {
	svcClient, err := w.svcClientCreator(session)
	if err != nil {
		return err
	}
	return svcClient.Delete(workspace)
}

package adapters

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"reflect"
	"testing"
)

func TestDeploymentService_Get(t *testing.T) {
	type fields struct {
		svcClientCreator func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator
	}
	type args struct {
		session types.Session
		id      common.ID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *service.Deployment
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator {
					return func(actor types.Session) service.DeploymentClient {
						assert.Equal(t, "actor-123", actor.Actor)
						assert.Equal(t, "emulator-123", actor.Emulator)
						svcClient.On("Get", common.ID("deployment-aaaaaaaaaa")).Return(&service.Deployment{
							ID:   "deployment-aaaaaaaaaa",
							Name: "foobar",
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				id: common.ID("deployment-aaaaaaaaaa"),
			},
			want: &service.Deployment{
				ID:   "deployment-aaaaaaaaaa",
				Name: "foobar",
			},
			wantErr: false,
		},
		{
			name: "errored",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator {
					return func(actor types.Session) service.DeploymentClient {
						assert.Equal(t, "actor-123", actor.Actor)
						assert.Equal(t, "emulator-123", actor.Emulator)
						svcClient.On("Get", common.ID("deployment-aaaaaaaaaa")).Return(nil, fmt.Errorf("failed"))
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				id: common.ID("deployment-aaaaaaaaaa"),
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSvcClient := &mocks.DeploymentSvcClient{}
			d := DeploymentService{
				svcClientCreator: tt.fields.svcClientCreator(t, mockSvcClient),
			}
			got, err := d.Get(tt.args.session, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
			mockSvcClient.AssertExpectations(t)
		})
	}
}

func TestDeploymentService_CheckDependOnCredential(t *testing.T) {
	type fields struct {
		svcClientCreator func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator
	}
	type args struct {
		session types.Session
		credID  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "success - 1 dependent",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator {
					return func(actor types.Session) service.DeploymentClient {
						assert.Equal(t, "actor-123", actor.Actor)
						assert.Equal(t, "emulator-123", actor.Emulator)
						svcClient.On("Search", service.DeploymentListOption{
							Filter: service.DeploymentFilter{
								CredentialID: "cred-id-123",
							},
						}).Return(mockDeploymentList{
							{
								ID: "deployment-aaaaaaaaaa",
								CloudCredentials: map[string]common.ID{
									"cred-id-123": "provider-aaaaaaaaaa",
								},
							},
						}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "success - 0 dependent",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator {
					return func(actor types.Session) service.DeploymentClient {
						assert.Equal(t, "actor-123", actor.Actor)
						assert.Equal(t, "emulator-123", actor.Emulator)
						svcClient.On("Search", service.DeploymentListOption{
							Filter: service.DeploymentFilter{
								CredentialID: "cred-id-123",
							},
						}).Return(mockDeploymentList{}, nil)
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "errored",
			fields: fields{
				svcClientCreator: func(t *testing.T, svcClient *mocks.DeploymentSvcClient) deploymentSvcClientCreator {
					return func(actor types.Session) service.DeploymentClient {
						assert.Equal(t, "actor-123", actor.Actor)
						assert.Equal(t, "emulator-123", actor.Emulator)
						svcClient.On("Search", service.DeploymentListOption{
							Filter: service.DeploymentFilter{
								CredentialID: "cred-id-123",
							},
						}).Return(nil, fmt.Errorf("failed"))
						return svcClient
					}
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
				},
				credID: "cred-id-123",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockSvcClient := &mocks.DeploymentSvcClient{}
			d := DeploymentService{
				svcClientCreator: tt.fields.svcClientCreator(t, mockSvcClient),
			}
			got, err := d.CheckDependOnCredential(tt.args.session, tt.args.credID)
			if (err != nil) != tt.wantErr {
				t.Errorf("CheckDependOnCredential() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			assert.Equalf(t, tt.want, got, "CheckDependOnCredential(%v, %v)", tt.args.session, tt.args.credID)
			mockSvcClient.AssertExpectations(t)
		})
	}
}

type mockDeploymentList []service.Deployment

func (m mockDeploymentList) GetOptions() service.DeploymentListOption {
	panic("not implemented")
}

func (m mockDeploymentList) GetDeployments() []service.Deployment {
	return []service.Deployment(m)
}

func (m mockDeploymentList) GetStart() int {
	panic("not implemented")
}

func (m mockDeploymentList) GetSize() int {
	return len(m)
}

func (m mockDeploymentList) GetNextStart() int {
	panic("not implemented")
}

package adapters

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"testing"
	"time"
)

func TestProviderService_CheckDependOnTemplate(t *testing.T) {

	type fields struct {
		svcClientCreator providerSvcClientCreator
	}
	type args struct {
		session  types.Session
		template common.ID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "no provider",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					svcClient.On("List").Return([]service.Provider{}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, has dependent",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					metadataStr := `{
									    "prerequisite_template": {
									        "template_id": "template-aaaaaaaaaaaaaaaaaaaa"
									    }
									}`
					var metadata map[string]interface{}
					_ = json.Unmarshal([]byte(metadataStr), &metadata)
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  metadata,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "one openstack provider, no dependent",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					metadataStr := `{
									    "prerequisite_template": {
									        "template_id": "template-bbbbbbbbbbbbbbbbbbbb"
									    }
									}`
					var metadata map[string]interface{}
					_ = json.Unmarshal([]byte(metadataStr), &metadata)
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  metadata,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, no prerequisite template",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					metadataStr := `{
										"foo": "bar"
									}`
					var metadata map[string]interface{}
					_ = json.Unmarshal([]byte(metadataStr), &metadata)
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  metadata,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, empty prerequisite template",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					metadataStr := `{
									    "prerequisite_template": {
									        "template_id": ""
									    }
									}`
					var metadata map[string]interface{}
					_ = json.Unmarshal([]byte(metadataStr), &metadata)
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  metadata,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "one openstack provider, no metadata",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  nil,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session: types.Session{
					Actor:    "actor-123",
					Emulator: "emulator-123",
					TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
				},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "non-openstack provider",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					svcClient.On("List").Return([]service.Provider{
						&service.ProviderModel{
							ID:        "provider-aaaaaaaaaaaaaaaaaaaa",
							Name:      "",
							Type:      "not-openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  nil,
						},
					}, nil)
					return svcClient, nil
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "list provider failed",
			fields: fields{
				svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
					svcClient := &mocks.ProviderSvcClient{}
					svcClient.On("List").Return(nil, fmt.Errorf("failed"))
					return svcClient, nil
				},
			},
			args: args{
				session:  types.Session{Actor: "actor-123", Emulator: "emulator-123", TID: "tid-aaaaaaaaaaaaaaaaaaaa"},
				template: "template-aaaaaaaaaaaaaaaaaaaa",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := ProviderService{
				svcClientCreator: tt.fields.svcClientCreator,
			}
			got, err := p.CheckDependOnTemplate(tt.args.session, tt.args.template)
			fmtStr := fmt.Sprintf("CheckDependOnTemplate(%v, %v)", tt.args.session, tt.args.template)
			if tt.wantErr {
				assert.Errorf(t, err, fmtStr)
			} else {
				assert.NoErrorf(t, err, fmtStr)
			}
			assert.Equalf(t, tt.want, got, fmtStr)
		})
	}
}

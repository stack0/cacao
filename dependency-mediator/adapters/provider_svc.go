package adapters

import (
	"context"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

type providerSvcClientCreator func(types.Session) (service.ProviderClient, error)

// ProviderService implements ports.ProviderMS
type ProviderService struct {
	svcClientCreator providerSvcClientCreator
}

// NewProviderService ...
func NewProviderService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ports.ProviderService {
	return ProviderService{
		svcClientCreator: func(session types.Session) (service.ProviderClient, error) {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return service.NewNatsProviderClient(context.TODO(), session.Actor, session.Emulator, newNatsConf, stanConf)
		},
	}
}

// Get fetches provider
func (p ProviderService) Get(session types.Session, id common.ID) (service.Provider, error) {
	svcClient, err := p.svcClientCreator(session)
	if err != nil {
		return nil, err
	}
	return svcClient.Get(id)
}

// CheckDependOnTemplate checks if there is any provider depends on the specified template.
func (p ProviderService) CheckDependOnTemplate(session types.Session, template common.ID) (bool, error) {
	// TODO needs to take ownership in consideration, e.g. if a public template owned by A is used by private provider owned by B, and A tries to delete the template
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderService.CheckDependOnTemplate",
		"template": template,
	})
	svcClient, err := p.svcClientCreator(session)
	if err != nil {
		return false, err
	}
	list, err := svcClient.List()
	if err != nil {
		return false, err
	}
	for _, provider := range list {
		if provider.GetType() != "openstack" {
			continue
		}
		var prerequisiteTemplate struct {
			TemplateID common.ID `mapstructure:"template_id"`
		}
		prerequisiteTemplateRaw, ok := provider.GetMetadata()["prerequisite_template"]
		if !ok {
			logger.Error("provider metadata has no 'prerequisite_template'")
			continue
		}
		err = mapstructure.Decode(prerequisiteTemplateRaw, &prerequisiteTemplate)
		if err != nil {
			logger.WithError(err).Error("fail to parse 'prerequisite_template' in provider metadata")
			continue
		}
		if prerequisiteTemplate.TemplateID == template {
			return true, nil
		}
	}
	return false, nil
}

// Delete deletes the provider
func (p ProviderService) Delete(session types.Session, provider common.ID) error {
	svcClient, err := p.svcClientCreator(session)
	if err != nil {
		return err
	}
	_, err = svcClient.Delete(provider)
	return err
}

// ProviderSvcClient is redefinition of service.ProviderClient, it is to allow mocks to be generated locally in this repo.
type ProviderSvcClient interface {
	service.ProviderClient
}

package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// DeploymentSvcClient is redefinition of service.DeploymentClient, it is to allow mocks to be generated locally in this repo.
type DeploymentSvcClient interface {
	service.DeploymentClient
}

type deploymentSvcClientCreator func(actor types.Session) service.DeploymentClient

// DeploymentService implements ports.DeploymentMS
type DeploymentService struct {
	svcClientCreator deploymentSvcClientCreator
}

// NewDeploymentService ...
func NewDeploymentService(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) DeploymentService {
	return DeploymentService{
		svcClientCreator: func(session types.Session) service.DeploymentClient {
			newNatsConf := natsConf
			newNatsConf.ClientID = newNATSClientID()
			return service.NewDeploymentClient(context.TODO(), session.Actor, session.Emulator, newNatsConf, stanConf)
		},
	}
}

// Get fetches deployment
func (d DeploymentService) Get(session types.Session, id common.ID) (*service.Deployment, error) {
	return d.svcClientCreator(session).Get(id)
}

// CheckDependOnCredential checks if there is any deployment depends on the specified credential.
func (d DeploymentService) CheckDependOnCredential(session types.Session, credID string) (bool, error) {
	result, err := d.svcClientCreator(session).Search(service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			CredentialID: credID,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnTemplate checks if there is any deployment depends on the specified template.
func (d DeploymentService) CheckDependOnTemplate(session types.Session, template common.ID) (bool, error) {
	result, err := d.svcClientCreator(session).Search(service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			Template: template,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnProvider checks if there is any deployment depends on the specified provider
func (d DeploymentService) CheckDependOnProvider(session types.Session, provider common.ID) (bool, error) {
	result, err := d.svcClientCreator(session).Search(service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			PrimaryCloudProvider: provider,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// CheckDependOnWorkspace checks if there is any deployment depends on the specified workspace.
func (d DeploymentService) CheckDependOnWorkspace(session types.Session, workspace common.ID) (bool, error) {
	result, err := d.svcClientCreator(session).Search(service.DeploymentListOption{
		Filter: service.DeploymentFilter{
			Workspace: workspace,
		},
	})
	if err != nil {
		return false, err
	}
	if result.GetSize() > 0 {
		return true, nil
	}
	return false, nil
}

// Delete deletes the deployment. Note that deployment deletion is async, so this will simply starts the deletion process.
func (d DeploymentService) Delete(session types.Session, id common.ID) error {
	deleted, err := d.svcClientCreator(session).Delete(session.TID, id)
	if err != nil {
		return err
	}
	log.WithField("deleted", deleted).Info()
	return nil
}

package ports

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// EventSrc is source of incoming events
type EventSrc interface {
	Start(handlers map[common.EventType]types.DeletionEventHandler) error
}

// TemplateService provides access to template microservice
type TemplateService interface {
	Get(types.Session, common.ID) (service.Template, error)
	Delete(types.Session, common.ID) error
}

// ProviderService provides access to provider metadata service
type ProviderService interface {
	Get(types.Session, common.ID) (service.Provider, error)
	CheckDependOnTemplate(session types.Session, template common.ID) (bool, error)
	Delete(types.Session, common.ID) error
}

// WorkspaceService provides access to workspace microservice
type WorkspaceService interface {
	Get(types.Session, common.ID) (service.Workspace, error)
	Delete(types.Session, common.ID) error
}

// CredentialService provides access to credential microservice
type CredentialService interface {
	Get(session types.Session, id string) (service.Credential, error)
	Delete(session types.Session, id string) error
}

// DeploymentService provides access to deployment microservice
type DeploymentService interface {
	Get(types.Session, common.ID) (*service.Deployment, error)
	CheckDependOnCredential(session types.Session, credID string) (bool, error)
	CheckDependOnTemplate(session types.Session, template common.ID) (bool, error)
	CheckDependOnProvider(session types.Session, provider common.ID) (bool, error)
	CheckDependOnWorkspace(session types.Session, workspace common.ID) (bool, error)
	Delete(types.Session, common.ID) error
}

// ProviderOpenStackService provides access to provider openstack service
type ProviderOpenStackService interface {
	DeleteApplicationCredential(session types.Session, providerID common.ID, credID string) error
}

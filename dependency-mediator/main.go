package main

import (
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/dependency-mediator/adapters"
	"gitlab.com/cyverse/cacao/dependency-mediator/domain"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

var config types.Config

func init() {
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Fatal("fail to load config from env var")
	}
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)
}

func main() {
	stan := adapters.NewStanAdapter(config.NatsConfig, config.StanConfig)
	defer stan.Close()

	credSvc := adapters.NewCredentialService(config.NatsConfig, config.StanConfig)
	templateSvc := adapters.NewTemplateService(config.NatsConfig, config.StanConfig)
	providerSvc := adapters.NewProviderService(config.NatsConfig, config.StanConfig)
	workspaceSvc := adapters.NewWorkspaceService(config.NatsConfig, config.StanConfig)
	deploymentSvc := adapters.NewDeploymentService(config.NatsConfig, config.StanConfig)
	providerOpenStackSvc := adapters.NewProviderOpenStackService(config.NatsConfig, config.StanConfig)

	svc := domain.NewService(stan, credSvc, templateSvc, providerSvc, providerOpenStackSvc, workspaceSvc, deploymentSvc)
	err := svc.Start()
	if err != nil {
		log.WithError(err).Fatal("fail to start service")
	}
}

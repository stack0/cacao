package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"strings"
)

// CredentialDeletionHandler handles credential deletion.
// Credential is depdended by deployment.
// Note that even though credential is used to import/sync template, it is specified every time it is used (import/sync),
// so there isn't a reference to a credential that needs to be maintained. Therefore, we don't need to check for template
// when deleting credential.
type CredentialDeletionHandler struct {
	credSvc              ports.CredentialService
	templateSvc          ports.TemplateService
	deploymentSvc        ports.DeploymentService
	openstackProviderSvc ports.ProviderOpenStackService
}

// Handle ...
func (h CredentialDeletionHandler) Handle(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	return h.handle(types.Session{
		Actor:    req.SessionActor,
		Emulator: req.SessionEmulator,
		TID:      tid,
	}, req.ID)
}

func (h CredentialDeletionHandler) handle(session types.Session, credID string) types.DeletionResponse {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "CredentialDeletionHandler.handle",
	})
	// check credential exists and accessible
	cred, err := h.credSvc.Get(session, credID)
	if err != nil {
		return errResponse(session, credID, err)
	}

	result, timeout := h.checkDependent(session, credID)
	if result.Errored() {
		return errResponse(session, credID, result.Err)
	}
	if timeout {
		return timeoutResponse(session, credID)
	}
	if result.Depended {
		return hasDependentResponse(session, credID)
	}

	err = h.preDeletionCleanup(session, cred)
	if err != nil {
		logger.WithError(err).Error("fail to perform pre-deletion cleanup for credential")
		return errResponse(session, credID, err)
	}

	err = h.credSvc.Delete(session, credID)
	if err != nil {
		return errResponse(session, credID, err)
	}

	return deletedResponse(session, credID)
}

func (h CredentialDeletionHandler) checkDependent(actor types.Session, credID string) (result types.DependencyResult, timeout bool) {
	const numResults = 1
	resultChan := make(chan types.DependencyResult, numResults)

	go func() { resultChan <- h.checkDeployment(actor, credID) }()

	return waitDependencyResults(resultChan, numResults)
}

func (h CredentialDeletionHandler) checkDeployment(actor types.Session, credID string) types.DependencyResult {
	depended, err := h.deploymentSvc.CheckDependOnCredential(actor, credID)
	if err != nil {
		return types.ErrWhenCheckingDependency(types.DeploymentDependent, err)
	}
	if depended {
		return types.Depended(types.DeploymentDependent)
	}
	return types.NoneDepended(types.DeploymentDependent)
}

func (h CredentialDeletionHandler) preDeletionCleanup(actor types.Session, credential service.Credential) error {
	switch credential.GetType() {
	case "openstack":
		return h.cleanupOpenStackCredential(actor, credential)
	default:
		return nil
	}
}

func (h CredentialDeletionHandler) cleanupOpenStackCredential(actor types.Session, credential service.Credential) error {
	providerID, err := providerIDFromTags(credential)
	if err != nil {
		return err
	}
	err = h.openstackProviderSvc.DeleteApplicationCredential(actor, providerID, credential.GetID())
	if err != nil {
		return err
	}
	return nil
}

func providerIDFromTags(credential service.Credential) (common.ID, error) {
	model := credential.(*service.CredentialModel)
	// cacao_provider takes priority over provider-xxxxxxxxxxxxxxxxxxxx
	for key, value := range model.Tags {
		if key == "cacao_provider" {
			id := common.ID(value)
			if id.Validate() {
				return id, nil
			}
		}
	}
	for key := range model.Tags {
		if strings.HasPrefix(key, "provider-") {
			id := common.ID(key)
			if id.Validate() {
				return id, nil
			}
		}
	}
	return "", service.NewCacaoGeneralError("credential is not associate with any provider ID")
}

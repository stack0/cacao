package domain

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
)

// WorkspaceDeletionHandler handles workspaceSvc deletion.
// workspace is depended by deployments, thus a workspace cannot be deleted when a deployment is using it.
type WorkspaceDeletionHandler struct {
	workspaceSvc  ports.WorkspaceService
	deploymentSvc ports.DeploymentService
}

// Handle ...
func (h WorkspaceDeletionHandler) Handle(req types.DeletionRequest, tid common.TransactionID) types.DeletionResponse {
	return h.handle(types.Session{
		Actor:    req.SessionActor,
		Emulator: req.SessionEmulator,
		TID:      tid,
	}, common.ID(req.ID))
}

func (h WorkspaceDeletionHandler) handle(session types.Session, workspaceID common.ID) types.DeletionResponse {
	// check workspaceSvc exists and accessible
	_, err := h.workspaceSvc.Get(session, workspaceID)
	if err != nil {
		return h.errResponse(session, workspaceID, err)
	}
	result, timeout := h.checkDependent(session, workspaceID)
	if result.Errored() {
		return h.errResponse(session, workspaceID, result.Err)
	}
	if timeout {
		return h.timeoutResponse(session, workspaceID)
	}
	if result.Depended {
		return h.hasDependentResponse(session, workspaceID)
	}

	err = h.workspaceSvc.Delete(session, workspaceID)
	if err != nil {
		return h.errResponse(session, workspaceID, err)
	}

	return h.deletedResponse(session, workspaceID)
}

func (h WorkspaceDeletionHandler) checkDependent(session types.Session, workspaceID common.ID) (result types.DependencyResult, timeout bool) {
	const numResults = 1
	resultChan := make(chan types.DependencyResult, numResults)

	go func() { resultChan <- h.checkDeployment(session, workspaceID) }()

	return waitDependencyResults(resultChan, numResults)
}

func (h WorkspaceDeletionHandler) checkDeployment(session types.Session, workspaceID common.ID) types.DependencyResult {
	depended, err := h.deploymentSvc.CheckDependOnWorkspace(session, workspaceID)
	if err != nil {
		return types.ErrWhenCheckingDependency(types.DeploymentDependent, err)
	}
	if depended {
		return types.Depended(types.DeploymentDependent)
	}
	return types.NoneDepended(types.DeploymentDependent)
}

func (h WorkspaceDeletionHandler) errResponse(session types.Session, workspace common.ID, err error) types.DeletionResponse {
	return errResponse(session, workspace.String(), err)
}

func (h WorkspaceDeletionHandler) timeoutResponse(session types.Session, workspace common.ID) types.DeletionResponse {
	return timeoutResponse(session, workspace.String())
}

func (h WorkspaceDeletionHandler) hasDependentResponse(session types.Session, workspace common.ID) types.DeletionResponse {
	return hasDependentResponse(session, workspace.String())
}

func (h WorkspaceDeletionHandler) deletedResponse(session types.Session, workspace common.ID) types.DeletionResponse {
	return deletedResponse(session, workspace.String())
}

package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/dependency-mediator/ports"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"time"
)

// Domain entry point object of the service
type Domain struct {
	eventSource ports.EventSrc

	credentialSvc        ports.CredentialService
	templateSvc          ports.TemplateService
	providerSvc          ports.ProviderService
	openstackProviderSvc ports.ProviderOpenStackService
	workspaceSvc         ports.WorkspaceService
	deploymentSvc        ports.DeploymentService
}

// NewService ...
func NewService(
	eventSource ports.EventSrc,
	credentialSvc ports.CredentialService,
	templateSvc ports.TemplateService,
	providerSvc ports.ProviderService,
	openstackProviderSvc ports.ProviderOpenStackService,
	workspaceSvc ports.WorkspaceService,
	deploymentSvc ports.DeploymentService,
) Domain {
	return Domain{
		eventSource:          eventSource,
		credentialSvc:        credentialSvc,
		templateSvc:          templateSvc,
		providerSvc:          providerSvc,
		openstackProviderSvc: openstackProviderSvc,
		workspaceSvc:         workspaceSvc,
		deploymentSvc:        deploymentSvc,
	}
}

// Start starts the service
func (svc Domain) Start() error {
	err := svc.eventSource.Start(svc.mapping())
	if err != nil {
		return err
	}
	log.Infof("event source started")
	for {
		time.Sleep(time.Second * 10)
	}
	return nil
}

func (svc Domain) mapping() map[common.EventType]types.DeletionEventHandler {
	credHandler := &CredentialDeletionHandler{
		credSvc:              svc.credentialSvc,
		templateSvc:          svc.templateSvc,
		deploymentSvc:        svc.deploymentSvc,
		openstackProviderSvc: svc.openstackProviderSvc,
	}
	templateHandler := &TemplateDeletionHandler{
		templateSvc:   svc.templateSvc,
		deploymentSvc: svc.deploymentSvc,
		providerSvc:   svc.providerSvc,
	}
	workspaceHandler := &WorkspaceDeletionHandler{
		workspaceSvc:  svc.workspaceSvc,
		deploymentSvc: svc.deploymentSvc,
	}
	return map[common.EventType]types.DeletionEventHandler{
		service.EventDepMedDeleteCredential: credHandler.Handle,
		service.EventDepMedDeleteTemplate:   templateHandler.Handle,
		service.EventDepMedDeleteWorkspace:  workspaceHandler.Handle,
	}
}

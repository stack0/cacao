package domain

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/dependency-mediator/ports/mocks"
	"gitlab.com/cyverse/cacao/dependency-mediator/types"
	"reflect"
	"testing"
)

func TestCredentialDeletionHandler_Handle(t *testing.T) {
	type mockObjs struct {
		credService          *portsmocks.CredentialService
		templateService      *portsmocks.TemplateService
		deploymentService    *portsmocks.DeploymentService
		openstackProviderSvc *portsmocks.ProviderOpenStackService
	}
	type args struct {
		req types.DeletionRequest
		tid common.TransactionID
	}
	tests := []struct {
		name     string
		mockObjs mockObjs
		args     args
		want     types.DeletionResponse
	}{
		{
			name: "deleted no-dependents",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID: "cred-id-123",
						}, nil)
					svc.On("Delete",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(nil)
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "cred-id-123",
				Deleted:      true,
				HasDependent: false,
			},
		},
		{
			name: "deleted no-dependents openstack",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID:   "cred-id-123",
							Type: "openstack",
							Tags: map[string]string{
								"cacao_provider": "provider-bbbbbbbbbbbbbbbbbbbb",
							},
						}, nil)
					svc.On("Delete",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(nil)
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, nil)
					return svc
				}(),
				openstackProviderSvc: func() *portsmocks.ProviderOpenStackService {
					svc := &portsmocks.ProviderOpenStackService{}
					svc.On("DeleteApplicationCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						common.ID("provider-bbbbbbbbbbbbbbbbbbbb"),
						"cred-id-123").Return(nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "cred-id-123",
				Deleted:      true,
				HasDependent: false,
			},
		},
		{
			name: "deployment-dependent",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID: "cred-id-123",
						}, nil)
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(true, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.CacaoErrorBase{},
				},
				ID:           "cred-id-123",
				Deleted:      false,
				HasDependent: true,
			},
		},
		{
			name: "fail to get credential",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(nil, fmt.Errorf("failed"))
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "cred-id-123",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "fail to check deployment dependent",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID: "cred-id-123",
						}, nil)
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, nil).Maybe() // this may not be called, due to concurrent nature of these calls
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, fmt.Errorf("failed"))
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "cred-id-123",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "openstack fail to delete app cred",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID:   "cred-id-123",
							Type: "openstack",
							Tags: map[string]string{
								"cacao_provider": "provider-aaaaaaaaaaaaaaaaaaaa",
							},
						}, nil)
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, nil)
					return svc
				}(),
				openstackProviderSvc: func() *portsmocks.ProviderOpenStackService {
					svc := &portsmocks.ProviderOpenStackService{}
					svc.On("DeleteApplicationCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						common.ID("provider-aaaaaaaaaaaaaaaaaaaa"),
						"cred-id-123").Return(fmt.Errorf("failed"))
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "cred-id-123",
				Deleted:      false,
				HasDependent: false,
			},
		},
		{
			name: "fail to delete",
			mockObjs: mockObjs{
				credService: func() *portsmocks.CredentialService {
					svc := &portsmocks.CredentialService{}
					svc.On("Get",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(
						&service.CredentialModel{
							ID: "cred-id-123",
						}, nil)
					svc.On("Delete",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(fmt.Errorf("failed"))
					return svc
				}(),
				templateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					return svc
				}(),
				deploymentService: func() *portsmocks.DeploymentService {
					svc := &portsmocks.DeploymentService{}
					svc.On("CheckDependOnCredential",
						types.Session{
							Actor:    "actor-123",
							Emulator: "emulator-123",
							TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
						},
						"cred-id-123").Return(false, nil)
					return svc
				}(),
			},
			args: args{
				req: types.DeletionRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
						ServiceError:    service.CacaoErrorBase{},
					},
					ID: "cred-id-123",
				},
				tid: common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"),
			},
			want: types.DeletionResponse{
				Session: service.Session{
					SessionActor:    "actor-123",
					SessionEmulator: "emulator-123",
					ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
				},
				ID:           "cred-id-123",
				Deleted:      false,
				HasDependent: false,
			},
		},
		// TODO
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := CredentialDeletionHandler{
				credSvc:              tt.mockObjs.credService,
				templateSvc:          tt.mockObjs.templateService,
				deploymentSvc:        tt.mockObjs.deploymentService,
				openstackProviderSvc: tt.mockObjs.openstackProviderSvc,
			}
			if got := h.Handle(tt.args.req, tt.args.tid); !assert.Equal(t, tt.want, got) {
				return
			}
			tt.mockObjs.credService.AssertExpectations(t)
			tt.mockObjs.templateService.AssertExpectations(t)
			tt.mockObjs.deploymentService.AssertExpectations(t)
		})
	}

	testCredentialDeletionHandlerHandleDepdendentAndFailure(t)
}

// a separate test func is needed, because there are 3 possible return value due to concurrent check, and not all calls to check dependent will be called.
func testCredentialDeletionHandlerHandleDepdendentAndFailure(t *testing.T) {

	credService := func() *portsmocks.CredentialService {
		svc := &portsmocks.CredentialService{}
		svc.On("Get",
			types.Session{
				Actor:    "actor-123",
				Emulator: "emulator-123",
				TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
			},
			"cred-id-123").Return(
			&service.CredentialModel{
				ID: "cred-id-123",
			}, nil)
		return svc
	}()
	templateService := func() *portsmocks.TemplateService {
		svc := &portsmocks.TemplateService{}
		svc.On("CheckDependOnCredential",
			types.Session{
				Actor:    "actor-123",
				Emulator: "emulator-123",
				TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
			},
			"cred-id-123").Return(true, nil).Maybe() // may not be called, if called, HasDependent in the return value must be true
		return svc
	}()
	deploymentService := func() *portsmocks.DeploymentService {
		svc := &portsmocks.DeploymentService{}
		svc.On("CheckDependOnCredential",
			types.Session{
				Actor:    "actor-123",
				Emulator: "emulator-123",
				TID:      "tid-aaaaaaaaaaaaaaaaaaaa",
			},
			"cred-id-123").Return(false, fmt.Errorf("failed")) // may not be called, if called, error in the return value must exist
		return svc
	}()

	req := types.DeletionRequest{
		Session: service.Session{
			SessionActor:    "actor-123",
			SessionEmulator: "emulator-123",
			ServiceError:    service.CacaoErrorBase{},
		},
		ID: "cred-id-123",
	}
	tid := common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa")
	want1 := types.DeletionResponse{
		Session: service.Session{
			SessionActor:    "actor-123",
			SessionEmulator: "emulator-123",
			ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
		},
		ID:           "cred-id-123",
		Deleted:      false,
		HasDependent: true, // if both dependent checks (template & deployment) gets called
	}
	want2 := types.DeletionResponse{
		Session: service.Session{
			SessionActor:    "actor-123",
			SessionEmulator: "emulator-123",
			ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
		},
		ID:           "cred-id-123",
		Deleted:      false,
		HasDependent: false, // if only the dependent check on template does NOT get called
	}
	want3 := types.DeletionResponse{
		Session: service.Session{
			SessionActor:    "actor-123",
			SessionEmulator: "emulator-123",
			ServiceError:    service.CacaoErrorBase{},
		},
		ID:           "cred-id-123",
		Deleted:      false,
		HasDependent: true, // if only the dependent check on deployment gets called
	}

	t.Run("fail to check deployment dependent && template-dependent", func(t *testing.T) {
		h := CredentialDeletionHandler{
			credSvc:       credService,
			templateSvc:   templateService,
			deploymentSvc: deploymentService,
		}
		got := h.Handle(req, tid)
		// 3 possible return value
		if !reflect.DeepEqual(got, want1) && !reflect.DeepEqual(got, want2) && !reflect.DeepEqual(got, want3) {
			t.Errorf("Handle() = %v, want\n%v\nor\n%v\nor\n%v", got, want1, want2, want3)
		}
		credService.AssertExpectations(t)
		templateService.AssertExpectations(t)
		deploymentService.AssertExpectations(t)
	})
}

func Test_providerIDFromTags(t *testing.T) {
	type args struct {
		credential service.Credential
	}
	tests := []struct {
		name    string
		args    args
		want    common.ID
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "tag is nil",
			args: args{
				credential: &service.CredentialModel{
					Tags: nil,
				},
			},
			want: "",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "tag is empty",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{},
				},
			},
			want: "",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "cacao_provider tag",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"cacao_provider": "provider-cccccccccccccccccccc",
					},
				},
			},
			want: "provider-cccccccccccccccccccc",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "cacao_provider tag w/ bad value",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"cacao_provider": "provider-cc",
					},
				},
			},
			want: "",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		{
			name: "provider-XXX tag",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"provider-aaaaaaaaaaaaaaaaaaaa": "foo bar",
					},
				},
			},
			want: "provider-aaaaaaaaaaaaaaaaaaaa",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "both cacao-provider and provider-XXX tag (1)",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"haha":                          "",
						"cacao_provider":                "provider-cccccccccccccccccccc",
						"bar":                           "",
						"provider-aaaaaaaaaaaaaaaaaaaa": "foo bar",
						"foo":                           "",
					},
				},
			},
			want: "provider-cccccccccccccccccccc", // cacao_provider takes priority
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "both cacao-provider and provider-XXX tag (2)",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"foo":                           "",
						"provider-aaaaaaaaaaaaaaaaaaaa": "foo bar",
						"bar":                           "",
						"cacao_provider":                "provider-cccccccccccccccccccc",
						"haha":                          "",
					},
				},
			},
			want: "provider-cccccccccccccccccccc", // cacao_provider takes priority
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		{
			name: "no relevant tag",
			args: args{
				credential: &service.CredentialModel{
					Tags: map[string]string{
						"foo": "bar",
					},
				},
			},
			want: "",
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Error(t, err)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := providerIDFromTags(tt.args.credential)
			if !tt.wantErr(t, err, fmt.Sprintf("providerIDFromTags(%v)", tt.args.credential)) {
				return
			}
			assert.Equalf(t, tt.want, got, "providerIDFromTags(%v)", tt.args.credential)
		})
	}
}

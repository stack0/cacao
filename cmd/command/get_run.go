package command

import (
	"fmt"
	"strings"
)

// GetRunCommand ...
type GetRunCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetRunCommand) Run(args []string) int {
	var urlPath string
	if len(args) < 1 {
		c.UI.Error(c.Help())
		return 1
	} else if len(args) == 1 {
		// list run in deployment
		urlPath = fmt.Sprintf("/deployments/%s/runs", args[0])
	} else {
		// get single run
		urlPath = fmt.Sprintf("/deployments/%s/runs/%s", args[0], args[1])
	}
	req := c.NewRequest("GET", urlPath, "")
	c.DoRequest(req)

	return 0
}

// Synopsis ...
func (c *GetRunCommand) Synopsis() string {
	return "Get a run by ID.  Returns all runs for the current user if no ID is specified"
}

// Help ...
func (c *GetRunCommand) Help() string {
	helpText := `
Usage: cacao get run [DEPLOYMENT ID]
	   cacao get run [DEPLOYMENT ID] [RUN ID]

	Returns all runs in a deployment if no run ID is specified.
`
	return strings.TrimSpace(helpText)
}

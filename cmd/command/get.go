package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// GetCommand ...
type GetCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *GetCommand) Synopsis() string {
	return "get information on a cluster, credential, isession, provider, user, workspace, template, template type, template custom field registry entry, js2allocation, js2user, user configs, user favorites, user recents, version, or Workflow Definition "
}

// Help ...
func (c *GetCommand) Help() string {
	helpText := `
	Usage: cacao get <subcommand>

`
	return strings.TrimSpace(helpText)
}

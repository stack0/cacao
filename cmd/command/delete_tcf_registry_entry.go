package command

import (
	"strings"
)

// DeleteTCFRegistryEntryCommand ...
type DeleteTCFRegistryEntryCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteTCFRegistryEntryCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/tcfregistry/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteTCFRegistryEntryCommand) Synopsis() string {
	return "delete a tcf registry entry"
}

// Help ...
func (c *DeleteTCFRegistryEntryCommand) Help() string {
	helpText := `
Usage: cacao delete tcf_registry_entry [entry name]

Run 'cacao get tcf_registry_entry' to view list of tcf registry entries.
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// ProviderCommand ..
type ProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *ProviderCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *ProviderCommand) Synopsis() string {
	return "Manages providers"
}

// Help ...
func (c *ProviderCommand) Help() string {
	helpText := `
	Usage: cacao provider <subcommand>

`
	return strings.TrimSpace(helpText)
}

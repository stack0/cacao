package command

import (
	"encoding/json"
	"flag"
	hm "gitlab.com/cyverse/cacao-common/http"
)

// CreateDeploymentCommand ...
type CreateDeploymentCommand struct {
	*BaseCommand
}

// Help ...
func (c CreateDeploymentCommand) Help() string {
	return `
Usage: create deployment [WORKSPACE ID] [TEMPLATE ID] [PROVIDER ID]
	-cloud-cred Cloud credential.
	-git-cred Git credential

	Create a new deployment.
`
}

// Run ...
func (c *CreateDeploymentCommand) Run(args []string) int {
	if len(args) < 3 {
		c.UI.Error(c.Help())
		return 1
	}
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	cloudCredID := flagSet.String("cloud-cred", "", "cloud credential ID")
	gitCredID := flagSet.String("git-cred", "", "git credential ID")
	err := flagSet.Parse(args[3:])
	if err != nil {
		c.UI.Error(err.Error())
		c.UI.Error(c.Help())
		return -1
	}

	workspaceID := args[0]
	templateID := args[1]
	primaryProviderID := args[2]

	var cloudCredentials []string
	if cloudCredID == nil {
		cloudCredentials = []string{}
	} else {
		if *cloudCredID == "" {
			panic("cloud cred empty")
		}
		cloudCredentials = []string{*cloudCredID}
	}
	if gitCredID == nil {
		credID := ""
		gitCredID = &credID
	}

	createRequest := hm.Deployment{
		WorkspaceID:       workspaceID,
		TemplateID:        templateID,
		PrimaryProviderID: primaryProviderID,
		CloudCredentials:  cloudCredentials,
		GitCredentialID:   *gitCredID,
	}
	marshal, err := json.Marshal(createRequest)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	req := c.NewRequest("POST", "/deployments", string(marshal))
	c.DoRequest(req)

	return 0
}

// Synopsis ...
func (c CreateDeploymentCommand) Synopsis() string {
	return "create deployment"
}

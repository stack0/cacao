package command

import (
	"strings"
)

// GetUserSettingsCommand ...
type GetUserSettingsCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserSettingsCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("GET", "/users/"+args[0]+"/settings", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserSettingsCommand) Synopsis() string {
	return "get user settings"
}

// Help ...
func (c *GetUserSettingsCommand) Help() string {
	helpText := `
Usage: cacao get user settings [USERNAME]
`
	return strings.TrimSpace(helpText)
}

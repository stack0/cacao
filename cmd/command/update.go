package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// UpdateCommand ...
type UpdateCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *UpdateCommand) Synopsis() string {
	return "update a cluster, credential, provider, user, workspace, template, template type, template custom field registry entry, or Workflow Definition "
}

// Help ...
func (c *UpdateCommand) Help() string {
	helpText := `
	Usage: cacao update <subcommand>

`
	return strings.TrimSpace(helpText)
}

package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// UpdateTCFRegistryEntryCommand ...
type UpdateTCFRegistryEntryCommand struct {
	*BaseCommand
}

const (
	emptyTCFRegistryArgumentString = "$$EMPTY_ARGUMENT_STRING$$"
)

// processEmptyArgument checks if arg is given or not. If given, return true.
// if arg is not given via CLI argument, make the variable empty and return false.
func (c *UpdateTCFRegistryEntryCommand) processEmptyArgument(arg *string) bool {
	setValue := true
	if *arg == emptyTCFRegistryArgumentString {
		setValue = false
		*arg = ""
	}
	return setValue
}

// Run ...
func (c *UpdateTCFRegistryEntryCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	description := flagSet.String("description", emptyTCFRegistryArgumentString, "description")
	entryType := flagSet.String("type", emptyTCFRegistryArgumentString, "entry type")
	queryTarget := flagSet.String("query_target", emptyTCFRegistryArgumentString, "query target")
	queryData := flagSet.String("query_data", emptyTCFRegistryArgumentString, "query data")
	queryResultJSONPathFilter := flagSet.String("query_result_jsonpath_filter", "", "query data jsonpath filter")

	flagSet.Parse(args)
	args = flagSet.Args()

	setDescription := c.processEmptyArgument(description)
	setEntryType := c.processEmptyArgument(entryType)
	setQueryTarget := c.processEmptyArgument(queryTarget)
	setQueryData := c.processEmptyArgument(queryData)
	setQueryResultJSONPathFilter := c.processEmptyArgument(queryResultJSONPathFilter)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  description: '%s'\n  entryType: '%s' queryTarget: '%s' queryData: '%s' queryResultJSONPathFilter: '%s'\n", *filename, *description, *entryType, *queryTarget, *queryData, *queryResultJSONPathFilter)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	if len(args) != 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	entryName := args[0]

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	data = c.override(data, description, setDescription, entryType, setEntryType, queryTarget, setQueryTarget, queryData, setQueryData, queryResultJSONPathFilter, setQueryResultJSONPathFilter)

	req := c.NewRequest("PATCH", "/tcfregistry/"+entryName, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateTCFRegistryEntryCommand) Synopsis() string {
	return "update a tcf registry entry"
}

// Help ...
func (c *UpdateTCFRegistryEntryCommand) Help() string {
	helpText := `
	Usage: cacao tcf_registry_entry update [options] [name]

	Options:
		-f <filename>
			name of JSON file to read info from
		-description <string>
			description
		-type <string>
			entry type ('rest_api_endpoint' or 'nats_message_channel')\
		-query_target <string>
			query target (e.g., API endpoint URI)
		-query_data <string>
			query data for input
		-query_result_jsonpath_filter <string>
			query result Json Path filter
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateTCFRegistryEntryCommand) override(data []byte, description *string, setDescription bool, entryType *string, setEntryType bool, queryTarget *string, setQueryTarget bool, queryData *string, setQueryData bool, queryResultJSONPathFilter *string, setQueryResultJSONPathFilter bool) []byte {
	var tcfRegistryEntryJSON struct {
		Description               string `json:"description,omitempty"`
		Type                      string `json:"type,omitempty"`
		QueryTarget               string `json:"query_target,omitempty"`
		QueryData                 string `json:"query_data,omitempty"`
		QueryResultJSONPathFilter string `json:"query_result_jsonpath_filter,omitempty"`
	}

	json.Unmarshal(data, &tcfRegistryEntryJSON)

	if setDescription {
		tcfRegistryEntryJSON.Description = *description
	}

	if setEntryType {
		tcfRegistryEntryJSON.Type = *entryType
	}

	if setQueryTarget {
		tcfRegistryEntryJSON.QueryTarget = *queryTarget
	}

	if setQueryData {
		tcfRegistryEntryJSON.QueryData = *queryData
	}

	if setQueryResultJSONPathFilter {
		tcfRegistryEntryJSON.QueryResultJSONPathFilter = *queryResultJSONPathFilter
	}

	data, err := json.Marshal(tcfRegistryEntryJSON)
	if err != nil {
		panic(err)
	}
	return data
}

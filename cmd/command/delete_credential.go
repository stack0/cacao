package command

import (
	"flag"
	"strings"
)

// DeleteCredentialCommand ...
type DeleteCredentialCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteCredentialCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	id := flagSet.String("name", "", "name of credential to use")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  name: '%s'", *id)

	if len(*id) < 1 {
		c.PrintDebug("Incorrect number of flags (expected 1)")
		c.PrintDebug(c.Help())
		c.UI.Error("Incorrect number of flags (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}
	if len(args) > 0 && args[0] == "help" {
		c.PrintDebug(c.Help())
		c.UI.Info(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/credentials/"+(*id), "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteCredentialCommand) Synopsis() string {
	return "delete a credential for current user"
}

// Help ...
func (c *DeleteCredentialCommand) Help() string {
	helpText := `
Usage: cacao delete credential -name a

Options:
	-name <string>
  		name of credential
`
	return strings.TrimSpace(helpText)
}

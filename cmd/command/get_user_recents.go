package command

import (
	"strings"
)

// GetUserRecentsCommand ...
type GetUserRecentsCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserRecentsCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("GET", "/users/"+args[0]+"/recents", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserRecentsCommand) Synopsis() string {
	return "get user configs"
}

// Help ...
func (c *GetUserRecentsCommand) Help() string {
	helpText := `
Usage: cacao get user recents [USERNAME]
`
	return strings.TrimSpace(helpText)
}

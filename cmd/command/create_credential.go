package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// CreateCredentialCommand ...
type CreateCredentialCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateCredentialCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	id := flagSet.String("name", "", "name of credential to use")
	credentialType := flagSet.String("type", "", "type of credential (gcr, dockerhub, or git)")
	//username := flagSet.String("username", "", "username to use with credential")
	value := flagSet.String("value", "", "value of the credential")
	valueFile := flagSet.String("value-file", "", "file that contains the value of the credential")
	if err := flagSet.Parse(args); err != nil {
		c.UI.Error(err.Error())
		c.UI.Info(c.Help())
		return 1
	}
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  name: '%s'\n  credentialType: '%s'\n  value: '%s'\n valueFile: '%s'\n", *id, *credentialType, *value, *valueFile)

	if len(*id) < 1 || len(*credentialType) < 1 || (len(*value) < 1 && len(*valueFile) < 1) {
		c.PrintDebug("Incorrect number of flags (expected 3)")
		c.PrintDebug(c.Help())
		c.UI.Error("Incorrect number of flags (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}

	if len(args) > 0 && args[0] == "help" {
		c.PrintDebug(c.Help())
		c.UI.Info(c.Help())
		return 1
	}

	if len(*valueFile) > 0 {
		fileContent, err := ioutil.ReadFile(*valueFile)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		*value = string(fileContent)
	}

	var credential = struct {
		Type  string
		ID    string
		Value string
	}{
		Type:  *credentialType,
		ID:    *id,
		Value: *value,
	}
	data, err := json.Marshal(credential)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/credentials", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateCredentialCommand) Synopsis() string {
	return "create a new credential for user"
}

// Help ...
func (c *CreateCredentialCommand) Help() string {
	helpText := `
Usage: cacao create credential -name a -type b -value c

Options:
	-name <string>
  		name of credential
	-type <string>
		type of the credential (git, dockerhub, or gcr)
	-value <string>
		value of the credential (often a personal access token)
	-value-file <string>
		file that contains the value of the credential
`
	return strings.TrimSpace(helpText)
}

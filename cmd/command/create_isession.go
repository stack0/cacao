package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// CreateInteractiveSessionCommand ...
type CreateInteractiveSessionCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateInteractiveSessionCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	instanceID := flagSet.String("instance_id", "", "ID of instance")
	instanceAddress := flagSet.String("instance_address", "", "IP Address of instance")
	instanceAdminUsername := flagSet.String("instance_admin", "", "Admin username of instance")
	cloudID := flagSet.String("cloud_id", "", "ID of the cloud provider")
	protocol := flagSet.String("protocol", "", "Protocol to use (SSH vs VNC")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  instanceID: '%s'\n  instanceAddress: '%s'\n  instanceAdminUsername: '%s'\n  cloudID: '%s'\n  protocol: '%s'\n", *instanceID, *instanceAddress, *instanceAdminUsername, *cloudID, *protocol)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*instanceID) > 0 || len(*instanceAddress) > 0 || len(*instanceAdminUsername) > 0 || len(*cloudID) > 0 || len(*protocol) > 0 {
			data = c.override(data, instanceID, instanceAddress, instanceAdminUsername, cloudID, protocol)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*instanceID) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*instanceAddress) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*instanceAdminUsername) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*cloudID) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*protocol) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		data = c.override(data, instanceID, instanceAddress, instanceAdminUsername, cloudID, protocol)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/isessions", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateInteractiveSessionCommand) Synopsis() string {
	return "create a new interactive session"
}

// Help ...
func (c *CreateInteractiveSessionCommand) Help() string {
	helpText := `
	Usage: cacao isession create [options]

	Of the following options, it is required that you use
	either '-f <filename>' or utilize other options

	Options:
		-f <filename>
			name of json file to read user info from. Cannot be used with
			other options
		-instance_id <string>
			ID of the instance you are trying to connect to
			(required)
		-instance_address <string>
			IP Address of the instance you are trying to connect to
			(required)
		-instance_admin <string>
			Admin username of the instance you are trying to connect to
			(required)
		-cloud_id <string>
			Cloud ID of the host
			(required)
		-protocol <string>
			Protocol to use, (VNC or SSH)
			(required)
`
	return strings.TrimSpace(helpText)
}

func (c *CreateInteractiveSessionCommand) override(data []byte, instanceID *string, instanceAddress *string, instanceAdminUsername *string, cloudID *string, protocol *string) []byte {
	var interactiveSessionJSON struct {
		InstanceID            string `json:"instance_id,omitempty"`
		InstanceAddress       string `json:"instance_address,omitempty"`
		InstanceAdminUsername string `json:"instance_admin_username,omitempty"`
		CloudID               string `json:"cloud_id,omitempty"`
		Protocol              string `json:"protocol,omitempty"`
	}

	json.Unmarshal(data, &interactiveSessionJSON)

	if len(*instanceID) > 0 {
		interactiveSessionJSON.InstanceID = *instanceID
	}
	if len(*instanceAddress) > 0 {
		interactiveSessionJSON.InstanceAddress = *instanceAddress
	}
	if len(*instanceAdminUsername) > 0 {
		interactiveSessionJSON.InstanceAdminUsername = *instanceAdminUsername
	}
	if len(*cloudID) > 0 {
		interactiveSessionJSON.CloudID = *cloudID
	}
	if len(*protocol) > 0 {
		interactiveSessionJSON.Protocol = strings.ToLower(*protocol)
	}

	data, err := json.Marshal(interactiveSessionJSON)
	if err != nil {
		panic(err)
	}
	return data
}

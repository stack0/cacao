package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// TCFRegistryEntryCommand ..
type TCFRegistryEntryCommand struct {
	*BaseCommand
}

// Run ...
func (c *TCFRegistryEntryCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *TCFRegistryEntryCommand) Synopsis() string {
	return "Manages template custom field registry entries"
}

// Help ...
func (c *TCFRegistryEntryCommand) Help() string {
	helpText := `
	Usage: cacao tcf_registry_entry <subcommand>

`
	return strings.TrimSpace(helpText)
}

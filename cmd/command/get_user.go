package command

import (
	"flag"
	"strings"

	log "github.com/sirupsen/logrus"
)

// GetUserCommand ...
type GetUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserCommand) Run(args []string) int {
	log.Trace("command/get_user.Run: start")
	var username string
	if len(args) > 0 && args[0][0] != '-' {
		username = "/" + args[0]
		args = args[1:]
	}
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	withSettings := flagSet.Bool("withSettings", false, "withSettings")
	flagSet.Parse(args)

	path := "/users" + username
	if *withSettings {
		path += "?withSettings"
	}
	req := c.NewRequest("GET", path, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserCommand) Synopsis() string {
	return "get users"
}

// Help ...
func (c *GetUserCommand) Help() string {
	helpText := `

Usage: cacao get user [USERNAME] [Options]


	Gets all users unless specific USERNAME is provided

	Options:
		-withSettings
			include the user's configs, favorites, and recents with the returned user
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CreateCommand ...
type CreateCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CreateCommand) Synopsis() string {
	return "create a cluster, credential, isession, provider, user, workspace, template, template type, template custom field registry entry, user favorite, or Workflow Definition "
}

// Help ...
func (c *CreateCommand) Help() string {
	helpText := `
	Usage: cacao create <subcommand> [options]

`
	return strings.TrimSpace(helpText)
}

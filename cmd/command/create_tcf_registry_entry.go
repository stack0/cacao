package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"
)

// CreateTCFRegistryEntryCommand ...
type CreateTCFRegistryEntryCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateTCFRegistryEntryCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	name := flagSet.String("name", "", "name of tcf registry entry")
	description := flagSet.String("description", "", "description")
	entryType := flagSet.String("type", "", "entry type")
	queryTarget := flagSet.String("query_target", "", "query target")
	queryData := flagSet.String("query_data", "", "query data")
	queryResultJSONPathFilter := flagSet.String("query_result_jsonpath_filter", "", "query data jsonpath filter")
	flagSet.Parse(args)

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  name: '%s'\n  description: '%s'\n  entryType: '%s' queryTarget: '%s' queryData: '%s' queryResultJSONPathFilter: '%s'\n", *filename, *name, *description, *entryType, *queryTarget, *queryData, *queryResultJSONPathFilter)

	var data []byte
	var err error
	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		if err != nil {
			panic(err)
		}

		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))

		if len(*name) > 0 || len(*description) > 0 || len(*entryType) > 0 || len(*queryTarget) > 0 || len(*queryData) > 0 || len(*queryResultJSONPathFilter) > 0 {
			data = c.override(data, name, description, entryType, queryTarget, queryData, queryResultJSONPathFilter)
			c.PrintDebug("Successfully overrode parameters in file:\n%s\n", string(data))
		}
	} else {
		if len(*name) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*entryType) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		if len(*queryTarget) == 0 {
			c.UI.Error(c.Help())
			return 1
		}

		data = c.override(data, name, description, entryType, queryTarget, queryData, queryResultJSONPathFilter)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("POST", "/tcfregistry", "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateTCFRegistryEntryCommand) Synopsis() string {
	return "create a tcf registry entry"
}

// Help ...
func (c *CreateTCFRegistryEntryCommand) Help() string {
	helpText := `
	Usage: cacao tcf_registry_entry create [options]

	Of the following options, it is required that you use
	either '-f <filename>' or utilize other options

	Options:
		-f <filename>
			name of json file to read user info from. Cannot be used with
			other options
		-name <string>
			name of the entry
			(required)
		-description <string>
			(optional) description
		-type <string>
			entry type ('rest_api_endpoint' or 'nats_message_channel')
			(required)
		-query_target <string>
			(required) query target (e.g., API endpoint URI)
		-query_data <string>
			(optional) query data for input
		-query_result_jsonpath_filter <string>
			(optional) query result Json Path filter
`
	return strings.TrimSpace(helpText)
}

func (c *CreateTCFRegistryEntryCommand) override(data []byte, name *string, description *string, entryType *string, queryTarget *string, queryData *string, queryResultJSONPathFilter *string) []byte {
	var tcfRegistryEntryJSON struct {
		Name                      string `json:"name,omitempty"`
		Description               string `json:"description,omitempty"`
		Type                      string `json:"type,omitempty"`
		QueryTarget               string `json:"query_target,omitempty"`
		QueryData                 string `json:"query_data,omitempty"`
		QueryResultJSONPathFilter string `json:"query_result_jsonpath_filter,omitempty"`
	}

	json.Unmarshal(data, &tcfRegistryEntryJSON)

	if len(*name) > 0 {
		tcfRegistryEntryJSON.Name = *name
	}
	if len(*description) > 0 {
		tcfRegistryEntryJSON.Description = *description
	}
	if len(*entryType) > 0 {
		tcfRegistryEntryJSON.Type = *entryType
	}
	if len(*queryTarget) > 0 {
		tcfRegistryEntryJSON.QueryTarget = *queryTarget
	}
	if len(*queryData) > 0 {
		tcfRegistryEntryJSON.QueryData = *queryData
	}
	if len(*queryResultJSONPathFilter) > 0 {
		tcfRegistryEntryJSON.QueryResultJSONPathFilter = *queryResultJSONPathFilter
	}

	data, err := json.Marshal(tcfRegistryEntryJSON)
	if err != nil {
		panic(err)
	}
	return data
}

package command

import (
	"strings"
)

// SetUserConfigCommand ...
type SetUserConfigCommand struct {
	*BaseCommand
}

// Run ...
func (c *SetUserConfigCommand) Run(args []string) int {
	if len(args) != 3 {
		c.UI.Error("Incorrect number of arguments (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("POST", "/users/"+args[0]+"/configs/"+args[1], args[2])
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *SetUserConfigCommand) Synopsis() string {
	return "add a user favorite"
}

// Help ...
func (c *SetUserConfigCommand) Help() string {
	helpText := `
Usage: cacao set user config [USERNAME] [NAME] [VALUE]
`
	return strings.TrimSpace(helpText)
}

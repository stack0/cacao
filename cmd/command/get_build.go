package command

import (
	"fmt"
	"strings"
)

// GetBuildCommand ...
type GetBuildCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetBuildCommand) Run(args []string) int {
	var urlPath string
	if len(args) == 0 {
		c.UI.Error(c.Help())
		return 1
	} else if len(args) == 1 {
		// list builds in deployment
		urlPath = fmt.Sprintf("/deployments/%s/builds", args[0])
	} else {
		// get a single build
		urlPath = fmt.Sprintf("/deployments/%s/builds/%s", args[0], args[1])
	}
	req := c.NewRequest("GET", urlPath, "")
	resp, err := c.Client().Do(req)
	if err != nil {
		c.UI.Error(err.Error())
	}
	c.PrintHTTPResponse(resp)
	return 0
}

// Synopsis ...
func (c *GetBuildCommand) Synopsis() string {
	return "Get a build by ID.  Returns all builds for the current user if no ID is specified"
}

// Help ...
func (c *GetBuildCommand) Help() string {
	helpText := `
Usage: cacao get build [DEPLOYMENT ID]
       cacao get build [DEPLOYMENT ID] [BUILD ID]

	Returns all builds in a deployment if no build ID is specified
`
	return strings.TrimSpace(helpText)
}

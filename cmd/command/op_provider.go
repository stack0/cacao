package command

import (
	"flag"
	"fmt"
	"net/url"
	"strings"
)

// OpProviderCommand ...
type OpProviderCommand struct {
	*BaseCommand
}

// Run ...
func (c *OpProviderCommand) Run(args []string) int {
	needsID := false
	if len(args) < 3 {
		c.UI.Error("You must provide a providerid, service, and operation")
		return 1
	}
	providerID := args[0]
	service := args[1] + "s"
	operation := args[2]

	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	credentialid := flagSet.String("credential", "", "credential")
	id := flagSet.String("id", "", "id")
	region := flagSet.String("region", "", "region")
	flagSet.Parse(args[3:])

	// we need to check if the operation is a get operation
	if operation == "get" || operation == "update" {
		if id != nil && *id != "" {
			needsID = true
		} else {
			c.UI.Error("You must provide an id")
			return 1
		}
	}

	requestType := ""
	switch operation {
	case "delete":
		requestType = "DELETE"
	case "create":
		requestType = "POST"
	default:
		requestType = "GET"
	}

	var queryParams url.Values
	queryParams.Add("credential", *credentialid)
	queryParams.Add("region", *region)

	if needsID {
		req := c.NewRequest(requestType, fmt.Sprintf("/providers/%s/%s/%s?%s", providerID, service, *id, queryParams.Encode()), "")
		c.DoRequest(req)
	} else {
		req := c.NewRequest(requestType, fmt.Sprintf("/providers/%s/%s?%s", providerID, service, queryParams.Encode()), "")
		c.DoRequest(req)
	}

	return 0
}

// Synopsis ...
func (c *OpProviderCommand) Synopsis() string {
	return "provider operations"
}

// Help ...
func (c *OpProviderCommand) Help() string {
	helpText := `
Usage: cacao provider op [PROVIDERID] [SERVICE] [OPERATION] [OPTIONS]
	Options:
		-credential <credentialid>
			required
		-region <region>
			required
		-id <id>
			required for get operation.

	Examples:
		cacao provider op provider-openstack-aaaaaaaaaaaaaaaaaaaa flavor list -credential cred-aaaaaaaaaaaaaaaaaaaa -region 'region-name'

		cacao provider op provider-aws-aaaaaaaaaaaaaaaaaaaa region list  -credential cred-aaaaaaaaaaaaaaaaaaaa

		cacao provider op provider-aws-aaaaaaaaaaaaaaaaaaaa image get  -id ami-aaaaaaaaaaaaaaaaa  -credential cred-aaaaaaaaaaaaaaaaaaaa -region 'us-east-1'
	
`
	return strings.TrimSpace(helpText)
}

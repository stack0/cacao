package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// UserCommand ..
type UserCommand struct {
	*BaseCommand
}

// Run ...
func (c *UserCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *UserCommand) Synopsis() string {
	return "Manages user"
}

// Help ...
func (c *UserCommand) Help() string {
	helpText := `
	Usage: cacao user <subcommand>

`
	return strings.TrimSpace(helpText)
}

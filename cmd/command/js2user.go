package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// JS2UserCommand ..
type JS2UserCommand struct {
	*BaseCommand
}

// Run ...
func (c *JS2UserCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *JS2UserCommand) Synopsis() string {
	return "Manages JetStream2 users"
}

// Help ...
func (c *JS2UserCommand) Help() string {
	helpText := `
	Usage: cacao j2user <subcommand>

`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// TemplateTypeCommand ..
type TemplateTypeCommand struct {
	*BaseCommand
}

// Run ...
func (c *TemplateTypeCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *TemplateTypeCommand) Synopsis() string {
	return "Manages template types"
}

// Help ...
func (c *TemplateTypeCommand) Help() string {
	helpText := `
	Usage: cacao template_type <subcommand>

`
	return strings.TrimSpace(helpText)
}

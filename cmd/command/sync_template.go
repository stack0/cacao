package command

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"
)

// SyncTemplateCommand ...
type SyncTemplateCommand struct {
	*BaseCommand
}

// Run ...
func (c *SyncTemplateCommand) Run(args []string) int {
	var templateID string
	if len(args) < 1 {
		c.UI.Error("Incorrect number of arguments (expected 1 or 2)")
		c.UI.Error(c.Help())
		return 1
	}

	templateID = args[0]
	credentialID := ""
	if len(args) > 1 {
		credentialID = args[1]
	}

	var data []byte
	data = c.override(data, &credentialID)

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("PATCH", "/templates/"+templateID, "")

	// Only fill request body if it is not empty
	if len(data) > 0 {
		req.Body = ioutil.NopCloser(bytes.NewReader(data))
	}
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *SyncTemplateCommand) Synopsis() string {
	return "sync a template"
}

// Help ...
func (c *SyncTemplateCommand) Help() string {
	helpText := `
Usage: cacao sync template [ID] [credential ID]
`
	return strings.TrimSpace(helpText)
}

func (c *SyncTemplateCommand) override(data []byte, credentialID *string) []byte {
	var templateJSON struct {
		CredentialID string `json:"credential_id,omitempty"`
		Sync         bool   `json:"sync,omitempty"`
	}

	json.Unmarshal(data, &templateJSON)

	if len(*credentialID) > 0 {
		templateJSON.CredentialID = *credentialID
	}

	templateJSON.Sync = true

	data, err := json.Marshal(templateJSON)
	if err != nil {
		panic(err)
	}
	return data
}

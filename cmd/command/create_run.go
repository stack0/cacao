package command

import (
	"encoding/json"
	"flag"
	hm "gitlab.com/cyverse/cacao-common/http"
	"io/ioutil"
)

// CreateRunCommand ...
type CreateRunCommand struct {
	*BaseCommand
}

// Help ...
func (c CreateRunCommand) Help() string {
	return `
Usage: create run [DEPLOYMENT ID]
	-f json filename to import the deployment run from
	Create a new deployment run.
`
}

// Run ...
func (c CreateRunCommand) Run(args []string) int {
	if len(args) < 1 {
		c.UI.Error(c.Help())
		return 1
	}
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	err := flagSet.Parse(args[1:])
	if err != nil {
		c.UI.Error(err.Error())
		c.UI.Error(c.Help())
		return -1
	}

	deploymentID := args[0]

	var createRequest hm.DeploymentRun

	if filename != nil && *filename != "" {
		fileContent, err := ioutil.ReadFile(*filename)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		err = json.Unmarshal(fileContent, &createRequest)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}
	createRequest.DeploymentID = deploymentID
	marshal, err := json.Marshal(createRequest)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	req := c.NewRequest("POST", "/deployments/"+deploymentID+"/runs", string(marshal))
	c.DoRequest(req)

	return 0
}

// Synopsis ...
func (c CreateRunCommand) Synopsis() string {
	return "create run"
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// QueryCommand ...
type QueryCommand struct {
	*BaseCommand
}

// Run ...
func (c *QueryCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *QueryCommand) Synopsis() string {
	return "query to tcf registry"
}

// Help ...
func (c *QueryCommand) Help() string {
	helpText := `
	Usage: cacao query <subcommand>

`
	return strings.TrimSpace(helpText)
}

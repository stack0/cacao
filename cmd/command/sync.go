package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// SyncCommand ...
type SyncCommand struct {
	*BaseCommand
}

// Run ...
func (c *SyncCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *SyncCommand) Synopsis() string {
	return "sync a template"
}

// Help ...
func (c *SyncCommand) Help() string {
	helpText := `
	Usage: cacao sync <subcommand>

`
	return strings.TrimSpace(helpText)
}

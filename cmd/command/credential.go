package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CredentialCommand ..
type CredentialCommand struct {
	*BaseCommand
}

// Run ...
func (c *CredentialCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CredentialCommand) Synopsis() string {
	return "Manages credentials"
}

// Help ...
func (c *CredentialCommand) Help() string {
	helpText := `
	Usage: cacao credential <subcommand>

`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"
)

// GetJS2UserCommand ...
type GetJS2UserCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetJS2UserCommand) Run(args []string) int {
	req := c.NewRequest("GET", "/js2user", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetJS2UserCommand) Synopsis() string {
	return "get js2user"
}

// Help ...
func (c *GetJS2UserCommand) Help() string {
	helpText := `
Usage: cacao get js2user

	Gets JetStream2 user
`
	return strings.TrimSpace(helpText)
}

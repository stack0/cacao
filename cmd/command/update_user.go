package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"

	"gitlab.com/cyverse/cacao-common/common"
)

// UpdateUserCommand ...
type UpdateUserCommand struct {
	*BaseCommand
}

// Run ...
func (c *UpdateUserCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	filename := flagSet.String("f", "", "filename to use")
	username := flagSet.String("username", "", "username")
	firstname := flagSet.String("firstname", "", "firstname")
	lastname := flagSet.String("lastname", "", "lastname")
	email := flagSet.String("email", "", "email")
	admin := flagSet.Bool("admin", false, "admin")
	flagSet.Parse(args)
	args = flagSet.Args()

	c.PrintDebug("Parsed command line flags:\n  filename: '%s'\n  username: '%s'\n  first_name: '%s'\n  last_name: '%s'\n  email: '%s'\n  admin: %t\n",
		*filename, *username, *firstname, *lastname, *email, *admin)

	if len(args) == 1 && args[0] == "help" {
		c.UI.Error(c.Help())
		return 0
	}

	var data []byte
	var err error
	huser := common.HTTPUser{}

	if len(*filename) > 0 {
		data, err = ioutil.ReadFile(*filename)
		json.Unmarshal(data, &huser)
		if err != nil {
			panic(err)
		}
		c.PrintDebug("Successfully read file '%s':\n%s", *filename, string(data))
	}

	c.override(&huser, username, firstname, lastname, email, admin)

	data, err = json.Marshal(&huser)
	if err != nil {
		panic(err)
	}

	c.PrintDebug("Constructed request body:\n%s\n", string(data))

	req := c.NewRequest("PUT", "/users/"+huser.Username, "")
	req.Body = ioutil.NopCloser(bytes.NewReader(data))
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *UpdateUserCommand) Synopsis() string {
	return "update a user"
}

// Help ...
func (c *UpdateUserCommand) Help() string {
	helpText := `
	Usage: cacao user update [options] USERNAME

	Options:
		-f <filename>
			name of JSON file to read secret info from
		-admin
			update user with admin permissions
`
	return strings.TrimSpace(helpText)
}

func (c *UpdateUserCommand) override(huser *common.HTTPUser, username *string, firstname *string, lastname *string, email *string, admin *bool) {

	if len(*username) > 0 {
		huser.Username = *username
	}
	if len(*firstname) > 0 {
		huser.FirstName = *firstname
	}
	if len(*username) > 0 {
		huser.Username = *username
	}
	if len(*lastname) > 0 {
		huser.LastName = *lastname
	}
	if len(*email) > 0 {
		huser.PrimaryEmail = *email
	}

	if *admin {
		huser.IsAdmin = true
	}
}

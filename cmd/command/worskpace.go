package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// WorkspaceCommand ..
type WorkspaceCommand struct {
	*BaseCommand
}

// Run ...
func (c *WorkspaceCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *WorkspaceCommand) Synopsis() string {
	return "Manages workspaces"
}

// Help ...
func (c *WorkspaceCommand) Help() string {
	helpText := `
	Usage: cacao workspace <subcommand>

`
	return strings.TrimSpace(helpText)
}

package command

import (
	"net/url"
	"strings"
)

// QueryTCFRegistryEntryCommand ...
type QueryTCFRegistryEntryCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *QueryTCFRegistryEntryCommand) Run(args []string) int {
	if len(args) < 1 {
		c.UI.Error("Incorrect number of positional arguments (expected 1 or more)")
		c.UI.Error(c.Help())
		return 1
	}

	entryName := args[0]
	queryURL := url.URL{}

	for i := 1; i < len(args); i++ {
		paramKeyVal := strings.Split(args[i], "=")
		if len(paramKeyVal) == 2 {
			paramKey := paramKeyVal[0]
			paramVal := paramKeyVal[1]

			queryURL.Query().Add(paramKey, paramVal)
		}
	}

	encodedQuery := ""
	if len(queryURL.Query()) > 0 {
		encodedQuery = "?" + queryURL.Query().Encode()
	}

	req := c.NewRequest("GET", "/tcfregistry/query/"+entryName+encodedQuery, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *QueryTCFRegistryEntryCommand) Synopsis() string {
	return "query to tcf registry entry's target"
}

// Help ...
func (c *QueryTCFRegistryEntryCommand) Help() string {
	helpText := `
Usage: cacao query tcf_registry_entry [name] [param1_key=param1_val] [param2_key=param2_val] ...
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"flag"
	"net/http"
	"strings"
)

// GetInteractiveSessionCommand ...
type GetInteractiveSessionCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetInteractiveSessionCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	useInstanceID := flagSet.Bool("instance_id", false, "instance id")
	useInstanceAddress := flagSet.Bool("instance_addr", false, "instance address")
	flagSet.Parse(args)

	var req *http.Request
	if *useInstanceID {
		var instanceID string
		if flagSet.NArg() > 0 {
			instanceID = "/" + flagSet.Arg(0)
		}

		req = c.NewRequest("GET", "/isessions/instanceid/"+instanceID, "")
	} else if *useInstanceAddress {
		var instanceAddress string
		if flagSet.NArg() > 0 {
			instanceAddress = "/" + flagSet.Arg(0)
		}

		req = c.NewRequest("GET", "/isessions/instanceaddr/"+instanceAddress, "")
	} else {
		var isessionID string
		if flagSet.NArg() > 0 {
			isessionID = "/" + flagSet.Arg(0)
		}

		req = c.NewRequest("GET", "/isessions"+isessionID, "")
	}

	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetInteractiveSessionCommand) Synopsis() string {
	return "get interactive sessions"
}

// Help ...
func (c *GetInteractiveSessionCommand) Help() string {
	helpText := `
	Usage: cacao get isession [options] [ID/Address]

	Gets all interactive sessions unless specific interactive-session ID is provided

	Options:
		-instance_id
			(optional) get interactive session by instance ID
		-instance_addr
			(optional) get interactive session by instance address
`
	return strings.TrimSpace(helpText)
}

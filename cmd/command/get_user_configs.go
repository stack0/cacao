package command

import (
	"strings"
)

// GetUserConfigsCommand ...
type GetUserConfigsCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserConfigsCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("GET", "/users/"+args[0]+"/configs", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserConfigsCommand) Synopsis() string {
	return "get user configs"
}

// Help ...
func (c *GetUserConfigsCommand) Help() string {
	helpText := `
Usage: cacao get user configs [USERNAME]
`
	return strings.TrimSpace(helpText)
}

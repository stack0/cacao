package command

import (
	"flag"
	"fmt"
	"net/http"
	"strings"
)

// CheckInteractiveSessionCommand ...
type CheckInteractiveSessionCommand struct {
	*BaseCommand
}

// Run ...
func (c *CheckInteractiveSessionCommand) Run(args []string) int {
	flagSet := flag.NewFlagSet("options", flag.ContinueOnError)
	flagSet.Parse(args)

	var req *http.Request
	var protocol string
	var instanceAddress string
	var instanceAdminUsername string

	if flagSet.NArg() > 2 {
		protocol = flagSet.Arg(0)
		instanceAddress = flagSet.Arg(1)
		instanceAdminUsername = flagSet.Arg(2)

		url := fmt.Sprintf("/isessions/check/%s", instanceAddress)
		req = c.NewRequest("GET", url, "")

		query := req.URL.Query()
		query.Add("protocol", protocol)
		query.Add("user", instanceAdminUsername)

		req.URL.RawQuery = query.Encode()
		c.DoRequest(req)
	} else {
		c.UI.Error("Incorrect number of arguments (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}
	return 0
}

// Synopsis ...
func (c *CheckInteractiveSessionCommand) Synopsis() string {
	return "check prerequisites for an interactive session"
}

// Help ...
func (c *CheckInteractiveSessionCommand) Help() string {
	helpText := `
	Usage: cacao isession check [Protocol] [InstanceAddress] [InstanceAdminUsername] 

	Checks an instance if the instance supports given protocol
`
	return strings.TrimSpace(helpText)
}

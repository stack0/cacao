package command

import (
	"strings"
)

// DeleteUserFavoriteCommand ...
type DeleteUserFavoriteCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteUserFavoriteCommand) Run(args []string) int {
	if len(args) != 3 {
		c.UI.Error("Incorrect number of arguments (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/users/"+args[0]+"/favorites/"+args[1]+"/"+args[2], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteUserFavoriteCommand) Synopsis() string {
	return "delete a user favorite"
}

// Help ...
func (c *DeleteUserFavoriteCommand) Help() string {
	helpText := `
Usage: cacao delete user favorite [USERNAME] [NAME] [VALUE]
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"
)

// GetUserFavoritesCommand ...
type GetUserFavoritesCommand struct {
	*BaseCommand
}

// Run ...
func (c *GetUserFavoritesCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("GET", "/users/"+args[0]+"/favorites", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetUserFavoritesCommand) Synopsis() string {
	return "get user configs"
}

// Help ...
func (c *GetUserFavoritesCommand) Help() string {
	helpText := `
Usage: cacao get user favorites [USERNAME]
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// TemplateSourceTypeCommand ...
type TemplateSourceTypeCommand struct {
	*BaseCommand
}

// Run ...
func (c *TemplateSourceTypeCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *TemplateSourceTypeCommand) Synopsis() string {
	return "Manages template source types"
}

// Help ...
func (c *TemplateSourceTypeCommand) Help() string {
	helpText := `
	Usage: cacao template_source_type <subcommand>

`
	return strings.TrimSpace(helpText)
}

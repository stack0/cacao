package command

import (
	"strings"
)

// GetJS2AllocationCommand ...
type GetJS2AllocationCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetJS2AllocationCommand) Run(args []string) int {
	req := c.NewRequest("GET", "/js2projects", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetJS2AllocationCommand) Synopsis() string {
	return "get js2allocations"
}

// Help ...
func (c *GetJS2AllocationCommand) Help() string {
	helpText := `
Usage: cacao get js2allocation

	Gets all JetStream2 allocations
`
	return strings.TrimSpace(helpText)
}

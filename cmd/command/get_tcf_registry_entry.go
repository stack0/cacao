package command

import (
	"strings"
)

// GetTCFRegistryEntryCommand ...
type GetTCFRegistryEntryCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetTCFRegistryEntryCommand) Run(args []string) int {
	var entryName string
	if len(args) > 0 {
		entryName = "/" + args[0]
	}

	req := c.NewRequest("GET", "/tcfregistry"+entryName, "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetTCFRegistryEntryCommand) Synopsis() string {
	return "get tcf registry entries"
}

// Help ...
func (c *GetTCFRegistryEntryCommand) Help() string {
	helpText := `
Usage: cacao get tcf_registry_entry [name]

	Gets all tcf registry entries unless specific name is provided
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// TemplateCommand ..
type TemplateCommand struct {
	*BaseCommand
}

// Run ...
func (c *TemplateCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *TemplateCommand) Synopsis() string {
	return "Manages templates"
}

// Help ...
func (c *TemplateCommand) Help() string {
	helpText := `
	Usage: cacao template <subcommand>

`
	return strings.TrimSpace(helpText)
}

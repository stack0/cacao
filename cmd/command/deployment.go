package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// DeploymentCommand ...
type DeploymentCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeploymentCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *DeploymentCommand) Synopsis() string {
	return "Manages deployments"
}

// Help ...
func (c *DeploymentCommand) Help() string {
	helpText := `
	Usage: cacao deployment <subcommand>

`
	return strings.TrimSpace(helpText)
}

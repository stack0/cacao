package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// JS2AllocationCommand ...
type JS2AllocationCommand struct {
	*BaseCommand
}

// Run ...
func (c *JS2AllocationCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *JS2AllocationCommand) Synopsis() string {
	return "Manages JetStream2 allocation"
}

// Help ...
func (c *JS2AllocationCommand) Help() string {
	helpText := `
	Usage: cacao j2allocation <subcommand>

`
	return strings.TrimSpace(helpText)
}

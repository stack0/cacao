package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// CheckCommand ...
type CheckCommand struct {
	*BaseCommand
}

// Run ...
func (c *CheckCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *CheckCommand) Synopsis() string {
	return "check an interactive session"
}

// Help ...
func (c *CheckCommand) Help() string {
	helpText := `
	Usage: cacao check <subcommand>
`
	return strings.TrimSpace(helpText)
}

package command

import (
	"strings"
)

// DeleteInteractiveSessionCommand ...
type DeleteInteractiveSessionCommand struct {
	*BaseCommand
}

// Run ...
func (c *DeleteInteractiveSessionCommand) Run(args []string) int {
	if len(args) != 1 {
		c.UI.Error("Incorrect number of arguments (expected 1)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("DELETE", "/isessions/"+args[0], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *DeleteInteractiveSessionCommand) Synopsis() string {
	return "delete an interactive session"
}

// Help ...
func (c *DeleteInteractiveSessionCommand) Help() string {
	helpText := `
	Usage: cacao isession delete [interactive-session ID]
`
	return strings.TrimSpace(helpText)
}

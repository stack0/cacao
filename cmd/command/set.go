package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// SetCommand ...
type SetCommand struct {
	*BaseCommand
}

// Run ...
func (c *SetCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *SetCommand) Synopsis() string {
	return "set user config or recent "
}

// Help ...
func (c *SetCommand) Help() string {
	helpText := `
	Usage: cacao set <subcommand>

`
	return strings.TrimSpace(helpText)
}

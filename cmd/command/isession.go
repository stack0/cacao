package command

import (
	"strings"

	"github.com/mitchellh/cli"
)

// ISessionCommand ..
type ISessionCommand struct {
	*BaseCommand
}

// Run ...
func (c *ISessionCommand) Run(args []string) int {
	return cli.RunResultHelp
}

// Synopsis ...
func (c *ISessionCommand) Synopsis() string {
	return "Manages interactive-session"
}

// Help ...
func (c *ISessionCommand) Help() string {
	helpText := `
	Usage: cacao isession <subcommand>

`
	return strings.TrimSpace(helpText)
}

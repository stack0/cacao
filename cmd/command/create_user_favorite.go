package command

import (
	"strings"
)

// CreateUserFavoriteCommand ...
type CreateUserFavoriteCommand struct {
	*BaseCommand
}

// Run ...
func (c *CreateUserFavoriteCommand) Run(args []string) int {
	if len(args) != 3 {
		c.UI.Error("Incorrect number of arguments (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("POST", "/users/"+args[0]+"/favorites/"+args[1]+"/"+args[2], "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *CreateUserFavoriteCommand) Synopsis() string {
	return "add a user favorite"
}

// Help ...
func (c *CreateUserFavoriteCommand) Help() string {
	helpText := `
Usage: cacao create user favorite [USERNAME] [NAME] [VALUE]
`
	return strings.TrimSpace(helpText)
}

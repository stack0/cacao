package command

import (
	"strings"
)

// GetTemplateSourceTypeCommand ...
type GetTemplateSourceTypeCommand struct {
	Meta
	*BaseCommand
}

// Run ...
func (c *GetTemplateSourceTypeCommand) Run(args []string) int {
	req := c.NewRequest("GET", "/templates/sourcetypes", "")
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *GetTemplateSourceTypeCommand) Synopsis() string {
	return "get template source types"
}

// Help ...
func (c *GetTemplateSourceTypeCommand) Help() string {
	helpText := `
Usage: cacao get template_source_type

	Gets all template types
`
	return strings.TrimSpace(helpText)
}

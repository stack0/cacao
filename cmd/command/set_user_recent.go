package command

import (
	"strings"
)

// SetUserRecentCommand ...
type SetUserRecentCommand struct {
	*BaseCommand
}

// Run ...
func (c *SetUserRecentCommand) Run(args []string) int {
	if len(args) != 3 {
		c.UI.Error("Incorrect number of arguments (expected 3)")
		c.UI.Error(c.Help())
		return 1
	}

	req := c.NewRequest("POST", "/users/"+args[0]+"/recents/"+args[1], args[2])
	c.DoRequest(req)
	return 0
}

// Synopsis ...
func (c *SetUserRecentCommand) Synopsis() string {
	return "add a user favorite"
}

// Help ...
func (c *SetUserRecentCommand) Help() string {
	helpText := `
Usage: cacao set user recent [USERNAME] [NAME] [VALUE]
`
	return strings.TrimSpace(helpText)
}

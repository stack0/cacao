package main

import (
	"github.com/mitchellh/cli"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/cmd/command"
)

// Commands holds a map of all possible commands
func Commands(meta *command.Meta) map[string]cli.CommandFactory {
	log.Trace("commands.go/Commands: start")
	baseCommand := command.NewBaseCommand(*meta)
	log.Trace("commands.go/Commands: received baseCommand")
	return map[string]cli.CommandFactory{
		"user": func() (cli.Command, error) {
			return &command.UserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"user create": func() (cli.Command, error) {
			return &command.CreateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"user get": func() (cli.Command, error) {
			return &command.GetUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"user delete": func() (cli.Command, error) {
			return &command.DeleteUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"user update": func() (cli.Command, error) {
			return &command.UpdateUserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider": func() (cli.Command, error) {
			return &command.ProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider create": func() (cli.Command, error) {
			return &command.CreateProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider get": func() (cli.Command, error) {
			return &command.GetProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider op": func() (cli.Command, error) {
			return &command.OpProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider delete": func() (cli.Command, error) {
			return &command.DeleteProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"provider update": func() (cli.Command, error) {
			return &command.UpdateProviderCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"credential": func() (cli.Command, error) {
			return &command.CredentialCommand{
				BaseCommand: &baseCommand,
			}, nil

		},
		"credential create": func() (cli.Command, error) {
			return &command.CreateCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"credential get": func() (cli.Command, error) {
			return &command.GetCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"credential delete": func() (cli.Command, error) {
			return &command.DeleteCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"credential update": func() (cli.Command, error) {
			return &command.UpdateCredentialCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"isession": func() (cli.Command, error) {
			return &command.ISessionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"isession create": func() (cli.Command, error) {
			return &command.CreateInteractiveSessionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"isession get": func() (cli.Command, error) {
			return &command.GetInteractiveSessionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"isession delete": func() (cli.Command, error) {
			return &command.DeleteInteractiveSessionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"isession check": func() (cli.Command, error) {
			return &command.CheckInteractiveSessionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"workspace": func() (cli.Command, error) {
			return &command.WorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"workspace create": func() (cli.Command, error) {
			return &command.CreateWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"workspace delete": func() (cli.Command, error) {
			return &command.DeleteWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"workspace get": func() (cli.Command, error) {
			return &command.GetWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"workspace update": func() (cli.Command, error) {
			return &command.UpdateWorkspaceCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template": func() (cli.Command, error) {
			return &command.TemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template create": func() (cli.Command, error) {
			return &command.CreateTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template get": func() (cli.Command, error) {
			return &command.GetTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template update": func() (cli.Command, error) {
			return &command.UpdateTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template delete": func() (cli.Command, error) {
			return &command.DeleteTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template sync": func() (cli.Command, error) {
			return &command.SyncTemplateCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_type ": func() (cli.Command, error) {
			return &command.TemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_type create": func() (cli.Command, error) {
			return &command.CreateTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_type delete": func() (cli.Command, error) {
			return &command.DeleteTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_type get": func() (cli.Command, error) {
			return &command.GetTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_type update": func() (cli.Command, error) {
			return &command.UpdateTemplateTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"tcf_registry_entry": func() (cli.Command, error) {
			return &command.TCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"tcf_registry_entry create": func() (cli.Command, error) {
			return &command.CreateTCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"tcf_registry_entry delete": func() (cli.Command, error) {
			return &command.DeleteTCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"tcf_registry_entry get": func() (cli.Command, error) {
			return &command.GetTCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"tcf_registry_entry update": func() (cli.Command, error) {
			return &command.UpdateTCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"tcf_registry_entry query": func() (cli.Command, error) {
			return &command.QueryTCFRegistryEntryCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"deployment": func() (cli.Command, error) {
			return &command.DeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"deployment create": func() (cli.Command, error) {
			return &command.CreateDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"deployment get": func() (cli.Command, error) {
			return &command.GetDeploymentCommand{
				BaseCommand: &baseCommand,
				Meta:        *meta,
			}, nil
		},
		"deployment delete": func() (cli.Command, error) {
			return &command.DeleteDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"deployment update": func() (cli.Command, error) {
			return &command.UpdateDeploymentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"run create": func() (cli.Command, error) {
			return &command.CreateRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"run get": func() (cli.Command, error) {
			return &command.GetRunCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"js2user": func() (cli.Command, error) {
			return &command.JS2UserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"js2user get": func() (cli.Command, error) {
			return &command.GetJS2UserCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"js2allocation": func() (cli.Command, error) {
			return &command.JS2AllocationCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"js2allocation get": func() (cli.Command, error) {
			return &command.GetJS2AllocationCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_source_type": func() (cli.Command, error) {
			return &command.TemplateSourceTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"template_source_type get": func() (cli.Command, error) {
			return &command.GetTemplateSourceTypeCommand{
				BaseCommand: &baseCommand,
			}, nil
		},

		"build get": func() (cli.Command, error) {
			return &command.GetBuildCommand{
				BaseCommand: &baseCommand,
				Meta:        *meta,
			}, nil
		},
		"version get": func() (cli.Command, error) {
			return &command.GetVersionCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"login": func() (cli.Command, error) {
			return &command.LoginCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"logout": func() (cli.Command, error) {
			return &command.LogoutCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"set": func() (cli.Command, error) {
			return &command.SetCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"set user config": func() (cli.Command, error) {
			return &command.SetUserConfigCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"set user recent": func() (cli.Command, error) {
			return &command.SetUserRecentCommand{
				BaseCommand: &baseCommand,
			}, nil
		},
		"version": func() (cli.Command, error) {
			return &command.VersionCommand{
				Meta:     *meta,
				Version:  Version,
				Revision: GitCommit,
				Name:     Name,
			}, nil
		},
	}
}

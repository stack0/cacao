package main

import (
	"fmt"
	"os"

	"github.com/mitchellh/cli"
	"gitlab.com/cyverse/cacao/cmd/command"

	log "github.com/sirupsen/logrus"
)

/*func checkRequirements() (error,error) {
	if len(os.Getenv("CACAO_TOKEN")) == 0 {
		return fmt.Errorf("CACAO_TOKEN env var is required"), nil
	}
	if len(os.Getenv("CACAO_API")) == 0 {
		return nil, fmt.Errorf("CACAO_API env var is required")
	}
	return nil, nil
}*/

// Run ...
func Run(args []string) int {
	log.Trace("Run: start")

	// Check for universal flags
	output := "default_pretty"
	outputCurlString := false
	printLog := false
	log.SetLevel(log.InfoLevel)
	for i, arg := range args {
		if (arg == "-o" || arg == "--output") && len(args) > i+1 {
			if args[i+1] == "raw" {
				output = "raw"
			}
			args[i] = ""
			args[i+1] = ""
		} else if arg == "-c" || arg == "--curl" {
			outputCurlString = true
			args[i] = ""
		} else if arg == "-v" || arg == "--verbose" {
			printLog = true
			log.SetLevel(log.DebugLevel)
		} else if arg == "-vv" {
			printLog = true
			log.SetLevel(log.TraceLevel)
		}
	}

	// Meta-option for executables.
	// It defines output color and its stdout/stderr stream.
	meta := &command.Meta{
		UI: &cli.ColoredUi{
			InfoColor:  cli.UiColorBlue,
			ErrorColor: cli.UiColorRed,
			Ui: &cli.BasicUi{
				Writer:      os.Stdout,
				ErrorWriter: os.Stderr,
				Reader:      os.Stdin,
			},
		},
		OutputFormat:     output,
		OutputCurlString: outputCurlString,
		PrintLog:         printLog,
	}

	// Check if 'help' or '--help' is in args
	/*help := false
	logIn := false
	for _, arg := range args {
		if arg == "help" || arg == "--help" {
			help = true
			break
		}
		if arg == "login"{
			logIn = true
		}
	}*/

	// Do not worry about requirements if requesting help or logging in
	/*tokenError, apiError := checkRequirements()
	if !help && !logIn && tokenError != nil || apiError !=nil {
		if tokenError != nil{
			meta.UI.Error(tokenError.Error())
			return 1
		}
		meta.UI.Error(apiError.Error())
		return 1
	}
	if logIn && apiError !=nil{
		meta.UI.Error(apiError.Error())
		return 1
	}
	*/
	return RunCustom(args, Commands(meta))
}

// RunCustom ...
func RunCustom(args []string, commands map[string]cli.CommandFactory) int {
	log.Trace("RunCustom: start")

	// Get the command line args. We shortcut "--version" and "-v" to
	// just show the version.
	for _, arg := range args {
		if arg == "--version" {
			newArgs := make([]string, len(args)+1)
			newArgs[0] = "version"
			copy(newArgs[1:], args)
			args = newArgs
			break
		}
	}

	cli := &cli.CLI{
		Args:     args,
		Commands: commands,
		Version:  Version,
		HelpFunc: func(cmds map[string]cli.CommandFactory) string {
			additionalOutput := fmt.Sprintf(`
Universal flags:

    -v, --verbose                  Sets the log level to debug
    -vv                            Sets the log level to trace
    -c --curl                      Instead of executing the command, prints the equivalent curl command
    -o --output <output_type>      Controls the output format of HTTP responses options: [raw,default_pretty]
`)

			return cli.BasicHelpFunc(Name)(cmds) + additionalOutput
		},
		HelpWriter: os.Stdout,
	}

	exitCode, err := cli.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute: %s\n", err.Error())
	}

	return exitCode
}

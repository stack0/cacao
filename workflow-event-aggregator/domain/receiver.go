package domain

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/ports"
	"gitlab.com/cyverse/cacao/workflow-event-aggregator/types"
)

// EventReceiver receives events from a single ports.EventSrc then write the
// events into a destination channel.
// If the source has failed, the receiver will clean up itself, and notify Domain via the failedNotify channel.
type EventReceiver struct {
	name string
	// destination to write the event into
	destChan      chan<- types.Event
	conf          types.StanConfig
	eventSrc      ports.EventSrc
	srcChan       chan types.Event
	failureNotify chan<- string
}

// NewEventReceiver creates new EventReceiver
func NewEventReceiver(name string, factory ports.EventSrcFactory, dest chan<- types.Event, failureNotify chan<- string) EventReceiver {
	receiver := EventReceiver{
		name:          name,
		destChan:      dest,
		eventSrc:      factory(name),
		srcChan:       make(chan types.Event),
		failureNotify: failureNotify,
	}
	return receiver
}

// Init initialize EventReceiver, EventSrc will be initialized
func (receiver *EventReceiver) Init(conf types.StanConfig) error {
	receiver.conf = conf
	receiver.eventSrc.InitChannel(receiver.srcChan)
	return receiver.eventSrc.InitSrc(conf)
}

// Start starts receiving events from EventSrc
func (receiver EventReceiver) Start() {

	var wg sync.WaitGroup

	receiver.eventSrc.Start()

	wg.Add(1)
	go receiver.receiveEvents(&wg)

	wg.Wait()

	// sends a notification that the receiver failed
	receiver.failureNotify <- receiver.name
}

func (receiver EventReceiver) receiveEvents(wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "Eventreceiver.forwardEvents",
	})
	logger.Info("starting EventReceiver.receiveEvents()")
	defer logger.Info("EventReceiver.receiveEvents() exiting")
	defer wg.Done()

	for event := range receiver.srcChan {
		logger.WithFields(log.Fields{"eventType": event.EventType()}).Info("received event")
		receiver.destChan <- event
	}
	// channel closed which indicate connection lost
}

// Close performs a cleanup
func (receiver EventReceiver) Close() {
	// cleanup
	select {
	case _, ok := <-receiver.srcChan:
		if ok {
			close(receiver.srcChan)
		}
	default:
		close(receiver.srcChan)
	}
	receiver.eventSrc.Close()
}

// ReceiverMonitor will monitor the failureNotify channel, and replace failed receiver.
// There will be the same number of go routine running ReceiverMonitor as the number of EventReceiver.
// This way there is the same number of producer and consumer on the go channel, and the channel will not be blocked.
type ReceiverMonitor struct {
	srcFactory    ports.EventSrcFactory
	eventChan     chan types.Event
	receivers     map[string]EventReceiver
	failureNotify chan string
	conf          types.Config
}

// NewReceiverMonitor creates new ReceiverMonitor
func NewReceiverMonitor(domain *Domain) ReceiverMonitor {
	return ReceiverMonitor{
		srcFactory:    domain.srcFactory,
		eventChan:     domain.eventChan,
		receivers:     domain.receivers,
		failureNotify: domain.failureNotify,
		conf:          domain.conf,
	}
}

// Monitor will monitor the failureNotify channel.
// Upon receiving a notification (name of the failed receiver), it will attempt to replace the failed receiver by creating a new one;
// and it will block until it is able to do so successfully.
func (mon ReceiverMonitor) Monitor(wg *sync.WaitGroup) {
	logger := log.WithField("function", "ReceiverMonitor.Monitor")
	defer wg.Done()
	defer logger.WithField("function", "ReceiverMonitor.Monitor").Info("monitor terminated")

	for receiverName := range mon.failureNotify {
		logger.WithField("receiver", receiverName).Info("receiver failed, attempt to re-create")
		failedReceiver := mon.receivers[receiverName]
		failedReceiver.Close()
		delete(mon.receivers, receiverName)

		// will retry until the receiver is created again
		for {
			// replace with new receiver
			receiver := NewEventReceiver(receiverName, mon.srcFactory, mon.eventChan, mon.failureNotify)
			err := receiver.Init(mon.conf.Srcs[receiverName])
			if err != nil {
				logger.Error(err)
			} else {
				mon.receivers[receiverName] = receiver
				go mon.receivers[receiverName].Start()
				break
			}
		}
	}
}

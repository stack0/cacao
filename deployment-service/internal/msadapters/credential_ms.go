package msadapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) CredentialMicroservice {
	return CredentialMicroservice{
		natsConf: natsConf,
		stanConf: stanConf,
	}
}

// Get get a credential by ID from credential microservice.
// Note: the combination of username and cred ID is unique. The ID alone is Not.
func (c CredentialMicroservice) Get(actor, emulator string, owner, id string) (service.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "CredentialMicroservice.Get",
		"actor":    actor,
		"emulator": emulator,
		"cred":     id,
	})
	if actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClient(context.TODO(), actor, emulator, c.natsConf, c.stanConf)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	cred, err := client.Get(id)
	if err != nil {
		errorMessage := "fail to get credential"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got cred from cred ms")
	return cred, nil
}

// List returns a list of all credentials owned by a user.
func (c CredentialMicroservice) List(actor, emulator string, owner string) ([]service.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "CredentialMicroservice.List",
		"actor":    actor,
		"emulator": emulator,
		"owner":    owner,
	})
	if actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	if actor != owner {
		// for now assume actor == owner, meaning user can only query their own credential without emulation.
		return nil, service.NewCacaoUnauthorizedError("actor != username")
	}
	client, err := service.NewNatsCredentialClient(context.TODO(), actor, emulator, c.natsConf, c.stanConf)
	if err != nil {
		errorMessage := "fail to create credential svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	credentials, err := client.List()
	if err != nil {
		errorMessage := "fail to list credentials"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got credentials from cred ms")
	return credentials, nil
}

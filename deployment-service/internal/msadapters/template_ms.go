package msadapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// TemplateMicroservice is a client for template microservice
type TemplateMicroservice struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewTemplateMicroservice ...
func NewTemplateMicroservice(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) TemplateMicroservice {
	return TemplateMicroservice{
		natsConf: natsConf,
		stanConf: stanConf,
	}
}

// Get fetches a template by ID
func (t TemplateMicroservice) Get(actor, emulator string, id common.ID) (service.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "TemplateMicroservice.Get",
		"actor":    actor,
		"emulator": emulator,
		"template": id,
	})
	if actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	client, err := service.NewNatsTemplateClient(context.TODO(), actor, emulator, t.natsConf, t.stanConf)
	if err != nil {
		errorMessage := "fail to create template svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	template, err := client.Get(id)
	if err != nil {
		errorMessage := "fail to get template"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got template from template ms")
	return template, nil
}

// GetTemplateType fetches a template type by name
func (t TemplateMicroservice) GetTemplateType(actor, emulator, templateTypeName string) (service.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":      "msadapters",
		"function":     "TemplateMicroservice.GetTemplateType",
		"actor":        actor,
		"emulator":     emulator,
		"templateType": templateTypeName,
	})
	if actor == "" {
		return nil, service.NewCacaoInvalidParameterError("actor is empty")
	}
	client, err := service.NewNatsTemplateClient(context.TODO(), actor, emulator, t.natsConf, t.stanConf)
	if err != nil {
		errorMessage := "fail to create template svc client"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoCommunicationError(errorMessage)
	}
	templateType, err := client.GetType(templateTypeName)
	if err != nil {
		errorMessage := "fail to get template type"
		logger.WithError(err).Error(errorMessage)
		return nil, service.NewCacaoGeneralError(errorMessage)
	}
	logger.Trace("got template type from template ms")
	return templateType, nil
}

package msadapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// WorkspaceMicroservice is a client to interact with Workspace microservice
type WorkspaceMicroservice struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) WorkspaceMicroservice {
	return WorkspaceMicroservice{
		natsConf: natsConf,
		stanConf: stanConf,
	}
}

// Get a workspace
func (w WorkspaceMicroservice) Get(actor, emulator string, id common.ID) (service.Workspace, error) {
	logger := log.WithFields(log.Fields{
		"package":   "msadapters",
		"function":  "WorkspaceMicroservice.Get",
		"actor":     actor,
		"emulator":  emulator,
		"workspace": id,
	})
	client, err := service.NewNatsWorkspaceClient(context.TODO(), actor, emulator, w.natsConf, w.stanConf)
	if err != nil {
		logger.WithError(err).Error("fail to create workspace svc client")
		return nil, err
	}

	workspace, err := client.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to get workspace")
		return nil, err
	}
	logger.Debug("got workspace from workspace ms")
	return workspace, nil
}

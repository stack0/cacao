package msadapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderMicroservice is a client to provider microservice
type ProviderMicroservice struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewProviderMicroservice ...
func NewProviderMicroservice(natsConf messaging.NatsConfig, stanConf messaging.StanConfig) ProviderMicroservice {
	return ProviderMicroservice{
		natsConf: natsConf,
		stanConf: stanConf,
	}
}

// Get fetch a provider by ID
func (p ProviderMicroservice) Get(actor, emulator string, id common.ID) (service.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "msadapters",
		"function": "ProviderMicroservice.Get",
		"actor":    actor,
		"emulator": emulator,
		"provider": id,
	})
	client, err := service.NewNatsProviderClient(context.TODO(), actor, emulator, p.natsConf, p.stanConf)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	provider, err := client.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to get provider")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"name": provider.GetName(),
		"type": provider.GetType(),
		"url":  provider.GetURL(),
	}).Trace("got provider from provider ms")
	return provider, nil
}

// List fetches a list of provider
func (p ProviderMicroservice) List(actor, emulator string) ([]service.Provider, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMicroservice.List",
		"actor":    actor,
		"emulator": emulator,
	})
	client, err := service.NewNatsProviderClient(context.TODO(), actor, emulator, p.natsConf, p.stanConf)
	if err != nil {
		logger.WithError(err).Error("fail to create provider svc client")
		return nil, err
	}
	providerList, err := client.List()
	if err != nil {
		logger.WithError(err).Error("fail to list providers")
		return nil, err
	}
	logger.WithFields(log.Fields{
		"len": len(providerList),
	}).Trace("got provider list from provider ms")
	return providerList, nil
}

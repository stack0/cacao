package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// DeploymentCreateRunResult is an event that contains the result to the create run request
type DeploymentCreateRunResult service.DeploymentCreateRunResult

// EventType ...
func (d DeploymentCreateRunResult) EventType() common.EventType {
	if d.ErrorType != "" {
		return service.DeploymentRunCreateFailed
	}
	return service.DeploymentRunCreated
}

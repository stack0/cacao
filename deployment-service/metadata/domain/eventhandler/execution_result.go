package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ExecutionSucceededHandler handles when execution stage of the run succeeded. Thus, the run has finished successfully
type ExecutionSucceededHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewExecutionSucceededHandler ...
func NewExecutionSucceededHandler(portsDependency ports.Ports) ExecutionSucceededHandler {
	return ExecutionSucceededHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h ExecutionSucceededHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "ExecutionSucceededHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.handle(incoming.Transaction(), eventBody)
}

func (h ExecutionSucceededHandler) unmarshalEvent(event types.IncomingEvent) (deploymentevents.RunExecutionSucceeded, error) {
	var eventBody deploymentevents.RunExecutionSucceeded
	err := json.Unmarshal(event.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunExecutionSucceeded{}, err
	}
	return eventBody, nil
}

// Note that if error(s) occurs during handling the event, they do NOT prevent the handler from emitting DeploymentRunStatusUpdated.
// This is because DeploymentRunActive is a terminal status, thus it is worth publishing the event and notifying whoever
// is listening for it, even though it is inconsistent with the statuses in storage; And the inconsistency will be
// reflected later in read query.
func (h ExecutionSucceededHandler) handle(tid common.TransactionID, event deploymentevents.RunExecutionSucceeded) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "ExecutionSucceededHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	var mergedErr error
	oldRunStatus, err := h.getOldRunStatus(event)
	if err != nil {
		logger.WithError(err).Error("fail to get old run status")
		mergedErr = mergeError(mergedErr, err)
	}
	err = h.updateRun(event)
	if err != nil {
		logger.WithError(err).Error("fail to update run")
		mergedErr = mergeError(mergedErr, err)
	}
	err = h.updateDeploymentStatus(event)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment")
		mergedErr = mergeError(mergedErr, err)
	}
	if mergedErr != nil {
		logger.WithError(err).Error("errors occurs while handling the event, but emit the DeploymentRunStatusUpdated event any way")
	}
	return h.runStatusUpdatedEvent(tid, event, oldRunStatus, mergedErr)
}

func (h ExecutionSucceededHandler) getOldRunStatus(event deploymentevents.RunExecutionSucceeded) (deploymentcommon.DeploymentRunStatus, error) {
	run, err := h.runStorage.Get(event.Run)
	if err != nil {
		// Note that the "" status could be used in the DeploymentRunStatus event
		return "", err
	}
	return run.Status, nil
}

// update run status, and save StateView
func (h ExecutionSucceededHandler) updateRun(event deploymentevents.RunExecutionSucceeded) error {
	now := h.timeSrc.Now()
	newStatus := deploymentcommon.DeploymentRunActive
	runUpdate := types.DeploymentRunUpdate{
		EndsAt:         &now,
		Status:         &newStatus,
		LastState:      &event.StateView,
		StateUpdatedAt: &now,
	}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status and state view", event.Run)
	}
	return nil
}

func (h ExecutionSucceededHandler) updateDeploymentStatus(event deploymentevents.RunExecutionSucceeded) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	runUpdate, filter := statusUpdater.StatusActive()
	updated, err := h.deploymentStorage.Update(event.Deployment, runUpdate, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find deployment %s in storage, fail to update status", event.Deployment)
	}
	return nil
}

func (h ExecutionSucceededHandler) runStatusUpdatedEvent(tid common.TransactionID, event deploymentevents.RunExecutionSucceeded, oldRunStatus deploymentcommon.DeploymentRunStatus, err error) ResponseEvent {
	svcErr := service.CacaoErrorBase{}
	if err != nil {
		svcErr = types.ErrorToServiceError(err).GetBase()
	}
	eventBody := deploymentevents.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    svcErr,
		},
		Deployment: event.Deployment,
		Run:        event.Run,
		Status:     deploymentcommon.DeploymentRunActive,
		OldStatus:  oldRunStatus,
	}
	return NewEvent(deploymentevents.EventDeploymentRunStatusUpdated, tid, eventBody)
}

// merge newErr into an existing err (could be nil)
func mergeError(err error, newErr error) error {
	if err != nil {
		return fmt.Errorf("%s, %w", err.Error(), newErr)
	}
	return newErr
}

// ExecutionFailedHandler handles when execution stage of the run failed (either the execution stage finishes with error, or fail to start execution stage).
type ExecutionFailedHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewExecutionFailedHandler ...
func NewExecutionFailedHandler(portsDependency ports.Ports) ExecutionFailedHandler {
	return ExecutionFailedHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h ExecutionFailedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "ExecutionFailedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.handle(eventBody)
}

func (h ExecutionFailedHandler) unmarshalEvent(event types.IncomingEvent) (deploymentevents.RunExecutionFailed, error) {
	var eventBody deploymentevents.RunExecutionFailed
	err := json.Unmarshal(event.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunExecutionFailed{}, err
	}
	return eventBody, nil
}

// This will update run status and the deployment status.
//
// Note that if error(s) occurs during handling the event, they do NOT prevent the handler from emitting DeploymentRunStatusUpdated.
// This is because DeploymentRunErrored is a terminal status, thus it is worth publishing the event and notifying whoever
// is listening for it, even though it is inconsistent with the statuses in storage; And the inconsistency will be
// reflected later in read query.
func (h ExecutionFailedHandler) handle(event deploymentevents.RunExecutionFailed) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "ExecutionFailedHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	err := h.updateRun(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err).Errorf("fail to update run, this does not prevent emitting %s event", deploymentevents.EventDeploymentRunStatusUpdated)
	}
	err2 := h.updateDeploymentStatus(event)
	if err2 != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err2).Errorf("fail to update deployment status, this does not prevent emitting %s event", deploymentevents.EventDeploymentRunStatusUpdated)
		// merge 2 errors, so that neither is lost
		err = fmt.Errorf("%w, %s", err2, err.Error())
	}
	return h.runStatusUpdatedEvent("", event, err)
}

func (h ExecutionFailedHandler) updateRun(event deploymentevents.RunExecutionFailed) error {
	now := h.timeSrc.Now()
	newStatus := deploymentcommon.DeploymentRunErrored
	statusMsg := fmt.Sprintf("execution stage failed, %s", event.GetServiceError().Error())
	newState := event.StateView
	runUpdate := types.DeploymentRunUpdate{
		EndsAt:         &now,
		Status:         &newStatus,
		StatusMsg:      &statusMsg,
		LastState:      &newState,
		StateUpdatedAt: &now,
	}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status", event.Run)
	}
	return nil
}

func (h ExecutionFailedHandler) updateDeploymentStatus(event deploymentevents.RunExecutionFailed) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	deploymentUpdate, filter := statusUpdater.StatusCreationFailed()

	statusMsg := fmt.Sprintf("run %s failed in execution stage", event.Run)
	deploymentUpdate.StatusMsg = &statusMsg

	updated, err := h.deploymentStorage.Update(event.Deployment, deploymentUpdate, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find deployment %s in storage, fail to update status", event.Deployment)
	}
	return nil
}

func (h ExecutionFailedHandler) runStatusUpdatedEvent(tid common.TransactionID, event deploymentevents.RunExecutionFailed, err error) ResponseEvent {
	if event.GetServiceError() == nil {
		// populate service error if missing from incoming event
		msg := fmt.Sprintf("execution stage failed, error not included in %s", deploymentevents.EventRunExecutionFailed)
		event.ServiceError = service.NewCacaoGeneralError(msg).GetBase()
	}
	eventBody := deploymentevents.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.MergeServiceErrorAndError(event.Session, err),
		},
		Deployment: event.Deployment,
		Run:        event.Run,
		Status:     deploymentcommon.DeploymentRunErrored,
		OldStatus:  deploymentcommon.DeploymentRunRunning, // hard-code running status to convey that the run failed in execution stage
	}
	return NewEvent(deploymentevents.EventDeploymentRunStatusUpdated, tid, eventBody)
}

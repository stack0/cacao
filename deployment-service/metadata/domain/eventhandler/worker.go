package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
)

// DoNotSent this constant is for event that should not be sent, but has to return an event
//
// FIXME consider making return response event optional, not all cases have a valid response event.
const DoNotSent common.EventType = "DoNotSentThis"

// EventWorker is a worker that will process incoming events
type EventWorker struct {
	eventSink                       ports.EventSink
	creationHandler                 CreationHandler
	runCreationHandler              RunCreationHandler
	updateHandler                   UpdateHandler
	deletionHandler                 DeletionHandler
	runStartedHandler               RunStartedHandler
	runStartFailedHandler           RunStartFailedHandler
	preflightFailedHandler          PreflightFailedHandler
	executionStartedHandler         ExecutionStartedHandler
	executionSucceededHandler       ExecutionSucceededHandler
	executionFailedHandler          ExecutionFailedHandler
	deletionCleanupSucceededHandler DeletionCleanupSucceededHandler
	deletionCleanupFailedHandler    DeletionCleanupFailedHandler
}

// NewEventWorker create a event worker
func NewEventWorker(portsDependency ports.Ports) EventWorker {
	return EventWorker{
		eventSink:                       portsDependency.EventSink,
		creationHandler:                 NewCreationHandler(portsDependency),
		runCreationHandler:              NewRunCreationHandler(portsDependency),
		updateHandler:                   NewUpdateHandler(portsDependency.DeploymentStorage),
		deletionHandler:                 NewDeletionHandler(portsDependency),
		runStartedHandler:               NewRunStartedHandler(portsDependency),
		runStartFailedHandler:           NewRunStartFailedHandler(portsDependency),
		preflightFailedHandler:          NewPreflightFailedHandler(portsDependency),
		executionStartedHandler:         NewExecutionStartedHandler(portsDependency),
		executionSucceededHandler:       NewExecutionSucceededHandler(portsDependency),
		executionFailedHandler:          NewExecutionFailedHandler(portsDependency),
		deletionCleanupSucceededHandler: NewDeletionCleanupSucceededHandler(portsDependency),
		deletionCleanupFailedHandler:    NewDeletionCleanupFailedHandler(portsDependency),
	}
}

// Run starts processing incoming events from the channel
func (w EventWorker) Run(wg *sync.WaitGroup, eventChan <-chan types.IncomingEvent) {
	defer wg.Done()

	for event := range eventChan {
		handler := w.SelectEventHandler(event)
		if handler != nil {
			response := handler.Handle(event)
			if response.EventType() == DoNotSent {
				// ignore
				continue
			}
			err := w.eventSink.Publish(response)
			if err != nil {
				log.WithFields(log.Fields{
					"incoming": event.EventType(),
					"outgoing": response.EventType(),
					"err":      err.Error(),
				}).Error("fail to publish response event")
			}
		}
	}
}

// SelectEventHandler selects an EventHandler for the event
func (w EventWorker) SelectEventHandler(event types.IncomingEvent) EventHandler {
	switch event.EventType() {
	case service.DeploymentCreationRequested:
		return w.creationHandler
	case service.DeploymentCreateRunRequested:
		return w.runCreationHandler
	case service.DeploymentDeletionRequested:
		return w.deletionHandler
	case deploymentevents.EventRunPreflightStarted:
		return w.runStartedHandler
	case deploymentevents.EventRunPreflightStartFailed:
		return w.runStartFailedHandler
	case deploymentevents.EventRunPreflightFailed:
		return w.preflightFailedHandler
	case deploymentevents.EventRunExecutionStarted:
		return w.executionStartedHandler
	case deploymentevents.EventRunExecutionSucceeded:
		return w.executionSucceededHandler
	case deploymentevents.EventRunExecutionFailed:
		return w.executionFailedHandler
	case service.DeploymentUpdateRequested:
		return w.updateHandler
	case deploymentevents.EventDeploymentDeletionCleanupSucceeded:
		return w.deletionCleanupSucceededHandler
	case deploymentevents.EventDeploymentDeletionCleanupFailed:
		return w.deletionCleanupFailedHandler
	default:
		return nil
	}
}

package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// RunStartFailedHandler handles when run failed to start, thus run creation failed.
type RunStartFailedHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewRunStartFailedHandler ...
func NewRunStartFailedHandler(portsDependency ports.Ports) RunStartFailedHandler {
	return RunStartFailedHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h RunStartFailedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "RunStartFailedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.toRespEvent(logger, incoming.Transaction(), h.handle(eventBody))
}

func (h RunStartFailedHandler) unmarshalEvent(event types.IncomingEvent) (deploymentevents.RunPreflightStartFailed, error) {
	var eventBody deploymentevents.RunPreflightStartFailed
	err := json.Unmarshal(event.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunPreflightStartFailed{}, err
	}
	return eventBody, nil
}

func (h RunStartFailedHandler) handle(event deploymentevents.RunPreflightStartFailed) service.DeploymentCreateRunResult {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "RunStartFailedHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	err := h.deletePendingRun(event)
	if err != nil {
		logger.WithError(err).Error("fail to delete record of pending run placeholder")
		return h.runCreateFailedWithNewError(event, err)
	}
	err = h.updateDeploymentStatus(event)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status")
		return h.runCreateFailedWithNewError(event, err)
	}
	return h.runCreateFailed(event)
}

// remove placeholder run object in storage (since run creation failed)
func (h RunStartFailedHandler) deletePendingRun(event deploymentevents.RunPreflightStartFailed) error {
	deleted, err := h.runStorage.DeletePlaceholder(event.Run)
	if err != nil {
		return err
	}
	if !deleted {
		return fmt.Errorf("fail to find placeholder for run creation request, %s", event.Run.String())
	}
	return nil
}

func (h RunStartFailedHandler) updateDeploymentStatus(event deploymentevents.RunPreflightStartFailed) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	deploymentUpdate, filter := statusUpdater.StatusCreationFailed()
	updated, err := h.deploymentStorage.Update(event.Deployment, deploymentUpdate, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find deployment %s in storage, fail to update status", event.Deployment)
	}
	return nil
}

// The error occurs in this handler will be merged with the original error in the incoming event.
func (h RunStartFailedHandler) runCreateFailedWithNewError(event deploymentevents.RunPreflightStartFailed, err error) service.DeploymentCreateRunResult {
	if event.GetServiceError() == nil {
		// populate service error if missing from incoming event
		event.ServiceError = h.defaultServiceError()
	}
	return service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.MergeServiceErrorAndError(event.Session, err),
		},
		Run: service.DeploymentRun{Deployment: event.Deployment},
	}
}

func (h RunStartFailedHandler) runCreateFailed(event deploymentevents.RunPreflightStartFailed) service.DeploymentCreateRunResult {
	var svcError service.CacaoErrorBase
	if event.GetServiceError() == nil {
		// populate service error if missing from incoming event
		event.ServiceError = h.defaultServiceError()
	} else {
		svcError = event.ServiceError
	}
	return service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    svcError,
		},
		Run: service.DeploymentRun{Deployment: event.Deployment},
	}
}

func (h RunStartFailedHandler) toRespEvent(logger *log.Entry, tid common.TransactionID, result service.DeploymentCreateRunResult) ResponseEvent {
	logger.Error("run create failed event")
	return NewEvent(service.DeploymentRunCreateFailed, tid, result)
}

// a default service error to use if service error is missing from incoming event
func (h RunStartFailedHandler) defaultServiceError() service.CacaoErrorBase {
	msg := fmt.Sprintf("create run failed, error not included in %s", deploymentevents.EventRunPreflightStartFailed)
	return service.NewCacaoGeneralError(msg).GetBase()
}

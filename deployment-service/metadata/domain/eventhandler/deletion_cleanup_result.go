package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// DeletionCleanupSucceededHandler handles success result of deletion cleanup.
type DeletionCleanupSucceededHandler struct {
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewDeletionCleanupSucceededHandler ...
func NewDeletionCleanupSucceededHandler(portsDependency ports.Ports) DeletionCleanupSucceededHandler {
	return DeletionCleanupSucceededHandler{
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h DeletionCleanupSucceededHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionCleanupSucceededHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	err = h.updateDeploymentStatus(eventBody)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status")
		return h.deploymentDeletionFailedEvent(eventBody, err)
	}
	return h.deploymentDeletedEvent(eventBody)
}

func (h DeletionCleanupSucceededHandler) unmarshalEvent(incoming types.IncomingEvent) (deploymentevents.DeploymentDeletionCleanupResult, error) {
	var eventBody deploymentevents.DeploymentDeletionCleanupResult
	err := json.Unmarshal(incoming.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.DeploymentDeletionCleanupResult{}, err
	}
	return eventBody, nil
}

func (h DeletionCleanupSucceededHandler) updateDeploymentStatus(event deploymentevents.DeploymentDeletionCleanupResult) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	update, filter := statusUpdater.StatusDeleted()
	updated, err := h.deploymentStorage.Update(event.Deployment, update, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("deployment %s not found in storage, failed to update status to %s", event.Deployment, service.DeploymentStatusDeleted)
	}
	return nil
}

func (h DeletionCleanupSucceededHandler) deploymentDeletedEvent(event deploymentevents.DeploymentDeletionCleanupResult) ResponseEvent {
	var eventBody = service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    event.GetSessionActor(),
			SessionEmulator: event.GetSessionEmulator(),
		},
		ID: event.Deployment,
	}
	return NewEvent(service.DeploymentDeleted, "", eventBody)
}

func (h DeletionCleanupSucceededHandler) deploymentDeletionFailedEvent(event deploymentevents.DeploymentDeletionCleanupResult, err error) ResponseEvent {
	var eventBody = service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    event.GetSessionActor(),
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.MergeServiceErrorAndError(event.Session, err),
		},
		ID: event.Deployment,
	}
	return NewEvent(service.DeploymentDeleteFailed, "", eventBody)
}

// DeletionCleanupFailedHandler handles failed deletion cleanup
type DeletionCleanupFailedHandler struct {
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewDeletionCleanupFailedHandler ...
func NewDeletionCleanupFailedHandler(portsDependency ports.Ports) DeletionCleanupFailedHandler {
	return DeletionCleanupFailedHandler{
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h DeletionCleanupFailedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionCleanupFailedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	err = h.updateDeploymentStatus(eventBody)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status")
		// log and continue
	}
	return h.deploymentDeletionFailedEvent(eventBody, err)
}

func (h DeletionCleanupFailedHandler) unmarshalEvent(incoming types.IncomingEvent) (deploymentevents.DeploymentDeletionCleanupResult, error) {
	var eventBody deploymentevents.DeploymentDeletionCleanupResult
	err := json.Unmarshal(incoming.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.DeploymentDeletionCleanupResult{}, err
	}
	return eventBody, nil
}

func (h DeletionCleanupFailedHandler) updateDeploymentStatus(event deploymentevents.DeploymentDeletionCleanupResult) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	update, filter := statusUpdater.StatusDeletionErrored()
	updated, err := h.deploymentStorage.Update(event.Deployment, update, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("deployment %s not found, failed to update status to %s", event.Deployment, service.DeploymentStatusDeletionErrored)
	}
	return nil
}

func (h DeletionCleanupFailedHandler) deploymentDeletionFailedEvent(event deploymentevents.DeploymentDeletionCleanupResult, err error) ResponseEvent {
	var eventBody = service.DeploymentDeletionResult{
		Session: service.Session{
			SessionActor:    event.GetSessionActor(),
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.MergeServiceErrorAndError(event.Session, err),
		},
		ID: event.Deployment,
	}
	return NewEvent(service.DeploymentDeleteFailed, "", eventBody)
}

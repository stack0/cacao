package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// PreflightFailedHandler handles when the preflight stage of the run failed.
type PreflightFailedHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewPreflightFailedHandler ...
func NewPreflightFailedHandler(portsDependency ports.Ports) PreflightFailedHandler {
	return PreflightFailedHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h PreflightFailedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "PreflightFailedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.handle(incoming.Transaction(), eventBody)
}

func (h PreflightFailedHandler) unmarshalEvent(event types.IncomingEvent) (deploymentevents.RunPreflightResult, error) {
	var eventBody deploymentevents.RunPreflightResult
	err := json.Unmarshal(event.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunPreflightResult{}, err
	}
	return eventBody, nil
}

// This will update run status and the deployment status.
//
// Note that if error(s) occurs during handling the event, they do NOT prevent the handler from emitting DeploymentRunStatusUpdated.
// This is because DeploymentRunErrored is a terminal status, thus it is worth publishing the event and notifying whoever
// is listening for it, even though it is inconsistent with the statuses in storage; And the inconsistency will be
// reflected later in read query.
func (h PreflightFailedHandler) handle(tid common.TransactionID, event deploymentevents.RunPreflightResult) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "PreflightFailedHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	err := h.updateRunStatus(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err).Errorf("failed to update run status, this does not prevent emitting %s event", deploymentevents.EventDeploymentRunStatusUpdated)
	}
	err2 := h.updateDeploymentStatus(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err2).Errorf("fail to update deployment status, this does not prevent emitting %s event", deploymentevents.EventDeploymentRunStatusUpdated)
		// merge 2 errors, so that neither is lost
		err = fmt.Errorf("%w, %s", err2, err.Error())
	}
	return h.runStatusUpdatedEvent(tid, event, err)
}

func (h PreflightFailedHandler) updateRunStatus(event deploymentevents.RunPreflightResult) error {
	now := h.timeSrc.Now()
	newStatus := deploymentcommon.DeploymentRunErrored
	newStatusMsg := fmt.Sprintf("preflight stage failed, %s", event.GetServiceError().Error())
	runUpdate := types.DeploymentRunUpdate{
		EndsAt:         &now,
		Status:         &newStatus,
		StatusMsg:      &newStatusMsg,
		StateUpdatedAt: &now,
	}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status", event.Run)
	}
	return nil
}

func (h PreflightFailedHandler) updateDeploymentStatus(event deploymentevents.RunPreflightResult) error {
	statusUpdater := types.NewDeploymentStatusUpdater(h.timeSrc.Now())
	deploymentUpdate, filter := statusUpdater.StatusCreationFailed()
	updated, err := h.deploymentStorage.Update(event.Deployment, deploymentUpdate, filter)
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find deployment %s in storage, fail to update status", event.Deployment)
	}
	return nil
}

func (h PreflightFailedHandler) runStatusUpdatedEvent(tid common.TransactionID, event deploymentevents.RunPreflightResult, err error) ResponseEvent {
	if event.GetServiceError() == nil {
		// populate service error if missing from incoming event
		msg := fmt.Sprintf("preflight stage failed, error not included in %s", deploymentevents.EventRunPreflightFailed)
		event.ServiceError = service.NewCacaoGeneralError(msg).GetBase()
	}
	eventBody := deploymentevents.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.MergeServiceErrorAndError(event.Session, err),
		},
		Deployment: event.Deployment,
		Run:        event.Run,
		Status:     deploymentcommon.DeploymentRunErrored,
		OldStatus:  deploymentcommon.DeploymentRunPreflight, // hard-code preflight status to convey that the run failed in preflight stage
	}
	return NewEvent(deploymentevents.EventDeploymentRunStatusUpdated, tid, eventBody)
}

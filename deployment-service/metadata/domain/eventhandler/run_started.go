package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// RunStartedHandler handles when run has started successfully, which means run creation has succeeded
type RunStartedHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewRunStartedHandler ...
func NewRunStartedHandler(portsDependency ports.Ports) RunStartedHandler {
	return RunStartedHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h RunStartedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "RunStartedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.toRespEvent(logger, incoming.Transaction(), h.handle(eventBody))
}

func (h RunStartedHandler) unmarshalEvent(event types.IncomingEvent) (deploymentevents.RunPreflightStarted, error) {
	var eventBody deploymentevents.RunPreflightStarted
	err := json.Unmarshal(event.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunPreflightStarted{}, err
	}
	return eventBody, nil
}

func (h RunStartedHandler) handle(event deploymentevents.RunPreflightStarted) service.DeploymentCreateRunResult {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "RunStartedHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	err := h.updateRun(event.Run, event.Parameters)
	if err != nil {
		logger.WithError(err).Error("fail to update run status")
		return h.runCreateFailedEvent(event, err)
	}
	err = h.updateDeployment(event)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status and last run")
		return h.runCreateFailedEvent(event, err)
	}
	run, err := h.runStorage.Get(event.Run)
	if err != nil {
		logger.WithError(err).Error("fail to fetch run from storage")
		return h.runCreateFailedEvent(event, err)
	}
	return h.runCreatedEvent(event, run)
}

// update run status, and save converted parameters
func (h RunStartedHandler) updateRun(runID common.ID, parameters deploymentcommon.DeploymentParameters) error {
	newStatus := deploymentcommon.DeploymentRunPreflight
	runUpdate := types.DeploymentRunUpdate{
		Parameters: &parameters,
		Status:     &newStatus,
	}
	updated, err := h.runStorage.Update(runID, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("run %s not found in storage, fail to update status", runID)
	}
	return nil
}

// update status msg and last run ID
func (h RunStartedHandler) updateDeployment(event deploymentevents.RunPreflightStarted) error {
	lastRunID := &event.Run
	statusMsg := fmt.Sprintf("run %s started", event.Run)
	deploymentUpdate := types.DeploymentUpdate{
		StatusMsg: &statusMsg,
		LastRun:   &lastRunID,
	}
	updated, err := h.deploymentStorage.Update(event.Deployment, deploymentUpdate, types.DeploymentFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find deployment %s in storage, fail to update last run", event.Deployment)
	}
	return nil
}

func (h RunStartedHandler) runCreatedEvent(event deploymentevents.RunPreflightStarted, run types.DeploymentRun) service.DeploymentCreateRunResult {
	var eventBody = service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
		},
		Run: run.ConvertToExternal(),
	}
	return eventBody
}

func (h RunStartedHandler) runCreateFailedEvent(event deploymentevents.RunPreflightStarted, err error) service.DeploymentCreateRunResult {
	var eventBody = service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		Run: service.DeploymentRun{Deployment: event.Deployment},
	}
	return eventBody
}

func (h RunStartedHandler) toRespEvent(logger *log.Entry, tid common.TransactionID, result service.DeploymentCreateRunResult) ResponseEvent {
	if result.ServiceError.StandardMessage != "" || result.ServiceError.ContextualMessage != "" {
		logger.Error("run create failed event")
		return NewEvent(service.DeploymentRunCreateFailed, tid, result)
	}
	logger.Error("run created event")
	return NewEvent(service.DeploymentRunCreated, tid, result)
}

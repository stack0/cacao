package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ExecutionStartedHandler handles when then execution stage has started
type ExecutionStartedHandler struct {
	runStorage ports.DeploymentRunStorage
	timeSrc    ports.TimeSrc
}

// NewExecutionStartedHandler ...
func NewExecutionStartedHandler(portsDependency ports.Ports) ExecutionStartedHandler {
	return ExecutionStartedHandler{
		runStorage: portsDependency.RunStorage,
		timeSrc:    portsDependency.TimeSrc,
	}
}

// Handle ...
func (h ExecutionStartedHandler) Handle(incoming types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "ExecutionStartedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return NewEvent(DoNotSent, "", nil)
	}
	return h.handle(incoming.Transaction(), eventBody)
}

func (h ExecutionStartedHandler) unmarshalEvent(incoming types.IncomingEvent) (deploymentevents.RunPreflightResult, error) {
	var eventBody deploymentevents.RunPreflightResult
	err := json.Unmarshal(incoming.CloudEvent().Data(), &eventBody)
	if err != nil {
		return deploymentevents.RunPreflightResult{}, err
	}
	return eventBody, nil
}

func (h ExecutionStartedHandler) handle(tid common.TransactionID, event deploymentevents.RunPreflightResult) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":        "eventhandler",
		"function":       "ExecutionStartedHandler.handle",
		"deployment":     event.Deployment,
		"deploymentType": event.DeploymentType,
		"run":            event.Run,
	})
	oldStatus, err := h.getOldStatus(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err).Errorf("fail to fetch run %s to get its status, this does not prevent emitting %s event", deploymentcommon.DeploymentRunRunning, deploymentevents.EventDeploymentRunStatusUpdated)
	} else if oldStatus != deploymentcommon.DeploymentRunPreflight {
		logger.WithField("runStatus", oldStatus).Warn("current status of run is not preflight when execution started")
	}
	err2 := h.updateRunStatus(event)
	if err2 != nil {
		logger.WithError(err2).Errorf("fail to update run status to %s", deploymentcommon.DeploymentRunRunning)
		// for the sake of consistency (run status in storage, and event gateway), skip emitting the DeploymentRunStatusUpdated event.
		// since DeploymentRunRunning is not a terminal status for run, it is okay to skip.
		return NewEvent(DoNotSent, "", nil)
	}
	return h.runStatusUpdatedEvent(tid, event, oldStatus, err)
}

func (h ExecutionStartedHandler) getOldStatus(event deploymentevents.RunPreflightResult) (deploymentcommon.DeploymentRunStatus, error) {
	run, err := h.runStorage.Get(event.Run)
	if err != nil {
		// Note that the "" status will be used in the DeploymentRunStatusUpdated event when fail to fetch run
		return "", err
	}
	return run.Status, nil
}

func (h ExecutionStartedHandler) updateRunStatus(event deploymentevents.RunPreflightResult) error {
	newStatus := deploymentcommon.DeploymentRunRunning
	runUpdate := types.DeploymentRunUpdate{Status: &newStatus}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status", event.Run)
	}
	return nil
}

func (h ExecutionStartedHandler) runStatusUpdatedEvent(tid common.TransactionID, incoming deploymentevents.RunPreflightResult, oldStatus deploymentcommon.DeploymentRunStatus, err error) ResponseEvent {
	var svcErr service.CacaoErrorBase
	if err != nil {
		svcErr = types.ErrorToServiceError(err).GetBase()
	}
	eventBody := deploymentevents.DeploymentRunStatusUpdated{
		Session: service.Session{
			SessionActor:    incoming.SessionActor,
			SessionEmulator: incoming.SessionEmulator,
			ServiceError:    svcErr,
		},
		Deployment: incoming.Deployment,
		Run:        incoming.Run,
		Status:     deploymentcommon.DeploymentRunRunning,
		OldStatus:  oldStatus,
	}
	return NewEvent(deploymentevents.EventDeploymentRunStatusUpdated, tid, eventBody)
}

package eventhandler

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// CreationHandler handles creation of deployment.
// This handles only perform permission checks and basic validation, just enough to create
// a valid deployment object in storage.
type CreationHandler struct {
	storage     ports.DeploymentStorage
	templateMS  ports.TemplateMicroservice
	workspaceMS ports.WorkspaceMicroservice
	providerMS  ports.ProviderMicroservice
	credMS      ports.CredentialMicroservice
	timeSrc     ports.TimeSrc
}

// NewCreationHandler ...
func NewCreationHandler(portsDependency ports.Ports) CreationHandler {
	return CreationHandler{
		storage:     portsDependency.DeploymentStorage,
		templateMS:  portsDependency.TemplateMS,
		workspaceMS: portsDependency.WorkspaceMS,
		providerMS:  portsDependency.ProviderMS,
		credMS:      portsDependency.CredentialMS,
		timeSrc:     portsDependency.TimeSrc,
	}
}

// Handle ...
func (h CreationHandler) Handle(event types.IncomingEvent) ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.Handle",
	})
	var request service.DeploymentCreationRequest
	request, err := h.eventToRequest(event)
	if err != nil {
		logger.WithError(err).Trace("fail to convert event to request")
		result := h.createFailedResult(request, err)

		return h.toRespEvent(event.Transaction(), result)
	}
	return h.toRespEvent(event.Transaction(), h.handle(request))
}

func (h CreationHandler) eventToRequest(event types.IncomingEvent) (service.DeploymentCreationRequest, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.eventToRequest",
	})

	var request service.DeploymentCreationRequest
	err := json.Unmarshal(event.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentCreationRequest"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentCreationRequest{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

func (h CreationHandler) handle(request service.DeploymentCreationRequest) service.DeploymentCreationResult {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "CreationHandler.handle",
	})
	var fac deploymentFactory
	err := fac.SetRequest(request)
	if err != nil {
		logger.WithError(err).Error("deployment creation request is invalid")
		return h.createFailedResult(request, err)
	}
	if err = h.dataGathering(request, &fac); err != nil {
		logger.WithError(err).Error("fail to gather data for deployment creation")
		return h.createFailedResult(request, err)
	}
	deployment, err := fac.Create(common.NewID(service.DeploymentIDPrefix))
	if err != nil {
		logger.WithError(err).Error("fail to create a deployment entity")
		return h.createFailedResult(request, err)
	}

	err = h.storage.Create(*deployment)
	if err != nil {
		logger.WithError(err).Error("fail to insert deployment into storage")
		return h.createFailedResult(request, err)
	}
	logger.WithField("deployment", deployment.ID).Info("deployment created")

	return service.DeploymentCreationResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.SessionEmulator,
		},
		ID: deployment.ID,
	}
}

// gather necessary data form external systems for creating a deployment
func (h CreationHandler) dataGathering(request service.DeploymentCreationRequest, fac *deploymentFactory) error {
	actor := types.ActorFromSession(request.Session)
	workspace, err := h.workspaceMS.Get(actor, request.CreateParam.Workspace)
	if err != nil {
		return err
	}
	if err = fac.SetWorkspace(workspace); err != nil {
		return err
	}
	template, err := h.templateMS.Get(actor, request.CreateParam.Template)
	if err != nil {
		return err
	}
	if err = fac.SetTemplate(template); err != nil {
		return err
	}
	if err = h.gatherProviders(request, fac); err != nil {
		return err
	}
	if err = h.gatherCredentials(request, fac); err != nil {
		return err
	}
	if err = fac.SetCreationTimestamp(h.timeSrc.Now()); err != nil {
		return err
	}
	return nil
}

func (h CreationHandler) gatherProviders(request service.DeploymentCreationRequest, fac *deploymentFactory) error {
	actor := types.ActorFromSession(request.Session)
	primary, err := h.providerMS.Get(actor, request.CreateParam.PrimaryCloud)
	if err != nil {
		return err
	}
	if err = fac.SetPrimaryProvider(primary); err != nil {
		return err
	}
	if request.CreateParam.CloudProviders == nil {
		return nil
	}
	for _, providerID := range request.CreateParam.CloudProviders {
		if providerID == request.CreateParam.PrimaryCloud {
			continue
		}
		provider, err2 := h.providerMS.Get(actor, providerID)
		if err2 != nil {
			return err2
		}
		if err = fac.AddProvider(provider); err != nil {
			return err
		}
	}
	return nil
}

// gather cloud credentials and git credential
func (h CreationHandler) gatherCredentials(request service.DeploymentCreationRequest, fac *deploymentFactory) error {
	actor := types.ActorFromSession(request.Session)
	for credID, providerID := range request.CreateParam.CloudCredentials {
		// only looks at credentials owned by actor
		cred, err := h.credMS.Get(actor, actor.Actor, credID)
		if err != nil {
			return err
		}
		if err = fac.AddCloudCredential(providerID, cred); err != nil {
			return err
		}
	}
	if request.CreateParam.GitCredential != "" {
		// only looks at credentials owned by actor
		gitCred, err := h.credMS.Get(actor, actor.Actor, request.CreateParam.GitCredential)
		if err != nil {
			return err
		}
		if err = fac.SetGitCredential(gitCred); err != nil {
			return err
		}
	}
	return nil
}

func (h CreationHandler) createFailedResult(request service.DeploymentCreationRequest, err error) service.DeploymentCreationResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentCreationResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
	}
}

func (h CreationHandler) toRespEvent(transaction common.TransactionID, result service.DeploymentCreationResult) ResponseEvent {
	if result.GetServiceError() == nil {
		return NewEvent(service.DeploymentCreated, transaction, result)
	}

	return NewEvent(service.DeploymentCreateFailed, transaction, result)
}

type credProviderIDPair struct {
	Credential service.Credential
	ProviderID common.ID
}

// deploymentFactory is a factory for creating a deployment entity object. This should NOT have any side effect and should NOT make external calls.
type deploymentFactory struct {
	request    service.DeploymentCreationRequest
	workspace  service.Workspace
	template   service.Template
	primary    service.Provider
	providers  []service.Provider
	cloudCreds []credProviderIDPair
	gitCred    service.Credential
	createdAt  time.Time
	// ID for the new deployment, will generate if empty. This is mostly for unit tests
	deploymentID common.ID
}

func (fac *deploymentFactory) SetRequest(request service.DeploymentCreationRequest) error {
	if request.GetSessionActor() == "" {
		return service.NewCacaoInvalidParameterError("actor cannot be empty")
	}
	if len(request.CreateParam.Description) > 255 {
		return service.NewCacaoInvalidParameterError("description too long")
	}
	if !request.CreateParam.Workspace.Validate() || request.CreateParam.Workspace.Prefix() != "workspace" {
		return service.NewCacaoInvalidParameterError("workspace ID in request is bad")
	}
	if !request.CreateParam.Template.Validate() || request.CreateParam.Template.Prefix() != "template" {
		return service.NewCacaoInvalidParameterError("template ID in request is bad")
	}
	if !request.CreateParam.PrimaryCloud.Validate() || request.CreateParam.PrimaryCloud.Prefix() != "provider" {
		return service.NewCacaoInvalidParameterError("provider ID in request is bad")
	}
	if request.CreateParam.CloudProviders != nil && len(request.CreateParam.CloudProviders) > 0 {
		for _, providerID := range request.CreateParam.CloudProviders {
			if !providerID.Validate() || providerID.Prefix() != "provider" {
				return service.NewCacaoInvalidParameterError("provider ID in request is bad")
			}
		}
	}
	if request.CreateParam.CloudCredentials == nil || len(request.CreateParam.CloudCredentials) == 0 {
		return service.NewCacaoInvalidParameterError("no cloud credential sepcified in request")
	}
	for credID, providerID := range request.CreateParam.CloudCredentials {
		if credID == "" {
			return service.NewCacaoInvalidParameterError("credential ID cannot be empty")
		}
		if !providerID.Validate() || providerID.Prefix() != "provider" {
			return service.NewCacaoInvalidParameterError("provider ID for credential is bad")
		}
		if providerID == request.CreateParam.PrimaryCloud {
			continue
		}
		if !idInList(providerID, request.CreateParam.CloudProviders) {
			return service.NewCacaoInvalidParameterError("provider ID for credential is not in list of provider IDs")
		}
	}
	fac.request = request
	return nil
}

func (fac *deploymentFactory) SetWorkspace(workspace service.Workspace) error {
	if workspace == nil {
		return service.NewCacaoInvalidParameterError("workspace object is nil")
	}
	if !workspace.GetID().Validate() || workspace.GetID().Prefix() != "workspace" {
		return service.NewCacaoInvalidParameterError("workspace object has bad ID")
	}
	if workspace.GetOwner() == "" {
		return service.NewCacaoInvalidParameterError("workspace has no owner")
	}
	fac.workspace = workspace
	return nil
}

func (fac *deploymentFactory) SetTemplate(template service.Template) error {
	if template == nil {
		return service.NewCacaoInvalidParameterError("template object is nil")
	}
	if !template.GetID().Validate() || template.GetID().Prefix() != "template" {
		return service.NewCacaoInvalidParameterError("template object has bad ID")
	}
	if template.GetOwner() == "" {
		return service.NewCacaoInvalidParameterError("template has no owner")
	}
	if template.GetSource().Type != "git" {
		return service.NewCacaoInvalidParameterError("template has unknown source type")
	}
	if template.GetSource().URI == "" {
		return service.NewCacaoInvalidParameterError("template has no source URI")
	}
	var accessParam struct {
		Branch string `mapstructure:"branch"`
		Tag    string `mapstructure:"tag"`
		Path   string `mapstructure:"path"`
	}
	err := mapstructure.Decode(template.GetSource().AccessParameters, &accessParam)
	if err != nil {
		return fmt.Errorf("fail to parse template access parameter, %w", err)
	}
	if accessParam.Branch == "" && accessParam.Tag == "" {
		return service.NewCacaoInvalidParameterError("template has neither branch nor tag in access parameter")
	}
	if accessParam.Path == "" {
		return service.NewCacaoInvalidParameterError("template has no path in access parameter")
	}
	if template.GetMetadata().Name == "" {
		return service.NewCacaoInvalidParameterError("template has no metadata name")
	}
	if template.GetMetadata().TemplateTypeName == "" {
		return service.NewCacaoInvalidParameterError("template has no metadata template type name")
	}
	if template.GetMetadata().Purpose == "" {
		return service.NewCacaoInvalidParameterError("template has no metadata purpose")
	}
	fac.template = template
	return nil
}

func (fac *deploymentFactory) SetPrimaryProvider(provider service.Provider) error {
	err := fac.basicCheckProvider(provider)
	if err != nil {
		return err
	}
	fac.primary = provider
	return nil
}

func (fac *deploymentFactory) AddProvider(provider service.Provider) error {
	err := fac.basicCheckProvider(provider)
	if err != nil {
		return err
	}
	if fac.primary != nil && fac.primary.GetID() == provider.GetID() {
		return service.NewCacaoInvalidParameterError("duplicate provider")
	}
	fac.providers = append(fac.providers, provider)
	return nil
}

func (fac *deploymentFactory) basicCheckProvider(provider service.Provider) error {
	if provider == nil {
		return service.NewCacaoInvalidParameterError("provider object is nil")
	}
	if !provider.GetID().Validate() || provider.GetID().Prefix() != "provider" {
		return service.NewCacaoInvalidParameterError("provider object has bad ID")
	}
	if provider.GetType() == "" {
		return service.NewCacaoInvalidParameterError("provider has no type")
	}
	if provider.GetURL() == "" {
		return service.NewCacaoInvalidParameterError("provider has no URL")
	}
	if fac.providers != nil {
		for _, prov := range fac.providers {
			if prov.GetID() == provider.GetID() {
				return service.NewCacaoInvalidParameterError("duplicate provider")
			}
		}
	}
	return nil
}

func (fac *deploymentFactory) AddCloudCredential(providerID common.ID, cred service.Credential) error {
	err := fac.basicCheckCredential(cred)
	if err != nil {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("cloud %s", err.Error()))
	}
	if !providerID.Validate() || providerID.Prefix() != "provider" {
		return service.NewCacaoInvalidParameterError("bad provider ID for cloud credential")
	}
	fac.cloudCreds = append(fac.cloudCreds, struct {
		Credential service.Credential
		ProviderID common.ID
	}{Credential: cred, ProviderID: providerID})
	return nil
}

func (fac *deploymentFactory) SetGitCredential(cred service.Credential) error {
	err := fac.basicCheckCredential(cred)
	if err != nil {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("git %s", err.Error()))
	}
	fac.gitCred = cred
	return nil
}

func (fac *deploymentFactory) basicCheckCredential(cred service.Credential) error {
	if cred == nil {
		return errors.New("credential object is nil")
	}
	if cred.GetID() == "" {
		return errors.New("credential object has bad ID")
	}
	if cred.GetType() == "" {
		return errors.New("credential has no type")
	}
	if cred.GetUsername() == "" {
		return errors.New("credential has empty username")
	}
	if cred.GetValue() == "" {
		return errors.New("credential has empty value")
	}
	return nil
}

func (fac *deploymentFactory) SetCreationTimestamp(createdAt time.Time) error {
	if createdAt.IsZero() {
		return service.NewCacaoInvalidParameterError("creation timestamp cannot be zero value")
	}
	fac.createdAt = createdAt
	return nil
}

// Create creates a new deployment entity object.
// Note: setters should be called before this.
func (fac deploymentFactory) Create(deploymentID common.ID) (*types.Deployment, error) {
	fac.checkAndSetDeploymentID(deploymentID)
	err := fac.checkIfPrerequisiteSet()
	if err != nil {
		return nil, err
	}
	if err = fac.checkPermission(); err != nil {
		return nil, err
	}
	if err = fac.checkConsistence(); err != nil {
		return nil, err
	}
	if err = fac.checkAllCloudCredentials(); err != nil {
		return nil, err
	}

	deployment := types.Deployment{
		ID:          fac.deploymentID,
		Name:        fac.request.CreateParam.Name,
		Description: fac.request.CreateParam.Description,
		CreatedAt:   fac.createdAt,
		UpdatedAt:   fac.createdAt,
		Workspace:   fac.request.CreateParam.Workspace,
		CreatedBy: deploymentcommon.Creator{
			User:     fac.request.GetSessionActor(),
			Emulator: fac.request.GetSessionEmulator(),
		},
		Template:             fac.request.CreateParam.Template,
		TemplateType:         fac.template.GetMetadata().TemplateTypeName,
		PrimaryCloudProvider: fac.request.CreateParam.PrimaryCloud,
		CurrentStatus:        service.DeploymentStatusNone,
		PendingStatus:        service.DeploymentStatusNoPending,
		StatusMsg:            "",
		CloudCredentials:     convertCloudCredentials(fac.request),
		GitCredential:        deploymentcommon.CredentialID(fac.request.CreateParam.GitCredential),
		LastRun:              nil,
	}
	err = types.ValidateDeployment(deployment)
	if err != nil {
		return nil, err
	}
	return &deployment, nil
}

func (fac deploymentFactory) checkIfPrerequisiteSet() error {
	if fac.workspace == nil {
		return service.NewCacaoInvalidParameterError("workspace not set for deployment creation")
	}
	if fac.template == nil {
		return service.NewCacaoInvalidParameterError("workspace not set for template creation")
	}
	if fac.primary == nil {
		return service.NewCacaoInvalidParameterError("workspace not set for primary provider creation")
	}
	if fac.cloudCreds == nil || len(fac.cloudCreds) == 0 {
		return service.NewCacaoInvalidParameterError("no cloud credential is provided")
	}
	if fac.createdAt.IsZero() {
		return service.NewCacaoInvalidParameterError("createdAt timestamp cannot be zero value")
	}
	if fac.template.GetSource().Visibility == service.TemplateSourceVisibilityPrivate && fac.gitCred == nil {
		return service.NewCacaoInvalidParameterError("git credential is required when template is private")
	}
	return nil
}

// check for consistence between prerequisite and request
func (fac deploymentFactory) checkConsistence() error {
	if fac.workspace.GetID() != fac.request.CreateParam.Workspace {
		return service.NewCacaoInvalidParameterError("inconsistent workspace ID")
	}
	if fac.template.GetID() != fac.request.CreateParam.Template {
		return service.NewCacaoInvalidParameterError("inconsistent template ID")
	}
	if fac.primary.GetID() != fac.request.CreateParam.PrimaryCloud {
		return service.NewCacaoInvalidParameterError("inconsistent primary provider ID")
	}
	for _, prov := range fac.providers {
		if !idInList(prov.GetID(), fac.request.CreateParam.CloudProviders) {
			return service.NewCacaoInvalidParameterError("inconsistent provider ID")
		}
	}
	for _, pair := range fac.cloudCreds {
		if pair.ProviderID != fac.request.CreateParam.PrimaryCloud && !idInList(pair.ProviderID, fac.request.CreateParam.CloudProviders) {
			return service.NewCacaoInvalidParameterError("inconsistent provider ID in credential pairs")
		}

		var credIDMatched bool
		for credID := range fac.request.CreateParam.CloudCredentials {
			if pair.Credential.GetID() == credID {
				credIDMatched = true
				break
			}
		}
		if !credIDMatched {
			return service.NewCacaoInvalidParameterError("inconsistent credential ID in credential pairs")
		}
	}
	if fac.request.CreateParam.GitCredential != "" && fac.gitCred.GetID() != fac.request.CreateParam.GitCredential {
		return service.NewCacaoInvalidParameterError("inconsistent git credential ID")
	}
	return nil
}

func (fac deploymentFactory) checkPermission() error {
	if fac.workspace.GetOwner() != fac.request.GetSessionActor() {
		return service.NewCacaoUnauthorizedError("unauthorized access to the workspace")
	}
	return nil
}

func (fac *deploymentFactory) checkAndSetDeploymentID(id common.ID) {
	fac.deploymentID = id
	if fac.deploymentID == "" || !fac.deploymentID.Validate() || fac.deploymentID.Prefix() != types.DeploymentIDPrefix {
		fac.deploymentID = common.NewID(types.DeploymentIDPrefix)
	}
}

func (fac deploymentFactory) checkAllCloudCredentials() error {
	for _, credPair := range fac.cloudCreds {
		err := fac.checkOneCloudCredential(credPair)
		if err != nil {
			return err
		}
	}
	return nil
}

// check if the provider ID associated with the credential is in deployment
func (fac deploymentFactory) checkOneCloudCredential(credPair credProviderIDPair) error {
	if fac.primary.GetID() == credPair.ProviderID {
		return nil
	}
	if fac.providers == nil {
		return service.NewCacaoInvalidParameterError("credential used for a provider not in deployment")
	}
	for _, prov := range fac.providers {
		if prov.GetID() == credPair.ProviderID {
			return nil
		}
	}
	return service.NewCacaoInvalidParameterError("credential used for a provider not in deployment")
}

func convertCloudCredentials(request service.DeploymentCreationRequest) deploymentcommon.ProviderCredentialMappings {
	var providerCredMapping = make(deploymentcommon.ProviderCredentialMappings, 0, len(request.CreateParam.CloudCredentials))
	for credID, providerID := range request.CreateParam.CloudCredentials {
		pair := deploymentcommon.ProviderCredentialPair{
			Credential: deploymentcommon.CredentialID(credID),
			Provider:   providerID,
		}
		providerCredMapping = append(providerCredMapping, pair)
	}
	return providerCredMapping
}

func idInList(id common.ID, idList []common.ID) bool {
	for _, id2 := range idList {
		if id2 == id {
			return true
		}
	}
	return false
}

package queryhandler

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
	"time"
)

// QueryWorker process queries from channel
type QueryWorker struct {
	getHandler     GetQueryHandler
	listHandler    ListQueryHandler
	getRunHandler  GetRunHandler
	listRunHandler ListRunHandler
	getLogs        GetLogsQueryHandler
	getRawState    GetRawStateQueryHandler

	clientID string
	sink     ports.ReplySink
}

// NewQueryWorker ...
func NewQueryWorker(
	clientID string,
	sink ports.ReplySink,
	storage ports.DeploymentStorage,
	runStorage ports.DeploymentRunStorage) QueryWorker {

	var w QueryWorker

	commonDependencies := HandlerCommon{Perm: NewPermissionCheck(storage)}
	w.getHandler = NewGetQueryHandler(storage, runStorage, commonDependencies)
	w.listHandler = NewListQueryHandler(storage, runStorage, commonDependencies)
	w.getRunHandler = NewGetRunHandler(runStorage, commonDependencies)
	w.listRunHandler = NewListRunHandler(runStorage, commonDependencies)
	w.getLogs = NewGetLogsQueryHandler(runStorage, commonDependencies)
	w.getRawState = NewGetRawStateQueryHandler(runStorage, commonDependencies)
	w.sink = sink
	w.clientID = clientID
	return w
}

// Run process queries from channel
func (w QueryWorker) Run(wg *sync.WaitGroup, queryChan <-chan types.Query) {
	defer wg.Done()

	for query := range queryChan {
		handler := w.SelectHandler(query.QueryType())
		if handler == nil {
			log.WithField("queryType", query.QueryType()).Trace("unsupported query")
			continue
		}
		replies := handler.HandleStream(query)
		for _, reply := range replies {
			w.ReplyToQuery(query, reply)
		}
	}
}

// SelectHandler returns appropriate handler for query based on its operation.
func (w QueryWorker) SelectHandler(op common.QueryOp) QueryHandler {
	switch op {
	case service.DeploymentGetQueryType:
		return w.getHandler
	case service.DeploymentListQueryType:
		return w.listHandler
	case service.DeploymentGetRawStateQueryType:
		return w.getRawState
	case service.DeploymentGetLogsQueryType:
		return w.getLogs
	case service.DeploymentGetRunQueryType:
		return w.getRunHandler
	case service.DeploymentListRunQueryType:
		return w.listRunHandler
	default:
		return nil
	}
}

// ReplyToQuery sends out the reply
func (w QueryWorker) ReplyToQuery(query types.Query, reply Reply) {
	logger := log.WithFields(log.Fields{
		"query":    query.QueryType(),
		"querySrc": query.CloudEvent().Source(),
	})

	replyCe, err := reply.ToCloudEvent(w.clientID)
	if err != nil {
		logger.WithError(err).Error("fail to marshal reply to cloudevent")
		return
	}

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*3)
	err = retry(ctx, 5, func() error {
		return w.sink.Reply(query.ReplySubject(), replyCe)
	})
	cancel()

	if err != nil {
		logger.WithError(err).Error("fail to send reply")
	} else {
		logger.Info("query replied")
	}
}

// retry a specified function for upto specified times within the specified context
func retry(ctx context.Context, retryTimes int, fn func() error) error {
	var err error
	for i := 0; i < retryTimes; i++ {
		err = fn()
		if err == nil {
			return nil
		}
		select {
		case <-ctx.Done():
			return nil
		default:
		}
	}
	return err
}

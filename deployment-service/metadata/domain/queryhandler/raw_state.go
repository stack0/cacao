package queryhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetRawStateQueryHandler handles GetRawState query
type GetRawStateQueryHandler struct {
	storage ports.DeploymentRunStorage
	HandlerCommon
}

// NewGetRawStateQueryHandler ...
func NewGetRawStateQueryHandler(
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) GetRawStateQueryHandler {
	return GetRawStateQueryHandler{
		storage:       runStorage,
		HandlerCommon: commonDependencies,
	}
}

// QueryOp ...
func (h GetRawStateQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRawStateQueryType
}

// Handle handles the query
func (h GetRawStateQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "getRawState",
	})
	var getRawState service.DeploymentGetRawStateQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getRawState)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetRawStateQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}
	logger = logger.WithFields(log.Fields{
		"deployment": getRawState.Deployment,
		"actor":      getRawState.SessionActor,
		"emulator":   getRawState.SessionEmulator,
	})

	run, err := h.storage.Get(getRawState.Run)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(err)
	}

	ok, err := h.Perm.DeploymentAccessByID(getRawState.GetSessionActor(), run.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	if !run.HasRawState() {
		return h.createErrorReply(service.NewCacaoNotFoundError("deployment raw state not available"))
	}

	// FIXME raw state needs to be handle by a different ms, since the raw state should not be stored by this microservice
	// this microservice will at most be forwarding the raw-state query to the ms that suppose to handle it
	panic("FIXME")
	//return h.createReply(*run.RawState)
}

// HandleStream handles the query
func (h GetRawStateQueryHandler) HandleStream(query types.Query) []Reply {
	return []Reply{h.Handle(query)}
}

func (h GetRawStateQueryHandler) createErrorReply(err error) service.DeploymentGetRawStateReply {
	var reply service.DeploymentGetRawStateReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	reply.RawState = nil
	return reply
}

func (h GetRawStateQueryHandler) createReply(rawState deploymentcommon.RawDeploymentState) service.DeploymentGetRawStateReply {
	var rawStateBytes []byte
	var err error
	switch rawState.Type {
	case deploymentcommon.TerraformRawState:
		rawStateBytes, err = json.Marshal(rawState.TFState)
		if err != nil {
			errorMessage := fmt.Sprintf("unable to unmarshal JSON bytes to TerraformRawState - %s", err.Error())
			return h.createErrorReply(service.NewCacaoMarshalError(errorMessage))
		}
	default:
		rawStateBytes = nil
	}

	return service.DeploymentGetRawStateReply{
		Session:  service.Session{},
		RawState: rawStateBytes,
	}
}

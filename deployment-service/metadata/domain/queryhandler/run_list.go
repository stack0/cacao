package queryhandler

import (
	"encoding/json"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// ListRunHandler is handler for ListRun query
type ListRunHandler struct {
	storage ports.DeploymentRunStorage
	HandlerCommon
}

// NewListRunHandler ...
func NewListRunHandler(
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) ListRunHandler {
	return ListRunHandler{
		storage:       runStorage,
		HandlerCommon: commonDependencies,
	}
}

// Handle handles the query
func (h ListRunHandler) Handle(query types.Query) Reply {
	panic("not implemented")
}

// HandleStream handles the query
func (h ListRunHandler) HandleStream(query types.Query) []Reply {
	logger := log.WithFields(log.Fields{
		"query": "listRun",
	})

	var listRunQuery service.DeploymentListRunQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &listRunQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentListRunQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}

	ok, err := h.Perm.DeploymentAccessByID(listRunQuery.GetSessionActor(), listRunQuery.Deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(service.NewCacaoUnauthorizedError(errorMessage))
	}

	filter := types.DeploymentRunFilter{
		Deployment: listRunQuery.Deployment,
	}
	runs, err := h.storage.List(filter, listRunQuery.RequestPagination)
	if err != nil {
		return h.createErrorReply(err)
	}

	return h.createReplies(listRunQuery.Offset, runs)
}

// QueryOp ...
func (h ListRunHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetRunQueryType
}

func (h ListRunHandler) createErrorReply(err error) []Reply {
	var reply service.DeploymentListRunReply
	reply.ReplyListStream = service.ReplyListStream{
		TotalReplies: 1,
		ReplyIndex:   0,
	}

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	return []Reply{reply}
}

func (h ListRunHandler) createReplies(initialOffset int, runs []types.DeploymentRun) []Reply {
	replies := make([]Reply, 0)
	offsetCounter := initialOffset

	if len(runs) == 0 {
		return []Reply{h.createEmptyReply(initialOffset)}
	} else if len(runs) < types.DeploymentRunStreamThreshold {
		return []Reply{h.singleReply(initialOffset, runs)}
	}
	allChunks := h.divideList(h.convertRunsToExternal(runs))
	for chunkIndex, chunk := range allChunks {
		replies = append(replies,
			service.DeploymentListRunReply{
				Session: service.Session{},
				Offset:  offsetCounter,
				Count:   len(chunk),
				Runs:    chunk,
				ReplyListStream: service.ReplyListStream{
					TotalReplies: len(allChunks),
					ReplyIndex:   chunkIndex,
				},
			})
		offsetCounter += len(chunk)
	}
	return replies
}

func (h ListRunHandler) createEmptyReply(initialOffset int) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session: service.Session{},
		Offset:  initialOffset,
		Count:   0,
		Runs:    []service.DeploymentRun{},
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListRunHandler) singleReply(initialOffset int, runs []types.DeploymentRun) service.DeploymentListRunReply {
	return service.DeploymentListRunReply{
		Session: service.Session{},
		Offset:  initialOffset,
		Count:   len(runs),
		Runs:    h.convertRunsToExternal(runs),
		ReplyListStream: service.ReplyListStream{
			TotalReplies: 1,
			ReplyIndex:   0,
		},
	}
}

func (h ListRunHandler) convertRunsToExternal(runs []types.DeploymentRun) []service.DeploymentRun {
	extRuns := make([]service.DeploymentRun, 0, len(runs))
	for _, run := range runs {
		extRuns = append(extRuns, run.ConvertToExternal())
	}
	return extRuns
}

func (h ListRunHandler) divideList(runs []service.DeploymentRun) [][]service.DeploymentRun {

	allChunks := make([][]service.DeploymentRun, 0)
	var chunk []service.DeploymentRun
	var start = 0
	var end = types.DeploymentRunStreamThreshold
	for start < len(runs) {
		chunk = runs[start:end]
		allChunks = append(allChunks, chunk)
		start = end
		if start+types.DeploymentRunStreamThreshold >= len(runs) {
			end = len(runs)
		} else {
			end = start + types.DeploymentRunStreamThreshold
		}
	}
	return allChunks
}

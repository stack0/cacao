package domain

import (
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain/eventhandler"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain/queryhandler"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"sync"
)

const queryChannDepth = 5
const eventChannDepth = 5
const workerCount = 5

// Domain is the entrypoint object of this microservice
type Domain struct {
	NatsSrc   ports.QuerySrc
	NatsSink  ports.ReplySink
	queryChan chan types.Query

	EventSrc  ports.EventSrc
	EventSink ports.EventSink
	eventChan chan types.IncomingEvent

	Storage    ports.DeploymentStorage
	RunStorage ports.DeploymentRunStorage
	TimeSrc    ports.TimeSrc

	WorkspaceMS ports.WorkspaceMicroservice
	TemplateMS  ports.TemplateMicroservice
	ProviderMS  ports.ProviderMicroservice
	CredMS      ports.CredentialMicroservice

	conf types.Config
}

// Init initialize all adapters, and domain internals
func (svc *Domain) Init(envConf types.EnvConfig, conf types.Config) error {

	svc.conf = conf

	if err := svc.Storage.Init(); err != nil {
		return err
	}

	if err := svc.RunStorage.Init(); err != nil {
		return err
	}

	if err := svc.NatsSrc.Init(conf.MessagingConfig.NatsConfig); err != nil {
		return err
	}

	svc.queryChan = make(chan types.Query, queryChannDepth)
	svc.NatsSrc.InitChannel(svc.queryChan)

	svc.eventChan = make(chan types.IncomingEvent, eventChannDepth)
	svc.EventSrc.InitChannel(svc.eventChan)
	if err := svc.EventSrc.Init(conf.MessagingConfig); err != nil {
		return err
	}
	return nil
}

// Start starts accepting incoming queries and processing them
func (svc Domain) Start() error {
	var wg sync.WaitGroup
	err := svc.startQuery(&wg)
	if err != nil {
		return err
	}
	err = svc.startEvent(&wg)
	if err != nil {
		return err
	}
	wg.Wait()
	return nil
}

func (svc Domain) startQuery(wg *sync.WaitGroup) error {
	err := svc.NatsSrc.Start()
	if err != nil {
		return err
	}

	svc.spawnQueryWorker(wg, workerCount)
	return nil
}

func (svc Domain) spawnQueryWorker(wg *sync.WaitGroup, workerCount int) {
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := queryhandler.NewQueryWorker(svc.conf.MessagingConfig.ClientID, svc.NatsSink, svc.Storage, svc.RunStorage)
		go worker.Run(wg, svc.queryChan)
	}
}

func (svc Domain) startEvent(wg *sync.WaitGroup) error {
	err := svc.EventSrc.Start()
	if err != nil {
		return err
	}

	svc.spawnEventWorker(wg, workerCount)
	return nil
}

func (svc Domain) spawnEventWorker(wg *sync.WaitGroup, workerCount int) {
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := eventhandler.NewEventWorker(ports.Ports{
			EventSink:         svc.EventSink,
			DeploymentStorage: svc.Storage,
			RunStorage:        svc.RunStorage,
			TimeSrc:           svc.TimeSrc,
			TemplateMS:        svc.TemplateMS,
			WorkspaceMS:       svc.WorkspaceMS,
			CredentialMS:      svc.CredMS,
			ProviderMS:        svc.ProviderMS,
		})
		go worker.Run(wg, svc.eventChan)
	}
}

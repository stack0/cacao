package main

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/domain"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

var envConf types.EnvConfig
var config types.Config

func init() {
	loadConfig()
}

func main() {
	svc := newService(envConf, config)

	if err := svc.Init(envConf, config); err != nil {
		log.Fatal(err)
	}
	if err := svc.Start(); err != nil {
		log.Fatal(err)
	}
}

func loadConfig() {
	err := envconfig.Process("", &envConf)
	if err != nil {
		log.Fatal(err)
	}
	lvl, err := log.ParseLevel(envConf.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(lvl)

	err = envconfig.Process("", &config.MessagingConfig)
	if err != nil {
		log.Fatal(err)
	}
	// set NATS client ID if missing
	if config.MessagingConfig.ClientID == "" {
		if envConf.PodName != "" {
			config.MessagingConfig.ClientID = envConf.PodName
		} else {
			config.MessagingConfig.ClientID = "deployment-metadata-" + xid.New().String()
		}
	}
	err = envconfig.Process("", &config.MongoConfig)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("envvar config loaded")
}

func newService(envConf types.EnvConfig, conf types.Config) domain.Domain {
	natsAdapter := adapters.NewNATSAdapter()
	stanAdapter := adapters.NewStanAdapter()
	svc := domain.Domain{
		NatsSrc:     natsAdapter,
		NatsSink:    natsAdapter,
		EventSrc:    stanAdapter,
		EventSink:   stanAdapter,
		Storage:     adapters.NewMongoDeploymentStorage(conf.MongoConfig),
		RunStorage:  adapters.NewMongoDeploymentRunStorage(conf.MongoConfig),
		TimeSrc:     adapters.UTCTimeSrc{},
		WorkspaceMS: adapters.NewWorkspaceMicroservice(conf.MessagingConfig),
		TemplateMS:  adapters.NewTemplateMicroservice(conf.MessagingConfig),
		ProviderMS:  adapters.NewProviderMicroservice(conf.MessagingConfig),
		CredMS:      adapters.NewCredentialMicroservice(conf.MessagingConfig),
	}
	return svc
}

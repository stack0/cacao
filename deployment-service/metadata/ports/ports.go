package ports

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"time"
)

// ReplySink is a sink to send the query reply into
type ReplySink interface {
	Init(messaging.NatsConfig) error
	Reply(subject string, reply cloudevents.Event) error
}

// QuerySrc is a source of incoming query to be processed
type QuerySrc interface {
	Init(messaging.NatsConfig) error
	InitChannel(chan<- types.Query)
	Start() error
}

// EventSink is a sink to publish events into
type EventSink interface {
	Init(types.StanConfig) error
	Publish(types.OutgoingEvent) error
}

// EventSrc is a sink to publish events into
type EventSrc interface {
	Init(types.StanConfig) error
	InitChannel(chan<- types.IncomingEvent)
	Start() error
}

// DeploymentStorage is storage for Deployment objects
type DeploymentStorage interface {
	Init() error
	Create(types.Deployment) error
	Get(common.ID) (types.Deployment, error)
	List(filters types.DeploymentFilter, offset int64, limit int64, sort types.DeploymentSort) ([]types.Deployment, error)
	Update(id common.ID, deployment types.DeploymentUpdate, filter types.DeploymentFilter) (bool, error)
	Delete(common.ID) error
}

// DeploymentBuildStorage is storage for DeploymentBuild
type DeploymentBuildStorage interface {
	// TODO implement later
}

// DeploymentRunStorage is storage for DeploymentRun objects
type DeploymentRunStorage interface {
	Init() error
	Get(runID common.ID) (types.DeploymentRun, error)
	// GetLatestRun returns the latest run of a deployment if any.
	GetLatestRun(deployment common.ID) (*types.DeploymentRun, error)
	List(filter types.DeploymentRunFilter, pagination service.RequestPagination) ([]types.DeploymentRun, error)
	BatchGet(runIDs []common.ID) ([]types.DeploymentRun, error)
	Create(run types.DeploymentRun) (runID common.ID, err error)
	Update(id common.ID, run types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (bool, error)
	// DeletePlaceholder deletes a placeholder record for a pending run creation request
	DeletePlaceholder(id common.ID) (bool, error)
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Workspace, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Template, error)
	GetTemplateType(actor types.Actor, templateTypeName string) (service.TemplateType, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Provider, error)
	List(actor types.Actor) ([]service.Provider, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor types.Actor, owner, id string) (service.Credential, error)
	// List return a list of credential ID
	List(actor types.Actor, owner string) ([]service.Credential, error)
}

// TimeSrc is source of current time
type TimeSrc interface {
	Now() time.Time
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	EventSink         EventSink
	DeploymentStorage DeploymentStorage
	RunStorage        DeploymentRunStorage
	TimeSrc           TimeSrc

	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
}

package adapters

// this file contains simple wrapper for microservice adapters from internal/msadapters package

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/msadapters"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// WorkspaceMicroservice is a client to interact with Workspace microservice
type WorkspaceMicroservice struct {
	ms msadapters.WorkspaceMicroservice
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(stanConf types.StanConfig) *WorkspaceMicroservice {
	return &WorkspaceMicroservice{ms: msadapters.NewWorkspaceMicroservice(stanConf.NatsConfig, stanConf.StanConfig)}
}

// Get a workspace
func (w WorkspaceMicroservice) Get(actor types.Actor, id common.ID) (service.Workspace, error) {
	return w.ms.Get(actor.Actor, actor.Emulator, id)
}

// ProviderMicroservice is a client to provider microservice
type ProviderMicroservice struct {
	ms msadapters.ProviderMicroservice
}

// NewProviderMicroservice ...
func NewProviderMicroservice(stanConf types.StanConfig) *ProviderMicroservice {
	return &ProviderMicroservice{ms: msadapters.NewProviderMicroservice(stanConf.NatsConfig, stanConf.StanConfig)}
}

// Get fetch a provider by ID
func (p ProviderMicroservice) Get(actor types.Actor, id common.ID) (service.Provider, error) {
	return p.ms.Get(actor.Actor, actor.Emulator, id)
}

// List fetches a list of provider
func (p ProviderMicroservice) List(actor types.Actor) ([]service.Provider, error) {
	return p.ms.List(actor.Actor, actor.Emulator)
}

// TemplateMicroservice is a client for template microservice
type TemplateMicroservice struct {
	ms msadapters.TemplateMicroservice
}

// NewTemplateMicroservice ...
func NewTemplateMicroservice(stanConf types.StanConfig) *TemplateMicroservice {
	return &TemplateMicroservice{ms: msadapters.NewTemplateMicroservice(stanConf.NatsConfig, stanConf.StanConfig)}
}

// Get fetches a template by ID
func (t TemplateMicroservice) Get(actor types.Actor, id common.ID) (service.Template, error) {
	return t.ms.Get(actor.Actor, actor.Emulator, id)
}

// GetTemplateType fetches a template type by name
func (t TemplateMicroservice) GetTemplateType(actor types.Actor, templateTypeName string) (service.TemplateType, error) {
	return t.ms.GetTemplateType(actor.Actor, actor.Emulator, templateTypeName)
}

// CredentialMicroservice ...
type CredentialMicroservice struct {
	ms msadapters.CredentialMicroservice
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(stanConf types.StanConfig) *CredentialMicroservice {
	return &CredentialMicroservice{ms: msadapters.NewCredentialMicroservice(stanConf.NatsConfig, stanConf.StanConfig)}
}

// Get a credential by ID from credential microservice.
// Note: the combination of username and cred ID is unique. The ID alone is Not.
func (c CredentialMicroservice) Get(actor types.Actor, owner, id string) (service.Credential, error) {
	return c.ms.Get(actor.Actor, actor.Emulator, owner, id)
}

// List returns a list of all credentials owned by a user.
func (c CredentialMicroservice) List(actor types.Actor, owner string) ([]service.Credential, error) {
	return c.ms.List(actor.Actor, actor.Emulator, owner)

}

package adapters

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// Query implements the types.Query interface
type Query struct {
	replySubject string
	ce           cloudevents.Event
}

// NewQuery creates a new query object
func NewQuery(replySubject string, ce cloudevents.Event) Query {
	return Query{replySubject: replySubject, ce: ce}
}

// QueryType returns the query operation
func (q Query) QueryType() common.QueryOp {
	return common.QueryOp(q.ce.Type())
}

// CloudEvent ...
func (q Query) CloudEvent() cloudevents.Event {
	return q.ce
}

// ReplySubject returns the reply subject of the query
func (q Query) ReplySubject() string {
	return q.replySubject
}

// NATSAdapter is an adapter for NATS, implements ports.ReplySink and ports.QuerySrc
type NATSAdapter struct {
	conf       messaging.NatsConfig
	nc         *nats.Conn
	sub        *nats.Subscription
	queryChann chan<- types.Query
}

// NewNATSAdapter creates a new NATSAdapter
func NewNATSAdapter() *NATSAdapter {
	return &NATSAdapter{}
}

// Init ...
func (adapter *NATSAdapter) Init(conf messaging.NatsConfig) error {
	adapter.conf = conf
	return nil
}

// InitChannel sets the query channel (where to send received query to)
func (adapter *NATSAdapter) InitChannel(chann chan<- types.Query) {
	adapter.queryChann = chann
}

// Start starts the subscription to receive queries
func (adapter *NATSAdapter) Start() error {
	nc, err := nats.Connect(adapter.conf.URL)
	if err != nil {
		return err
	}
	adapter.nc = nc
	log.WithField("url", adapter.conf.URL).Info("NATS connection established")

	handler := func(m *nats.Msg) {
		if m.Reply == "" {
			log.WithField("msg", m.Data).Error("no reply subject")
			return
		}
		ce, err := messaging.ConvertNats(m)
		if err != nil {
			log.WithField("err", err).Error("invalid cloudevent")
			return
		}
		q := Query{
			replySubject: m.Reply,
			ce:           ce,
		}
		adapter.queryChann <- q
	}
	sub, err := adapter.nc.QueueSubscribe(string(types.DeploymentWildcardSubject), adapter.conf.QueueGroup, handler)
	if err != nil {
		return err
	}
	adapter.sub = sub
	log.WithFields(log.Fields{
		"subject": types.DeploymentWildcardSubject,
		"queue":   adapter.conf.QueueGroup}).Info("subscribed to NATS")

	return nil
}

// Reply sends a reply to the specified subject
func (adapter *NATSAdapter) Reply(subject string, reply cloudevents.Event) error {
	payload, err := reply.MarshalJSON()
	if err != nil {
		return err
	}
	log.WithField("payloadLen", len(payload)).Trace("reply about to be sent")
	return adapter.nc.Publish(subject, payload)
}

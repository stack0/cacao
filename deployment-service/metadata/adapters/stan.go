package adapters

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// StanAdapter is an adapter that implements both EventSrc and EventSink
type StanAdapter struct {
	natsConf      messaging.NatsConfig
	stanConf      messaging.StanConfig
	cloudeventSrc string
	conn          *messaging.StanConnection
	eventChan     chan<- types.IncomingEvent
	// source field of cloudevent
}

// NewStanAdapter ...
func NewStanAdapter() *StanAdapter {
	return &StanAdapter{}
}

// Init initialize the STAN connection.
func (s *StanAdapter) Init(config types.StanConfig) error {
	s.cloudeventSrc = "deployment_workflow_monitor-" + config.NatsConfig.ClientID
	s.natsConf = config.NatsConfig
	s.stanConf = config.StanConfig
	return nil
}

// InitChannel ...
func (s *StanAdapter) InitChannel(channel chan<- types.IncomingEvent) {
	s.eventChan = channel
}

// Start starts the event subscriber. Incoming events will be deposited into the channel.
func (s *StanAdapter) Start() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Start",
	})
	if s.eventChan == nil {
		logger.Fatal("event channel is nil")
	}
	var mapping = []messaging.StreamingEventHandlerMapping{
		{
			Subject:           "", // empty subject for default handler
			CloudEventHandler: s.eventCallback,
			EventHandler:      nil,
		},
	}

	conn, err := messaging.ConnectStanForService(&s.natsConf, &s.stanConf, mapping)
	if err != nil {
		logger.WithError(err).Error()
		return err
	}
	s.conn = conn
	return nil
}

func (s StanAdapter) eventCallback(event *cloudevents.Event) error {
	if event != nil {
		s.eventChan <- incomingEvent{ce: *event}
	}
	return nil
}

// Publish ...
func (s StanAdapter) Publish(event types.OutgoingEvent) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Publish",
	})
	ce, err := event.ToCloudEvent(s.cloudeventSrc)
	if err != nil {
		logger.WithError(err).Error("fail to convert to cloudevent")
		return err
	}
	err = s.conn.PublishCloudEvent(&ce)
	if err != nil {
		logger.WithError(err).Error("fail to publish event")
		return err
	}
	return nil
}

type incomingEvent struct {
	ce cloudevents.Event
}

// EventType ...
func (i incomingEvent) EventType() common.EventType {
	return common.EventType(i.ce.Type())
}

// Transaction ...
func (i incomingEvent) Transaction() common.TransactionID {
	return messaging.GetTransactionID(&i.ce)
}

// CloudEvent ...
func (i incomingEvent) CloudEvent() cloudevents.Event {
	return i.ce
}

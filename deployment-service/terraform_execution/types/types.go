package types

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// IncomingEvent is a incoming event to be handled
type IncomingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	CloudEvent() cloudevents.Event
}

// OutgoingEvent is a outgoing event to be published
type OutgoingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// EnvConfig ...
type EnvConfig struct {
	PodName     string `envconfig:"POD_NAME"`
	LogLevel    string `envconfig:"LOG_LEVEL" default:"trace"`
	CacaoSSHKey string `envconfig:"CACAO_SSH_KEY"`
}

// Config ...
type Config struct {
	StanConfig StanConfig
	// STAN config for AWM
	AWMStanConfig StanConfig
	MongoConfig   db.MongoDBConfig
}

// StanConfig is config for STAN
type StanConfig struct {
	messaging.NatsConfig
	messaging.StanConfig
}

// TerraformOpenStackDeploymentType is a deployment type for Terraform template engine and OpenStack cloud provider
const TerraformOpenStackDeploymentType = "terraform_openstack"

// Actor is the user that performs certain action.
type Actor struct {
	Actor    string
	Emulator string
}

// ActorFromSession construct Actor from service.Session
func ActorFromSession(session service.Session) Actor {
	return Actor{
		Actor:    session.GetSessionActor(),
		Emulator: session.GetSessionEmulator(),
	}
}

// CopySessionActors copies over the actor and emulator in Session.
func CopySessionActors(session service.Session) service.Session {
	return service.Session{
		SessionActor:    session.SessionActor,
		SessionEmulator: session.SessionEmulator,
	}
}

// ErrorToServiceError converts built-in error object into service error that can be transported in events.
func ErrorToServiceError(err error) service.CacaoError {
	if err == nil {
		return &service.CacaoErrorBase{}
	}
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}
	return serviceError
}

// ProviderCredentialPair is a cloud credential associated with a provider.
type ProviderCredentialPair struct {
	Provider   common.ID
	Credential service.Credential
}

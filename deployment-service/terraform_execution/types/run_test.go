package types

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"reflect"
	"testing"
	"time"
)

func TestDeploymentRunUpdate_ToBSON(t *testing.T) {
	assert.Equalf(t, 3, reflect.TypeOf(DeploymentRunUpdate{}).NumField(), "currently there are only 3 fields")

	type fields struct {
		Status         *deploymentcommon.DeploymentRunStatus
		StateUpdatedAt *time.Time
		TerraformState *deploymentcommon.TerraformState
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]interface{}
	}{
		{
			name:   "none",
			fields: fields{},
			want:   map[string]interface{}{},
		},
		{
			name: "Status",
			fields: fields{
				Status: func() *deploymentcommon.DeploymentRunStatus {
					status := deploymentcommon.DeploymentRunActive
					return &status
				}(),
				StateUpdatedAt: nil,
				TerraformState: nil,
			},
			want: map[string]interface{}{
				"status": deploymentcommon.DeploymentRunActive,
			},
		},
		{
			name: "StateUpdatedAt",
			fields: fields{
				Status: nil,
				StateUpdatedAt: func() *time.Time {
					t := time.Unix(50000, 0)
					return &t
				}(),
				TerraformState: nil,
			},
			want: map[string]interface{}{
				"state_updated_at": time.Unix(50000, 0),
			},
		},
		{
			name: "TerraformState",
			fields: fields{
				Status:         nil,
				StateUpdatedAt: nil,
				TerraformState: func() *deploymentcommon.TerraformState {
					state := deploymentcommon.TerraformState{
						Version: 100,
					}
					return &state
				}(),
			},
			want: map[string]interface{}{
				"tf_state": deploymentcommon.TerraformState{
					Version: 100,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			update := DeploymentRunUpdate{
				Status:         tt.fields.Status,
				StateUpdatedAt: tt.fields.StateUpdatedAt,
				TerraformState: tt.fields.TerraformState,
			}
			if got := update.ToBSON(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBSON() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDeploymentRunFilter_ToBSON(t *testing.T) {
	assert.Equalf(t, 2, reflect.TypeOf(DeploymentRunFilter{}).NumField(), "currently there are only 2 fields")

	type fields struct {
		ID         common.ID
		Deployment common.ID
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]interface{}
	}{
		{
			name:   "none",
			fields: fields{},
			want:   map[string]interface{}{},
		},
		{
			name: "run",
			fields: fields{
				ID:         "run-aaaaaaaaaaaaaaaaaaaa",
				Deployment: "",
			},
			want: map[string]interface{}{
				"_id": common.ID("run-aaaaaaaaaaaaaaaaaaaa"),
			},
		},
		{
			name: "deployment",
			fields: fields{
				ID:         "",
				Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
			},
			want: map[string]interface{}{
				"deployment": common.ID("deployment-aaaaaaaaaaaaaaaaaaaa"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			filter := DeploymentRunFilter{
				ID:         tt.fields.ID,
				Deployment: tt.fields.Deployment,
			}
			if got := filter.ToBSON(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToBSON() = %v, want %v", got, tt.want)
			}
		})
	}
}

package domain

import (
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/eventhandler"
	"sync"

	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

const eventWorkerCount = 5
const channelDepth = 5

// Domain is the entrypoint object for this service
type Domain struct {
	EventSrc        ports.EventSrc
	EventSink       ports.EventSink
	TFRunStorage    ports.TFRunStorage
	WorkflowStorage ports.DeploymentWorkflowStorage
	KeySrc          ports.KeySrc
	TimeSrc         ports.TimeSrc
	SSHKeySrc       ports.SSHKeySrc

	MetadataSvc  ports.DeploymentMetadataService
	WorkspaceMS  ports.WorkspaceMicroservice
	TemplateMS   ports.TemplateMicroservice
	CredentialMS ports.CredentialMicroservice
	ProviderMS   ports.ProviderMicroservice
	AWM          ports.ArgoWorkflowMediator

	eventChan chan types.IncomingEvent
}

// Init ...
func (svc *Domain) Init(conf types.Config) error {
	var err error
	err = svc.TFRunStorage.Init()
	if err != nil {
		return err
	}
	err = svc.WorkflowStorage.Init()
	if err != nil {
		return err
	}
	err = svc.EventSrc.Init(conf.StanConfig)
	if err != nil {
		return err
	}
	svc.eventChan = make(chan types.IncomingEvent, channelDepth)
	svc.EventSrc.InitChannel(svc.eventChan)
	err = svc.EventSink.Init(conf.StanConfig)
	if err != nil {
		return err
	}
	return nil
}

// Start starts receiving and processing events
func (svc *Domain) Start() error {
	err := svc.EventSrc.Start()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	svc.spawnEventWorker(&wg, eventWorkerCount)

	wg.Wait()
	return nil
}

func (svc Domain) spawnEventWorker(wg *sync.WaitGroup, workerCount int) {
	for i := 0; i < workerCount; i++ {
		wg.Add(1)
		worker := eventhandler.NewEventWorker(svc.commonDependencies())
		go worker.Run(wg, svc.eventChan)
	}
}

func (svc Domain) commonDependencies() ports.Ports {
	return ports.Ports{
		EventSink:       svc.EventSink,
		TFRunStorage:    svc.TFRunStorage,
		WorkflowStorage: svc.WorkflowStorage,
		KeySrc:          svc.KeySrc,
		TimeSrc:         svc.TimeSrc,
		SSHKeySrc:       svc.SSHKeySrc,
		MetadataSvc:     svc.MetadataSvc,
		TemplateMS:      svc.TemplateMS,
		WorkspaceMS:     svc.WorkspaceMS,
		CredentialMS:    svc.CredentialMS,
		ProviderMS:      svc.ProviderMS,
		AWM:             svc.AWM,
	}
}

package domainutils

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"sigs.k8s.io/yaml"
	"strings"
	"testing"
)

var users = []string{
	"foobarrr",
	"rtetqertyu123456iw",
	"fUDksghaSUk13A6wi372reu9kr4wuA",
}
var sshKeys = []string{
	"ssh-ed25519 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA123/aaaaaaaaaaaaaaaa user123@instance123",
	"ssh-ed25519 BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB123/bbbbbbbbbbbbbbbb user123@instance123",
	"ssh-ed25519 CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC123/cccccccccccccccc user123@instance123",
	"ssh-ed25519 DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD123/dddddddddddddddd user123@instance123",
	"ssh-ed25519 EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE123/eeeeeeeeeeeeeeee user123@instance123",
	"ssh-rsa AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA123/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/aaaaaaaaaaaaaaaaaaa/aaaaaa+aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/aaaaaa user123@instance123",
	"ssh-rsa BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB123/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbbbbbbbbbbbbbbb/bbbbbb+bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb/bbbbbb user123@instance123",
	"ssh-rsa CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC123/cccccccccccccccccccccccccccccccccccccccccccccccccccccc/cccccccccccccccccccccccccccccccccccccccccccccccc/cccccccccccccccccccccccccccccccccccc/ccccccccccccccccccccccccccccccccccccccccccccccc/ccccccccccccccccccc/cccccc+cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc/cccccc user123@instance123",
	"ssh-rsa DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD123/dddddddddddddddddddddddddddddddddddddddddddddddddddddd/dddddddddddddddddddddddddddddddddddddddddddddddd/dddddddddddddddddddddddddddddddddddd/ddddddddddddddddddddddddddddddddddddddddddddddd/ddddddddddddddddddd/dddddd+dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd/dddddd user123@instance123",
	"ssh-rsa EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE123/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/ddddddddddddddddddddddddddddddddddddddddddddddd/eeeeeeeeeeeeeeeeeee/eeeeee+eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee/eeeeee user123@instance123",
}

func BenchmarkCloudInitGenerator_GenerateWithTemplate(b *testing.B) {
	var allResults = make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		var generator = CloudInitGenerator{
			users:   users[:1], // 1 new user
			sshKeys: sshKeys,
		}
		result, _ := generator.GenerateWithTemplate()
		allResults = append(allResults, result)
	}
	b.Log(len(allResults))
}

func BenchmarkCloudInitGenerator_Generate(b *testing.B) {
	var allResults = make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		var generator = CloudInitGenerator{
			users:   users[:1], // 1 new user
			sshKeys: sshKeys,
		}
		result, _ := generator.Generate()
		allResults = append(allResults, result)
	}
	b.Log(len(allResults))
}

func TestGrow(t *testing.T) {
	var output bytes.Buffer
	output.Grow(2500)
	assert.GreaterOrEqual(t, output.Cap(), 2500)
}

func TestCloudInitGenerator_Limit(t *testing.T) {
	var longKey strings.Builder
	for i := 0; i < 10; i++ {
		longKey.WriteString("aaaaaaaaaa")
	}

	t.Run("out of limit", func(t *testing.T) {
		var generator = NewCloudInitGenerator(100)
		generator.CreateUser("foo")

		generator.AddSSHKey(longKey.String())
		_, err := generator.GenerateWithTemplate()
		assert.Error(t, err)
		_, err = generator.GenerateWithStringBuilder()
		assert.Error(t, err)
	})
	t.Run("within limit", func(t *testing.T) {
		base := `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ?
`
		var generator = NewCloudInitGenerator(longKey.Len() + len(base))
		generator.CreateUser("foo")

		generator.AddSSHKey(longKey.String())
		_, err := generator.GenerateWithTemplate()
		assert.NoError(t, err)
		_, err = generator.GenerateWithStringBuilder()
		assert.NoError(t, err)
	})
}

func TestCloudInitGenerator(t *testing.T) {
	var testCases = []struct {
		TestCaseName string
		Users        []string
		SSHKeys      []string
		Expected     string
		WantErr      bool
	}{
		{
			TestCaseName: "no user no sshkey",
			Users:        []string{},
			SSHKeys:      []string{},
			Expected:     "",
			WantErr:      true,
		},
		{
			TestCaseName: "no user 1 sshkey",
			Users:        []string{},
			SSHKeys:      []string{"ssh-rsa AAA"},
			Expected:     "",
			WantErr:      true,
		},
		{
			TestCaseName: "empty user",
			Users:        []string{"foo", ""},
			SSHKeys:      []string{"ssh-rsa AAA"},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
`,
			WantErr: false,
		},
		{
			TestCaseName: "empty sshkey",
			Users:        []string{"foo"},
			SSHKeys:      []string{""},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
`,
			WantErr: false,
		},
		{
			TestCaseName: "empty & non-empty sshkey",
			Users:        []string{"foo"},
			SSHKeys:      []string{"", "ssh-rsa AAA"},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
`,
			WantErr: false,
		},
		{
			TestCaseName: "1 user no sshkey",
			Users:        []string{"foo"},
			SSHKeys:      []string{},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
`,
			WantErr: false,
		},
		{
			TestCaseName: "1 user 1 sshkey",
			Users:        []string{"foo"},
			SSHKeys: []string{
				"ssh-rsa AAA",
			},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
`,
			WantErr: false,
		},
		{
			TestCaseName: "1 user 1 sshkey (escape char)",
			Users:        []string{"foo"},
			SSHKeys: []string{
				"ssh-rsa AAA",
			},
			Expected: "#cloud-config\nusers:\n  - default\n  - name: foo\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa AAA\n",
			WantErr:  false,
		},
		{
			TestCaseName: "1 user 2 sshkey",
			Users:        []string{"foo"},
			SSHKeys: []string{
				"ssh-rsa AAA",
				"ssh-rsa BBB",
			},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
      - ssh-rsa BBB
`,
			WantErr: false,
		},
		{
			TestCaseName: "2 user 2 sshkey",
			Users:        []string{"foo", "bar"},
			SSHKeys: []string{
				"ssh-rsa AAA",
				"ssh-rsa BBB",
			},
			Expected: `#cloud-config
users:
  - default
  - name: foo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
      - ssh-rsa BBB
  - name: bar
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ssh-rsa AAA
      - ssh-rsa BBB
`,
			WantErr: false,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.TestCaseName, func(t *testing.T) {
			var generator = NewCloudInitGenerator(0)
			for _, user := range tc.Users {
				generator.CreateUser(user)
			}
			generator.AddSSHKeys(tc.SSHKeys)

			templateResult, err := generator.GenerateWithTemplate()
			if tc.WantErr {
				if !assert.Errorf(t, err, "GenerateWithTemplate()") {
					return
				}
			} else {
				if !assert.NoErrorf(t, err, "GenerateWithTemplate()") {
					return
				}
			}
			assert.Equalf(t, tc.Expected, templateResult, "GenerateWithTemplate()")

			strBuilderResult, err := generator.GenerateWithStringBuilder()
			if tc.WantErr {
				if !assert.Errorf(t, err, "GenerateWithStringBuilder()") {
					return
				}
			} else {
				if !assert.NoErrorf(t, err, "GenerateWithStringBuilder()") {
					return
				}
			}
			assert.Equalf(t, tc.Expected, strBuilderResult, "GenerateWithStringBuilder()")
		})
	}
}

func TestCloudInitGenerator_Compare2GenerateImplementations(t *testing.T) {
	var generator = NewCloudInitGenerator(0)
	generator.users = users
	generator.sshKeys = sshKeys
	templateResult, err := generator.GenerateWithTemplate()
	if !assert.NoError(t, err) {
		return
	}
	result, err := generator.Generate()
	if !assert.NoError(t, err) {
		return
	}

	var templateCloudInitScript map[string]interface{}
	err = yaml.Unmarshal([]byte(templateResult), &templateCloudInitScript)
	if !assert.NoError(t, err) {
		return
	}
	var cloudInitScript map[string]interface{}
	err = yaml.Unmarshal([]byte(result), &cloudInitScript)
	if !assert.NoError(t, err) {
		return
	}

	if templateResult == result {
		return
	}
	assert.Equal(t, templateCloudInitScript, cloudInitScript)
}

func TestTrimUsername(t *testing.T) {
	type args struct {
		cacaoUsername string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{
				cacaoUsername: "foobar",
			},
			want: "foobar",
		},
		{
			name: "email",
			args: args{
				cacaoUsername: "foo@bar.com",
			},
			want: "foo",
		},
		{
			name: "1st is non-alphanumerical",
			args: args{
				cacaoUsername: "_foo",
			},
			want: "cacao",
		},
		{
			name: "with .-_",
			args: args{
				cacaoUsername: "foo.bar-foo_bar@bar.com",
			},
			want: "foo.bar-foo_bar",
		},
		{
			name: "all bad characters",
			args: args{
				cacaoUsername: "!@#$%^&*()",
			},
			want: "cacao",
		},
		{
			name: "too long 1",
			args: args{
				cacaoUsername: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			},
			want: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		},
		{
			name: "too long 2",
			args: args{
				cacaoUsername: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb@foo.com",
			},
			want: "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimUsername(tt.args.cacaoUsername); got != tt.want {
				t.Errorf("trimUsername() = %v, want %v", got, tt.want)
			}
		})
	}
}

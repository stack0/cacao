package domainutils

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// EncodeGitCred encrypts and encodes git credential, also convert from the template source cred format to the format accepted by AWM.
func EncodeGitCred(cred service.Credential, keySrc ports.KeySrc) (string, error) {
	templateSrcCred, err := unmarshalTemplateSrcCred(cred)
	if err != nil {
		return "", err
	}
	gitCredMarshaled, err := marshalGitCredential(templateSrcCred)
	if err != nil {
		return "", err
	}
	cipher, err := encrypt(gitCredMarshaled, keySrc)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", cred.GetID())
	}
	return base64Encode(cipher), nil
}

func unmarshalTemplateSrcCred(cred service.Credential) (service.TemplateSourceCredential, error) {
	// unmarshal to check schema
	var credOut service.TemplateSourceCredential
	err := json.Unmarshal([]byte(cred.GetValue()), &credOut)
	if err != nil {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is not valid template source credential", cred.GetID())
	}

	if credOut.Username == "" {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is missing username", cred.GetID())
	}
	if credOut.Password == "" {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is missing password", cred.GetID())
	}
	return credOut, nil
}

func marshalGitCredential(cred service.TemplateSourceCredential) ([]byte, error) {
	var credOut = awmclient.GitCredential{
		Username: cred.Username,
		Password: cred.Password,
	}
	return json.Marshal(credOut)
}

// EncodeOpenstackCred encrypts and encodes OpenStack credential.
func EncodeOpenstackCred(cred service.Credential, keySrc ports.KeySrc) (string, error) {
	if err := ValidateOpenstackCredential(cred); err != nil {
		return "", fmt.Errorf("credential %s is not valid openstack credential", cred.GetID())
	}
	cipher, err := encrypt([]byte(cred.GetValue()), keySrc)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", cred.GetID())
	}
	return base64Encode(cipher), nil
}

// ValidateOpenstackCredential validates an OpenStack credential
func ValidateOpenstackCredential(cred service.Credential) error {
	if cred.GetID() == "" {
		return errors.New("bad cloud credential ID")
	}
	if cred.GetUsername() == "" {
		return errors.New("cloud credential has no owner")
	}
	if cred.GetType() != "openstack" {
		return errors.New("credential type not openstack")
	}
	if cred.GetValue() == "" {
		return errors.New("empty cloud credential value")
	}
	var osCred awmclient.OpenStackCredential
	err := json.Unmarshal([]byte(cred.GetValue()), &osCred)
	if err != nil {
		return errors.New("bad cloud credential value")
	}
	// TODO more validation on awmclient.OpenStackCredential
	return nil
}

// ValidateGitCredential validates a git credential
func ValidateGitCredential(credential service.Credential) error {
	if credential.GetID() == "" {
		return service.NewCacaoInvalidParameterError("git credential object has bad ID")
	}
	if credential.GetType() != "git" {
		return service.NewCacaoInvalidParameterError("git credential object must be of type 'git'")
	}
	if credential.GetValue() == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty value")
	}
	if credential.GetUsername() == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty owner")
	}
	var gitCred service.TemplateSourceCredential
	err := json.Unmarshal([]byte(credential.GetValue()), &gitCred)
	if err != nil {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("git credential object has bad value, %s", err.Error()))
	}
	if gitCred.Username == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty username")
	}
	if gitCred.Password == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty password")
	}
	return nil
}

func encrypt(input []byte, keySrc ports.KeySrc) ([]byte, error) {
	encryptionKey := keySrc.GetKey()
	cipher, err := AESEncrypt(encryptionKey, input)
	if err != nil {
		return nil, err
	}
	return cipher, nil
}

func base64Encode(input []byte) string {
	return base64.StdEncoding.EncodeToString(input)
}

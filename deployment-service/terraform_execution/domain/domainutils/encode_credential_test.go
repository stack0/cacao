package domainutils

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"testing"
)

func TestValidateGitCredential(t *testing.T) {
	type args struct {
		credential service.Credential
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    `{"username":"foo","password":"bar"}`,
					Type:     "git",
					ID:       "cloudcred123",
				},
			},
			wantErr: false,
		},
		{
			name: "cred no owner",
			args: args{
				credential: &service.CredentialModel{
					Username: "",
					Value:    `{"username":"foo","password":"bar"}`,
					Type:     "git",
					ID:       "cloudcred123",
				},
			},
			wantErr: true,
		},
		{
			name: "cred empty value",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    "",
					Type:     "git",
					ID:       "cloudcred123",
				},
			},
			wantErr: true,
		},
		{
			name: "cred missing key",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    `{"username":"foo"}`,
					Type:     "git",
					ID:       "cloudcred123",
				},
			},
			wantErr: true,
		},
		{
			name: "empty username & password",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    `{"username":"","password":""}`,
					Type:     "git",
					ID:       "cloudcred123",
				},
			},
			wantErr: true,
		},
		{
			name: "type not git",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    `{"username":"foo","password":"bar"}`,
					Type:     "not-git",
					ID:       "cloudcred123",
				},
			},
			wantErr: true,
		},
		{
			name: "empty ID",
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    `{"username":"foo","password":"bar"}`,
					Type:     "git",
					ID:       "",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateGitCredential(tt.args.credential)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateGitCredential() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateOpenstackCredential(t *testing.T) {
	var openstackCred = awmclient.OpenStackCredential{
		IdentityAPIVersion: "3",
		RegionName:         "regionname",
		Interface:          "interface",
		AuthURL:            "authurl",
		ProjectDomainID:    "projectdomainid",
		ProjectDomainName:  "projectdomainname",
		ProjectID:          "projectid",
		ProjectName:        "projectname",
		UserDomainName:     "userdomainname",
		Username:           "username",
		Password:           "password",
		AuthType:           "authtype",
		AppCredID:          "",
		AppCredName:        "",
		AppCredSecret:      "",
	}
	openstackCredMarshaled, err := json.Marshal(openstackCred)
	if err != nil {
		panic(err)
	}
	type args struct {
		cred service.Credential
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				cred: &service.CredentialModel{
					Username: "testuser123",
					Value:    string(openstackCredMarshaled),
					Type:     "openstack",
					ID:       "cred123",
				},
			},
			wantErr: false,
		},
		{
			name: "no username",
			args: args{
				cred: &service.CredentialModel{
					Username: "",
					Value:    string(openstackCredMarshaled),
					Type:     "openstack",
					ID:       "cred123",
				},
			},
			wantErr: true,
		},
		{
			name: "empty value",
			args: args{
				cred: &service.CredentialModel{
					Username: "testuser123",
					Value:    "",
					Type:     "openstack",
					ID:       "cred123",
				},
			},
			wantErr: true,
		},
		{
			name: "value not json",
			args: args{
				cred: &service.CredentialModel{
					Username: "testuser123",
					Value:    "fsdjalfhsdfjkhsk",
					Type:     "openstack",
					ID:       "cred123",
				},
			},
			wantErr: true,
		},
		{
			name: "bad type",
			args: args{
				cred: &service.CredentialModel{
					Username: "testuser123",
					Value:    string(openstackCredMarshaled),
					Type:     "git",
					ID:       "cred123",
				},
			},
			wantErr: true,
		},
		{
			name: "no ID",
			args: args{
				cred: &service.CredentialModel{
					Username: "testuser123",
					Value:    string(openstackCredMarshaled),
					Type:     "openstack",
					ID:       "",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateOpenstackCredential(tt.args.cred)
			if tt.wantErr {
				assert.Errorf(t, err, "ValidateOpenstackCredential(%v)", tt.args.cred)
				return
			}
			assert.NoErrorf(t, err, "ValidateOpenstackCredential(%v)", tt.args.cred)
		})
	}
}

package eventhandler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/resourcedeletion"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

type deletionResult struct {
	service.Session
	Deployment common.ID
}

// ToRespEvent ...
func (result deletionResult) ToRespEvent(transaction common.TransactionID) types.ResponseEvent {
	if result.GetServiceError() != nil {
		return types.NewEvent(deploymentevents.EventDeploymentDeletionCleanupFailed, transaction, deploymentevents.DeploymentDeletionCleanupResult{
			Session:        result.Session,
			DeploymentType: types.TerraformOpenStackDeploymentType,
			Deployment:     result.Deployment,
		})
	}
	return types.NewEvent(deploymentevents.EventDeploymentDeletionCleanupStarted, transaction, deploymentevents.DeploymentDeletionCleanupStarted{
		Session:        result.Session,
		DeploymentType: types.TerraformOpenStackDeploymentType,
		Deployment:     result.Deployment,
	})
}

// DeletionStartedHandler handles the DeploymentDeletionStarted event, it performs resource cleanup for the deletion of deployment
type DeletionStartedHandler struct {
	runStorage  ports.TFRunStorage
	wfStorage   ports.DeploymentWorkflowStorage
	awm         ports.ArgoWorkflowMediator
	credMS      ports.CredentialMicroservice
	metadataSvc ports.DeploymentMetadataService
	keySrc      ports.KeySrc
}

// NewDeletionHandler ...
func NewDeletionHandler(portsDependency ports.Ports) DeletionStartedHandler {
	return DeletionStartedHandler{
		runStorage:  portsDependency.TFRunStorage,
		wfStorage:   portsDependency.WorkflowStorage,
		awm:         portsDependency.AWM,
		credMS:      portsDependency.CredentialMS,
		metadataSvc: portsDependency.MetadataSvc,
		keySrc:      portsDependency.KeySrc,
	}
}

// Handle handles service.DeploymentDeletionStarted (emitted by metadata service), and perform resource cleanup on the deployment
func (h DeletionStartedHandler) Handle(incoming types.IncomingEvent) types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionStartedHandler.Handle",
	})
	eventBody, err := h.unmarshalEvent(incoming)
	if err != nil {
		logger.WithError(err).Trace("fail to parse event body")
		result := h.deleteFailedResult(eventBody, err)
		return result.ToRespEvent(incoming.Transaction())
	}
	result := h.handle(eventBody)
	h.logResponse(logger, result)
	return result.ToRespEvent(incoming.Transaction())
}

func (h DeletionStartedHandler) unmarshalEvent(incoming types.IncomingEvent) (service.DeploymentDeletionResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionStartedHandler.unmarshalEvent",
	})
	var request service.DeploymentDeletionResult
	err := json.Unmarshal(incoming.CloudEvent().Data(), &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentDeletionResult"
		logger.WithError(err).Error(errorMessage)
		return service.DeploymentDeletionResult{}, service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error()))
	}
	return request, nil
}

func (h DeletionStartedHandler) handle(event service.DeploymentDeletionResult) deletionResult {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "DeletionStartedHandler.handle",
		"deployment": event.ID,
	})
	gatherer := resourcedeletion.NewPrerequisiteGatherer(h.runStorage, h.credMS, h.metadataSvc)
	data, err := gatherer.Gather(types.ActorFromSession(event.Session), event.ID)
	if err != nil {
		logger.WithError(err).Error("failed to gather data for deployment resource deletion")
		return h.deleteFailedResult(event, err)
	}
	awmProvider, wfName, err := resourcedeletion.StartResourceCleanup(h.awm, h.keySrc, data)
	if err != nil {
		logger.WithError(err).Error("failed to launch deletion workflow")
		return h.deleteFailedResult(event, err)
	}
	logger.WithFields(log.Fields{
		"awmProvider": awmProvider,
		"wfName":      wfName,
	}).Info("deletion workflow launched")

	err = h.saveWorkflow(event.ID, awmProvider, wfName)
	if err != nil {
		logger.WithError(err).Error("fail to save deletion workflow to storage")
		return h.deleteFailedResult(event, err)
	}
	logger.Info("workflow deletion started")
	return h.deletionStartedResult(event)
}

func (h DeletionStartedHandler) saveWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, wfName string) error {
	err := h.wfStorage.CreateDeploymentDeletionWorkflow(deployment, awmProvider, wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h DeletionStartedHandler) deleteFailedResult(request service.DeploymentDeletionResult, err error) deletionResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
		Deployment: request.ID,
	}
}

func (h DeletionStartedHandler) deletionStartedResult(request service.DeploymentDeletionResult) deletionResult {
	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment: request.ID,
	}
}

func (h DeletionStartedHandler) logResponse(logger *log.Entry, result deletionResult) {
	logger.WithFields(log.Fields{
		"actor":      result.GetSessionActor(),
		"emulator":   result.GetSessionEmulator(),
		"deployment": result.Deployment,
		"err":        result.GetServiceError(),
	}).Info("response event")
}

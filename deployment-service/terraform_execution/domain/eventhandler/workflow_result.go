package eventhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/preflightstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/resourcedeletion"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowSucceededHandler handles workflow succeeded event, and dispatch it to another handler based on its purpose
type WorkflowSucceededHandler struct {
	wfStorage ports.DeploymentWorkflowStorage
	timeSrc   ports.TimeSrc
	handlers  specificWorkflowPurposeHandlers
}

// NewWorkflowSucceededHandler ...
func NewWorkflowSucceededHandler(dependencies ports.Ports) WorkflowSucceededHandler {
	return WorkflowSucceededHandler{
		wfStorage: dependencies.WorkflowStorage,
		timeSrc:   dependencies.TimeSrc,
		handlers:  newSpecificWorkflowPurposeHandlers(dependencies),
	}
}

// Handle ...
func (h WorkflowSucceededHandler) Handle(event types.IncomingEvent) types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowSucceededHandler.Handle",
	})
	incoming, err := h.unmarshalCloudEvent(event)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cloud event data")
		return types.NewDoNotSentEvent()
	}
	response := h.handle(logger, event.Transaction(), incoming)
	if response == nil {
		// this might not be an error, some workflow handlers does not a response event
		logger.Error("handler has no response event")
		return types.NewDoNotSentEvent()
	}
	return *response
}

func (h WorkflowSucceededHandler) unmarshalCloudEvent(event types.IncomingEvent) (awmclient.WorkflowSucceeded, error) {
	var result awmclient.WorkflowSucceeded
	err := json.Unmarshal(event.CloudEvent().Data(), &result)
	if err != nil {
		return result, err
	}
	return result, nil
}

func (h WorkflowSucceededHandler) handle(logger *log.Entry, tid common.TransactionID, event awmclient.WorkflowSucceeded) *types.ResponseEvent {
	workflow, err := lookupWorkflow(h.wfStorage, awmclient.AWMProvider(event.Provider), event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to look up workflow in storage")
		return nil
	}
	logger.Info("workflow record found in storage")
	err = h.wfStorage.WorkflowSucceededAt(awmclient.AWMProvider(workflow.AWMProvider), workflow.WorkflowName, h.timeSrc.Now())
	if err != nil {
		logger.WithError(err).Error("failed to update workflow status in storage")
		return nil
	}
	handler := dispatchBasedOnPurpose(h.handlers, workflow.Purpose)
	if handler == nil {
		err = fmt.Errorf("unknown workflow purpose %s from WorkflowFailed event, %s", workflow.Purpose, event.WorkflowName)
		logger.WithError(err).Error()
		return nil
	}
	return handler.HandleWorkflowSuccess(tid, event, workflow)
}

// WorkflowFailedHandler handles workflow failed event, and dispatch it to another handler based on its purpose
type WorkflowFailedHandler struct {
	wfStorage ports.DeploymentWorkflowStorage
	timeSrc   ports.TimeSrc
	handlers  specificWorkflowPurposeHandlers
}

// NewWorkflowFailedHandler ...
func NewWorkflowFailedHandler(dependencies ports.Ports) WorkflowFailedHandler {
	return WorkflowFailedHandler{
		wfStorage: dependencies.WorkflowStorage,
		timeSrc:   dependencies.TimeSrc,
		handlers:  newSpecificWorkflowPurposeHandlers(dependencies),
	}
}

// Handle ...
func (h WorkflowFailedHandler) Handle(event types.IncomingEvent) types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "WorkflowFailedHandler.Handle",
	})
	incoming, err := h.unmarshalCloudEvent(event)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cloud event data")
		return types.NewDoNotSentEvent()
	}
	response := h.handle(logger, event.Transaction(), incoming)
	if response == nil {
		// this might not be an error, some workflow handlers does not a response event
		logger.Error("handler has no response event")
		return types.NewDoNotSentEvent()
	}
	return *response
}

func (h WorkflowFailedHandler) unmarshalCloudEvent(event types.IncomingEvent) (awmclient.WorkflowFailed, error) {
	var result awmclient.WorkflowFailed
	err := json.Unmarshal(event.CloudEvent().Data(), &result)
	if err != nil {
		return result, err
	}
	return result, nil
}

func (h WorkflowFailedHandler) handle(logger *log.Entry, tid common.TransactionID, event awmclient.WorkflowFailed) *types.ResponseEvent {
	workflow, err := lookupWorkflow(h.wfStorage, awmclient.AWMProvider(event.Provider), event.WorkflowName)
	if err != nil {
		logger.WithError(err).Error("fail to look up workflow in storage")
		return nil
	}
	logger.Info("workflow record found in storage")
	err = h.wfStorage.WorkflowFailedAt(awmclient.AWMProvider(workflow.AWMProvider), workflow.WorkflowName, h.timeSrc.Now())
	if err != nil {
		logger.WithError(err).Error("failed to update workflow status in storage")
		return nil
	}
	handler := dispatchBasedOnPurpose(h.handlers, workflow.Purpose)
	if handler == nil {
		err = fmt.Errorf("unknown workflow purpose %s from WorkflowFailed event, %s", workflow.Purpose, event.WorkflowName)
		logger.WithError(err).Error()
		return nil
	}
	return handler.HandleWorkflowFailure(tid, event, workflow)
}

func lookupWorkflow(wfStorage ports.DeploymentWorkflowStorage, provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error) {
	workflow, err := wfStorage.Search(provider, workflowName)
	if err != nil {
		return workflow, err
	}
	if workflow.Status != "" {
		// workflow already has completed and has its status updated, this could be a workflow retry or a collision on workflow name
		return workflow, fmt.Errorf("workflow already completed with status %s at %s", workflow.Status, workflow.EndedAt.String())
	}
	return workflow, err
}

type specificWorkflowPurposeHandlers struct {
	preflight preflightstage.WorkflowHandler
	execution executionstage.WorkflowHandler
	deletion  resourcedeletion.WorkflowHandler
}

func newSpecificWorkflowPurposeHandlers(dependencies ports.Ports) specificWorkflowPurposeHandlers {
	return specificWorkflowPurposeHandlers{
		preflight: preflightstage.NewWorkflowHandler(dependencies),
		execution: executionstage.NewWorkflowHandler(dependencies),
		deletion:  resourcedeletion.NewWorkflowHandler(dependencies),
	}
}

// SpecificPurposeWorkflowHandler is handler that handles workflow of a specific purpose.
// Note the handler will return nil when there is no response event generated.
type SpecificPurposeWorkflowHandler interface {
	HandleWorkflowSuccess(tid common.TransactionID, event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow) *types.ResponseEvent
	HandleWorkflowFailure(tid common.TransactionID, event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow) *types.ResponseEvent
}

// this may return nil
func dispatchBasedOnPurpose(handlers specificWorkflowPurposeHandlers, purpose types.WorkflowPurpose) SpecificPurposeWorkflowHandler {
	switch purpose {
	case types.PreflightPurpose:
		return handlers.preflight
	case types.ExecutionPurpose:
		return handlers.execution
	case types.DeletionPurpose:
		return handlers.deletion
	default:
		// unknown workflow purpose
		return nil
	}
}

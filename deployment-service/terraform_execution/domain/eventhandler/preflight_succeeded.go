package eventhandler

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/executionstage"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PreflightSucceededHandler handles the success result of preflight stage for a Terraform run. And it starts the
// execution stage for the run.
type PreflightSucceededHandler struct {
	runStorage ports.TFRunStorage
	wfStorage  ports.DeploymentWorkflowStorage
	awm        ports.ArgoWorkflowMediator
	credMS     ports.CredentialMicroservice
	keySrc     ports.KeySrc
}

// NewPreflightSucceededHandler ...
func NewPreflightSucceededHandler(portsDependency ports.Ports) PreflightSucceededHandler {
	return PreflightSucceededHandler{
		runStorage: portsDependency.TFRunStorage,
		wfStorage:  portsDependency.WorkflowStorage,
		awm:        portsDependency.AWM,
		credMS:     portsDependency.CredentialMS,
		keySrc:     portsDependency.KeySrc,
	}
}

// Handle ...
func (h PreflightSucceededHandler) Handle(incoming types.IncomingEvent) types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "PreflightSucceededHandler.Handle",
	})
	eventBody, err := h.unmarshalIncoming(incoming)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal event body")
		return types.NewDoNotSentEvent()
	}
	if h.ignoreEvent(logger, incoming, eventBody) {
		logger.Debug("event ignored")
		return types.NewDoNotSentEvent()
	}
	err = h.validateEvent(logger, eventBody)
	if err != nil {
		logger.WithError(err).Error("event body not valid")
		return types.NewDoNotSentEvent()
	}
	return h.handle(incoming.Transaction(), eventBody)
}

func (h PreflightSucceededHandler) unmarshalIncoming(event types.IncomingEvent) (deploymentevents.RunPreflightResult, error) {
	var result deploymentevents.RunPreflightResult
	err := json.Unmarshal(event.CloudEvent().Data(), &result)
	if err != nil {
		return deploymentevents.RunPreflightResult{}, err
	}

	return result, nil
}

// return true if the event should be ignored, and NOT handled by this handler
func (h PreflightSucceededHandler) ignoreEvent(logger *log.Entry, incoming types.IncomingEvent, event deploymentevents.RunPreflightResult) bool {
	if incoming.EventType() != deploymentevents.EventRunPreflightSucceeded {
		// this handler should only handle RunPreflightSucceeded
		logger.WithField("eventType", incoming.EventType()).Error("wrong event type has been dispatched here")
		return true
	}
	if event.DeploymentType != types.TerraformOpenStackDeploymentType {
		logger.WithField("deploymentType", event.DeploymentType).Trace("this svc does not handle this deployment type, event is ignored")
		return true
	}
	return false
}

func (h PreflightSucceededHandler) validateEvent(logger *log.Entry, event deploymentevents.RunPreflightResult) error {
	if !event.Run.Validate() || event.Run.Prefix() != service.RunIDPrefix {
		logger.WithField("runID", event.Run).Error("incoming event has bad run ID")
		return service.NewCacaoInvalidParameterError("bad run ID")
	}
	if !event.Deployment.Validate() || event.Deployment.Prefix() != service.DeploymentIDPrefix {
		logger.WithField("deploymentID", event.Run).Error("incoming event has bad deployment ID")
		return service.NewCacaoInvalidParameterError("bad deployment ID")
	}
	return nil
}

func (h PreflightSucceededHandler) handle(tid common.TransactionID, event deploymentevents.RunPreflightResult) types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "PreflightSucceededHandler.handle",
	})
	gatherer := executionstage.NewPrerequisiteGatherer(h.runStorage, h.credMS)
	prerequisite, err := gatherer.Gather(event.Run)
	if err != nil {
		logger.WithError(err).Error("fail to gather prerequisite data for execution stage")
		return h.executionFailedEvent(tid, event, err)
	}

	var executor executionstage.TemplateExecutor
	awmProvider, wfName, err := executor.Execute(h.awm, h.keySrc, prerequisite)
	if err != nil {
		logger.WithError(err).Error("fail to launch workflow for execution template")
		return h.executionFailedEvent(tid, event, err)
	}
	logger.WithFields(log.Fields{
		"awmProvider": awmProvider,
		"wfName":      wfName,
	}).Info("execution workflow launched")

	err = h.saveWorkflow(prerequisite.TFRun, awmProvider, wfName)
	if err != nil {
		logger.WithError(err).Error("fail to save execution workflow to storage")
		return h.executionFailedEvent(tid, event, err)
	}
	logger.Info("execution stage started")
	return h.executionStartedEvent(tid, prerequisite.TFRun)
}

func (h PreflightSucceededHandler) saveWorkflow(tfRun types.DeploymentRun, awmProvider awmclient.AWMProvider, wfName string) error {
	err := h.wfStorage.CreateDeploymentExecWorkflow(tfRun.Deployment, tfRun.ID, awmProvider, wfName)
	if err != nil {
		return err
	}
	return nil
}

func (h PreflightSucceededHandler) executionFailedEvent(tid common.TransactionID, event deploymentevents.RunPreflightResult, err error) types.ResponseEvent {
	var eventBody = deploymentevents.RunExecutionFailed{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		DeploymentType: types.TerraformOpenStackDeploymentType,
		Deployment:     event.Deployment,
		Run:            event.Run,
	}
	return types.NewEvent(deploymentevents.EventRunExecutionFailed, tid, eventBody)
}

func (h PreflightSucceededHandler) executionStartedEvent(tid common.TransactionID, tfRun types.DeploymentRun) types.ResponseEvent {
	var eventBody = deploymentevents.RunExecutionStarted{
		Session: service.Session{
			SessionActor:    tfRun.CreatedBy.User,
			SessionEmulator: tfRun.CreatedBy.Emulator,
		},
		DeploymentType: types.TerraformOpenStackDeploymentType,
		Deployment:     tfRun.Deployment,
		Run:            tfRun.ID,
	}
	return types.NewEvent(deploymentevents.EventRunExecutionStarted, tid, eventBody)
}

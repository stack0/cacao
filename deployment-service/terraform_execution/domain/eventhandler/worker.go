package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"sync"
)

// EventWorker is a worker that will process incoming events
type EventWorker struct {
	eventSink                 ports.EventSink
	startRunHandler           StartRunHandler
	preflightSucceededHandler PreflightSucceededHandler
	deletionHandler           DeletionStartedHandler
	wfSucceededHandler        WorkflowSucceededHandler
	wfFailedHandler           WorkflowFailedHandler
}

// NewEventWorker create a event worker
func NewEventWorker(portsDependency ports.Ports) EventWorker {
	return EventWorker{
		eventSink:                 portsDependency.EventSink,
		startRunHandler:           NewStartRunHandler(portsDependency),
		preflightSucceededHandler: NewPreflightSucceededHandler(portsDependency),
		deletionHandler:           NewDeletionHandler(portsDependency),
		wfSucceededHandler:        NewWorkflowSucceededHandler(portsDependency),
		wfFailedHandler:           NewWorkflowFailedHandler(portsDependency),
	}
}

// Run starts processing incoming events from the channel
func (w EventWorker) Run(wg *sync.WaitGroup, eventChan <-chan types.IncomingEvent) {
	defer wg.Done()

	for event := range eventChan {
		handler := w.SelectEventHandler(event)
		if handler != nil {
			log.Infof("received %s event", event.EventType())
			response := handler.Handle(event)
			if response.EventType() == types.DoNotSent {
				// ignore
				continue
			}
			err := w.eventSink.Publish(response)
			if err != nil {
				log.WithFields(log.Fields{
					"incoming": event.EventType(),
					"outgoing": response.EventType(),
					"err":      err.Error(),
				}).Error("fail to publish response event")
			}
		}
	}
}

// SelectEventHandler selects an EventHandler for the event
func (w EventWorker) SelectEventHandler(event types.IncomingEvent) types.EventHandler {
	switch event.EventType() {
	case deploymentevents.EventStartRunRequested:
		return w.startRunHandler
	case deploymentevents.EventRunPreflightSucceeded:
		return w.preflightSucceededHandler
	case service.DeploymentDeletionStarted:
		return w.deletionHandler
	case awmclient.WorkflowSucceededEvent:
		return w.wfSucceededHandler
	case awmclient.WorkflowFailedEvent:
		return w.wfFailedHandler
	default:
		return nil
	}
}

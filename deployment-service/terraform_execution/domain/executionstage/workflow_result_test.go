package executionstage

import (
	"encoding/json"
	"errors"
	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

func TestWorkflowHandler_HandleWorkflowSuccess(t *testing.T) {
	var now = time.Now()
	var tfState = deploymentcommon.TerraformState{
		Version:          0,
		TerraformVersion: "",
		Serial:           0,
		Lineage:          "",
		Outputs:          nil,
		Resources: []deploymentcommon.TerraformResource{
			{
				Module:     "",
				Mode:       "managed",
				Type:       "openstack_compute_instance_v2",
				Name:       "os_instances",
				TFProvider: "provider[\"terraform.cyverse.org/cyverse/openstack\"]",
				Instances: []deploymentcommon.TerraformResourceInstance{
					{
						IndexKey:      0,
						SchemaVersion: 0,
						Attributes: map[string]interface{}{
							"id": "2c2e00b3-c86f-4c0a-9876-9d35bdc4378b",
						},
						SensitiveAttributes: nil,
						Private:             "",
						Dependencies:        nil,
					},
				},
			},
		},
	}
	var wfOutputs map[string]interface{}
	err := mapstructure.Decode(tfState, &wfOutputs)
	if err != nil {
		panic(err)
	}
	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
		timeSrc    func() *portsmocks.TimeSrc
	}
	type args struct {
		tid      common.TransactionID
		event    awmclient.WorkflowSucceeded
		workflow types.DeploymentWorkflow
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		check  func(*testing.T, *types.ResponseEvent)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					newStatus := deploymentcommon.DeploymentRunActive
					runStorage.On("Update", common.ID("run-aaaaaaaaaaaaaaaaaaaa"), types.DeploymentRunUpdate{
						Status:         &newStatus,
						StateUpdatedAt: &now,
						TerraformState: &tfState,
					}, types.DeploymentRunFilter{}).Return(true, nil)
					return runStorage
				},
				timeSrc: func() *portsmocks.TimeSrc {
					timeSrc := &portsmocks.TimeSrc{}
					timeSrc.On("Now").Return(now)
					return timeSrc
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					Status:       "Succeeded",
					WfOutputs:    wfOutputs,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      types.ExecutionPurpose,
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
					CreatedAt:    time.Time{},
					EndedAt:      time.Time{},
					Status:       "",
					Logs:         nil,
				},
			},
			check: func(t *testing.T, event *types.ResponseEvent) {
				assert.NotNil(t, event)
				assert.Equal(t, deploymentevents.EventRunExecutionSucceeded, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				assert.NoError(t, err)
				var eventBody deploymentevents.RunExecutionSucceeded
				err = json.Unmarshal(ce.Data(), &eventBody)
				assert.NoError(t, err)
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.Equal(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
			},
		},
		{
			name: "run storage Get() err",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
				timeSrc: func() *portsmocks.TimeSrc {
					timeSrc := &portsmocks.TimeSrc{}
					return timeSrc
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					Status:       "Succeeded",
					WfOutputs:    wfOutputs,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      types.ExecutionPurpose,
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
					CreatedAt:    time.Time{},
					EndedAt:      time.Time{},
					Status:       "",
					Logs:         nil,
				},
			},
			check: func(t *testing.T, event *types.ResponseEvent) {
				assert.NotNil(t, event)
				assert.Equal(t, deploymentevents.EventRunExecutionFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				assert.NoError(t, err)
				var eventBody deploymentevents.RunExecutionSucceeded
				err = json.Unmarshal(ce.Data(), &eventBody)
				assert.NoError(t, err)
				assert.Equal(t, "", eventBody.SessionActor)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Greater(t, len(eventBody.ServiceError.Error()), 0)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
			},
		},
		{
			name: "run storage Update() err",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					newStatus := deploymentcommon.DeploymentRunActive
					runStorage.On("Update", common.ID("run-aaaaaaaaaaaaaaaaaaaa"), types.DeploymentRunUpdate{
						Status:         &newStatus,
						StateUpdatedAt: &now,
						TerraformState: &tfState,
					}, types.DeploymentRunFilter{}).Return(false, errors.New("failed"))
					return runStorage
				},
				timeSrc: func() *portsmocks.TimeSrc {
					timeSrc := &portsmocks.TimeSrc{}
					timeSrc.On("Now").Return(now)
					return timeSrc
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					Status:       "Succeeded",
					WfOutputs:    wfOutputs,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      types.ExecutionPurpose,
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
					CreatedAt:    time.Time{},
					EndedAt:      time.Time{},
					Status:       "",
					Logs:         nil,
				},
			},
			check: func(t *testing.T, event *types.ResponseEvent) {
				assert.NotNil(t, event)
				assert.Equal(t, deploymentevents.EventRunExecutionFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				assert.NoError(t, err)
				var eventBody deploymentevents.RunExecutionSucceeded
				err = json.Unmarshal(ce.Data(), &eventBody)
				assert.NoError(t, err)
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Greater(t, len(eventBody.ServiceError.Error()), 0)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
			},
		},
		{
			name: "run storage Update() not updated",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					newStatus := deploymentcommon.DeploymentRunActive
					runStorage.On("Update", common.ID("run-aaaaaaaaaaaaaaaaaaaa"), types.DeploymentRunUpdate{
						Status:         &newStatus,
						StateUpdatedAt: &now,
						TerraformState: &tfState,
					}, types.DeploymentRunFilter{}).Return(false, nil)
					return runStorage
				},
				timeSrc: func() *portsmocks.TimeSrc {
					timeSrc := &portsmocks.TimeSrc{}
					timeSrc.On("Now").Return(now)
					return timeSrc
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					Status:       "Succeeded",
					WfOutputs:    wfOutputs,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      types.ExecutionPurpose,
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
					CreatedAt:    time.Time{},
					EndedAt:      time.Time{},
					Status:       "",
					Logs:         nil,
				},
			},
			check: func(t *testing.T, event *types.ResponseEvent) {
				assert.NotNil(t, event)
				assert.Equal(t, deploymentevents.EventRunExecutionFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				assert.NoError(t, err)
				var eventBody deploymentevents.RunExecutionSucceeded
				err = json.Unmarshal(ce.Data(), &eventBody)
				assert.NoError(t, err)
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Greater(t, len(eventBody.ServiceError.Error()), 0)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
			},
		},
		{
			name: "bad workflow outputs",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
				timeSrc: func() *portsmocks.TimeSrc {
					timeSrc := &portsmocks.TimeSrc{}
					return timeSrc
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					Status:       "Succeeded",
					WfOutputs:    nil,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "aaaaaaaaaaaaaaaaaaaa",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      types.ExecutionPurpose,
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
					CreatedAt:    time.Time{},
					EndedAt:      time.Time{},
					Status:       "",
					Logs:         nil,
				},
			},
			check: func(t *testing.T, event *types.ResponseEvent) {
				assert.NotNil(t, event)
				assert.Equal(t, deploymentevents.EventRunExecutionFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				assert.NoError(t, err)
				var eventBody deploymentevents.RunExecutionSucceeded
				err = json.Unmarshal(ce.Data(), &eventBody)
				assert.NoError(t, err)
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Greater(t, len(eventBody.ServiceError.Error()), 0)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			timeSrc := tt.fields.timeSrc()
			h := WorkflowHandler{
				runStorage: runStorage,
				timeSrc:    timeSrc,
			}
			result := h.HandleWorkflowSuccess(tt.args.tid, tt.args.event, tt.args.workflow)
			tt.check(t, result)
			runStorage.AssertExpectations(t)
			timeSrc.AssertExpectations(t)
		})
	}
}

func TestWorkflowHandler_parseTFStateFromWorkflowOutputs(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		h := WorkflowHandler{}
		_, err := h.parseTFStateFromWorkflowOutputs(map[string]interface{}{})
		assert.Error(t, err)
	})
	t.Run("missing key", func(t *testing.T) {
		h := WorkflowHandler{}
		_, err := h.parseTFStateFromWorkflowOutputs(map[string]interface{}{
			"foo": "bar",
		})
		assert.Error(t, err)
	})
}

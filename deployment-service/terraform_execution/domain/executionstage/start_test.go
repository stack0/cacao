package executionstage

import (
	"crypto/rand"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"io"
	"testing"
	"time"
)

var testRun = types.DeploymentRun{
	ID:         "run-aaaaaaaaaaaaaaaaaaaa",
	Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
	CreatedBy: deploymentcommon.Creator{
		User:     "testuser123",
		Emulator: "",
	},
	CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
	PrimaryProvider: "provider-aaaaaaaaaaaaaaaaaaaa",
	PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
		TemplateID: "template-cccccccccccccccccccc",
		UpstreamTracked: deploymentcommon.GitUpstream{
			Branch: "main",
			Tag:    "",
		},
		GitURL:     "github.com/cyverse/foo",
		CommitHash: "",
		SubPath:    "/",
	},
	TemplateSnapshot: deploymentcommon.TemplateSnapshot{
		TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
		UpstreamTracked: deploymentcommon.GitUpstream{
			Branch: "main",
			Tag:    "",
		},
		GitURL:     "github.com/cyverse/foobar",
		CommitHash: "",
		SubPath:    "/",
	},
	Parameters: []deploymentcommon.DeploymentParameter{
		{
			Name:  "param1",
			Type:  "string",
			Value: "value1",
		},
	},
	CloudCredentials: []deploymentcommon.ProviderCredentialPair{
		{
			Credential: "cloudcred123",
			Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
		},
	},
	GitCredential:  "",
	Status:         deploymentcommon.DeploymentRunRunning,
	StateUpdatedAt: time.Time{},
	TerraformState: nil,
}

func TestPrerequisiteGatherer_gatherRun(t *testing.T) {

	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
		credMS     ports.CredentialMicroservice
	}
	type args struct {
		runID common.ID
		data  *ExecutionPrerequisite
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(testRun, nil)
					return runStorage
				},
				credMS: nil,
			},
			args: args{
				runID: "run-aaaaaaaaaaaaaaaaaaaa",
				data: &ExecutionPrerequisite{
					TFRun: testRun,
				},
			},
			wantErr: false,
		},
		{
			name: "runStorage err",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
				credMS: nil,
			},
			args: args{
				runID: "run-aaaaaaaaaaaaaaaaaaaa",
				data: &ExecutionPrerequisite{
					TFRun: types.DeploymentRun{},
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			g := PrerequisiteGatherer{
				runStorage: runStorage,
				credMS:     nil,
			}
			if err := g.gatherRun(tt.args.runID, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("gatherRun() error = %v, wantErr %v", err, tt.wantErr)
			}
			runStorage.AssertExpectations(t)
		})
	}
}

func TestPrerequisiteGatherer_gatherCloudCredential(t *testing.T) {
	type fields struct {
		runStorage ports.TFRunStorage
		credMS     func() *portsmocks.CredentialMicroservice
	}
	type args struct {
		tfRun types.DeploymentRun
		data  *ExecutionPrerequisite
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantErr   bool
		postCheck func(t *testing.T, args args)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: nil,
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("Get", types.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}, "testuser123", "cloudcred123").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "{}",
							Type:     "openstack",
							ID:       "cloudcred123",
						},
						nil,
					)
					return credMS
				},
			},
			args: args{
				tfRun: testRun,
				data: &ExecutionPrerequisite{
					TFRun:     testRun,
					CloudCred: nil,
					GitCred:   nil,
				},
			},
			wantErr: false,
			postCheck: func(t *testing.T, args args) {
				if !assert.NotNil(t, args.data.CloudCred) {
					return
				}
				assert.Equal(t, "cloudcred123", args.data.CloudCred.GetID())
			},
		},
		{
			name: "credMS err",
			fields: fields{
				runStorage: nil,
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("Get", types.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}, "testuser123", "cloudcred123").Return(nil, errors.New("failed"))
					return credMS
				},
			},
			args: args{
				tfRun: testRun,
				data: &ExecutionPrerequisite{
					TFRun:     testRun,
					CloudCred: nil,
					GitCred:   nil,
				},
			},
			wantErr: true,
			postCheck: func(t *testing.T, args args) {
				assert.Nil(t, args.data.CloudCred)
			},
		},
		{
			name: "cred not openstack type",
			fields: fields{
				runStorage: nil,
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("Get", types.Actor{
						Actor:    "testuser123",
						Emulator: "",
					}, "testuser123", "cloudcred123").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "{}",
							Type:     "not-openstack",
							ID:       "cloudcred123",
						},
						nil,
					)
					return credMS
				},
			},
			args: args{
				tfRun: testRun,
				data: &ExecutionPrerequisite{
					TFRun:     testRun,
					CloudCred: nil,
					GitCred:   nil,
				},
			},
			wantErr: true,
			postCheck: func(t *testing.T, args args) {
				assert.Nil(t, args.data.CloudCred)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credMS := tt.fields.credMS()
			g := PrerequisiteGatherer{
				runStorage: tt.fields.runStorage,
				credMS:     credMS,
			}
			if err := g.gatherCloudCredential(tt.args.tfRun, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("gatherCloudCredential() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.postCheck(t, tt.args)
			credMS.AssertExpectations(t)
		})
	}
}

func TestExecutionPrerequisite_ValidateAndSetTFRun(t *testing.T) {
	type fields struct {
		TFRun     types.DeploymentRun
		CloudCred service.Credential
		GitCred   service.Credential
	}
	type args struct {
		tfRun types.DeploymentRun
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "normal",
			fields: fields{},
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-aaaaaaaaaaaaaaaaaaaa",
					Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-aaaaaaaaaaaaaaaaaaaa",
					PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-cccccccccccccccccccc",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foo",
						CommitHash: "",
						SubPath:    "/",
					},
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: false,
		},
		{
			name:   "err - bad deployment ID",
			fields: fields{},
			args: args{
				tfRun: types.DeploymentRun{
					ID:         "run-aaaaaaaaaaaaaaaaaaaa",
					Deployment: "badID",
					CreatedBy: deploymentcommon.Creator{
						User:     "testuser123",
						Emulator: "",
					},
					CreateRunTID:    "tid-aaaaaaaaaaaaaaaaaaaa",
					PrimaryProvider: "provider-aaaaaaaaaaaaaaaaaaaa",
					TemplateSnapshot: deploymentcommon.TemplateSnapshot{
						TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
						UpstreamTracked: deploymentcommon.GitUpstream{
							Branch: "main",
							Tag:    "",
						},
						GitURL:     "github.com/cyverse/foobar",
						CommitHash: "",
						SubPath:    "/",
					},
					Parameters: []deploymentcommon.DeploymentParameter{
						{
							Name:  "param1",
							Type:  "string",
							Value: "value1",
						},
					},
					CloudCredentials: []deploymentcommon.ProviderCredentialPair{
						{
							Credential: "cloudcred123",
							Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
						},
					},
					GitCredential:  "",
					Status:         deploymentcommon.DeploymentRunRunning,
					StateUpdatedAt: time.Time{},
					TerraformState: nil,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data := &ExecutionPrerequisite{
				TFRun:     tt.fields.TFRun,
				CloudCred: tt.fields.CloudCred,
				GitCred:   tt.fields.GitCred,
			}
			if err := data.ValidateAndSetTFRun(tt.args.tfRun); (err != nil) != tt.wantErr {
				t.Errorf("ValidateAndSetTFRun() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestExecutionPrerequisite_ValidateAndSetCloudCredential(t *testing.T) {
	var openstackCred = awmclient.OpenStackCredential{
		IdentityAPIVersion: "3",
		RegionName:         "regionname",
		Interface:          "interface",
		AuthURL:            "authurl",
		ProjectDomainID:    "projectdomainid",
		ProjectDomainName:  "projectdomainname",
		ProjectID:          "projectid",
		ProjectName:        "projectname",
		UserDomainName:     "userdomainname",
		Username:           "username",
		Password:           "password",
		AuthType:           "authtype",
		AppCredID:          "",
		AppCredName:        "",
		AppCredSecret:      "",
	}
	openstackCredMarshaled, err := json.Marshal(openstackCred)
	if err != nil {
		panic(err)
	}
	type fields struct {
		TFRun     types.DeploymentRun
		CloudCred service.Credential
		GitCred   service.Credential
	}
	type args struct {
		credential service.Credential
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "normal",
			fields: fields{},
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    string(openstackCredMarshaled),
					Type:     "openstack",
					ID:       "cred123",
				},
			},
			wantErr: false,
		},
		{
			name:   "err - bad type",
			fields: fields{},
			args: args{
				credential: &service.CredentialModel{
					Username: "testuser123",
					Value:    string(openstackCredMarshaled),
					Type:     "git",
					ID:       "cred123",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data := &ExecutionPrerequisite{
				TFRun:     tt.fields.TFRun,
				CloudCred: tt.fields.CloudCred,
				GitCred:   tt.fields.GitCred,
			}
			if err := data.ValidateAndSetCloudCredential(tt.args.credential); (err != nil) != tt.wantErr {
				t.Errorf("ValidateAndSetCloudCredential() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTemplateExecutor_prepareTerraformWorkflowData(t *testing.T) {
	var randomKey = make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, randomKey); err != nil {
		panic(err)
	}
	osCredMarshalled, err := json.Marshal(awmclient.OpenStackCredential{})
	if err != nil {
		panic(err)
	}
	type args struct {
		keySrc func() *portsmocks.KeySrc
		data   ExecutionPrerequisite
	}
	tests := []struct {
		name    string
		args    args
		want    awmclient.TerraformWorkflowData
		wantErr bool
	}{
		{
			name: "normal",
			args: args{
				keySrc: func() *portsmocks.KeySrc {
					keySrc := &portsmocks.KeySrc{}
					keySrc.On("GetKey").Return(randomKey)
					return keySrc
				},
				data: ExecutionPrerequisite{
					TFRun: types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot: deploymentcommon.TemplateSnapshot{
							TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
							UpstreamTracked: deploymentcommon.GitUpstream{
								Branch: "main",
								Tag:    "",
							},
							GitURL:     "github.com/cyverse/foobar",
							CommitHash: "",
							SubPath:    "/",
						},
						Parameters: []deploymentcommon.DeploymentParameter{
							{
								Name:  "param1",
								Type:  "string",
								Value: "value1",
							},
						},
						CloudCredentials: []deploymentcommon.ProviderCredentialPair{
							{
								Credential: "cloudcred123",
								Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
							},
						},
						GitCredential:  "",
						Status:         "",
						StateUpdatedAt: time.Time{},
						TerraformState: &deploymentcommon.TerraformState{},
					},
					CloudCred: &service.CredentialModel{
						Username: "testuser123",
						Value:    string(osCredMarshalled),
						Type:     "openstack",
						ID:       "cloudcred123",
					},
					GitCred: nil,
				},
			},
			want: awmclient.TerraformWorkflowData{
				Username:          "testuser123",
				Deployment:        "deployment-aaaaaaaaaaaaaaaaaaaa",
				TemplateID:        "template-aaaaaaaaaaaaaaaaaaaa",
				TerraformStateKey: "deployment-aaaaaaaaaaaaaaaaaaaa",
				GitURL:            "github.com/cyverse/foobar",
				GitTrackedUpStream: struct {
					Branch string `mapstructure:"branch"`
					Tag    string `mapstructure:"tag"`
					Commit string `mapstructure:"commit"`
				}{
					Branch: "main",
					Tag:    "",
					Commit: "",
				},
				Path:        "/",
				CloudCredID: "cloudcred123",
				AnsibleVars: map[string]interface{}{
					"TF_MODULE": "cacao_module",
					"TF_INPUT_VALUES": map[string]interface{}{
						"param1": "value1",
					},
					"TF_INPUT_VARS": map[string]interface{}{
						"param1": map[string]string{"type": "string"},
					},
				},
				CloudCredentialBase64: "",
				GitCredID:             "",
				GitCredentialBase64:   "",
			},
			wantErr: false,
		},
		{
			name: "bad template parameter",
			args: args{
				keySrc: func() *portsmocks.KeySrc {
					keySrc := &portsmocks.KeySrc{}
					keySrc.On("GetKey").Return(randomKey)
					return keySrc
				},
				data: ExecutionPrerequisite{
					TFRun: types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot: deploymentcommon.TemplateSnapshot{
							TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
							UpstreamTracked: deploymentcommon.GitUpstream{
								Branch: "main",
								Tag:    "",
							},
							GitURL:     "github.com/cyverse/foobar",
							CommitHash: "",
							SubPath:    "/",
						},
						Parameters: []deploymentcommon.DeploymentParameter{
							{
								Name:  "param1",
								Type:  "parameter-type-that-no-one-knows-about",
								Value: "value1",
							},
						},
						CloudCredentials: []deploymentcommon.ProviderCredentialPair{
							{
								Credential: "cloudcred123",
								Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
							},
						},
						GitCredential:  "",
						Status:         "",
						StateUpdatedAt: time.Time{},
						TerraformState: &deploymentcommon.TerraformState{},
					},
					CloudCred: &service.CredentialModel{
						Username: "testuser123",
						Value:    string(osCredMarshalled),
						Type:     "openstack",
						ID:       "cloudcred123",
					},
					GitCred: nil,
				},
			},
			want:    awmclient.TerraformWorkflowData{},
			wantErr: true,
		},
		{
			name: "bad cloud cred",
			args: args{
				keySrc: func() *portsmocks.KeySrc {
					keySrc := &portsmocks.KeySrc{}
					keySrc.On("GetKey").Return(randomKey)
					return keySrc
				},
				data: ExecutionPrerequisite{
					TFRun: types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot: deploymentcommon.TemplateSnapshot{
							TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
							UpstreamTracked: deploymentcommon.GitUpstream{
								Branch: "main",
								Tag:    "",
							},
							GitURL:     "github.com/cyverse/foobar",
							CommitHash: "",
							SubPath:    "/",
						},
						Parameters: []deploymentcommon.DeploymentParameter{
							{
								Name:  "param1",
								Type:  "string",
								Value: "value1",
							},
						},
						CloudCredentials: []deploymentcommon.ProviderCredentialPair{
							{
								Credential: "cloudcred123",
								Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
							},
						},
						GitCredential:  "",
						Status:         "",
						StateUpdatedAt: time.Time{},
						TerraformState: &deploymentcommon.TerraformState{},
					},
					CloudCred: &service.CredentialModel{
						Username: "testuser123",
						Value:    "", // empty
						Type:     "openstack",
						ID:       "cloudcred123",
					},
					GitCred: nil,
				},
			},
			want:    awmclient.TerraformWorkflowData{},
			wantErr: true,
		},
		{
			name: "bad key",
			args: args{
				keySrc: func() *portsmocks.KeySrc {
					keySrc := &portsmocks.KeySrc{}
					keySrc.On("GetKey").Return([]byte{1, 2, 3, 4, 5})
					return keySrc
				},
				data: ExecutionPrerequisite{
					TFRun: types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot: deploymentcommon.TemplateSnapshot{
							TemplateID: "template-aaaaaaaaaaaaaaaaaaaa",
							UpstreamTracked: deploymentcommon.GitUpstream{
								Branch: "main",
								Tag:    "",
							},
							GitURL:     "github.com/cyverse/foobar",
							CommitHash: "",
							SubPath:    "/",
						},
						Parameters: []deploymentcommon.DeploymentParameter{
							{
								Name:  "param1",
								Type:  "string",
								Value: "value1",
							},
						},
						CloudCredentials: []deploymentcommon.ProviderCredentialPair{
							{
								Credential: "cloudcred123",
								Provider:   "provider-aaaaaaaaaaaaaaaaaaaa",
							},
						},
						GitCredential:  "",
						Status:         "",
						StateUpdatedAt: time.Time{},
						TerraformState: &deploymentcommon.TerraformState{},
					},
					CloudCred: &service.CredentialModel{
						Username: "testuser123",
						Value:    string(osCredMarshalled),
						Type:     "openstack",
						ID:       "cloudcred123",
					},
					GitCred: nil,
				},
			},
			want:    awmclient.TerraformWorkflowData{},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	compareWorkflowData := func(t *testing.T, expected awmclient.TerraformWorkflowData, actual awmclient.TerraformWorkflowData) {
		// the reason for this special comparison function is that the base64 encoded credential string are not comparable, since the nonce used for AES encryption is random
		assert.Equal(t, expected.Username, actual.Username)
		assert.Equal(t, expected.Deployment, actual.Deployment)
		assert.Equal(t, expected.TemplateID, actual.TemplateID)
		assert.Equal(t, expected.TerraformStateKey, actual.TerraformStateKey)
		assert.Equal(t, expected.GitURL, actual.GitURL)
		assert.Equal(t, expected.GitTrackedUpStream, actual.GitTrackedUpStream)
		assert.Equal(t, expected.Path, actual.Path)
		assert.Equal(t, expected.CloudCredID, actual.CloudCredID)
		assert.Equal(t, expected.AnsibleVars, actual.AnsibleVars)
		assert.Greater(t, len(actual.CloudCredentialBase64), 0) // for base64 credential string, only check for empty
		assert.Equal(t, expected.GitCredID, actual.GitCredID)
		if len(expected.GitCredID) > 0 {
			assert.Greater(t, len(actual.GitCredentialBase64), 0) // for base64 credential string, only check for empty
		}
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keySrc := tt.args.keySrc()
			exec := TemplateExecutor{}
			got, err := exec.prepareTerraformWorkflowData(keySrc, tt.args.data)
			if tt.wantErr {
				assert.Errorf(t, err, "prepareTerraformWorkflowData(%v, %v)", keySrc, tt.args.data)
				return
			}
			assert.NoError(t, err, "prepareTerraformWorkflowData(%v, %v)", keySrc, tt.args.data)
			compareWorkflowData(t, tt.want, got)
		})
	}
}

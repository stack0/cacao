package preflightstage

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
	"time"
)

func TestWorkflowHandler_HandleWorkflowSuccess(t *testing.T) {
	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
	}
	type args struct {
		tid      common.TransactionID
		event    awmclient.WorkflowSucceeded
		workflow types.DeploymentWorkflow
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantNil     bool
		checkReturn func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightSucceeded, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, deploymentevents.RunPreflightResult{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					DeploymentType: types.TerraformOpenStackDeploymentType,
					Deployment:     "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:            "run-aaaaaaaaaaaaaaaaaaaa",
				}, eventBody)
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "empty tid in args",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "", // empty TID
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightSucceeded, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, deploymentevents.RunPreflightResult{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					DeploymentType: types.TerraformOpenStackDeploymentType,
					Deployment:     "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:            "run-aaaaaaaaaaaaaaaaaaaa",
				}, eventBody)
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "bad wf output",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs:    nil, // bad wf outputs
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.NotEqual(t, "", eventBody.ServiceError.StandardMessage)
				assert.NotEqual(t, "", eventBody.ServiceError.ContextualMessage)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)

				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())

				runStorage.AssertExpectations(t)
			},
		},
		{
			// Note, that it should be rare that WorkflowStorage succeeded in fetching workflow, but RunStorage failed
			// to fetch run.
			name: "run storage error",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowSucceeded{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "",
					WfOutputs: map[string]interface{}{
						"version":           0,
						"terraform_version": "",
						"serial":            0,
						"lineage":           "",
						"outputs":           map[string]interface{}{},
						"resources":         []interface{}{},
					},
					Metadata: nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equalf(t, "", eventBody.SessionActor, "Since failed to fetch run, there is nowhere to get actor from (event does not have it)")
				assert.NotEqual(t, "", eventBody.ServiceError.StandardMessage)
				assert.NotEqual(t, "", eventBody.ServiceError.ContextualMessage)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)

				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())

				runStorage.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			h := WorkflowHandler{
				runStorage: runStorage,
			}
			result := h.HandleWorkflowSuccess(tt.args.tid, tt.args.event, tt.args.workflow)
			if tt.wantNil {
				assert.Nil(t, result)
				return
			}
			assert.NotNil(t, result)
			tt.checkReturn(t, result, runStorage)
		})
	}
}

func TestWorkflowHandler_HandleWorkflowFailure(t *testing.T) {
	type fields struct {
		runStorage func() *portsmocks.TFRunStorage
	}
	type args struct {
		tid      common.TransactionID
		event    awmclient.WorkflowFailed
		workflow types.DeploymentWorkflow
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantNil     bool
		checkReturn func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage)
	}{
		{
			name: "normal",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{
						ID:         "run-aaaaaaaaaaaaaaaaaaaa",
						Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						CreatedBy: deploymentcommon.Creator{
							User:     "testuser123",
							Emulator: "",
						},
						CreateRunTID:         "tid-aaaaaaaaaaaaaaaaaaaa",
						PrimaryProvider:      "provider-aaaaaaaaaaaaaaaaaaaa",
						PrerequisiteTemplate: deploymentcommon.TemplateSnapshot{},
						TemplateSnapshot:     deploymentcommon.TemplateSnapshot{},
						Parameters:           nil,
						CloudCredentials:     nil,
						GitCredential:        "",
						Status:               "",
						StateUpdatedAt:       time.Time{},
						TerraformState:       nil,
					}, nil)
					newStatus := deploymentcommon.DeploymentRunErrored
					runStorage.On("Update", common.ID("run-aaaaaaaaaaaaaaaaaaaa"), types.DeploymentRunUpdate{
						Status: &newStatus,
					}, types.DeploymentRunFilter{}).Return(true, nil)
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowFailed{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "Failed",
					NodeStatus:   nil,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "testuser123", eventBody.SessionActor)
				assert.Equal(t, "", eventBody.SessionEmulator)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
				runStorage.AssertExpectations(t)
			},
		},
		{
			name: "run storage Get() err",
			fields: fields{
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
			},
			args: args{
				tid: "tid-aaaaaaaaaaaaaaaaaaaa",
				event: awmclient.WorkflowFailed{
					Provider:     "provider-aaaaaaaaaaaaaaaaaaaa",
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					Status:       "Failed",
					NodeStatus:   nil,
					Metadata:     nil,
				},
				workflow: types.DeploymentWorkflow{
					WorkflowName: "bbbbbbbbbbbbbbbbbbbb",
					AWMProvider:  "provider-aaaaaaaaaaaaaaaaaaaa",
					Purpose:      "",
					Deployment:   "deployment-aaaaaaaaaaaaaaaaaaaa",
					Run:          "run-aaaaaaaaaaaaaaaaaaaa",
				},
			},
			wantNil: false,
			checkReturn: func(t *testing.T, event *types.ResponseEvent, runStorage *portsmocks.TFRunStorage) {
				assert.Equal(t, deploymentevents.EventRunPreflightFailed, event.EventType())
				assert.Equal(t, common.TransactionID("tid-aaaaaaaaaaaaaaaaaaaa"), event.Transaction())
				ce, err := event.ToCloudEvent("")
				if !assert.NoError(t, err) {
					return
				}
				var eventBody deploymentevents.RunPreflightResult
				err = json.Unmarshal(ce.Data(), &eventBody)
				if !assert.NoError(t, err) {
					return
				}
				assert.Equal(t, "", eventBody.SessionActor)
				assert.Equal(t, "", eventBody.SessionEmulator)
				assert.NotEqual(t, service.CacaoErrorBase{}, eventBody.ServiceError)
				assert.Equal(t, types.TerraformOpenStackDeploymentType, eventBody.DeploymentType)
				assert.Equal(t, "deployment-aaaaaaaaaaaaaaaaaaaa", eventBody.Deployment.String())
				assert.Equal(t, "run-aaaaaaaaaaaaaaaaaaaa", eventBody.Run.String())
				runStorage.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			h := WorkflowHandler{
				runStorage: runStorage,
			}
			result := h.HandleWorkflowFailure(tt.args.tid, tt.args.event, tt.args.workflow)
			if tt.wantNil {
				assert.Nilf(t, result, "HandleWorkflowFailure(%v, %v, %v)", tt.args.tid, tt.args.event, tt.args.workflow)
			} else {
				assert.NotNilf(t, result, "HandleWorkflowFailure(%v, %v, %v)", tt.args.tid, tt.args.event, tt.args.workflow)
			}
			tt.checkReturn(t, result, runStorage)
		})
	}
}

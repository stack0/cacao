package tfrun

import (
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"math/rand"
)

// TemplateParameterGenerator is a function that generates parameters from PrerequisiteData for template.
type TemplateParameterGenerator func(PrerequisiteData) ([]deploymentcommon.DeploymentParameter, error)

// templateParameterGenerator generates parameters for execution template.
type templateParameterGenerator struct {
	credMS ports.CredentialMicroservice
}

// NewTemplateParameterGenerator creates a new generator for execution template.
func NewTemplateParameterGenerator(credMS ports.CredentialMicroservice) TemplateParameterGenerator {
	gen := templateParameterGenerator{
		credMS: credMS,
	}
	return gen.Generate
}

// Generate generates parameters for execution template
func (g templateParameterGenerator) Generate(data PrerequisiteData) ([]deploymentcommon.DeploymentParameter, error) {
	parameters, err := generateParameters(data, g.credMS)
	if err != nil {
		return nil, err
	}
	return parameters, nil
}

func generateParameters(data PrerequisiteData, credMS ports.CredentialMicroservice) ([]deploymentcommon.DeploymentParameter, error) {

	var osMetadata types.OpenStackProviderMetadata
	err := mapstructure.Decode(data.PrimaryProvider.GetMetadata(), &osMetadata)
	if err != nil {
		return nil, err
	}

	var paramConverter domainutils.ParameterConverter
	extNetworkName := osMetadata.ExternalNetworkName
	extSubnetUUID := selectExternalSubnet(osMetadata.ExternalSubnetUUIDs)

	// override param value for username and keypair-name
	overrideValues := map[string]interface{}{
		domainutils.TemplateUsernameType:                data.Request.GetSessionActor(),
		domainutils.TemplateProviderKeyPairType:         domainutils.KeyPairName(data.Request.GetSessionActor()),
		domainutils.TemplateProviderExternalNetworkType: extNetworkName,
		domainutils.TemplateProviderExternalSubnetType:  extSubnetUUID,
	}

	// only generate cloud-init script if there is a template param that requires it.
	if domainutils.TemplateParameterDefHasType(data.Template.GetMetadata().Parameters, domainutils.TemplateCloudInitType) {
		cloudInitScript, err := generateCloudInitScript(data.Request, data.CACAOSSHKey, credMS)
		if err != nil {
			return nil, err
		}
		overrideValues[domainutils.TemplateCloudInitType] = cloudInitScript
	}

	parameters, err := paramConverter.Convert(
		data.Template.GetMetadata().Parameters,
		data.Request.Run.RequestParameters,
		domainutils.OverrideValueForTypes(overrideValues),
	)
	if err != nil {
		return nil, err
	}
	return parameters, nil
}

func selectExternalSubnet(extSubnetList []string) string {
	return extSubnetList[rand.Intn(len(extSubnetList))]
}

func generateCloudInitScript(request deploymentevents.StartRunRequest, cacaoSSHKey string, credMS ports.CredentialMicroservice) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "tfrun",
		"function": "generateCloudInitScript",
	})
	generator := domainutils.NewCloudInitGenerator(0)
	sshKeys, err := listAllSSHCredentials(credMS, types.ActorFromSession(request.Session))
	if err != nil {
		logger.WithError(err).Error("fail to list all SSH credentials for cloud-init script generation")
		return "", err
	}
	logger.WithField("sshKeyCount", len(sshKeys)).Debug("ssh keys fetched for cloud-init script generation")
	generator.AddSSHKeys(sshKeys)
	if cacaoSSHKey != "" {
		generator.AddSSHKey(cacaoSSHKey)
	}

	generator.CreateUser(request.GetSessionActor())

	cloudInitScript, err := generator.Generate()
	if err != nil {
		logger.WithError(err).Error("fail to generate cloud-init script")
		return "", err
	}
	return cloudInitScript, nil
}

// list all ssh key credential that owned by the actor.
func listAllSSHCredentials(credMS ports.CredentialMicroservice, actor types.Actor) ([]string, error) {
	list, err := credMS.List(actor, actor.Actor)
	if err != nil {
		return nil, err
	}
	sshCredIDs := make([]string, 0)
	for _, cred := range list {
		if cred.GetType() == "ssh" {
			sshCredIDs = append(sshCredIDs, cred.GetID())
		}
	}
	// because the list operation returns value as REDACTED, thus each credential is fetched individually
	allPublicSSHKeys := make([]string, 0, len(sshCredIDs))
	for _, credID := range sshCredIDs {
		var cred service.Credential
		cred, err = credMS.Get(actor, actor.Actor, credID)
		if err != nil {
			return nil, err
		}
		if len(cred.GetValue()) > 0 {
			allPublicSSHKeys = append(allPublicSSHKeys, cred.GetValue())
		}
	}
	return allPublicSSHKeys, nil
}

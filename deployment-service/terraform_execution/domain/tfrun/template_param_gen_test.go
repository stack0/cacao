package tfrun

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"testing"
)

func Test_generateParameters(t *testing.T) {
	type args struct {
		data   PrerequisiteData
		credMS func() *portsmocks.CredentialMicroservice
	}
	tests := []struct {
		name    string
		args    args
		want    []deploymentcommon.DeploymentParameter
		wantErr bool
		check   func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string)
	}{
		{
			name: "normal without CACAO ssh key",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-aaaaaaaaaaaaaaaaaaaa",
						Metadata: service.TemplateMetadata{
							Name:             "",
							TemplateTypeName: "",
							Purpose:          "",
							CacaoPostTasks:   nil,
							Parameters: map[string]interface{}{
								"int1": map[string]interface{}{
									"type": "integer",
								},
								"int2": map[string]interface{}{
									"type":    "integer",
									"default": 100,
								},
								"str1": map[string]interface{}{
									"type": "string",
								},
								"username": map[string]interface{}{
									"type": domainutils.TemplateUsernameType,
								},
								"keypair": map[string]interface{}{
									"type": domainutils.TemplateProviderKeyPairType,
								},
								"ext_net_name": map[string]interface{}{
									"type": domainutils.TemplateProviderExternalNetworkType,
								},
								"cloud_init": map[string]interface{}{
									"type": domainutils.TemplateCloudInitType,
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
						Type:     "openstack",
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					var actor = types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa FFFFF",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: 123,
				},
				{
					Name:  "int2",
					Type:  "integer",
					Value: 100,
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "username",
					Type:  domainutils.TemplateUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "keypair",
					Type:  domainutils.TemplateProviderKeyPairType,
					Value: "keypair-testuser123",
				},
				{
					Name:  "ext_net_name",
					Type:  domainutils.TemplateProviderExternalNetworkType,
					Value: "public",
				},
				{
					Name:  "cloud_init",
					Type:  domainutils.TemplateCloudInitType,
					Value: "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "normal with CACAO ssh key",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-aaaaaaaaaaaaaaaaaaaa",
						Metadata: service.TemplateMetadata{
							Name:             "",
							TemplateTypeName: "",
							Purpose:          "",
							CacaoPostTasks:   nil,
							Parameters: map[string]interface{}{
								"int1": map[string]interface{}{
									"type": "integer",
								},
								"int2": map[string]interface{}{
									"type":    "integer",
									"default": 100,
								},
								"str1": map[string]interface{}{
									"type": "string",
								},
								"username": map[string]interface{}{
									"type": domainutils.TemplateUsernameType,
								},
								"keypair": map[string]interface{}{
									"type": domainutils.TemplateProviderKeyPairType,
								},
								"ext_net_name": map[string]interface{}{
									"type": domainutils.TemplateProviderExternalNetworkType,
								},
								"cloud_init": map[string]interface{}{
									"type": domainutils.TemplateCloudInitType,
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
						Type:     "openstack",
						Metadata: exampleOpenStackProviderMetadata,
					},
					CACAOSSHKey: "ssh-rsa cacao123",
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					var actor = types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa FFFFF",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: 123,
				},
				{
					Name:  "int2",
					Type:  "integer",
					Value: 100,
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "username",
					Type:  domainutils.TemplateUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "keypair",
					Type:  domainutils.TemplateProviderKeyPairType,
					Value: "keypair-testuser123",
				},
				{
					Name:  "ext_net_name",
					Type:  domainutils.TemplateProviderExternalNetworkType,
					Value: "public",
				},
				{
					Name:  "cloud_init",
					Type:  domainutils.TemplateCloudInitType,
					Value: "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n      - ssh-rsa cacao123\n",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "missing parameter, no cloud init",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								//"str1": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-aaaaaaaaaaaaaaaaaaaa",
						Metadata: service.TemplateMetadata{
							Name:             "",
							TemplateTypeName: "",
							Purpose:          "",
							CacaoPostTasks:   nil,
							Parameters: map[string]interface{}{
								"int1": map[string]interface{}{
									"type": "integer",
								},
								"int2": map[string]interface{}{
									"type":    "integer",
									"default": 100,
								},
								"str1": map[string]interface{}{
									"type": "string",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
						Type:     "openstack",
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "extra parameter, no cloud init",
			args: args{
				data: PrerequisiteData{
					Request: deploymentevents.StartRunRequest{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Run: deploymentevents.Run{
							RequestParameters: map[string]interface{}{
								"int1": 123,
								"str1": "foobar",
								"str2": "foobar",
							},
						},
					},
					Template: &service.TemplateModel{
						ID: "template-aaaaaaaaaaaaaaaaaaaa",
						Metadata: service.TemplateMetadata{
							Name:             "",
							TemplateTypeName: "",
							Purpose:          "",
							CacaoPostTasks:   nil,
							Parameters: map[string]interface{}{
								"int1": map[string]interface{}{
									"type": "integer",
								},
								"str1": map[string]interface{}{
									"type": "string",
								},
							},
						},
					},
					PrimaryProvider: &service.ProviderModel{
						ID:       "provider-aaaaaaaaaaaaaaaaaaaa",
						Type:     "openstack",
						Metadata: exampleOpenStackProviderMetadata,
					},
				},
				credMS: func() *portsmocks.CredentialMicroservice {
					credMS := &portsmocks.CredentialMicroservice{}
					return credMS
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "int1",
					Type:  "integer",
					Value: 123,
				},
				{
					Name:  "str1",
					Type:  "string",
					Value: "foobar",
				},
			},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credMS := tt.args.credMS()
			got, err := generateParameters(tt.args.data, credMS)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			tt.check(t, err, credMS, fmt.Sprintf("generateParameters(%v, %v)", tt.args.data, credMS))
			assert.ElementsMatchf(t, tt.want, got, "generateParameters(%v, %v)", tt.args.data, credMS)
		})
	}
}

func Test_generateCloudInitScript(t *testing.T) {
	var actor = types.Actor{Actor: "testuser123", Emulator: ""}
	var request = deploymentevents.StartRunRequest{
		Session: service.Session{
			SessionActor:    actor.Actor,
			SessionEmulator: actor.Emulator,
		},
	}
	var credMS = &portsmocks.CredentialMicroservice{}
	credMS.On("List", actor, "testuser123").Return(
		[]service.Credential{
			&service.CredentialModel{
				Username: "testuser123",
				Value:    "",
				Type:     "ssh",
				ID:       "foobar",
			},
		},
		nil,
	)
	credMS.On("Get", actor, actor.Actor, "foobar").Return(
		&service.CredentialModel{
			Username: "testuser123",
			Value:    "ssh-rsa FFFFF",
			Type:     "ssh",
			ID:       "foobar",
		},
		nil,
	)
	cloudInit, err := generateCloudInitScript(request, "ssh-rsa foobar", credMS)
	assert.NoError(t, err)
	assert.Greater(t, len(cloudInit), 0)
	expected := "#cloud-config\nusers:\n  - default\n  - name: testuser123\n    shell: /bin/bash\n    sudo: ALL=(ALL) NOPASSWD:ALL\n    ssh_authorized_keys:\n      - ssh-rsa FFFFF\n      - ssh-rsa foobar\n"
	assert.Equal(t, expected, cloudInit)
}

func Test_listAllSSHCredentials(t *testing.T) {
	type args struct {
		credMS func() *portsmocks.CredentialMicroservice
		actor  types.Actor
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
		check   func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string)
	}{
		{
			name: "normal",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa AAAAA",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{"ssh-rsa AAAAA"},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "2 ssh keys",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar1",
							},
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar2",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar1").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa AAAAA",
							Type:     "ssh",
							ID:       "foobar1",
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar2").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa BBBBB",
							Type:     "ssh",
							ID:       "foobar2",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{"ssh-rsa AAAAA", "ssh-rsa BBBBB"},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "2 cred, 1 ssh key",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "openstack",
								ID:       "foobar1",
							},
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar2",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar2").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "ssh-rsa BBBBB",
							Type:     "ssh",
							ID:       "foobar2",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{"ssh-rsa BBBBB"},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "no cred",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "1 cred, no ssh key",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "openstack",
								ID:       "foobar1",
							},
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "empty cred value",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(
						&service.CredentialModel{
							Username: "testuser123",
							Value:    "",
							Type:     "ssh",
							ID:       "foobar",
						},
						nil,
					)
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    []string{},
			wantErr: false,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "credMS list err",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(nil, errors.New("failed"))
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
		{
			name: "credMS get err",
			args: args{
				credMS: func() *portsmocks.CredentialMicroservice {
					actor := types.Actor{Actor: "testuser123", Emulator: ""}
					credMS := &portsmocks.CredentialMicroservice{}
					credMS.On("List", actor, "testuser123").Return(
						[]service.Credential{
							&service.CredentialModel{
								Username: "testuser123",
								Value:    "",
								Type:     "ssh",
								ID:       "foobar",
							},
						},
						nil,
					)
					credMS.On("Get", actor, actor.Actor, "foobar").Return(nil, errors.New("failed"))
					return credMS
				},
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    nil,
			wantErr: true,
			check: func(t *testing.T, err error, credMS *portsmocks.CredentialMicroservice, call string) {
				credMS.AssertExpectations(t)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credMS := tt.args.credMS()
			got, err := listAllSSHCredentials(credMS, tt.args.actor)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			tt.check(t, err, credMS, fmt.Sprintf("listAllSSHCredentials(%v, %v)", credMS, tt.args.actor))
			assert.Equalf(t, tt.want, got, "listAllSSHCredentials(%v, %v)", credMS, tt.args.actor)
		})
	}
}

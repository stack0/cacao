package resourcedeletion

import (
	"errors"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"reflect"
	"testing"
)

func TestPrerequisiteGatherer_getLastRun(t *testing.T) {
	type fields struct {
		runStorage  func() *portsmocks.TFRunStorage
		credMS      ports.CredentialMicroservice
		metadataSvc func() *portsmocks.DeploymentMetadataService
	}
	type args struct {
		deploymentID common.ID
		actor        types.Actor
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    types.DeploymentRun
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{Actor: "testuser123", Emulator: ""},
						common.ID("deployment-aaaaaaaaaaaaaaaaaaaa"),
					).Return(service.Deployment{
						ID: "deployment-aaaaaaaaaaaaaaaaaaaa",
						LastRun: &service.DeploymentRun{
							ID: "run-aaaaaaaaaaaaaaaaaaaa",
						},
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(
						types.DeploymentRun{
							ID:         "run-aaaaaaaaaaaaaaaaaaaa",
							Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
						}, nil)
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-aaaaaaaaaaaaaaaaaaaa",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want: types.DeploymentRun{
				ID:         "run-aaaaaaaaaaaaaaaaaaaa",
				Deployment: "deployment-aaaaaaaaaaaaaaaaaaaa",
			},
			wantErr: false,
		},
		{
			name: "metadata svc Get() err",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{Actor: "testuser123", Emulator: ""},
						common.ID("deployment-aaaaaaaaaaaaaaaaaaaa"),
					).Return(service.Deployment{}, errors.New("failed"))
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-aaaaaaaaaaaaaaaaaaaa",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "deployment no last run",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{
							Actor:    "testuser123",
							Emulator: "",
						},
						common.ID("deployment-aaaaaaaaaaaaaaaaaaaa"),
					).Return(service.Deployment{
						ID:      "deployment-aaaaaaaaaaaaaaaaaaaa",
						LastRun: nil, // no last run
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-aaaaaaaaaaaaaaaaaaaa",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
		{
			name: "run storage Get() err",
			fields: fields{
				credMS: nil,
				metadataSvc: func() *portsmocks.DeploymentMetadataService {
					metadataSvc := &portsmocks.DeploymentMetadataService{}
					metadataSvc.On("Get",
						types.Actor{
							Actor:    "testuser123",
							Emulator: "",
						},
						common.ID("deployment-aaaaaaaaaaaaaaaaaaaa"),
					).Return(service.Deployment{
						ID: "deployment-aaaaaaaaaaaaaaaaaaaa",
						LastRun: &service.DeploymentRun{
							ID: "run-aaaaaaaaaaaaaaaaaaaa",
						},
					}, nil)
					return metadataSvc
				},
				runStorage: func() *portsmocks.TFRunStorage {
					runStorage := &portsmocks.TFRunStorage{}
					runStorage.On("Get", common.ID("run-aaaaaaaaaaaaaaaaaaaa")).Return(
						types.DeploymentRun{}, errors.New("failed"))
					return runStorage
				},
			},
			args: args{
				deploymentID: "deployment-aaaaaaaaaaaaaaaaaaaa",
				actor: types.Actor{
					Actor:    "testuser123",
					Emulator: "",
				},
			},
			want:    types.DeploymentRun{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			runStorage := tt.fields.runStorage()
			metadataSvc := tt.fields.metadataSvc()
			gatherer := PrerequisiteGatherer{
				runStorage:  runStorage,
				credMS:      tt.fields.credMS,
				metadataSvc: metadataSvc,
			}
			got, err := gatherer.getLastRun(tt.args.deploymentID, tt.args.actor)
			if (err != nil) != tt.wantErr {
				t.Errorf("getLastRun() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getLastRun() got = %v, want %v", got, tt.want)
			}
			runStorage.AssertExpectations(t)
			metadataSvc.AssertExpectations(t)
		})
	}
}

package resourcedeletion

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowHandler handles result of deletion workflow (workflow that cleanup resource)
type WorkflowHandler struct {
	runStorage ports.TFRunStorage
	timeSrc    ports.TimeSrc
}

// NewWorkflowHandler ...
func NewWorkflowHandler(dependencies ports.Ports) WorkflowHandler {
	return WorkflowHandler{
		runStorage: dependencies.TFRunStorage,
		timeSrc:    dependencies.TimeSrc,
	}
}

// HandleWorkflowSuccess handles success workflow. Success workflow means that the resource has been cleaned up successfully.
func (h WorkflowHandler) HandleWorkflowSuccess(tid common.TransactionID, event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow) *types.ResponseEvent {
	logger := log.WithFields(log.Fields{
		"package":    "resourcedeletion",
		"function":   "WorkflowHandler.HandleWorkflowSuccess",
		"workflow":   workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	if !workflow.Deployment.Validate() || workflow.Deployment.Prefix() != service.DeploymentIDPrefix {
		logger.Error("workflow object has bad deployment ID")
		return nil
	}
	err := h.checkWorkflowOutputs(logger, event)
	if err != nil {
		logger.WithError(err).Error("error while checking workflow outputs")
		h.cleanupFailedEvent(tid, workflow, err)
	}
	logger.Info("cleanup succeeded")
	return h.cleanupSucceededEvent(tid, workflow)
}

func (h WorkflowHandler) checkWorkflowOutputs(logger *log.Entry, event awmclient.WorkflowSucceeded) error {
	var tfState deploymentcommon.TerraformState
	err := mapstructure.Decode(event.WfOutputs, &tfState)
	if err != nil {
		logger.WithError(err).Error("fail to parse terraform state from workflow outputs")
		return err
	}
	if len(tfState.Resources) != 0 {
		err = fmt.Errorf("resource cleanup does not delete all resources, there are %d resources left in terraform state after cleanup", len(tfState.Resources))
		logger.WithError(err).Error("resource cleanup does not delete all resources")
		return err
	}
	return nil
}

func (h WorkflowHandler) cleanupSucceededEvent(tid common.TransactionID, workflow types.DeploymentWorkflow) *types.ResponseEvent {
	eventBody := deploymentevents.DeploymentDeletionCleanupResult{
		Session: service.Session{
			SessionActor:    "", // TODO add this when workflow event has actor
			SessionEmulator: "",
		},
		DeploymentType: types.TerraformOpenStackDeploymentType,
		Deployment:     workflow.Deployment,
	}
	event := types.NewEvent(deploymentevents.EventDeploymentDeletionCleanupSucceeded, tid, eventBody)
	return &event
}

// HandleWorkflowFailure handles failed workflow.
func (h WorkflowHandler) HandleWorkflowFailure(tid common.TransactionID, event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow) *types.ResponseEvent {
	log.WithFields(log.Fields{
		"package":    "resourcedeletion",
		"function":   "WorkflowHandler.HandleWorkflowFailure",
		"workflow":   workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	}).Info("deletion workflow failed")
	// emit event
	err := fmt.Errorf("workflow %s failed", workflow.WorkflowName)
	return h.cleanupFailedEvent(tid, workflow, err)
}

func (h WorkflowHandler) cleanupFailedEvent(tid common.TransactionID, workflow types.DeploymentWorkflow, err error) *types.ResponseEvent {
	eventBody := deploymentevents.DeploymentDeletionCleanupResult{
		Session: service.Session{
			SessionActor:    "", // TODO add this when workflow event has actor
			SessionEmulator: "",
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		DeploymentType: types.TerraformOpenStackDeploymentType,
		Deployment:     workflow.Deployment,
	}
	event := types.NewEvent(deploymentevents.EventDeploymentDeletionCleanupFailed, tid, eventBody)
	return &event
}

package resourcedeletion

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/tfrun"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// ResourceCleanupPrerequisite is data that is required for starting the resource cleanup.
type ResourceCleanupPrerequisite struct {
	//Deployment service.Deployment
	Actor types.Actor
	//Template  service.Template
	LastRun   types.DeploymentRun
	CloudCred service.Credential
	GitCred   service.Credential
}

func (data *ResourceCleanupPrerequisite) validateAndSetActor(actor types.Actor) error {
	if actor.Actor == "" {
		return service.NewCacaoInvalidParameterError("actor is empty")
	}
	data.Actor = actor
	return nil
}

func (data *ResourceCleanupPrerequisite) validateAndSetLastRun(run types.DeploymentRun) error {
	err := tfrun.Validate(run)
	if err != nil {
		return err
	}
	data.LastRun = run
	return nil
}

func (data *ResourceCleanupPrerequisite) validateAndSetCloudCredential(cred service.Credential) error {
	err := domainutils.ValidateOpenstackCredential(cred)
	if err != nil {
		return err
	}
	data.CloudCred = cred
	return nil
}

func (data *ResourceCleanupPrerequisite) validateAndSetGitCredential(cred service.Credential) error {
	err := domainutils.ValidateGitCredential(cred)
	if err != nil {
		return err
	}
	data.GitCred = cred
	return nil
}

// PrerequisiteGatherer gathers prerequisite for launching deletion workflow that cleans up resources for deployment.
type PrerequisiteGatherer struct {
	runStorage  ports.TFRunStorage
	credMS      ports.CredentialMicroservice
	metadataSvc ports.DeploymentMetadataService
}

// NewPrerequisiteGatherer ...
func NewPrerequisiteGatherer(runStorage ports.TFRunStorage, credMS ports.CredentialMicroservice, metadataSvc ports.DeploymentMetadataService) PrerequisiteGatherer {
	return PrerequisiteGatherer{
		runStorage:  runStorage,
		credMS:      credMS,
		metadataSvc: metadataSvc,
	}
}

// Gather gathers prerequisite
func (gatherer PrerequisiteGatherer) Gather(actor types.Actor, deploymentID common.ID) (ResourceCleanupPrerequisite, error) {
	var data ResourceCleanupPrerequisite
	err := data.validateAndSetActor(actor)
	if err != nil {
		return ResourceCleanupPrerequisite{}, err
	}
	err = gatherer.gatherLastRun(deploymentID, &data)
	if err != nil {
		return ResourceCleanupPrerequisite{}, err
	}
	err = gatherer.gatherCloudCredential(data.LastRun, &data)
	if err != nil {
		return ResourceCleanupPrerequisite{}, err
	}
	err = gatherer.gatherGitCredentialIfNecessary(data.LastRun, &data)
	if err != nil {
		return ResourceCleanupPrerequisite{}, err
	}
	return data, nil
}

func (gatherer PrerequisiteGatherer) gatherLastRun(deploymentID common.ID, data *ResourceCleanupPrerequisite) error {
	lastRun, err := gatherer.getLastRun(deploymentID, data.Actor)
	if err != nil {
		return err
	}
	err = data.validateAndSetLastRun(lastRun)
	if err != nil {
		return err
	}
	return nil
}

func (gatherer PrerequisiteGatherer) getLastRun(deploymentID common.ID, actor types.Actor) (types.DeploymentRun, error) {
	deployment, err := gatherer.metadataSvc.Get(actor, deploymentID)
	if err != nil {
		return types.DeploymentRun{}, fmt.Errorf("fail to fetch deployment to find its last run, %w", err)
	}
	if deployment.LastRun == nil {
		return types.DeploymentRun{}, fmt.Errorf("deployment %s has no last run", deploymentID)
	}
	lastRun, err := gatherer.runStorage.Get(deployment.LastRun.ID)
	if err != nil {
		return types.DeploymentRun{}, err
	}
	return lastRun, nil
}

func (gatherer PrerequisiteGatherer) gatherCloudCredential(tfRun types.DeploymentRun, data *ResourceCleanupPrerequisite) error {
	credID := domainutils.FindPrimaryCloudCredential(tfRun)
	if credID == "" {
		return service.NewCacaoInvalidParameterError("credential for primary provider is missing")
	}
	actor := types.Actor{
		Actor:    tfRun.CreatedBy.User,
		Emulator: tfRun.CreatedBy.Emulator,
	}
	credential, err := gatherer.credMS.Get(actor, actor.Actor, credID)
	if err != nil {
		return err
	}
	err = data.validateAndSetCloudCredential(credential)
	if err != nil {
		return err
	}
	return nil
}

func (gatherer PrerequisiteGatherer) gatherGitCredentialIfNecessary(tfRun types.DeploymentRun, data *ResourceCleanupPrerequisite) error {
	if tfRun.GitCredential == "" {
		// skip if there is no git credential
		return nil
	}
	actor := types.Actor{
		Actor:    tfRun.CreatedBy.User,
		Emulator: tfRun.CreatedBy.Emulator,
	}
	credential, err := gatherer.credMS.Get(actor, actor.Actor, tfRun.GitCredential)
	if err != nil {
		return err
	}
	err = data.validateAndSetGitCredential(credential)
	if err != nil {
		return err
	}
	return nil
}

// StartResourceCleanup creates a workflow that will clean up the resources provisioned by the deployment.
func StartResourceCleanup(awm ports.ArgoWorkflowMediator, keySrc ports.KeySrc, data ResourceCleanupPrerequisite) (awmProvider awmclient.AWMProvider, wfName string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "resourcedeletion",
		"function": "StartResourceCleanup",
		"run":      data.LastRun.ID,
	})
	wfData, err := prepareWorkflowData(keySrc, data)
	if err != nil {
		logger.WithError(err).Error("failed to prepare workflow data")
		return "", "", err
	}

	awmProvider, wfName, err = awm.Create(
		context.TODO(),
		data.LastRun.PrimaryProvider,
		data.Actor.Actor,
		awmclient.TerraformWorkflowFilename,
		awmclient.AWMTerraformWorflowData(wfData),
	)
	if err != nil {
		logger.WithError(err).Error("fail to create workflow via AWM")
		return "", "", err
	}
	logger.WithField("wfName", wfName).Info("workflow created")
	return awmProvider, wfName, nil
}

func prepareWorkflowData(keySrc ports.KeySrc, data ResourceCleanupPrerequisite) (awmclient.TerraformWorkflowData, error) {
	var err error
	wfData := initializeWorkflowData(data)
	wfData, err = prepareAnsibleVars(data, wfData)
	if err != nil {
		return awmclient.TerraformWorkflowData{}, err
	}
	wfData, err = prepareCredentials(data, wfData, keySrc)
	if err != nil {
		return awmclient.TerraformWorkflowData{}, err
	}
	return wfData, nil
}

func initializeWorkflowData(data ResourceCleanupPrerequisite) awmclient.TerraformWorkflowData {
	return awmclient.TerraformWorkflowData{
		Username:          data.Actor.Actor,
		Deployment:        data.LastRun.Deployment,
		TemplateID:        data.LastRun.TemplateSnapshot.TemplateID,
		TerraformStateKey: tfStateKey(data.LastRun),
		GitURL:            data.LastRun.TemplateSnapshot.GitURL,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: data.LastRun.TemplateSnapshot.UpstreamTracked.Branch,
			Tag:    data.LastRun.TemplateSnapshot.UpstreamTracked.Tag,
			Commit: data.LastRun.TemplateSnapshot.CommitHash,
		},
		Path:                  data.LastRun.TemplateSnapshot.SubPath,
		CloudCredID:           "",
		AnsibleVars:           nil,
		CloudCredentialBase64: "",
		GitCredID:             "",
		GitCredentialBase64:   "",
	}
}

func prepareAnsibleVars(data ResourceCleanupPrerequisite, wfData awmclient.TerraformWorkflowData) (awmclient.TerraformWorkflowData, error) {
	ansibleVars, err := domainutils.ParamToAnsibleVars(data.LastRun.Parameters)
	if err != nil {
		return wfData, err
	}
	ansibleVars["TF_OPERATION"] = "destroy"
	wfData.AnsibleVars = ansibleVars
	return wfData, nil
}

func prepareCredentials(data ResourceCleanupPrerequisite, wfData awmclient.TerraformWorkflowData, keySrc ports.KeySrc) (awmclient.TerraformWorkflowData, error) {
	cloudCredBase64, err := domainutils.EncodeOpenstackCred(data.CloudCred, keySrc)
	if err != nil {
		return wfData, err
	}
	wfData.CloudCredID = data.CloudCred.GetID()
	wfData.CloudCredentialBase64 = cloudCredBase64
	if data.GitCred != nil {
		gitCredBase64, err := domainutils.EncodeGitCred(data.GitCred, keySrc)
		if err != nil {
			return wfData, err
		}
		wfData.GitCredID = data.GitCred.GetID()
		wfData.GitCredentialBase64 = gitCredBase64
	}
	return wfData, nil
}

func tfStateKey(tfRun types.DeploymentRun) string {
	return tfRun.Deployment.String()
}

package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// DeploymentMetadataService is a client to interact with deployment metadata service
type DeploymentMetadataService struct {
	natsConf messaging.NatsConfig
	stanConf messaging.StanConfig
}

// NewDeploymentMetadataService ...
func NewDeploymentMetadataService(stanConf types.StanConfig) DeploymentMetadataService {
	return DeploymentMetadataService{
		natsConf: stanConf.NatsConfig,
		stanConf: stanConf.StanConfig,
	}
}

// Get fetches a deployment by ID
func (svc DeploymentMetadataService) Get(actor types.Actor, id common.ID) (service.Deployment, error) {
	logger := log.WithFields(log.Fields{
		"package":    "adapters",
		"function":   "DeploymentMetadataService.Get",
		"actor":      actor.Actor,
		"emulator":   actor.Emulator,
		"deployment": id,
	})
	client := service.NewDeploymentClient(context.TODO(), actor.Actor, actor.Emulator, svc.natsConf, svc.stanConf)
	deployment, err := client.Get(id)
	if err != nil {
		logger.WithError(err).Error("fail to get deployment")
		return service.Deployment{}, err
	}
	logger.Debug("got deployment from deployment metadata ms")
	return *deployment, nil
}

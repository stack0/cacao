package adapters

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

const deploymentWorkflowCollectionName = "deploymentWorkflow"

// DeploymentWorkflowStorage is a mongodb storage adapter for deployment workflow records
type DeploymentWorkflowStorage struct {
	conf db.MongoDBConfig
	conn *db.MongoDBConnection
}

// NewDeploymentWorkflowStorage ...
func NewDeploymentWorkflowStorage(config db.MongoDBConfig) *DeploymentWorkflowStorage {
	return &DeploymentWorkflowStorage{conf: config}
}

// Init initialize the mongo connection
func (storage *DeploymentWorkflowStorage) Init() error {
	conn, err := db.NewMongoDBConnection(&storage.conf)
	if err != nil {
		return err
	}
	storage.conn = conn
	return nil
}

// CreateDeploymentPreflightWorkflow inserts a workflow record for workflow that executes the prerequisite template.
func (storage DeploymentWorkflowStorage) CreateDeploymentPreflightWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.CreateDeploymentPreflightWorkflow",
		"deployment":  deployment,
		"run":         run,
		"awmProvider": awmProvider,
		"wfName":      workflowName,
	})
	workflow := types.DeploymentWorkflow{
		Purpose:      types.PreflightPurpose,
		WorkflowName: workflowName,
		AWMProvider:  common.ID(awmProvider),
		Deployment:   deployment,
		Run:          run,
	}
	return storage.createWorkflow(logger, workflow)
}

// CreateDeploymentExecWorkflow inserts a workflow record for workflow that executes the execution template.
func (storage DeploymentWorkflowStorage) CreateDeploymentExecWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.CreateDeploymentExecWorkflow",
		"deployment":  deployment,
		"run":         run,
		"awmProvider": awmProvider,
		"wfName":      workflowName,
	})
	workflow := types.DeploymentWorkflow{
		Purpose:      types.ExecutionPurpose,
		WorkflowName: workflowName,
		AWMProvider:  common.ID(awmProvider),
		Deployment:   deployment,
		Run:          run,
	}
	return storage.createWorkflow(logger, workflow)
}

// CreateDeploymentDeletionWorkflow inserts a workflow record for workflow that deletes deployment resource.
func (storage DeploymentWorkflowStorage) CreateDeploymentDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.CreateDeploymentDeletionWorkflow",
		"deployment":  deployment,
		"awmProvider": awmProvider,
		"wfName":      workflowName,
	})
	workflow := types.DeploymentWorkflow{
		Purpose:      types.DeletionPurpose,
		WorkflowName: workflowName,
		AWMProvider:  common.ID(awmProvider),
		Deployment:   deployment,
	}
	return storage.createWorkflow(logger, workflow)
}

// CreatePrerequisiteDeletionWorkflow inserts a workflow record for workflow that deletes prerequisite (resource provisioned for prerequisite template).
func (storage DeploymentWorkflowStorage) CreatePrerequisiteDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.CreatePrerequisiteDeletionWorkflow",
		"deployment":  deployment,
		"awmProvider": awmProvider,
		"wfName":      workflowName,
	})
	workflow := types.DeploymentWorkflow{
		Purpose:      types.PrerequisiteDeletionPurpose,
		WorkflowName: workflowName,
		AWMProvider:  common.ID(awmProvider),
		Deployment:   deployment,
	}
	return storage.createWorkflow(logger, workflow)
}

func (storage DeploymentWorkflowStorage) createWorkflow(logger *log.Entry, workflow types.DeploymentWorkflow) error {
	err := storage.conn.Insert(deploymentWorkflowCollectionName, workflow)
	if err != nil {
		logger.WithError(err).Error("fail to insert deployment workflow to mongodb")
		return err
	}
	return err
}

// Search searches for a workflow record.
func (storage DeploymentWorkflowStorage) Search(provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error) {
	filter := map[string]interface{}{"wf_name": workflowName, "provider": provider}
	var workflow types.DeploymentWorkflow
	err := storage.conn.Get(deploymentWorkflowCollectionName, filter, &workflow)
	if err != nil {
		return types.DeploymentWorkflow{}, err
	}
	return workflow, nil
}

// WorkflowSucceededAt update the workflow record to succeeded
func (storage DeploymentWorkflowStorage) WorkflowSucceededAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.WorkflowSucceededAt",
		"awmProvider": provider,
		"wfName":      workflowName,
	})
	return storage.workflowFinishedAt(logger, provider, workflowName, "Succeeded", endsAt)
}

// WorkflowFailedAt update the workflow record to failed
func (storage DeploymentWorkflowStorage) WorkflowFailedAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error {
	logger := log.WithFields(log.Fields{
		"package":     "adapters",
		"function":    "DeploymentWorkflowStorage.WorkflowFailedAt",
		"awmProvider": provider,
		"wfName":      workflowName,
	})
	return storage.workflowFinishedAt(logger, provider, workflowName, "", endsAt)
}

func (storage DeploymentWorkflowStorage) workflowFinishedAt(logger *log.Entry, provider awmclient.AWMProvider, workflowName string, workflowStatus string, endsAt time.Time) error {
	filter := map[string]interface{}{"wf_name": workflowName, "provider": provider}
	updates := bson.M{"$set": bson.M{"status": workflowStatus, "ended_at": endsAt}}
	updated, err := storage.conn.Update(deploymentWorkflowCollectionName, filter, updates)
	if err != nil {
		logger.WithError(err).Error("fail to update the status of workflow in mongodb")
		return err
	}
	if !updated {
		// this can happen if workflow is created manually
		errMsg := fmt.Sprintf("workflow %s (%s) not found", workflowName, provider)
		err = service.NewCacaoNotFoundError(errMsg)
		logger.WithError(err).Error("workflow not found in mongodb")
		return service.NewCacaoNotFoundError(errMsg)
	}
	return nil
}

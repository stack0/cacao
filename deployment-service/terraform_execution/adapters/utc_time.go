package adapters

import "time"

// UTCTimeSrc is a source of UTC time
type UTCTimeSrc struct {
}

// Now ...
func (t UTCTimeSrc) Now() time.Time {
	return time.Now().UTC()
}

package adapters

import (
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// Event implements types.IncomingEvent & types.OutgoingEvent
type Event cloudevents.Event

// EventType returns the event type
func (e Event) EventType() common.EventType {
	return common.EventType(cloudevents.Event(e).Type())
}

// Transaction returns the transaction ID
func (e Event) Transaction() common.TransactionID {
	return messaging.GetTransactionID((*cloudevents.Event)(&e))
}

// CloudEvent returns the event as a cloudevent
func (e Event) CloudEvent() cloudevents.Event {
	return cloudevents.Event(e)
}

// ToCloudEvent returns the event as a cloudevent with the specified source
func (e Event) ToCloudEvent(source string) (cloudevents.Event, error) {
	ce := cloudevents.Event(e)
	ce.SetSource(source)
	return ce, nil
}

// StanAdapter is an adapter for STAN that does both publishing and subscription
type StanAdapter struct {
	sc        stan.Conn
	sub       stan.Subscription
	conf      types.StanConfig
	eventChan chan<- types.IncomingEvent
}

// NewStanAdapter ...
func NewStanAdapter() *StanAdapter {
	return &StanAdapter{}
}

// Init establish a STAN connection
func (src *StanAdapter) Init(conf types.StanConfig) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Init",
	})
	// skip init if connection is already established
	if src.sc != nil {
		return nil
	}
	src.generateClientIDIfNotSet(&conf)
	sc, err := stan.Connect(conf.ClusterID, conf.ClientID, stan.NatsURL(conf.URL))
	if err != nil {
		return err
	}
	src.sc = sc
	src.conf = conf
	logger.Info("Connect to STAN")
	return nil
}

func (src StanAdapter) generateClientIDIfNotSet(conf *types.StanConfig) {
	if conf.ClientID == "" {
		conf.ClientID = "deploymentexec" + xid.New().String()
	}
}

// InitChannel sets the channel to forward received events to
func (src *StanAdapter) InitChannel(chann chan<- types.IncomingEvent) {
	src.eventChan = chann
}

// Start starts STAN subscription and start receiving events
func (src *StanAdapter) Start() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Start",
	})
	handler := func(m *stan.Msg) {
		ce, err := messaging.ConvertStan(m)
		if err != nil {
			logger.WithError(err).Error("fail to convert to cloudevent")
			return
		}
		if messaging.GetTransactionID(&ce) == "" {
			logger.WithField("evenType", ce.Type()).Warn("incoming event has no transaction ID")
		}
		src.eventChan <- Event(ce)
	}
	sub, err := src.sc.QueueSubscribe(common.EventsSubject, src.conf.QueueGroup, handler, stan.DurableName(src.conf.DurableName))
	if err != nil {
		return err
	}
	src.sub = sub
	logger.Info("Subscribed to STAN")
	return nil
}

// Publish publish an event to STAN
func (src *StanAdapter) Publish(event types.OutgoingEvent) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Publish",
	})
	logger.Trace()
	ce, err := event.ToCloudEvent(src.conf.ClientID)
	if err != nil {
		logger.WithError(err).Error("fail to convert event to cloudevent")
		return err
	}
	payload, err := ce.MarshalJSON()
	if err != nil {
		return err
	}
	err = src.sc.Publish(common.EventsSubject, payload)
	if err != nil {
		return err
	}
	logger.WithFields(log.Fields{
		"eventType":   event.EventType(),
		"transaction": event.Transaction(),
	}).Info("event published")
	return nil
}

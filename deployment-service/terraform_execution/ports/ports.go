package ports

import (
	"context"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// EventSink is a sink to publish events into
type EventSink interface {
	Init(types.StanConfig) error
	Publish(types.OutgoingEvent) error
}

// EventSrc is a sink to publish events into
type EventSrc interface {
	Init(types.StanConfig) error
	InitChannel(chan<- types.IncomingEvent)
	Start() error
}

// TFRunStorage is storage for Terraform Run
type TFRunStorage interface {
	Init() error
	Get(runID common.ID) (types.DeploymentRun, error)
	Create(run types.DeploymentRun) error
	Update(runID common.ID, update types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (updated bool, err error)
	Search(filter types.DeploymentRunFilter) ([]types.DeploymentRun, error)
}

// DeploymentWorkflowStorage is storage for DeploymentWorkflow
type DeploymentWorkflowStorage interface {
	Init() error
	CreateDeploymentPreflightWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreateDeploymentExecWorkflow(deployment common.ID, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreateDeploymentDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreatePrerequisiteDeletionWorkflow(deployment common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	Search(provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error)
	WorkflowSucceededAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error
	WorkflowFailedAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error
}

// DeploymentMetadataService ...
type DeploymentMetadataService interface {
	Get(actor types.Actor, id common.ID) (service.Deployment, error)
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Workspace, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Template, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Provider, error)
	List(actor types.Actor) ([]service.Provider, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor types.Actor, owner, id string) (service.Credential, error)
	// List return a list of credential ID
	List(actor types.Actor, owner string) ([]service.Credential, error)
}

// ArgoWorkflowMediator is an interface to interact with ArgoWorkflowMediator
type ArgoWorkflowMediator interface {
	Create(ctx context.Context, provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) (awmProvider awmclient.AWMProvider, wfName string, err error)
	CreateAsync(provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) error
}

// KeySrc is an way to get encryption key
type KeySrc interface {
	GetKey() []byte
}

// SSHKeySrc is a way to get public ssh key to use to inject into users' VMs.
type SSHKeySrc interface {
	GetPublicSSHKey() string
}

// TimeSrc is a source of current time
type TimeSrc interface {
	Now() time.Time
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	EventSink       EventSink
	TFRunStorage    TFRunStorage
	WorkflowStorage DeploymentWorkflowStorage
	KeySrc          KeySrc
	TimeSrc         TimeSrc
	SSHKeySrc       SSHKeySrc

	MetadataSvc  DeploymentMetadataService
	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
	AWM          ArgoWorkflowMediator
}

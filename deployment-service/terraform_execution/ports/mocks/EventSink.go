// Code generated by mockery (devel). DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"

	types "gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// EventSink is an autogenerated mock type for the EventSink type
type EventSink struct {
	mock.Mock
}

// Init provides a mock function with given fields: _a0
func (_m *EventSink) Init(_a0 types.StanConfig) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(types.StanConfig) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Publish provides a mock function with given fields: _a0
func (_m *EventSink) Publish(_a0 types.OutgoingEvent) error {
	ret := _m.Called(_a0)

	var r0 error
	if rf, ok := ret.Get(0).(func(types.OutgoingEvent) error); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
)

var registryEntryName string

var tests07TCFRegistry = []testMap{
	{"CreateTCFRegistryEntry": testCreateTCFRegistryEntry},
	{"ListTCFRegistryEntries": testListTCFRegistryEntries},
	{"GetTCFRegistryEntry": testGetTCFRegistryEntry},
	{"UpdateTCFRegistryEntry": testUpdateTCFRegistryEntry},
	{"PatchTCFRegistryEntry": testPatchTCFRegistryEntry},
	{"DeleteTCFRegistryEntry": testDeleteTCFRegistryEntry},
}

func testCreateTCFRegistryEntry(t *testing.T) {
	registryEntryName = "test_tcf_name1"

	data := fmt.Sprintf(`{"name":"%s","description":"test_tcf_entry","type":"rest_api_endpoint","query_target":"/providers/{provider_id}/flavors"}`, registryEntryName)

	req, err := newRequest("POST", "/tcfregistry", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testGetTCFRegistryEntry(t *testing.T) {
	req, err := newRequest("GET", "/tcfregistry/"+registryEntryName, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TCFRegistryEntry
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, registryEntryName, result.Name)
}

func testListTCFRegistryEntries(t *testing.T) {
	req, err := newRequest("GET", "/tcfregistry", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_http.TCFRegistryEntry
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testUpdateTCFRegistryEntry(t *testing.T) {
	// update
	data := `{"description":"test_tcf_entry_updated","query_target":"/providers/{provider_id}/flavors_updated","query_data":"data_updated"}`

	reqUpdate, err := newRequest("PUT", "/tcfregistry/"+registryEntryName, data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/tcfregistry/"+registryEntryName, "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TCFRegistryEntry
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, registryEntryName, result.Name)
	assert.Equal(t, "test_tcf_entry_updated", result.Description)
	assert.Equal(t, "/providers/{provider_id}/flavors_updated", string(result.QueryTarget))
	assert.Equal(t, "data_updated", string(result.QueryData))
}

func testPatchTCFRegistryEntry(t *testing.T) {
	// patch
	data := `{"description":"test_tcf_entry_patched","query_target":"/providers/{provider_id}/flavors_patched","query_data":""}`

	reqUpdate, err := newRequest("PATCH", "/tcfregistry/"+registryEntryName, data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/tcfregistry/"+registryEntryName, "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.TCFRegistryEntry
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, registryEntryName, result.Name)
	assert.Equal(t, "test_tcf_entry_patched", result.Description)
	assert.Equal(t, "/providers/{provider_id}/flavors_patched", result.QueryTarget)
	assert.Equal(t, "", result.QueryData)
}

func testDeleteTCFRegistryEntry(t *testing.T) {
	req, err := newRequest("DELETE", "/tcfregistry/"+registryEntryName, "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

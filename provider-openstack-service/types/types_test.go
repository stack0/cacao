package types

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOpenStackCatalogEntry_unmarshal(t *testing.T) {
	var input = []byte(`{
  "endpoints": [
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "public",
      "region_id": "Region1-ID",
      "url": "https://cyverse.org",
      "region": "Region1"
    },
    {
      "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
      "interface": "public",
      "region_id": "Region2-ID",
      "url": "https://cyverse.org",
      "region": "Region2"
    },
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "admin",
      "region_id": "Region1-ID",
      "url": "https://cyverse.org",
      "region": "Region1"
    }
  ],
  "id": "ffffffffffffffffffffffffffffffff",
  "name": "nova",
  "type": "compute"
}`)
	var entry OpenStackCatalogEntry
	err := json.Unmarshal(input, &entry)
	assert.NoError(t, err)
	assert.Equal(t, OpenStackCatalogEntry{
		Name: "nova",
		Type: "compute",
		Endpoints: []OpenStackCatalogEndpoint{
			{
				ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				Interface: "public",
				RegionID:  "Region1-ID",
				URL:       "https://cyverse.org",
				Region:    "Region1",
			},
			{
				ID:        "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
				Interface: "public",
				RegionID:  "Region2-ID",
				URL:       "https://cyverse.org",
				Region:    "Region2",
			},
			{
				ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				Interface: "admin",
				RegionID:  "Region1-ID",
				URL:       "https://cyverse.org",
				Region:    "Region1",
			},
		},
	}, entry)
}

func TestOpenStackCatalogEntry_unmarshal_list(t *testing.T) {
	// For catalog list operation, the field names at root level start with a capitalized character.
	var input = []byte(`[
  {
    "Name": "keystone",
    "Type": "identity",
    "Endpoints": [
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "public",
        "region_id": "Region1-ID",
        "url": "https://foobar.cyverse.org",
        "region": "Region1"
      }
    ]
  },
  {
    "Name": "nova",
    "Type": "compute",
    "Endpoints": [
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "public",
        "region_id": "Region1-ID",
        "url": "https://foo.cyverse.org",
        "region": "Region1"
      },
      {
        "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        "interface": "admin",
        "region_id": "Region1-ID",
        "url": "https://bar.cyverse.org",
        "region": "Region1"
      }
    ]
  }
]`)
	var list []OpenStackCatalogEntry
	err := json.Unmarshal(input, &list)
	assert.NoError(t, err)
	if !assert.NotNil(t, list) {
		return
	}
	if !assert.Len(t, list, 2) {
		return
	}
	assert.ElementsMatch(t, []OpenStackCatalogEntry{
		{
			Name: "keystone",
			Type: "identity",
			Endpoints: []OpenStackCatalogEndpoint{
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "public",
					RegionID:  "Region1-ID",
					URL:       "https://foobar.cyverse.org",
					Region:    "Region1",
				},
			},
		},
		{
			Name: "nova",
			Type: "compute",
			Endpoints: []OpenStackCatalogEndpoint{
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "public",
					RegionID:  "Region1-ID",
					URL:       "https://foo.cyverse.org",
					Region:    "Region1",
				},
				{
					ID:        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
					Interface: "admin",
					RegionID:  "Region1-ID",
					URL:       "https://bar.cyverse.org",
					Region:    "Region1",
				},
			},
		},
	}, list)
}

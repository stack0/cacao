package types

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"reflect"
)

// the context value key type should be unexported to avoid collision.
type key int

// the context value key constants should be unexported to avoid collision.
const (
	// usernameKey is what you should use as a key for the username in context.Context.
	usernameKey key = iota

	// credentialsIDKey is what you should use as a key for the credentials ID in context.Context.
	credentialsIDKey

	// transactionIDKey is the key for the transaction ID in context.Context.
	transactionIDKey

	// environmentKey is the key for the environment variables stored in context.Context.
	environmentKey
)

// WithCredential inject credential ID and environment into context
func WithCredential(parent context.Context, credentials *Credential) context.Context {
	ctx := context.WithValue(parent, credentialsIDKey, credentials.ID)
	return WithEnvironment(ctx, credentials.OpenStackEnv)
}

// WithCredentialID inject credential ID into context
func WithCredentialID(parent context.Context, credentialID string) context.Context {
	return context.WithValue(parent, credentialsIDKey, credentialID)
}

// WithEnvironment inject environment into context
func WithEnvironment(parent context.Context, env Environment) context.Context {
	var ctx = parent
	logrus.Info(env) // TODO temporary print to help with debugging, leave for a while
	ctx = context.WithValue(ctx, environmentKey, env)
	return ctx
}

// WithUsername inject username into context
func WithUsername(parent context.Context, username string) context.Context {
	return context.WithValue(parent, usernameKey, username)
}

// WithTransactionID inject transaction ID into context
func WithTransactionID(parent context.Context, tid common.TransactionID) context.Context {
	return context.WithValue(parent, transactionIDKey, tid)
}

// CredentialIDFromCtx extracts credential ID from context, return "" if key not exists or value not string
func CredentialIDFromCtx(ctx context.Context) string {
	var credID string
	credIDRaw := ctx.Value(credentialsIDKey)
	if credIDRaw != nil {
		var ok bool
		credID, ok = credIDRaw.(string)
		if !ok {
			credID = ""
		}
	}
	return credID
}

// EnvironmentFromCtx extracts environment from context, return nil if key not exists or value is wrong type
func EnvironmentFromCtx(ctx context.Context) Environment {
	var env Environment
	envRaw := ctx.Value(environmentKey)
	if envRaw != nil {
		var ok bool
		env, ok = envRaw.(Environment)
		if !ok {
			env = nil
		}
	}
	return env
}

// EnvironmentFromCtxWithErr extracts environment from context, return error if key not exists or value is wrong type
func EnvironmentFromCtxWithErr(ctx context.Context) (Environment, error) {
	envRaw := ctx.Value(environmentKey)
	if envRaw == nil {
		return nil, fmt.Errorf("environment missing from context values")
	}
	env, ok := envRaw.(Environment)
	if !ok {
		return nil, fmt.Errorf("environment in context values has wrong type, %s", reflect.TypeOf(envRaw))
	}
	return env, nil
}

// UsernameFromCtx extracts username from context, return "' if key not exists or value not string
func UsernameFromCtx(ctx context.Context) string {
	usernameRaw := ctx.Value(usernameKey)
	if usernameRaw == nil {
		return ""
	}
	username, ok := usernameRaw.(string)
	if !ok {
		return ""
	}
	return username
}

// TransactionIDFromCtx extracts transaction ID from context, return "' if key not exists or value is wrong type
func TransactionIDFromCtx(ctx context.Context) common.TransactionID {
	tidRaw := ctx.Value(transactionIDKey)
	if tidRaw == nil {
		return ""
	}
	tid, ok := tidRaw.(common.TransactionID)
	if !ok {
		return ""
	}
	return tid
}

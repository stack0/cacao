package types

import (
	"context"
	"errors"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.com/cyverse/cacao-common/messaging"
)

// CachePrefix is a string prefix for cache keys, which is also used as a cache tag for invalidation.
type CachePrefix string

const (
	// CachePrefixListApplicationCredentials is the prefix on the cache key for the results of a
	// user requesting a list of ApplicationCredentials.
	CachePrefixListApplicationCredentials CachePrefix = "ListApplicationCredentials"

	// CachePrefixGetApplicationCredential is the prefix on the cache key for the results of a
	// user requesting a single ApplicationCredential.
	CachePrefixGetApplicationCredential CachePrefix = "GetApplicationCredential"

	// CachePrefixListFlavors is the prefix on the cache key for the results of a
	// user requesting a list of flavors.
	CachePrefixListFlavors CachePrefix = "ListFlavors"

	// CachePrefixGetFlavor is the prefix on the cache key for the results of a
	// user requesting a single flavor.
	CachePrefixGetFlavor CachePrefix = "GetFlavor"

	// CachePrefixListImages is the prefix on the cache key for the results of a
	// user requesting a list of images.
	CachePrefixListImages CachePrefix = "ListImages"

	// CachePrefixGetImage is the rpefix on the cache key for the results of a
	// user requesting a single image.
	CachePrefixGetImage CachePrefix = "GetImage"

	// CachePrefixListProjects is the prefix on the cache key for the results of a
	// user requesting a list of Projects.
	CachePrefixListProjects CachePrefix = "ListProjects"

	// CachePrefixGetProject is the rpefix on the cache key for the results of a
	// user requesting a single Project.
	CachePrefixGetProject CachePrefix = "GetProject"

	// CachePrefixListRegions is the prefix on the cache key for the results of a
	// user requesting a list of Regions.
	CachePrefixListRegions CachePrefix = "ListRegions"

	// CachePrefixListCatalog is the prefix on the cache key for the results of a
	// user requesting a list of Catalog.
	CachePrefixListCatalog CachePrefix = "ListCatalog"
)

// Configuration contains the settings needed to configure this microservice.
type Configuration struct {
	NATS                     messaging.NatsConfig
	STAN                     messaging.StanConfig
	DefaultChannelBufferSize int    `envconfig:"DEFAULT_CHANNEL_BUFFER_SIZE" default:"1"`
	QueryWorkerCount         int    `envconfig:"QUERY_WORKER_COUNT" default:"10"`
	PodName                  string `envconfig:"POD_NAME"`
	LogLevel                 string `envconfig:"LOG_LEVEL" default:"debug"`
	RedisAddress             string `envconfig:"REDIS_ADDRESS" default:"redis:6379"`
	RedisPassword            string `envconfig:"REDIS_PASSWORD" default:""`
	RedisDB                  string `envConfig:"REDIS_DB" default:"0"`
	// expiration period for cache entry in seconds, default to 1 hour
	CacheTTL int `envConfig:"CACHE_TTL" default:"3600"`
}

// Validate returns an error if there's something wrong with the configuration.
func (c *Configuration) Validate() error {
	if c.NATS.ClientID == "" {
		return errors.New("NATS_CLIENT_ID environment variable must be set")
	}

	if c.NATS.QueueGroup == "" {
		return errors.New("NATS_QGROUP environment variable must be set")
	}

	if c.QueryWorkerCount <= 0 {
		return errors.New("QUERY_WORKER_COUNT cannot be zero or negative")
	}

	// if c.NATS.NatsDurableName == "" {
	// 	return errors.New("NATS_DURABLE_NAME environment variable must be set")
	// }

	return nil
}

// QueryReply defines a type that can send a response of some sort.
type QueryReply interface {
	Reply(cloudevents.Event) error
}

// CloudEventRequest is what gets comes in as a request
type CloudEventRequest struct {
	CloudEvent event.Event
	Replyer    QueryReply
}

// OpenStackGetter represents a function that calls `openstack show` via
// os/exec.CommandContext and returns the output.
type OpenStackGetter func(context.Context, Environment, string) ([]byte, error)

// Credential ...
type Credential struct {
	ID           string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	OpenStackEnv Environment
}

// Environment is environment variables represented in a key-value pairs fashion.
// This is used to carry OpenStack credential info and pass to OpenStack CLI.
type Environment map[string]string

// OpenStackCatalogEntry is an entry in the catalog, basically endpoints for an openstack service.
// TODO this duplicate with types in cacao-common/service/providers
type OpenStackCatalogEntry struct {
	Name      string                     `json:"name"` // name of the service, e.g. "nova"
	Type      string                     `json:"type"` // service type, e.g. "compute"
	Endpoints []OpenStackCatalogEndpoint `json:"endpoints"`
}

// OpenStackCatalogEndpoint is an REST endpoint for a service
// TODO this duplicate with types in cacao-common/service/providers
type OpenStackCatalogEndpoint struct {
	ID        string `json:"id"`
	Interface string `json:"interface"` // type of interface, e.g. "public", "admin"
	RegionID  string `json:"region_id"`
	URL       string `json:"url"`
	Region    string `json:"region"`
}

// ApplicationCredential is the structure returned by cli when creating an application credential
type ApplicationCredential struct {
	Description string    `json:"description"`
	ExpiresAt   time.Time `json:"expires_at"`
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	ProjectID   string    `json:"project_id"`
	Roles       string    `json:"roles"`
	Secret      string    `json:"secret"`
	//System       interface{} `json:"system"`
	Unrestricted bool   `json:"unrestricted"`
	UserID       string `json:"user_id"`
}

var (
	// ErrApplicationCredentialNotFound HTTP 404, happens when the application credential is not found, e.g. the application credential ID is not found.
	ErrApplicationCredentialNotFound = errors.New("could not find application credential")
	// ErrAuthentication is HTTP 401, happens when the credential is bad.
	// Note: if application credential ID does NOT exist, ErrApplicationCredentialNotFound will be used instead.
	ErrAuthentication = errors.New("the request you have made requires authentication")
)

package domain

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/eventhandlers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/queryhandlers"

	log "github.com/sirupsen/logrus"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// Domain define the functions that a Domain type must support.
type Domain interface {
	Init(c types.Configuration)
	Start()
}

// OpenStackDomain is a domain that lists images/flavors from OpenStack and writes the responses out
type OpenStackDomain struct {
	config     *types.Configuration
	EventIn    ports.IncomingEventPort
	QueryIn    ports.IncomingQueryPort
	EventOut   ports.OutgoingEventPort
	OpenStack  ports.OpenStack
	CredMS     ports.CredentialMS
	ProviderMS ports.ProviderMetadataMS
	TimeSrc    ports.TimeSrc
}

// NewOpenStackDomain returns a new *OpenStackDomain. Init is called
// on it as part of this function.
func NewOpenStackDomain(c *types.Configuration, eventIn ports.IncomingEventPort, queryIn ports.IncomingQueryPort, eventOut ports.OutgoingEventPort, openstack *adapters.OpenStackAdapter, credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS, timeSrc ports.TimeSrc) *OpenStackDomain {
	retval := &OpenStackDomain{
		config:     c,
		EventIn:    eventIn,
		QueryIn:    queryIn,
		EventOut:   eventOut,
		OpenStack:  openstack,
		CredMS:     credMS,
		ProviderMS: providerMS,
		TimeSrc:    timeSrc,
	}
	return retval
}

// Start fires up the domain object, allowing it to listen for incoming queries.
func (domain *OpenStackDomain) Start() {
	var wg sync.WaitGroup
	domain.startQueryWorker(&wg)
	domain.startEventWorker(&wg)

	wg.Wait()
	domain.EventIn.Finalize()
}

func (domain *OpenStackDomain) startQueryWorker(wg *sync.WaitGroup) {
	incomingQueryChan := make(chan types.CloudEventRequest, domain.config.DefaultChannelBufferSize)
	domain.QueryIn.InitChannel(incomingQueryChan)
	go domain.QueryIn.Start()

	for i := 0; i < domain.config.QueryWorkerCount; i++ {
		wg.Add(1)
		go domain.processQueryWorker(incomingQueryChan, wg)
	}
}

// processQueryWorker performs the listing and writes out the response on the ResponsePort.
func (domain *OpenStackDomain) processQueryWorker(cloudEventReqChan chan types.CloudEventRequest, wg *sync.WaitGroup) {
	defer wg.Done()

	origin := context.Background()

	for cloudEventReq := range cloudEventReqChan {
		ctx, cancel := context.WithTimeout(origin, time.Second*60)
		domain.processQuery(ctx, cloudEventReq)
		cancel()
	}
}

func (domain *OpenStackDomain) processQuery(ctx context.Context, cloudEventReq types.CloudEventRequest) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "OpenStackDomain.processQuery",
	})
	request, err := domain.parseRequest(cloudEventReq.CloudEvent)
	if err != nil {
		logger.WithError(err).Error("fail to parse request")
		return
	}
	ctx = types.WithUsername(ctx, request.SessionActor)
	ctx = types.WithTransactionID(ctx, messaging.GetTransactionID(&cloudEventReq.CloudEvent))
	handler := domain.selectQueryHandler(request)
	if handler == nil {
		logger.WithField("op", request.Operation).Error("no handler selected for query")
		return
	}
	response := handler.Handle(ctx, request)
	replyCe, err := response.ToCloudEvent("openstack-provider-service", providers.OpenStackQueryPrefix)
	if err != nil {
		logger.WithError(err).Error("fail to convert reply to cloudevent")
		return
	}
	if err = cloudEventReq.Replyer.Reply(replyCe); err != nil {
		logger.WithError(err).Error("fail to send reply")
	}
}

func (domain *OpenStackDomain) parseRequest(ce cloudevents.Event) (providers.ProviderRequest, error) {
	var req providers.ProviderRequest
	err := json.Unmarshal(ce.Data(), &req)
	if err != nil {
		return providers.ProviderRequest{}, err
	}
	return req, nil
}

// selects a handler based on the operation
func (domain *OpenStackDomain) selectQueryHandler(request providers.ProviderRequest) queryhandlers.Handler {
	credFac := credsrc.NewCredentialFactory(domain.CredMS, domain.ProviderMS)
	switch request.Operation {
	case providers.ApplicationCredentialsGetOp:
		return queryhandlers.ApplicationCredentialGetHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.ApplicationCredentialsListOp:
		return queryhandlers.ApplicationCredentialListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.AuthenticationTestOp:
		return queryhandlers.AuthenticationTestHandler{OpenStack: domain.OpenStack}
	case providers.RegionsListOp:
		return queryhandlers.RegionListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.ImagesGetOp:
		return queryhandlers.ImageGetHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.ImagesListOp:
		return queryhandlers.ImageListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.FlavorsGetOp:
		return queryhandlers.FlavorGetHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.FlavorsListOp:
		return queryhandlers.FlavorListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.ProjectsGetOp:
		return queryhandlers.ProjectGetHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.ProjectsListOp:
		return queryhandlers.ProjectListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.CatalogListOp:
		return queryhandlers.CatalogListHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	case providers.TokenGetOp:
		return queryhandlers.TokenGetHandler{OpenStack: domain.OpenStack, CredFac: credFac}
	// Force the cache to get filled in response to the cache.populate operation.
	// Remember, the user is set in the environment variables passed along in the
	// credentials, so the username will not appear in the args here.
	// case providers.CachePopulateOp:
	// 	return CachePopulateHandler{domain.OpenStack, credSelector}
	default:
		return nil
	}
}

func (domain *OpenStackDomain) startEventWorker(wg *sync.WaitGroup) {
	domain.EventIn.SetHandlers(domain.getEventHandlers())

	wg.Add(1)
	go func() {
		defer wg.Done()
		domain.EventIn.Start()
	}()
}

func (domain *OpenStackDomain) getEventHandlers() []messaging.StreamingEventHandlerMapping {
	userLoginHandler := eventhandlers.NewUserLoginHandler(domain.OpenStack, domain.CredMS)
	appCredCreationHandler := eventhandlers.NewAppCredCreationHandler(domain.OpenStack, domain.CredMS, domain.ProviderMS, domain.TimeSrc, domain.EventOut)
	appCredDeletionHandler := eventhandlers.NewAppCredDeletionHandler(domain.OpenStack, domain.ProviderMS, domain.CredMS, domain.EventOut)
	return []messaging.StreamingEventHandlerMapping{
		{
			Subject:      service.EventUserLogin,
			EventHandler: userLoginHandler.Handle,
		},
		{
			Subject:      providers.EventApplicationCredentialsCreationRequested,
			EventHandler: appCredCreationHandler.Handle,
		},
		{
			Subject:      providers.EventApplicationCredentialsDeletionRequested,
			EventHandler: appCredDeletionHandler.Handle,
		},
	}
}

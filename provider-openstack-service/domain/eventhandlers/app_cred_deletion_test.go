package eventhandlers

import (
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	credsrcmocks "gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc/mocks"
	portsmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

func matchDeleteAppCredCloudEventResponse(t *testing.T, expectedCe *cloudevents.Event, expectedResp providers.DeleteApplicationCredentialResponse) interface{} {
	return mock.MatchedBy(
		func(ce *cloudevents.Event) bool {
			if !assert.Equal(t, expectedCe.Type(), ce.Type()) {
				return false
			}
			if !assert.Equal(t, messaging.GetTransactionID(expectedCe), messaging.GetTransactionID(ce)) {
				return false
			}
			var resp providers.DeleteApplicationCredentialResponse
			err := json.Unmarshal(ce.Data(), &resp)
			if !assert.NoError(t, err) {
				return false
			}
			if !assert.Equalf(t, expectedResp, resp, "providers.DeleteApplicationCredentialResponse is different from expected") {
				return false
			}
			return true
		},
	)
}

var testDeleteAppCredRequest = providers.ProviderRequest{
	Session: service.Session{
		SessionActor:    "actor-123",
		SessionEmulator: "emulator-123",
	},
	Operation: "",
	Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
	Credential: providers.ProviderCredential{
		Type: providers.ProviderCredentialID,
		Data: "cred-id-123",
	},
	Args: nil,
}

var testDeleteAppCredEnv = map[string]string{
	"OS_AUTH_TYPE":                     "v3applicationcredential",
	"OS_AUTH_URL":                      "https://cyverse.org",
	"OS_IDENTITY_API_VERSION":          "3",
	"OS_REGION_NAME":                   "region-123",
	"OS_INTERFACE":                     "public",
	"OS_APPLICATION_CREDENTIAL_ID":     "app-cred-id-delete-123",
	"OS_APPLICATION_CREDENTIAL_SECRET": "app-cred-secret-123",
}

func TestAppCredDeletionHandler_Handle(t *testing.T) {
	// some objects used in multiple places in a single test case. this struct provides a place to define them in one place,
	// rather than copy and paste the same definition.
	type sharedObjs struct {
		request providers.ProviderRequest
	}
	type args struct {
		subject  common.EventType
		tid      common.TransactionID
		jsonData func(objs sharedObjs) []byte
	}
	type fields struct {
		credFac   func(sharedObjs) *credsrcmocks.CredentialFactory
		openStack func(*testing.T) *portsmocks.OpenStack
		eventOut  func(*testing.T, args) *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name       string
		sharedObjs sharedObjs
		fields     fields
		args       args
		wantErr    bool
	}{
		{
			name: "success",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(nil)
					openStack.On("DeleteApplicationCredential", contextWithCred(t), testDeleteAppCredEnv["OS_APPLICATION_CREDENTIAL_ID"]).Return(nil)
					openStack.On("ListApplicationCredentialsForceable", contextWithCred(t), true).Return([]providers.ApplicationCredential{}, nil)
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleted),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "success app cred ID not exists",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(types.ErrApplicationCredentialNotFound)
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleted),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "fail to parse request - bad json",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{},
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					return &credsrcmocks.CredentialFactory{}
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					return &portsmocks.OutgoingEventPort{}
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					return []byte{'{'} // bad json
				},
			},
			wantErr: true,
		},
		{
			name: "fail to parse request - no actor",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "",
						SessionEmulator: "emulator-123",
					},
					Provider: "provider-aaaaaaaaaaaaaaaaaaaa",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-delete-123",
					},
					Args: nil,
				},
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					return &credsrcmocks.CredentialFactory{}
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					return &portsmocks.OutgoingEventPort{}
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: true,
		},
		{
			name: "bad credential type - token",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor-123",
						SessionEmulator: "emulator-123",
					},
					Provider: "provider-aaaaaaaaaaaaaaaaaaaa",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "token-123",
							Scope: providers.ProjectScope{},
						},
					},
					Args: "bad args",
				},
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					return &credsrcmocks.CredentialFactory{}
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoInvalidParameterError("this operation requires credential ID").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: "",
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "fail to get credential",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(nil, fmt.Errorf("failed"))
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "credential not app cred",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:        objs.request.Credential.Data.(string),
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						OpenStackEnv: map[string]string{
							"OS_AUTH_TYPE":            "v3token",
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "public",
							"OS_TOKEN":                "token-123",
						},
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    providers.NewNotApplicationCredentialError(testDeleteAppCredRequest.Credential.Data.(string), "bad application credential, OS_AUTH_TYPE not v3applicationcredential").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "credential missing key",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:        objs.request.Credential.Data.(string),
						CreatedAt: time.Time{},
						UpdatedAt: time.Time{},
						OpenStackEnv: map[string]string{
							"OS_AUTH_TYPE":            "v3applicationcredential",
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "public",
							//"OS_APPLICATION_CREDENTIAL_ID":     "app-cred-id-delete-123", // missing OS_APPLICATION_CREDENTIAL_ID
							"OS_APPLICATION_CREDENTIAL_SECRET": "app-cred-secret-123",
						},
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					return &portsmocks.OpenStack{}
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    providers.NewNotApplicationCredentialError(testDeleteAppCredRequest.Credential.Data.(string), "bad application credential, missing OS_APPLICATION_CREDENTIAL_ID or OS_APPLICATION_CREDENTIAL_NAME").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "auth failed - auth error",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(types.ErrAuthentication)
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoInvalidParameterError(types.ErrAuthentication.Error()).GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "auth failed - other error",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(fmt.Errorf("failed"))
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "fail to delete app cred",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(nil)
					openStack.On("DeleteApplicationCredential", contextWithCred(t), testDeleteAppCredEnv["OS_APPLICATION_CREDENTIAL_ID"]).Return(fmt.Errorf("failed"))
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
								ServiceError:    service.NewCacaoGeneralError("failed").GetBase(),
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleteFailed),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "fail to update cache (do nothing)",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(nil)
					openStack.On("DeleteApplicationCredential", contextWithCred(t), "app-cred-id-delete-123").Return(nil)
					openStack.On("ListApplicationCredentialsForceable", contextWithCred(t), true).Return([]providers.ApplicationCredential{}, nil)
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleted), // event type for success response
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(nil)
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: false,
		},
		{
			name: "fail to publish response",
			sharedObjs: sharedObjs{
				request: testDeleteAppCredRequest,
			},
			fields: fields{
				credFac: func(objs sharedObjs) *credsrcmocks.CredentialFactory {
					fac := &credsrcmocks.CredentialFactory{}
					fac.On("GetCredential", anyNotNilContext(), objs.request).Return(&types.Credential{
						ID:           objs.request.Credential.Data.(string),
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}, nil)
					return fac
				},
				openStack: func(t *testing.T) *portsmocks.OpenStack {
					openStack := &portsmocks.OpenStack{}
					openStack.On("DeleteApplicationCredential", contextWithCred(t), "app-cred-id-delete-123").Return(nil)
					openStack.On("AuthenticationTest", contextWithCred(t)).Return(nil)
					openStack.On("ListApplicationCredentialsForceable", contextWithCred(t), true).Return([]providers.ApplicationCredential{}, nil)
					return openStack
				},
				eventOut: func(t *testing.T, args args) *portsmocks.OutgoingEventPort {
					eventOut := &portsmocks.OutgoingEventPort{}
					expectedResp := providers.DeleteApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor-123",
								SessionEmulator: "emulator-123",
							},
							Operation: "",
							Provider:  "provider-aaaaaaaaaaaaaaaaaaaa",
						},
						CredentialID: testDeleteAppCredRequest.Credential.Data.(string),
					}
					expectedCe, _ := messaging.CreateCloudEventWithTransactionID(
						expectedResp,
						string(providers.EventApplicationCredentialsDeleted),
						"",
						args.tid,
					)
					eventOut.On("Publish", matchDeleteAppCredCloudEventResponse(t, &expectedCe, expectedResp)).Return(fmt.Errorf("failed"))
					return eventOut
				},
			},
			args: args{
				subject: providers.EventApplicationCredentialsDeletionRequested,
				tid:     messaging.NewTransactionID(),
				jsonData: func(objs sharedObjs) []byte {
					data, _ := json.Marshal(objs.request)
					return data
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			credFac := tt.fields.credFac(tt.sharedObjs)
			openStack := tt.fields.openStack(t)
			eventOut := tt.fields.eventOut(t, tt.args)
			h := AppCredDeletionHandler{
				credFac:   credFac,
				openStack: openStack,
				eventOut:  eventOut,
			}

			jsonData := tt.args.jsonData(tt.sharedObjs)
			err := h.Handle(tt.args.subject, tt.args.tid, jsonData)
			if tt.wantErr {
				assert.Errorf(t, err, "Handle(%v, %v, %v)", tt.args.subject, tt.args.tid, jsonData)
			} else {
				assert.NoErrorf(t, err, "Handle(%v, %v, %v)", tt.args.subject, tt.args.tid, jsonData)
			}
			credFac.AssertExpectations(t)
			openStack.AssertExpectations(t)
			eventOut.AssertExpectations(t)
		})
	}
}

package eventhandlers

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

func checkRequest(req providers.ProviderRequest) error {
	if req.GetSessionActor() == "" {
		return fmt.Errorf("requet has no actor")
	}
	if req.Provider == "" {
		return fmt.Errorf("request missing provider ID")
	}
	if !req.Provider.Validate() || req.Provider.Prefix() != "provider" {
		return fmt.Errorf("request has bad provider ID")
	}
	return nil
}

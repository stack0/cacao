package eventhandlers

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// UserLoginHandler handles the user login event
type UserLoginHandler struct {
	openStack ports.OpenStack
	credMS    ports.CredentialMS
}

// NewUserLoginHandler ...
func NewUserLoginHandler(OpenStack ports.OpenStack, CredMS ports.CredentialMS) *UserLoginHandler {
	return &UserLoginHandler{
		openStack: OpenStack,
		credMS:    CredMS,
	}
}

// Handle handles the user login event and populate the cache for user
func (h UserLoginHandler) Handle(subject common.EventType, tid common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-openstack-service.domain.eventhandlers",
		"function": "UserLoginHandler.Handle",
	})
	if subject != service.EventUserLogin {
		return fmt.Errorf("wrong event type dispatched here, %s", subject)
	}
	request, err := h.parseRequest(logger, jsonData)
	if err != nil {
		return err
	}
	return h.populateCache(tid, request)
}

func (h UserLoginHandler) parseRequest(logger *log.Entry, jsonData []byte) (providers.ProviderRequest, error) {
	var request providers.ProviderRequest // FIXME the structure for EventUserLogin is not providers.ProviderRequest, see natsclient for user ms
	err := json.Unmarshal(jsonData, &request)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into ProviderRequest"
		logger.WithError(err).Error(errorMessage)
		return providers.ProviderRequest{}, fmt.Errorf(errorMessage)
	}
	return request, nil
}

func (h UserLoginHandler) populateCache(transactionID common.TransactionID, request providers.ProviderRequest) error {
	ctx, cancel := context.WithCancel(context.Background())
	ctx = types.WithUsername(ctx, request.GetSessionActor())
	defer cancel()
	return h.openStack.PopulateCache(ctx, h.credMS, request)
}

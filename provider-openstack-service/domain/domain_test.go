package domain

import (
	"context"
	"encoding/json"
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	portmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

type MockReplyer struct {
	ce *cloudevents.Event
}

func (m *MockReplyer) Reply(ce cloudevents.Event) error {
	m.ce = &ce
	return nil
}

func TestOpenStackDomain_processQuery_imageList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.ImageListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.ImagesListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: "cred-123",
			},
			Args: requestArgs,
		}
		subject     = providers.OpenStackQueryPrefix + providers.ImagesListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		imageList   = []providers.OpenStackImage{
			{
				Image: providers.Image{
					ID:     "image-1",
					Name:   "image-1-name",
					Status: "",
				},
				MinDisk:         10,
				MinRAM:          10,
				DiskFormat:      "",
				ContainerFormat: "",
				Checksum:        "",
				Size:            123,
				Visibility:      "",
				Protected:       false,
				Project:         "",
				Tags:            nil,
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := types.WithUsername(originalCtx, actor)
	expectedCtx = types.WithTransactionID(expectedCtx, messaging.GetTransactionID(&ce))
	expectedCtx = types.WithCredentialID(expectedCtx, credID)
	expectedCtx = types.WithEnvironment(expectedCtx, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListImages", expectedCtx, requestArgs.Region).Return(imageList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", anyContext(), actor, emulator, credID).Return(&cred, nil)
	domain := &OpenStackDomain{
		OpenStack: osMock,
		CredMS:    credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.ImageListReply[providers.OpenStackImage]
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Images)
	assert.Equal(t, imageList, reply.Images)
	assert.NoError(t, reply.Session.GetServiceError())
}

func TestOpenStackDomain_processQuery_flavorList(t *testing.T) {
	var (
		actor    = "actor123"
		emulator = "emulator123"
		credID   = "cred-123"
		cred     = types.Credential{
			ID: credID,
			OpenStackEnv: types.Environment{
				"key1": "val1",
			},
		}
		requestArgs = providers.FlavorListingArgs{
			Region: "region-123",
		}
		request = providers.ProviderRequest{
			Session: service.Session{
				SessionActor:    actor,
				SessionEmulator: emulator,
			},
			Operation: providers.FlavorsListOp,
			Provider:  common.NewID("provider"),
			Credential: providers.ProviderCredential{
				Type: providers.ProviderCredentialID,
				Data: "cred-123",
			},
			Args: requestArgs,
		}
		subject     = providers.OpenStackQueryPrefix + providers.FlavorsListOp
		originalCtx = context.Background()
		ce, _       = messaging.CreateCloudEventWithTransactionID(request, string(subject), "mock", messaging.NewTransactionID())
		flavorList  = []providers.Flavor{
			{
				ID:        "flavor-1",
				Name:      "flavor-1-name",
				RAM:       123,
				Ephemeral: 234,
				VCPUs:     1,
				IsPublic:  false,
				Disk:      345,
			},
		}
	)
	mockReplyer := &MockReplyer{}
	ceRequest := types.CloudEventRequest{
		CloudEvent: ce,
		Replyer:    mockReplyer,
	}
	expectedCtx := types.WithUsername(originalCtx, actor)
	expectedCtx = types.WithTransactionID(expectedCtx, messaging.GetTransactionID(&ce))
	expectedCtx = types.WithCredentialID(expectedCtx, credID)
	expectedCtx = types.WithEnvironment(expectedCtx, cred.OpenStackEnv)
	osMock := &portmocks.OpenStack{}
	osMock.On("ListFlavors", expectedCtx, requestArgs.Region).Return(flavorList, nil)
	credMock := &portmocks.CredentialMS{}
	credMock.On("GetCredential", anyContext(), actor, emulator, credID).Return(&cred, nil)
	domain := &OpenStackDomain{
		OpenStack: osMock,
		CredMS:    credMock,
	}

	domain.processQuery(originalCtx, ceRequest)
	osMock.AssertExpectations(t)
	credMock.AssertExpectations(t)
	if !assert.NotNil(t, mockReplyer.ce) {
		return
	}
	var reply providers.FlavorListReply
	assert.NoError(t, json.Unmarshal(mockReplyer.ce.Data(), &reply))
	assert.NotNil(t, reply.Flavors)
	assert.Equal(t, flavorList, reply.Flavors)
	assert.NoError(t, reply.Session.GetServiceError())
}

// this return an argument matcher that matches anything of context.Context type
func anyContext() interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool { return true })
}

package credsrc

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"strings"
)

// tokenAuth authenticate with token (scoped/unscoped)
type tokenAuth struct {
	providerSvc ports.ProviderMetadataMS
}

// CreateCredential creates a credential based on the token specified in the request.
func (ta tokenAuth) CreateCredential(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	token, err := ta.getToken(request)
	if err != nil {
		return nil, err
	}

	providerMetadata, err := ta.providerSvc.GetMetadata(ctx, request.GetSessionActor(), request.SessionEmulator, request.Provider)
	if err != nil {
		return nil, err
	}

	if token.Scope.Unscoped() {
		// create unscoped credential if the token.Scope is empty, note that the token itself (token.Token) can still be scoped.
		return ta.createUnscopedCred(providerMetadata, token)
	}
	err = token.Scope.CheckScoped()
	if err != nil {
		return nil, err
	}
	return ta.createProjectScopedCred(providerMetadata, token)
}

// createUnscopedCred creates unscoped credential from an unscoped token.
func (ta tokenAuth) createUnscopedCred(providerMetadata map[string]interface{}, token providers.OpenStackTokenCredential) (*types.Credential, error) {

	cred, err := ta.composeTokenCredFromMetadata(token.Token, providerMetadata, ta.requiredUnscopedEnvNames())
	if err != nil {
		return nil, err
	}
	err = ta.checkCredential(cred, ta.requiredUnscopedEnvNames())
	if err != nil {
		return nil, err
	}

	return &cred, nil
}

// createProjectScopedCred creates scoped credential from a token. The scope is from providers.OpenStackTokenCredential.Scope.
func (ta tokenAuth) createProjectScopedCred(providerMetadata map[string]interface{}, token providers.OpenStackTokenCredential) (*types.Credential, error) {
	cred, err := ta.composeTokenCredFromMetadata(token.Token, providerMetadata, ta.requiredUnscopedEnvNames())
	if err != nil {
		return nil, err
	}
	ta.injectProjectScope(&cred, token.Scope)
	err = ta.checkProjectScopedCred(cred)
	if err != nil {
		return nil, err
	}
	return &cred, nil
}

func (ta tokenAuth) getToken(request providers.ProviderRequest) (providers.OpenStackTokenCredential, error) {
	if request.Credential.Type != providers.ProviderOpenStackTokenCredential {
		return providers.OpenStackTokenCredential{}, fmt.Errorf("wrong credential type passed in, requires an openstack token")
	}
	cred, ok := request.Credential.Data.(providers.OpenStackTokenCredential)
	if !ok {
		return providers.OpenStackTokenCredential{}, service.NewCacaoMarshalError("data for openstack token credential is not of type OpenStackTokenCredential")
	}
	return cred, nil
}

func (ta tokenAuth) composeTokenCredFromMetadata(token string, providerMetadata map[string]interface{}, allowedEnvName map[string]struct{}) (types.Credential, error) {
	osEnv, err := ta.extractEnvFromProviderMetadata(providerMetadata, allowedEnvName)
	if err != nil {
		return types.Credential{}, nil
	}

	osEnv["OS_AUTH_TYPE"] = "v3token"
	osEnv["OS_TOKEN"] = token

	var cred = types.Credential{
		ID:           "",
		OpenStackEnv: osEnv,
	}
	return cred, nil
}

func (ta tokenAuth) extractEnvFromProviderMetadata(providerMetadata map[string]interface{}, allowedEnvName map[string]struct{}) (types.Environment, error) {
	var osEnv = make(types.Environment)
	for name, value := range providerMetadata {
		if !strings.HasPrefix(name, "OS_") {
			// skip metadata that does not start with "OS_"
			continue
		}
		_, ok := allowedEnvName[name]
		if !ok {
			// only allowed env name is used from provider metadata
			continue
		}
		strVal, ok := value.(string)
		if !ok {
			log.WithField("envName", name).Error("metadata field value is not string")
			continue
		}
		osEnv[name] = strVal
	}
	return osEnv, nil
}

func (ta tokenAuth) injectProjectScope(cred *types.Credential, scope providers.ProjectScope) {
	if scope.Project.ID != "" {
		cred.OpenStackEnv["OS_PROJECT_ID"] = scope.Project.ID
	}
	if scope.Project.Name != "" {
		cred.OpenStackEnv["OS_PROJECT_NAME"] = scope.Project.Name
	}
	if scope.Domain.ID != "" {
		cred.OpenStackEnv["OS_PROJECT_DOMAIN_ID"] = scope.Domain.ID
	}
	if scope.Domain.Name != "" {
		cred.OpenStackEnv["OS_PROJECT_DOMAIN_NAME"] = scope.Domain.Name
	}
}

func (ta tokenAuth) checkCredential(credential types.Credential, requiredEnvName map[string]struct{}) error {
	for envName := range requiredEnvName {
		envVal, ok := credential.OpenStackEnv[envName]
		if !ok {
			return fmt.Errorf("token credential requires '%s'", envName)
		}
		if len(envVal) == 0 {
			return fmt.Errorf("token credential cannot have empty '%s'", envName)
		}
	}
	return nil
}

func (ta tokenAuth) checkProjectScopedCred(credential types.Credential) error {
	err := ta.checkCredential(credential, ta.requiredUnscopedEnvNames())
	if err != nil {
		return err
	}
	var ok bool
	if _, ok = credential.OpenStackEnv["OS_PROJECT_DOMAIN_NAME"]; !ok {
		if _, ok = credential.OpenStackEnv["OS_PROJECT_DOMAIN_ID"]; !ok {
			return fmt.Errorf("neither OS_PROJECT_DOMAIN_NAME nor OS_PROJECT_DOMAIN_ID is present in credential")
		}
	}
	if _, ok = credential.OpenStackEnv["OS_PROJECT_NAME"]; !ok {
		if _, ok = credential.OpenStackEnv["OS_PROJECT_ID"]; !ok {
			return fmt.Errorf("neither OS_PROJECT_NAME nor OS_PROJECT_ID is present in credential")
		}
	}
	return nil
}

func (ta tokenAuth) requiredUnscopedEnvNames() map[string]struct{} {
	// this is construct as a map for convenient lookup
	return map[string]struct{}{
		"OS_AUTH_TYPE":            {},
		"OS_AUTH_URL":             {},
		"OS_IDENTITY_API_VERSION": {},
		"OS_REGION_NAME":          {},
		"OS_INTERFACE":            {},
		"OS_TOKEN":                {},
	}
}

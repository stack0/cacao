package credsrc

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// CredentialFactory creates credential based on what's specified in the request
type CredentialFactory interface {
	GetCredential(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error)
}

// credentialFactory implements CredentialFactory
type credentialFactory struct {
	CredSelector credentialSelector
	TokenAuth    tokenAuth
}

// NewCredentialFactory ...
func NewCredentialFactory(credMS ports.CredentialMS, providerMS ports.ProviderMetadataMS) CredentialFactory {
	return credentialFactory{
		CredSelector: credentialSelector{
			credMS: credMS,
		},
		TokenAuth: tokenAuth{
			providerSvc: providerMS,
		},
	}
}

// GetCredential construct credential based on credential specified in the request
func (src credentialFactory) GetCredential(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	if ctx == nil {
		return nil, fmt.Errorf("CredentialFactory: context is nil")
	}
	switch request.Credential.Type {
	case providers.ProviderCredentialID:
		return src.getCredViaID(ctx, request)
	case providers.ProviderOpenStackTokenCredential:
		return src.TokenAuth.CreateCredential(ctx, request)
	default:
		return nil, fmt.Errorf("unknown credential type")
	}
}

// getCredViaID fetches credential based on ID
func (src credentialFactory) getCredViaID(ctx context.Context, request providers.ProviderRequest) (*types.Credential, error) {
	return src.CredSelector.SelectCredential(ctx, request)
}

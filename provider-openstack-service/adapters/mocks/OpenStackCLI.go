// Code generated by mockery v2.14.0. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	types "gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// OpenStackCLI is an autogenerated mock type for the OpenStackCLI type
type OpenStackCLI struct {
	mock.Mock
}

// CreateApplicationCredential provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) CreateApplicationCredential(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteApplicationCredential provides a mock function with given fields: ctx, env, id
func (_m *OpenStackCLI) DeleteApplicationCredential(ctx context.Context, env types.Environment, id string) ([]byte, error) {
	ret := _m.Called(ctx, env, id)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(ctx, env, id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(ctx, env, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetApplicationCredential provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) GetApplicationCredential(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCatalogEntry provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) GetCatalogEntry(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetFlavor provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) GetFlavor(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetImage provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) GetImage(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetProject provides a mock function with given fields: _a0, _a1, _a2
func (_m *OpenStackCLI) GetProject(_a0 context.Context, _a1 types.Environment, _a2 string) ([]byte, error) {
	ret := _m.Called(_a0, _a1, _a2)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment, string) []byte); ok {
		r0 = rf(_a0, _a1, _a2)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment, string) error); ok {
		r1 = rf(_a0, _a1, _a2)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetToken provides a mock function with given fields: ctx, env
func (_m *OpenStackCLI) GetToken(ctx context.Context, env types.Environment) ([]byte, error) {
	ret := _m.Called(ctx, env)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(ctx, env)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(ctx, env)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListApplicationCredentials provides a mock function with given fields: _a0, _a1
func (_m *OpenStackCLI) ListApplicationCredentials(_a0 context.Context, _a1 types.Environment) ([]byte, error) {
	ret := _m.Called(_a0, _a1)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListCatalogs provides a mock function with given fields: ctx, env
func (_m *OpenStackCLI) ListCatalogs(ctx context.Context, env types.Environment) ([]byte, error) {
	ret := _m.Called(ctx, env)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(ctx, env)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(ctx, env)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListFlavors provides a mock function with given fields: _a0, _a1
func (_m *OpenStackCLI) ListFlavors(_a0 context.Context, _a1 types.Environment) ([]byte, error) {
	ret := _m.Called(_a0, _a1)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListImages provides a mock function with given fields: _a0, _a1
func (_m *OpenStackCLI) ListImages(_a0 context.Context, _a1 types.Environment) ([]byte, error) {
	ret := _m.Called(_a0, _a1)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListProjects provides a mock function with given fields: _a0, _a1
func (_m *OpenStackCLI) ListProjects(_a0 context.Context, _a1 types.Environment) ([]byte, error) {
	ret := _m.Called(_a0, _a1)

	var r0 []byte
	if rf, ok := ret.Get(0).(func(context.Context, types.Environment) []byte); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]byte)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, types.Environment) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewOpenStackCLI interface {
	mock.TestingT
	Cleanup(func())
}

// NewOpenStackCLI creates a new instance of OpenStackCLI. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewOpenStackCLI(t mockConstructorTestingTNewOpenStackCLI) *OpenStackCLI {
	mock := &OpenStackCLI{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}

package adapters

import (
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// EventAdapter communicates to IncomingEventPort
type EventAdapter struct {
	config         types.Configuration
	connection     messaging.StreamingEventService
	eventWaitGroup sync.WaitGroup
	handlers       []messaging.StreamingEventHandlerMapping
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(conf types.Configuration) {
	logger := log.WithFields(log.Fields{
		"package":  "providers-openstack-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("Initializing Event Adapter")

	adapter.config = conf
	var clientID = conf.PodName
	if clientID == "" {
		clientID = fmt.Sprintf("provider-openstack-event-%s", xid.New().String())
	}
	adapter.config.NATS.ClientID = clientID

	// Use a WaitGroup to wait for a message to arrive
	adapter.eventWaitGroup = sync.WaitGroup{}
	adapter.eventWaitGroup.Add(1)
}

func (adapter *EventAdapter) generateClientID(conf types.Configuration) string {
	var clientID string
	if conf.PodName == "" {
		clientID = fmt.Sprintf("provider-openstack-event-%s", xid.New().String())
	} else {
		clientID = conf.PodName + "-event"
	}
	return clientID
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "providers-openstack-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("Finalizing Event Adapter")

	adapter.eventWaitGroup.Done()
}

// SetHandlers sets the mapping of event type to event handlers
func (adapter *EventAdapter) SetHandlers(handlers []messaging.StreamingEventHandlerMapping) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *EventAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("Starting Event Adapter")

	natsConfig := adapter.config.NATS

	if len(adapter.handlers) == 0 {
		logger.Fatal("handlers has not been set")
	}

	// append a default handler
	eventHandlerMappings := append(
		adapter.handlers,
		messaging.StreamingEventHandlerMapping{
			Subject:      "",
			EventHandler: adapter.handleDefaultEvent,
		})

	stanConn, err := messaging.ConnectStanForService(&natsConfig, &adapter.config.STAN, eventHandlerMappings)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Fatal("Unable to connect to NATS Streaming")
	}

	adapter.connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.eventWaitGroup.Wait()

	adapter.connection = nil
}

func (adapter *EventAdapter) handleDefaultEvent(subject common.EventType, transactionID common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "providers-service.adapters",
		"function": "EventAdapter.handleDefaultEvent",
	})

	logger.Tracef("Received an unhandled event %s, TransactionID %s", subject, transactionID)
	return nil
}

// Publish publishes a cloudevent
func (adapter *EventAdapter) Publish(event *cloudevents.Event) error {
	if event.Source() == "" {
		event.SetSource(adapter.config.NATS.ClientID)
	}
	err := adapter.connection.PublishCloudEvent(event)
	if err != nil {
		return err
	}
	return nil
}

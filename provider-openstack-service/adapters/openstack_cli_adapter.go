package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// OpenStackCLI is an interface that comprises the necessary operations for
// interacting with the OpenStack CLI
type OpenStackCLI interface {
	GetApplicationCredential(context.Context, types.Environment, string) ([]byte, error)
	GetImage(context.Context, types.Environment, string) ([]byte, error)
	GetFlavor(context.Context, types.Environment, string) ([]byte, error)
	GetProject(context.Context, types.Environment, string) ([]byte, error)
	GetCatalogEntry(context.Context, types.Environment, string) ([]byte, error)
	GetToken(ctx context.Context, env types.Environment) ([]byte, error)
	ListApplicationCredentials(context.Context, types.Environment) ([]byte, error)
	ListFlavors(context.Context, types.Environment) ([]byte, error)
	ListImages(context.Context, types.Environment) ([]byte, error)
	ListProjects(context.Context, types.Environment) ([]byte, error)
	ListCatalogs(ctx context.Context, env types.Environment) ([]byte, error)
	CreateApplicationCredential(context.Context, types.Environment, string) ([]byte, error)
	DeleteApplicationCredential(ctx context.Context, env types.Environment, id string) ([]byte, error)
}

// OpenStackCLIAdapter is a concrete implementation of the OpenStackCLI interface
type OpenStackCLIAdapter struct {
}

// GetApplicationCredential retrieves a specific OpenStack ApplicationCredential by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetApplicationCredential(ctx context.Context, env types.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "applicationCredential", id)
}

// GetFlavor retrieves a specifc OpenStack Flavor by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetFlavor(ctx context.Context, env types.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "flavor", id)
}

// GetImage retrieves a specifc OpenStack Image by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetImage(ctx context.Context, env types.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "image", id)
}

// GetProject retrieves a specifc OpenStack Project by ID and returns it in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) GetProject(ctx context.Context, env types.Environment, id string) ([]byte, error) {
	return o.show(ctx, env, "project", id)
}

// show is an unexported helper function used for retrieving a single OpenStack
// resource by ID
func (o OpenStackCLIAdapter) show(ctx context.Context, env types.Environment, resource, id string) ([]byte, error) {
	parsedArgs := []string{resource, "show", id, "-f", "json"}
	return o.execute(ctx, env, parsedArgs)

}

// GetCatalogEntry retrieves catalog entry for a service (e.g. "nova") and returns it in JSON formatted byte slice.
func (o OpenStackCLIAdapter) GetCatalogEntry(ctx context.Context, env types.Environment, serviceName string) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackCLIAdapter.GetCatalogEntry",
	})
	output, err := o.execute(ctx, env, []string{"catalog", "show", serviceName, "-f", "json"})
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	// unmarshal to check output
	var entry types.OpenStackCatalogEntry
	err = json.Unmarshal(output, &entry)
	if err != nil {
		if strings.Contains(string(output), "not found") {
			// sometimes CLI prints the error msg directly, so output is not fully JSON, and has exit code 0
			return nil, fmt.Errorf("%s service not found", serviceName)
		}
		logger.WithField("output", output).WithError(err).Error("'catalog show' result not json")
		return nil, fmt.Errorf("'catalog show' result not json, %w", err)
	}
	return output, nil
}

// ListApplicationCredentials retrieves all the available OpenStack application credentials and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListApplicationCredentials(ctx context.Context, env types.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"application", "credential", "list", "-f", "json"})
}

// ListImages retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListImages(ctx context.Context, env types.Environment) ([]byte, error) {

	return o.execute(ctx, env, []string{"image", "list", "-f", "json", "--long"})

}

// ListFlavors retrieves all the available OpenStack flavors and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListFlavors(ctx context.Context, env types.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"flavor", "list", "-f", "json", "--all"})
}

// ListProjects retrieves all the available OpenStack projects and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListProjects(ctx context.Context, env types.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"project", "list", "-f", "json", "--my-projects", "--long"})
}

// ListCatalogs retrieves the catalog list (list of REST endpoints) and returns them in a JSON
// formatted byte slice.
func (o OpenStackCLIAdapter) ListCatalogs(ctx context.Context, env types.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"catalog", "list", "-f", "json"})
}

// CreateApplicationCredential creates an application credential. The app cred is unrestricted because that is the only way to allow it to delete itself.
func (o OpenStackCLIAdapter) CreateApplicationCredential(ctx context.Context, env types.Environment, name string) ([]byte, error) {
	// TODO consider adding access rule to restrict the ability to create application credential with the newly created one.
	// Note that this might be difficult because access rule is whitelist rather than blacklist. So we need to list all other endpoints.
	return o.execute(ctx, env, []string{"application", "credential", "create", "--unrestricted", "-f", "json", name})
}

// DeleteApplicationCredential ...
func (o OpenStackCLIAdapter) DeleteApplicationCredential(ctx context.Context, env types.Environment, idOrName string) ([]byte, error) {
	return o.execute(ctx, env, []string{"application", "credential", "delete", idOrName})
}

// GetToken issues a token based on the current scope and returns result in a JSON formatted byte slice.
func (o OpenStackCLIAdapter) GetToken(ctx context.Context, env types.Environment) ([]byte, error) {
	return o.execute(ctx, env, []string{"token", "issue", "-f", "json"})
}

// execute calls the openstack command with the provided context and arguments
// and returns a []byte containing the stdout of the command.
// Note: baseEnv is read only.
func (o OpenStackCLIAdapter) execute(ctx context.Context, baseEnv types.Environment, args []string) ([]byte, error) {
	var (
		err    error
		ctxEnv types.Environment
	)

	// make a copy of baseEnv to prevent race condition, since the caller is likely passing in baseEnv as a singleton,
	// thus concurrent read & write is possible with concurrent requests.
	env := copyMap(baseEnv)

	// Merge the passed in environment with the process's environment
	// This will pick up the EDITOR env var, which the credentials
	// are unlikely to provide.
	ctxEnv, err = types.EnvironmentFromCtxWithErr(ctx)
	if err != nil {
		return nil, err
	}
	for name, val := range ctxEnv {
		env[name] = val
	}

	flattenedEnvironment := flattenEnvironment(env)

	cmd := exec.CommandContext(ctx, "openstack", args...)
	cmd.Env = flattenedEnvironment

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, err
	}

	if err = cmd.Start(); err != nil {
		return nil, err
	}

	var stdoutOutput bytes.Buffer
	_, err = stdoutOutput.ReadFrom(stdout)
	if err != nil {
		return nil, err
	}
	var stderrOutput strings.Builder
	_, err = io.Copy(&stderrOutput, stderr)
	if err != nil {
		return nil, err
	}

	if err = cmd.Wait(); err != nil {
		log.WithField("stdout", stdoutOutput.String()).Info("cli output")
		log.WithField("stderr", stderrOutput.String()).Info("cli output")
		return nil, fmt.Errorf("%w, %s", err, stderrOutput.String())
	}

	return stdoutOutput.Bytes(), nil
}

// flattenEnvironment is a helper function that accepts an Environment struct
// and returns a []string with values in the format "<property name>=<value>".
// This makes it possible to set the environment in which a command executes.
func flattenEnvironment(env types.Environment) []string {
	var flattenedEnv []string
	for k, v := range env {
		flattenedEnv = append(flattenedEnv, fmt.Sprintf("%s=%s", k, v))
	}
	return flattenedEnv
}

// make a copy of a map
func copyMap(old map[string]string) map[string]string {
	newMap := make(map[string]string, len(old))
	for key, val := range old {
		newMap[key] = val
	}
	return newMap
}

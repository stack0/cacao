package adapters

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/hashicorp/go-multierror"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"

	"github.com/eko/gocache/cache"
)

// OpenStackAdapter is used to access metadata from OpenStack clusters
type OpenStackAdapter struct {
	cli     OpenStackCLI
	cache   openstackCache
	baseEnv types.Environment
	http    OpenStackHTTPClient
}

// NewOpenStackAdapter creates a new *OpenStackAdapter.
func NewOpenStackAdapter(ttl time.Duration, cacheMgr cache.CacheInterface) *OpenStackAdapter {
	if ttl.Seconds() < 1.0 {
		// enforce a min ttl
		ttl = time.Second
	}
	baseEnv := splitEnvironment(os.Environ())

	retval := &OpenStackAdapter{
		cli:  &OpenStackCLIAdapter{},
		http: &OpenStackHTTPClientAdapter{},
		cache: openstackCache{
			baseEnv: baseEnv,
			cache:   cacheMgr,
			ttl:     ttl,
		},
		baseEnv: baseEnv,
	}

	return retval
}

func splitEnvironment(env []string) types.Environment {
	retval := map[string]string{}

	for _, setting := range env {
		parts := strings.Split(setting, "=")
		if len(parts) != 2 {
			continue
		}
		retval[parts[0]] = parts[1]
	}

	return retval
}

// GetApplicationCredential attempts to retrieve a specific OpenStack application credential by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetApplicationCredential(ctx context.Context, id string) (*providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetApplicationCredential",
	})
	var (
		applicationCredential *providers.ApplicationCredential
		err                   error
	)

	applicationCredentials, err := o.ListApplicationCredentials(ctx)
	if err != nil {
		return nil, err
	}

	for _, i := range applicationCredentials {
		if i.ID == id {
			applicationCredential = &i
			logger.WithField("applicationCredential", applicationCredential.ID).Info("ApplicationCredential fetched")
			return applicationCredential, nil
		}
	}

	err = fmt.Errorf("ApplicationCredential not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetImage attempts to retrieve a specific OpenStack image by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetImage(ctx context.Context, id string, region string) (*providers.OpenStackImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetImage",
	})
	var (
		image *providers.OpenStackImage
		err   error
	)

	images, err := o.ListImages(ctx, region)
	if err != nil {
		return nil, err
	}

	for _, i := range images {
		if i.ID == id {
			image = &i
			logger.WithField("image", image.ID).Info("image fetched")
			return image, nil
		}
	}

	err = fmt.Errorf("image not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetFlavor attempts to retrieve a specific OpenStack flavor by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetFlavor(ctx context.Context, id string, region string) (*providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetFlavor",
	})
	var (
		flavor *providers.Flavor
		err    error
	)

	flavors, err := o.ListFlavors(ctx, region)
	if err != nil {
		return nil, err
	}

	for _, f := range flavors {
		if f.ID == id {
			flavor = &f
			logger.WithField("flavor", flavor.ID).Info("flavor fetched")
			return flavor, nil
		}
	}

	err = fmt.Errorf("flavor not found: %s", id)
	logger.WithError(err).Error()
	return nil, err
}

// GetProject attempts to retrieve a specific OpenStack Project by ID. If a cached
// value exists, it will be returned rather than contacting the OpenStackCluster.
func (o *OpenStackAdapter) GetProject(ctx context.Context, id string) (*providers.Project, error) {
	var (
		project *providers.Project
		err     error
	)

	projects, err := o.ListProjects(ctx)
	if err != nil {
		return nil, err
	}

	for _, f := range projects {
		if f.ID == id {
			project = &f
			break
		}
	}

	if project == nil {
		err = fmt.Errorf("project not found: %s", id)
	}

	return project, err
}

// ListApplicationCredentialsForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster. If force is set to true, the cache is reset
// with the results of a new execution of the listing command.
func (o *OpenStackAdapter) ListApplicationCredentialsForceable(ctx context.Context, force bool) ([]providers.ApplicationCredential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListApplicationCredentialsForceable",
	})
	var (
		applicationCredentials     []providers.ApplicationCredential
		applicationCredentialBytes []byte
		err                        error
	)
	applicationCredentialBytes, err = o.cache.doIfNoCache(
		ctx, logger, types.CachePrefixListApplicationCredentials,
		o.cli.ListApplicationCredentials,
		force,
	)
	if err != nil {
		return nil, err
	}

	var appCredList []applicationCredentialListItem
	err = json.Unmarshal(applicationCredentialBytes, &appCredList)
	applicationCredentials = o.convertApplicationCredentialList(appCredList)
	logger.WithField("length", len(applicationCredentials)).Info("ApplicationCredentials listed")
	return applicationCredentials, err
}

func (o *OpenStackAdapter) convertApplicationCredentialList(appCredList []applicationCredentialListItem) []providers.ApplicationCredential {
	var result []providers.ApplicationCredential
	for _, appCred := range appCredList {
		result = append(result, appCred.Convert())
	}
	return result
}

// ListApplicationCredentials calls ListApplicationCredentialsForceable with the force option set to false.
// You should use this one, it's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListApplicationCredentials(ctx context.Context) ([]providers.ApplicationCredential, error) {
	return o.ListApplicationCredentialsForceable(ctx, false)
}

// ListImagesForceable attempts to list all the available images in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster. If force is set to true, the cache is reset
// with the results of a new execution of the listing command.
func (o *OpenStackAdapter) ListImagesForceable(ctx context.Context, region string, force bool) ([]providers.OpenStackImage, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListImagesForceable",
	})
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		return nil, err
	}

	imageBytes, err := o.cache.doRegionalOperationIfNoCache(
		ctxWithRegion, logger, types.CachePrefixListImages, region, o.listImagesUsingAPIWithPagination, force,
	)
	if err != nil {
		return nil, err
	}

	var images []providers.OpenStackImage
	err = json.Unmarshal(imageBytes, &images)
	logger.WithField("length", len(images)).Info("images listed.")
	return images, err
}

func (o *OpenStackAdapter) listImagesUsingAPIWithPagination(ctx context.Context, baseEnv types.Environment) ([]byte, error) {
	endpointURL, err := o.findPublicGlanceEndpointURL(ctx, baseEnv)
	if err != nil {
		return nil, err
	}

	token, err := o.GetToken(ctx)
	if err != nil {
		return nil, err
	}

	var marker string
	var images = make([]providers.OpenStackImage, 0)
	for {
		var imageListPage []imageListItem
		imageListPage, marker, err = o.listImagesUsingAPISinglePage(endpointURL, token.ID, marker)
		if err != nil {
			return nil, err
		}
		images = append(images, o.convertImageList(imageListPage)...)
		if marker == "" {
			break
		}
	}
	return json.Marshal(images)
}

func (o *OpenStackAdapter) listImagesUsingAPISinglePage(glanceEndpointURL string, tokenID string, markerInput string) (imageList []imageListItem, markerOutput string, err error) {
	respBody, err := o.RequestToAPI(glanceEndpointURL, tokenID, markerInput, "images")
	if err != nil {
		return nil, "", err
	}
	var resp imageListAPIResponse
	err = json.Unmarshal(respBody, &resp)
	if err != nil {
		return nil, "", err
	}
	markerOutput = resp.Next
	return resp.Images, markerOutput, nil
}

func (o *OpenStackAdapter) findPublicGlanceEndpointURL(ctx context.Context, baseEnv types.Environment) (url string, err error) {
	credEnv := types.EnvironmentFromCtx(ctx)
	region, ok := credEnv["OS_REGION_NAME"]
	if !ok {
		return "", fmt.Errorf("OS_REGION_NAME missing from credential, cannot find glance endpoint")
	}

	catalogEntryBytes, err := o.cli.GetCatalogEntry(ctx, baseEnv, "glance")
	if err != nil {
		return "", err
	}

	var catalogEntry types.OpenStackCatalogEntry
	err = json.Unmarshal(catalogEntryBytes, &catalogEntry)
	if err != nil {
		return "", err
	}

	for _, endpoint := range catalogEntry.Endpoints {
		// find the public endpoint of the region
		if endpoint.Interface == "public" && (endpoint.Region == region || endpoint.RegionID == region) {
			return endpoint.URL, nil
		}
	}
	return "", fmt.Errorf("no public OpenStack Glance endpoint available, fail to list images")
}

// RequestToAPI requests the API with the given url, token, paginationMarker and resource, and returns the response body.
func (o *OpenStackAdapter) RequestToAPI(endpointURL string, tokenID string, paginationMarker string, resource string) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.Request",
	})
	var (
		url           string
		err           error
		responseBytes []byte
	)

	if paginationMarker != "" {
		url = endpointURL + paginationMarker
	} else {
		url = endpointURL + "/v2/" + resource
	}
	logger = logger.WithField("url", url)
	logger.Info()

	req, err := o.http.NewRequestWithContext(context.Background(), "GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("X-Auth-Token", tokenID)

	resp, err := o.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			logger.Error(err)
		}
	}(resp.Body)
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("%s, status code != 200", url)
	}

	// read the response body and store it in the imageBytes variable
	responseBytes, err = io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	logger.WithField("response", string(responseBytes)).Debug("response")
	return responseBytes, nil
}

func (o *OpenStackAdapter) convertImageList(imageList []imageListItem) []providers.OpenStackImage {
	var result []providers.OpenStackImage
	for _, img := range imageList {
		result = append(result, img.Convert())
	}
	return result
}

// ListImages calls ListImagesForceable with the force option set to false.
// You should use this one, it's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListImages(ctx context.Context, region string) ([]providers.OpenStackImage, error) {
	return o.ListImagesForceable(ctx, region, false)
}

// ListFlavorsForceable attempts to list all the available flavors in an OpenStack
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster.
func (o *OpenStackAdapter) ListFlavorsForceable(ctx context.Context, region string, force bool) ([]providers.Flavor, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListFlavorsForceable",
	})
	var (
		flavors     []providers.Flavor
		flavorBytes []byte
		err         error
	)
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		return nil, err
	}
	flavorBytes, err = o.cache.doRegionalOperationIfNoCache(
		ctxWithRegion, logger,
		types.CachePrefixListFlavors, region,
		o.cli.ListFlavors,
		force,
	)
	if err != nil {
		return nil, err
	}

	var flavorList []flavorListItem
	err = json.Unmarshal(flavorBytes, &flavorList)
	if err != nil {
		return nil, err
	}
	flavors = o.convertFlavorList(flavorList)
	logger.WithField("length", len(flavors)).Info("flavors listed")
	return flavors, err
}

func (o *OpenStackAdapter) convertFlavorList(flavorList []flavorListItem) []providers.Flavor {
	var result []providers.Flavor
	for _, flavor := range flavorList {
		result = append(result, flavor.Convert())
	}
	return result
}

// ListFlavors calls ListFlavorsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListFlavors(ctx context.Context, region string) ([]providers.Flavor, error) {
	return o.ListFlavorsForceable(ctx, region, false)
}

// ListProjectsForceable attempts to list all the available projects in an OpenStack/
// cluster. If a cached value exists, it will be returned rather than
// contacting the OpenStackCluster.
func (o *OpenStackAdapter) ListProjectsForceable(ctx context.Context, force bool) ([]providers.Project, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListProjectsForceable",
	})
	var (
		projects     []providers.Project
		projectBytes []byte
		err          error
	)

	projectBytes, err = o.cache.doIfNoCache(
		ctx, logger, types.CachePrefixListProjects,
		o.cli.ListProjects,
		force,
	)
	if err != nil {
		return nil, err
	}

	var projectList []projectListItem
	err = json.Unmarshal(projectBytes, &projectList)
	if err != nil {
		return nil, err
	}
	projects = o.convertProjectList(projectList)
	logger.WithField("length", len(projects)).Info("Projects listed")
	return projects, err
}

func (o *OpenStackAdapter) convertProjectList(projectList []projectListItem) []providers.Project {
	var result []providers.Project
	for _, proj := range projectList {
		result = append(result, proj.Convert())
	}
	return result
}

// ListProjects calls ListProjectsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListProjects(ctx context.Context) ([]providers.Project, error) {
	return o.ListProjectsForceable(ctx, false)
}

// ListRegionsForceable attempts to list all the regions that is available to user in an OpenStack cluster.
// If a cached value exists, it will be returned rather than contacting the OpenStackCluster.
//
// Note: this retrieves regions from catalog entry for nova service rather than region list. This is because regions
// can be disabled by endpoint filtering. This will only return regions that is reported in the catalog nova endpoints.
func (o *OpenStackAdapter) ListRegionsForceable(ctx context.Context, force bool) ([]providers.Region, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListRegionsForceable",
	})
	var (
		regions           []providers.Region
		catalogEntryBytes []byte
		err               error
	)

	catalogEntryBytes, err = o.cache.doIfNoCache(
		ctx, logger, types.CachePrefixListRegions,
		func(ctx context.Context, env types.Environment) ([]byte, error) {
			return o.cli.GetCatalogEntry(ctx, env, "nova")
		},
		force,
	)
	if err != nil {
		return nil, err
	}

	var catalogEntry types.OpenStackCatalogEntry
	err = json.Unmarshal(catalogEntryBytes, &catalogEntry)
	if err != nil {
		return nil, err
	}
	regions = getRegionsFromCatalogEntry(catalogEntry)
	logger.WithField("length", len(regions)).Info("Regions listed")
	return regions, nil
}

func getRegionsFromCatalogEntry(entry types.OpenStackCatalogEntry) []providers.Region {
	var regions []providers.Region
	for _, endpoint := range entry.Endpoints {
		if endpoint.Interface == "public" {
			// only include public endpoints
			regions = append(regions, providers.Region{
				ID:   endpoint.RegionID,
				Name: endpoint.Region,
			})
		}
	}
	return regions
}

// ListRegions calls ListRegionsForceable with the force option set to false.
// If in doubt, call this one. It's rare to want to bypass the cache.
func (o *OpenStackAdapter) ListRegions(ctx context.Context) ([]providers.Region, error) {
	return o.ListRegionsForceable(ctx, false)
}

// ListCatalogForceable returns a list of catalog. When force == true, cache will be override.
func (o *OpenStackAdapter) ListCatalogForceable(ctx context.Context, force bool) ([]providers.CatalogEntry, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.ListCatalogForceable",
	})
	var (
		catalogBytes []byte
		err          error
	)

	catalogBytes, err = o.cache.doIfNoCache(
		ctx, logger, types.CachePrefixListCatalog,
		func(ctx context.Context, env types.Environment) ([]byte, error) {
			return o.cli.ListCatalogs(ctx, env)
		},
		force,
	)
	if err != nil {
		return nil, err
	}

	var catalogEntries []providers.CatalogEntry
	err = json.Unmarshal(catalogBytes, &catalogEntries)
	if err != nil {
		return nil, err
	}
	return catalogEntries, err
}

// ListCatalog returns a list of catalog.
func (o *OpenStackAdapter) ListCatalog(ctx context.Context) ([]providers.CatalogEntry, error) {
	return o.ListCatalogForceable(ctx, false)
}

// GetToken issues a token.
func (o *OpenStackAdapter) GetToken(ctx context.Context) (*providers.Token, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.GetToken",
	})

	tokenBytes, err := o.cli.GetToken(ctx, o.baseEnv)
	if err != nil {
		return nil, err
	}
	var token providers.Token
	err = json.Unmarshal(tokenBytes, &token)
	if err != nil {
		return nil, err
	}
	logger.WithField("token", token).Info("Token retrieved")
	return &token, err
}

// PopulateCache fetches the application credential, flavor, image, and project lists from OpenStack and caches them
func (o *OpenStackAdapter) PopulateCache(ctx context.Context, credMS ports.CredentialMS, request providers.ProviderRequest) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "OpenStackAdapter.PopulateCache",
	})
	if request.Credential.Type != providers.ProviderCredentialID {
		return fmt.Errorf("populate cache requires a credential ID")
	}
	credID, ok := request.Credential.Data.(string)
	if !ok {
		return fmt.Errorf("credential ID is not string")
	}
	credentials, err := credMS.GetCredential(ctx, request.GetSessionActor(), request.GetSessionEmulator(), credID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return err
	}

	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           credentials.ID,
		OpenStackEnv: credentials.OpenStackEnv,
	})

	var errors error
	_, err = o.ListApplicationCredentialsForceable(ctx, true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch application credential list")
		errors = multierror.Append(errors, err)
	}
	regions, err := o.ListRegions(ctx)
	if err != nil {
		return err
	}
	for _, region := range regions {
		_, err = o.ListFlavorsForceable(ctx, region.Name, true)
		if err != nil {
			logger.WithError(err).Error("fail to force fetch flavor list")
			errors = multierror.Append(errors, err)
		}
		_, err = o.ListImagesForceable(ctx, region.Name, true)
		if err != nil {
			logger.WithError(err).Error("fail to force fetch image list")
			errors = multierror.Append(errors, err)
		}
	}

	_, err = o.ListProjectsForceable(ctx, true)
	if err != nil {
		logger.WithError(err).Error("fail to force fetch project list")
		errors = multierror.Append(errors, err)
	}
	return errors
}

// AuthenticationTest attempts to list all the available projects in an OpenStack cluster.
func (o *OpenStackAdapter) AuthenticationTest(ctx context.Context) error {
	output, err := o.cli.GetToken(ctx, o.baseEnv)
	if err == nil {
		return nil
	}
	var jsonOutput interface{}
	errJSON := json.Unmarshal(output, &jsonOutput)
	if errJSON == nil {
		// openstack CLI does not output JSON when errored, at least when targeting JS2.
		// unmarshal to json is meant as a check to detect changes to CLI.
		log.WithField("output", string(output)).Warn("openstack cli is returning json when errored, this is NOT expected")
	}

	lowerCaseErrorOutput := strings.ToLower(err.Error())
	if strings.Contains(lowerCaseErrorOutput, "could not find application credential") {
		// application credential ID does not exist
		return types.ErrApplicationCredentialNotFound
	}
	if strings.Contains(lowerCaseErrorOutput, "the request you have made requires authentication") {
		// application credential ID exist, but secret is incorrect
		return types.ErrAuthentication
	}
	return err
}

// CreateApplicationCredential creates an application credential with the given name.
func (o *OpenStackAdapter) CreateApplicationCredential(ctx context.Context, scope providers.ProjectScope, name string) (types.ApplicationCredential, error) {
	output, err := o.cli.CreateApplicationCredential(o.changeProjectScope(ctx, scope), o.baseEnv, name)
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	var appCred types.ApplicationCredential
	err = json.Unmarshal(output, &appCred)
	if err != nil {
		return types.ApplicationCredential{}, err
	}
	return appCred, nil
}

// changes the project scope of the credential inside context
func (o *OpenStackAdapter) changeProjectScope(ctx context.Context, newScope providers.ProjectScope) context.Context {
	ctxEnv, err := types.EnvironmentFromCtxWithErr(ctx)
	if err != nil {
		return nil
	}
	// clear the scope before re-setting it
	clearCredScope(ctxEnv)
	setCredScope(ctxEnv, newScope)
	return types.WithCredential(ctx, &types.Credential{
		ID:           "",
		OpenStackEnv: ctxEnv,
	})
}

// DeleteApplicationCredential deletes an application credential
func (o *OpenStackAdapter) DeleteApplicationCredential(ctx context.Context, idOrName string) error {
	_, err := o.cli.DeleteApplicationCredential(ctx, o.baseEnv, idOrName)
	return err
}

func injectRegion(ctx context.Context, region string) (context.Context, error) {
	credEnv, err := types.EnvironmentFromCtxWithErr(ctx)
	if err != nil {
		return nil, errors.New("no credential in the context")
	}
	credEnv["OS_REGION_NAME"] = region
	return types.WithEnvironment(ctx, credEnv), nil
}

func clearCredScope(env types.Environment) {
	delete(env, "OS_PROJECT_NAME")
	delete(env, "OS_PROJECT_ID")
	delete(env, "OS_PROJECT_DOMAIN_NAME")
	delete(env, "OS_PROJECT_DOMAIN_ID")
}

func setCredScope(env types.Environment, scope providers.ProjectScope) {
	if scope.Project.Name != "" {
		env["OS_PROJECT_NAME"] = scope.Project.Name
	} else if scope.Project.ID != "" {
		env["OS_PROJECT_ID"] = scope.Project.ID
	}
	if scope.Domain.Name != "" {
		env["OS_PROJECT_DOMAIN_NAME"] = scope.Domain.Name
	} else if scope.Domain.ID != "" {
		env["OS_PROJECT_DOMAIN_ID"] = scope.Domain.ID
	}
}

// imageList is a list of images
type imageListAPIResponse struct {
	Images []imageListItem `json:"images"`
	Next   string          `json:"next"`
}

// imageListItem is an item in the "image list" output, this has different json field names than "image show" which is what providers.Image is based on
type imageListItem struct {
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	DiskFormat      string   `json:"disk_format"`
	ContainerFormat string   `json:"container_format"`
	MinDisk         int64    `json:"min_disk"`
	MinRAM          int64    `json:"min_ram"`
	Size            int64    `json:"size"`
	Checksum        string   `json:"checksum"`
	Status          string   `json:"status"`
	Visibility      string   `json:"visibility"`
	Protected       bool     `json:"protected"`
	Project         string   `json:"project"`
	Tags            []string `json:"tags"`
}

// Convert to providers.Image
func (img imageListItem) Convert() providers.OpenStackImage {
	return providers.OpenStackImage{
		Image: providers.Image{
			ID:     img.ID,
			Name:   img.Name,
			Status: img.Status,
		},
		DiskFormat:      img.DiskFormat,
		MinDisk:         img.MinDisk,
		MinRAM:          img.MinRAM,
		ContainerFormat: img.ContainerFormat,
		Checksum:        img.Checksum,
		Size:            img.Size,
		Visibility:      img.Visibility,
		Protected:       img.Protected,
		Project:         img.Project,
		Tags:            img.Tags,
	}
}

// flavorListItem is an item in the "flavor list" output, this has different json field names than "flavor show" which is what providers.Flavor is based on
type flavorListItem struct {
	ID        string `json:"ID"`
	Name      string `json:"Name"`
	RAM       int64  `json:"RAM"`
	Disk      int64  `json:"Disk"`
	Ephemeral int64  `json:"Ephemeral"`
	VCPUs     int64  `json:"VCPUs"`
	IsPublic  bool   `json:"Is Public"` // different tag than providers.Flavor
}

// Convert converts to providers.Flavor
func (f flavorListItem) Convert() providers.Flavor {
	return providers.Flavor{
		ID:        f.ID,
		Name:      f.Name,
		RAM:       f.RAM,
		Ephemeral: f.Ephemeral,
		VCPUs:     f.VCPUs,
		IsPublic:  f.IsPublic,
		Disk:      f.Disk,
	}
}

// projectListItem is an item in the "project list" output, this has different json field names than "project show" which is what providers.Project is based on
type projectListItem struct {
	ID          string `json:"ID"`
	Name        string `json:"Name"`
	DomainID    string `json:"Domain ID"` // different tag than providers.Project
	Description string `json:"Description"`
	Enabled     bool   `json:"Enabled"`
}

// Convert converts to providers.Project
func (proj projectListItem) Convert() providers.Project {
	return providers.Project{
		ID:          proj.ID,
		Name:        proj.Name,
		Description: proj.Description,
		DomainID:    proj.DomainID,
		Enabled:     proj.Enabled,
	}
}

// applicationCredentialListItem is an item in the "application credential list" output, this has different json field names than "application credential show" which is what providers.ApplicationCredential is based on
type applicationCredentialListItem struct {
	ID          string      `json:"ID"`
	Name        string      `json:"Name"`
	ProjectID   string      `json:"Project ID"`
	Description string      `json:"Description"`
	ExpiresAt   interface{} `json:"Expires At"`
}

// Convert converts to providers.ApplicationCredential
func (appCred applicationCredentialListItem) Convert() providers.ApplicationCredential {
	return providers.ApplicationCredential{
		ID:           appCred.ID,
		Name:         appCred.Name,
		Description:  appCred.Description,
		ExpiresAt:    appCred.ExpiresAt,
		ProjectID:    appCred.ProjectID,
		Roles:        nil,
		Unrestricted: false,
	}
}

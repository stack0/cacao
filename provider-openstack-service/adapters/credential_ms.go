package adapters

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	NATS messaging.NatsConfig
	STAN messaging.StanConfig
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(NATS messaging.NatsConfig, STAN messaging.StanConfig) *CredentialMicroservice {
	return &CredentialMicroservice{NATS: NATS, STAN: STAN}
}

// GetCredential ...
func (svc CredentialMicroservice) GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.GetCredential",
		"actor":    actor,
		"emulator": emulator,
		"ID":       ID,
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credential, err := client.Get(ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return nil, err
	}
	converted, err := svc.convertCredential(credential)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cred value")
		return nil, err
	}
	logger.Info("credential fetched")
	return &converted, nil
}

// ListCredentialsByTag ...
func (svc CredentialMicroservice) ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentials",
		"actor":    actor,
		"emulator": emulator,
		"tag":      tagName,
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credList, err := client.List()
	if err != nil {
		logger.WithError(err).Error("fail to list credentials")
		return nil, err
	}
	matchedCred := make([]types.Credential, 0)
	for _, cred := range credList {
		if svc.credentialHasTag(cred, tagName) {
			// list operation does not include value (redacted), thus re-fetch each cred individually for the value.
			refetchedCred, err := svc.GetCredential(ctx, actor, emulator, cred.GetID())
			if err != nil {
				logger.WithError(err).Error("fail to re-fetch cred for cred value")
				continue
			}
			if refetchedCred == nil {
				// guard against nil
				logger.Error("GetCredential return nil on success")
				continue
			}
			matchedCred = append(matchedCred, *refetchedCred)
		}
	}
	logger.WithFields(log.Fields{
		"listCount":    len(credList),
		"matchedCount": len(matchedCred),
	}).Info("credential fetched")

	if len(matchedCred) == 0 {
		return []types.Credential{}, nil
	}
	return matchedCred, nil
}

func (svc CredentialMicroservice) credentialHasTag(cred service.Credential, tagName string) bool {
	for _, tag := range cred.GetTags() {
		if tag == tagName {
			return true
		}
	}
	return false
}

func (svc CredentialMicroservice) convertCredential(cred service.Credential) (types.Credential, error) {
	var environment types.Environment
	if err := json.Unmarshal([]byte(cred.GetValue()), &environment); err != nil {
		return types.Credential{}, providers.NewNotOpenStackCredentialError(cred.GetID(), err.Error())
	}
	return types.Credential{
		ID:           cred.GetID(),
		CreatedAt:    cred.GetCreatedAt(),
		UpdatedAt:    cred.GetUpdatedAt(),
		OpenStackEnv: environment,
	}, nil
}

// CreateCredential creates a credential via credential microservice
func (svc CredentialMicroservice) CreateCredential(ctx context.Context, actor, emulator string, cred service.Credential) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.CreateCredential",
		"actor":    actor,
		"emulator": emulator,
		"credID":   cred.GetID(),
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	_, err = client.Add(cred)
	return err
}

// DeleteCredential deletes a credential
func (svc CredentialMicroservice) DeleteCredential(ctx context.Context, actor, emulator, ID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.DeleteCredential",
		"actor":    actor,
		"emulator": emulator,
		"credID":   ID,
	})
	client, err := service.NewNatsCredentialClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	_, err = client.Delete(ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete credential")
		return err
	}
	return nil
}

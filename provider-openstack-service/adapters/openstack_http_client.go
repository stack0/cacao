package adapters

import (
	"context"
	"io"
	"net/http"
	"time"
)

// OpenStackHTTPClient is an interface for the http client
type OpenStackHTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
	NewRequestWithContext(ctx context.Context, method, url string, body io.Reader) (*http.Request, error)
}

// OpenStackHTTPClientAdapter is the concrete implementation of the OpenStackHTTPClient interface
type OpenStackHTTPClientAdapter struct {
}

// NewRequestWithContext creates an HTTP request with the provided context.
func (o OpenStackHTTPClientAdapter) NewRequestWithContext(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	return http.NewRequestWithContext(ctx, method, url, body)
}

// Do sends an HTTP request and returns an HTTP response, following policy (e.g. redirects, cookies, auth) as configured on the client.
func (o OpenStackHTTPClientAdapter) Do(req *http.Request) (*http.Response, error) {
	ctxWithTimeout, cancel := context.WithTimeout(req.Context(), time.Second*3)
	defer cancel()
	return http.DefaultClient.Do(req.WithContext(ctxWithTimeout))
}

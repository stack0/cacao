package adapters

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

var testCatalogEntry = []byte(`{
	"endpoints": [
	  {
		"id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		"interface": "public",
		"region_id": "region-123",
		"url": "https://cyverse.org",
		"region": "BAR"
	  }
	],
	"id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
	"name": "nova",
	"type": "compute"
  }`)

var testToken = []byte(`{

		
	"expires": "2021-06-22T20:48:59.000000Z",
	"id": "cccccccccccccccccccccccccccccccc",
	"project_id": "dddddddddddddddddddddddddddddddd",
	"user_id": "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"


}`)
var testEnv = types.Environment{
	"OS_REGION_NAME":                   "RegionOne",
	"OS_PROJECT_DOMAIN_ID":             "eef394bdb97165674e11dca9d8c6934d",
	"OS_INTERFACE":                     "public",
	"OS_AUTH_URL":                      "https://example.com:5000/v3",
	"OS_APPLICATION_CREDENTIAL_SECRET": "YqMyZgBNx5LiC9N0X-GM8RA5kGvv5NEajtAjSqIwFidu2xasTDLXzjE6GIYm8WlFVH0Y5c6mE2CDiSgBFi4Irw",
	"OS_APPLICATION_CREDENTIAL_ID":     "0a00d21d9b188dc4ca823e6440bbe36c",
	"OS_AUTH_TYPE":                     "v3applicationcredential",
	"OS_USER_DOMAIN_NAME":              "abc",
	"OS_IDENTITY_API_VERSION":          "3",
}

var testApplicationCredentials = []byte(`
	[
		{
			"ID":           "00000000-1111-2222-aaaa-abcdefabcdef",
			"Name":         "cred1",
			"Description":  "first cred",
			"ExpiresAt":    "2022-01-01",
			"ProjectID":    "12345678-1111-2222-aaaa-abcdefabcdef",
			"Unrestricted": true
		},
		{
			"ID":           "00000000-1111-2222-bbbb-abcdefabcdef",
			"Name":         "cred2",
			"Description":  "second cred",
			"ExpiresAt":    "2022-01-01",
			"ProjectID":    "12345678-1111-2222-bbbb-abcdefabcdef",
			"Unrestricted": true
		},
		{
			"ID":           "00000000-1111-2222-cccc-abcdefabcdef",
			"Name":         "cred3",
			"Description":  "third cred",
			"ExpiresAt":    "2022-01-01",
			"ProjectID":    "12345678-1111-2222-cccc-abcdefabcdef",
			"Unrestricted": true
		}
	]
`)

// Trailing spaces are present since that matches what the OpenStack CLI does
var testFlavors = []byte(`
[
  {
    "Name": "large3", 
    "RAM": 65536, 
    "Ephemeral": 0, 
    "VCPUs": 8, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "000d1872-c4a7-11eb-8529-0242ac130003"
  }, 
  {
    "Name": "xlarge1", 
    "RAM": 32768, 
    "Ephemeral": 0, 
    "VCPUs": 16, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "08daa0be-c4a7-11eb-8529-0242ac130003"
  }, 
  {
    "Name": "xxlarge1", 
    "RAM": 65536, 
    "Ephemeral": 0, 
    "VCPUs": 32, 
    "Is Public": true, 
    "Disk": 0, 
    "ID": "12492d6e-c4a7-11eb-8529-0242ac130003"
  }
]
	`)

// unused
// var testFlavor = []byte(`
//   {
//     "Name": "large3",
//     "RAM": 65536,
//     "Ephemeral": 0,
//     "VCPUs": 8,
//     "Is Public": true,
//     "Disk": 0,
//     "ID": "000d1872-c4a7-11eb-8529-0242ac130003"
//   }
// `)

var testImagesResponse = []byte(`
{
    "images": [
        {
            "status": "active",
            "name": "cirros-0.3.2-x86_64-disk",
            "tags": [],
            "container_format": "bare",
            "created_at": "2014-11-07T17:07:06Z",
            "disk_format": "qcow2",
            "updated_at": "2014-11-07T17:19:09Z",
            "visibility": "public",
            "self": "/v2/images/1bea47ed-f6a9-463b-b423-14b9cca9ad27",
            "min_disk": 0,
            "protected": false,
            "id": "1bea47ed-f6a9-463b-b423-14b9cca9ad27",
            "file": "/v2/images/1bea47ed-f6a9-463b-b423-14b9cca9ad27/file",
            "checksum": "64d7c1cd2b6f60c92c14662941cb7913",
            "os_hash_algo": "sha512",
            "os_hash_value": "073b4523583784fbe01daff81eba092a262ec37ba6d04dd3f52e4cd5c93eb8258af44881345ecda0e49f3d8cc6d2df6b050ff3e72681d723234aff9d17d0cf09",
            "os_hidden": false,
            "owner": "5ef70662f8b34079a6eddb8da9d75fe8",
            "size": 13167616,
            "min_ram": 10,
            "schema": "/v2/schemas/image",
            "virtual_size": null
        },
        {
            "status": "active",
            "name": "F17-x86_64-cfntools",
            "tags": [],
            "container_format": "bare",
            "created_at": "2014-10-30T08:23:39Z",
            "disk_format": "qcow2",
            "updated_at": "2014-11-03T16:40:10Z",
            "visibility": "public",
            "self": "/v2/images/781b3762-9469-4cec-b58d-3349e5de4e9c",
            "min_disk": 0,
            "protected": false,
            "id": "781b3762-9469-4cec-b58d-3349e5de4e9c",
            "file": "/v2/images/781b3762-9469-4cec-b58d-3349e5de4e9c/file",
            "checksum": "afab0f79bac770d61d24b4d0560b5f70",
            "os_hash_algo": "sha512",
            "os_hash_value": "ea3e20140df1cc65f53d4c5b9ee3b38d0d6868f61bbe2230417b0f98cef0e0c7c37f0ebc5c6456fa47f013de48b452617d56c15fdba25e100379bd0e81ee15ec",
            "os_hidden": false,
            "owner": "5ef70662f8b34079a6eddb8da9d75fe8",
            "size": 476704768,
            "min_ram": 10,
            "schema": "/v2/schemas/image",
            "virtual_size": null
        }
    ],
    "schema": "/v2/schemas/images",
    "first": "/v2/images"
}
`)

var testImages = []byte(`
	[
		{
			"id": "1bea47ed-f6a9-463b-b423-14b9cca9ad27",
			"name": "cirros-0.3.2-x86_64-disk",
			"disk_format": "qcow2",
			"container_format": "bare",
			"min_disk": 0,
			"min_ram": 10,
			"size": 13167616,
			"checksum": "64d7c1cd2b6f60c92c14662941cb7913",
			"status": "active",
			"visibility": "public",
			"protected": false,
			"Tags": []
		},
		{
			"id": "781b3762-9469-4cec-b58d-3349e5de4e9c",
			"name": "F17-x86_64-cfntools",
			"disk_format": "qcow2",
			"container_format": "bare",
			"min_disk": 0,
			"min_ram": 10,
			"size": 476704768,
			"checksum": "afab0f79bac770d61d24b4d0560b5f70",
			"status": "active",
			"visibility": "public",
			"protected": false,
			"tags": []
		}
	]
`)

var testProjects = []byte(`
	[
		{
			"ID":          "abcdef12-1111-2222-aaaa-abcdefabcdef",
			"Name":        "project1",
			"Description": "first project",
			"DomainID":    "dddddd12-1111-2222-aaaa-abcdefabcdef",
			"Enabled":     true,
			"IsDomain":    false,
			"ParentID":    "ffffeedd-1111-2222-aaaa-abcdefabcdef"
		},
		{
			"ID":          "abcdef12-1111-3333-aaaa-abcdefabcdef",
			"Name":        "project2",
			"Description": "second project",
			"DomainID":    "dddddd12-1111-3333-aaaa-abcdefabcdef",
			"Enabled":     true,
			"IsDomain":    false,
			"ParentID":    "ffffeedd-1111-3333-aaaa-abcdefabcdef"
		},
		{
			"ID":          "abcdef12-1111-4444-aaaa-abcdefabcdef",
			"Name":        "project3",
			"Description": "third project",
			"DomainID":    "dddddd12-1111-4444-aaaa-abcdefabcdef",
			"Enabled":     true,
			"IsDomain":    false,
			"ParentID":    "ffffeedd-1111-4444-aaaa-abcdefabcdef"
		}
	]
`)

func openstackAdapterTestSetup() (OpenStackAdapter, *mocks.OpenStackCLI, *mocks.OpenStackHTTPClient, *cache.Cache) {
	var err error
	cli := new(mocks.OpenStackCLI)
	http := new(mocks.OpenStackHTTPClient)
	rCache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,   // Far more than we will probably need.
		MaxCost:     2e9,   // MaxCost of the cache. Assume this is bytes.
		BufferItems: 64,    // The recommended default value, according to the docs.
		Metrics:     false, // This is the default, but make it explicit. Don't want the overhead.
	})
	if err != nil {
		log.Fatalf("Unable to create test cache: %s\n", err)
	}
	rStore := store.NewRistretto(rCache, &store.Options{
		Expiration: 5 * time.Minute,
	})
	cacheMgr := cache.New(rStore)

	adapter := OpenStackAdapter{
		cli:  cli,
		http: http,
		cache: openstackCache{
			baseEnv: testEnv,
			cache:   cacheMgr,
			ttl:     5 * time.Minute,
		},
		baseEnv: testEnv,
	}
	return adapter, cli, http, cacheMgr
}

func checkCache(cacheMgr *cache.Cache, key string) (interface{}, error) {
	// The cache used for unit tests (ristretto) has some timing problem, the write operation is not immediately visible to read operation.
	// Thus, this does some polling  to check whether a cache is actually set or not.
	var result interface{}
	var err error
	for i := 0; i < 20; i++ {
		result, err = cacheMgr.Get(key)
		if err == nil {
			return result, err
		}
		time.Sleep(time.Millisecond * 100)
	}
	return result, err
}

func TestOpenStackCLIListApplicationCredentials(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListApplicationCredentials", ctx, testEnv).Return(testApplicationCredentials, nil)
	_, err := adapter.ListApplicationCredentials(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
}

func TestOpenStackCLICheckApplicationCredentialID(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListApplicationCredentials", ctx, testEnv).Return(testApplicationCredentials, nil)
	applicationCredentials, err := adapter.ListApplicationCredentials(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, applicationCredential := range applicationCredentials {
		if applicationCredential.ID == "00000000-1111-2222-bbbb-abcdefabcdef" {
			found = true
		}
	}

	if !found {
		t.Error("ApplicationCredential ID 00000000-1111-2222-bbbb-abcdefabcdef was not found in results")
	}
}

func TestOpenStackCLICheckApplicationCredentialCache(t *testing.T) {
	adapter, cli, _, cacheMgr := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListApplicationCredentials", ctx, testEnv).Return(testApplicationCredentials, nil)
	_, err := adapter.ListApplicationCredentials(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	key := GetCacheKey("ListApplicationCredentials", "test-username", "placeholder")
	assert.NoError(t, err)
	cachedResult, err := checkCache(cacheMgr, key)
	assert.NoError(t, err, "results were not found in the cache")
	assert.Equal(t, testApplicationCredentials, []byte(cachedResult.(string)))
}

func TestOpenStackCLIGetApplicationCredential(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	id := "00000000-1111-2222-bbbb-abcdefabcdef"

	cli.On("ListApplicationCredentials", ctx, testEnv).Return(testApplicationCredentials, nil)
	applicationCredential, err := adapter.GetApplicationCredential(ctx, id)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, id, applicationCredential.ID)
}

func TestOpenStackCLIListFlavors(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("ListFlavors", ctxWithRegion, testEnv).Return(testFlavors, nil)
	_, err = adapter.ListFlavors(ctx, region)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
}

func TestOpenStackCLICheckFlavorID(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("ListFlavors", ctxWithRegion, testEnv).Return(testFlavors, nil)
	flavors, err := adapter.ListFlavors(ctx, region)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, flavor := range flavors {
		if flavor.ID == "08daa0be-c4a7-11eb-8529-0242ac130003" {
			found = true
		}
	}

	if !found {
		t.Error("flavor ID 08daa0be-c4a7-11eb-8529-0242ac130003 was not found in results")
	}
}

func TestOpenStackCLICheckFlavorCache(t *testing.T) {
	adapter, cli, _, cacheMgr := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("ListFlavors", ctxWithRegion, testEnv).Return(testFlavors, nil)
	_, err = adapter.ListFlavors(ctx, region)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	key := GetCacheKeyRegional("ListFlavors", "test-username", "placeholder", "region-123")
	assert.NoError(t, err)
	cachedResult, err := checkCache(cacheMgr, key)
	assert.NoError(t, err, "results were not found in the cache")
	assert.Equal(t, testFlavors, []byte(cachedResult.(string)))
}

func TestOpenStackCLIGetFlavor(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}
	id := "000d1872-c4a7-11eb-8529-0242ac130003"

	cli.On("ListFlavors", ctxWithRegion, testEnv).Return(testFlavors, nil)
	flavor, err := adapter.GetFlavor(ctx, id, region)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, id, flavor.ID)
}

func TestOpenStackCLICheckImageID(t *testing.T) {
	adapter, cli, http, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("GetCatalogEntry", ctxWithRegion, testEnv, "glance").Return(testCatalogEntry, nil)
	cli.On("GetToken", ctxWithRegion, testEnv).Return(testToken, nil)
	httpRequest := httptest.NewRequest("GET", "https://cyverse.org/v2/images", nil)
	if err != nil {
		panic(err)
	}
	http.On("NewRequestWithContext", context.Background(), "GET", "https://cyverse.org/v2/images", nil).Return(httpRequest, nil)
	httpResponse := httptest.NewRecorder()
	httpResponse.Body = bytes.NewBuffer(testImagesResponse)
	http.On("Do", httpRequest).Return(httpResponse.Result(), nil)
	images, err := adapter.ListImages(ctx, region)
	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, image := range images {
		if image.ID == "1bea47ed-f6a9-463b-b423-14b9cca9ad27" {
			found = true
		}
	}

	assert.True(t, found, "image ID 1bea47ed-f6a9-463b-b423-14b9cca9ad27 was not found in results")
}

func TestOpenStackCLICheckMinRam(t *testing.T) {
	adapter, cli, http, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("GetCatalogEntry", ctxWithRegion, testEnv, "glance").Return(testCatalogEntry, nil)
	cli.On("GetToken", ctxWithRegion, testEnv).Return(testToken, nil)
	httpRequest := httptest.NewRequest("GET", "https://cyverse.org/v2/images", nil)
	if err != nil {
		panic(err)
	}
	http.On("NewRequestWithContext", context.Background(), "GET", "https://cyverse.org/v2/images", nil).Return(httpRequest, nil)
	httpResponse := httptest.NewRecorder()
	httpResponse.Body = bytes.NewBuffer(testImagesResponse)
	http.On("Do", httpRequest).Return(httpResponse.Result(), nil)
	images, err := adapter.ListImages(ctx, region)
	assert.NoError(t, err)
	cli.AssertExpectations(t)
	// check if the Min RAM is set to 10 for the image with ID 1bea47ed-f6a9-463b-b423-14b9cca9ad27
	for _, image := range images {
		if image.ID == "1bea47ed-f6a9-463b-b423-14b9cca9ad27" {
			assert.Equal(t, image.MinRAM, int64(10))

		}
	}
}

func TestOpenStackCLICheckImageCache(t *testing.T) {
	adapter, cli, http, cacheMgr := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredential(ctx, &types.Credential{
		ID:           "placeholder",
		OpenStackEnv: map[string]string{},
	})
	region := "region-123"
	ctxWithRegion, err := injectRegion(ctx, region)
	if err != nil {
		panic(err)
	}

	cli.On("GetCatalogEntry", ctxWithRegion, testEnv, "glance").Return(testCatalogEntry, nil)
	cli.On("GetToken", ctxWithRegion, testEnv).Return(testToken, nil)
	httpRequest := httptest.NewRequest("GET", "https://cyverse.org/v2/images", nil)
	if err != nil {
		panic(err)
	}
	http.On("NewRequestWithContext", context.Background(), "GET", "https://cyverse.org/v2/images", nil).Return(httpRequest, nil)
	httpResponse := httptest.NewRecorder()
	// we need to cast testImagesResponse to buffer to get the bytes
	httpResponse.Body = bytes.NewBuffer(testImagesResponse)
	http.On("Do", httpRequest).Return(httpResponse.Result(), nil)
	_, err = adapter.ListImages(ctx, region)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	key := GetCacheKeyRegional("ListImages", "test-username", "placeholder", "region-123")
	assert.NoError(t, err)
	cachedResult, err := checkCache(cacheMgr, key)
	assert.NoError(t, err, "results were not found in the cache")
	var images []providers.OpenStackImage
	json.Unmarshal(testImages, &images)
	testImages, err = json.Marshal(images)
	assert.NoError(t, err)
	assert.Equal(t, testImages, []byte(cachedResult.(string)))
}

func TestOpenStackCLIListProjects(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListProjects", ctx, testEnv).Return(testProjects, nil)
	_, err := adapter.ListProjects(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
}

func TestOpenStackCLICheckProjectID(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListProjects", ctx, testEnv).Return(testProjects, nil)
	projects, err := adapter.ListProjects(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	found := false
	for _, project := range projects {
		if project.ID == "abcdef12-1111-3333-aaaa-abcdefabcdef" {
			found = true
		}
	}

	if !found {
		t.Error("Project ID abcdef12-1111-3333-aaaa-abcdefabcdef was not found in results")
	}
}

func TestOpenStackCLICheckProjectCache(t *testing.T) {
	adapter, cli, _, cacheMgr := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")

	cli.On("ListProjects", ctx, testEnv).Return(testProjects, nil)
	_, err := adapter.ListProjects(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)

	key := GetCacheKey("ListProjects", "test-username", "placeholder")
	assert.NoError(t, err)
	cachedResult, err := checkCache(cacheMgr, key)
	assert.NoError(t, err, "results were not found in the cache")
	assert.Equal(t, testProjects, []byte(cachedResult.(string)))
}

func TestOpenStackCLIGetProject(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	id := "abcdef12-1111-3333-aaaa-abcdefabcdef"

	cli.On("ListProjects", ctx, testEnv).Return(testProjects, nil)
	project, err := adapter.GetProject(ctx, id)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, id, project.ID)
}

func TestOpenStackCLIListRegions(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	expectedRegion := []providers.Region{
		{
			ID:   "FOO",
			Name: "BAR",
		},
	}
	mockReturn := []byte(`{
  "endpoints": [
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "public",
      "region_id": "FOO",
      "url": "https://cyverse.org",
      "region": "BAR"
    }
  ],
  "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
  "name": "nova",
  "type": "compute"
}`)

	cli.On("GetCatalogEntry", ctx, testEnv, "nova").Return(mockReturn, nil)
	regions, err := adapter.ListRegions(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, expectedRegion, regions)
}

func TestOpenStackCLIListRegionsMultiple(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	expectedRegion := []providers.Region{
		{
			ID:   "FOO",
			Name: "BAR",
		},
		{
			ID:   "FOO1",
			Name: "BAR1",
		},
	}
	mockReturn := []byte(`{
  "endpoints": [
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "public",
      "region_id": "FOO",
      "url": "https://cyverse.org",
      "region": "BAR"
    },
    {
      "id": "cccccccccccccccccccccccccccccccc",
      "interface": "public",
      "region_id": "FOO1",
      "url": "https://cyverse.org",
      "region": "BAR1"
    }
  ],
  "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
  "name": "nova",
  "type": "compute"
}`)

	cli.On("GetCatalogEntry", ctx, testEnv, "nova").Return(mockReturn, nil)
	regions, err := adapter.ListRegions(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Equal(t, expectedRegion, regions)
}

func TestOpenStackCLIListRegionsNoPublicInterface(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	mockReturn := []byte(`{
  "endpoints": [
    {
      "id": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "interface": "admin",
      "region_id": "FOO",
      "url": "https://cyverse.org",
      "region": "BAR"
    }
  ],
  "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
  "name": "nova",
  "type": "compute"
}`)

	cli.On("GetCatalogEntry", ctx, testEnv, "nova").Return(mockReturn, nil)
	regions, err := adapter.ListRegions(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Nil(t, regions)
}

func TestOpenStackCLIListRegionsNoEndpoint(t *testing.T) {
	adapter, cli, _, _ := openstackAdapterTestSetup()
	ctx := types.WithUsername(context.Background(), "test-username")
	ctx = types.WithCredentialID(ctx, "placeholder")
	mockReturn := []byte(`{
  "endpoints": [],
  "id": "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
  "name": "nova",
  "type": "compute"
}`)

	cli.On("GetCatalogEntry", ctx, testEnv, "nova").Return(mockReturn, nil)
	regions, err := adapter.ListRegions(ctx)

	assert.NoError(t, err)
	cli.AssertExpectations(t)
	assert.Nil(t, regions)
}

// This is to test if the auth error returned by openstack cli is what we expected.
// This test requires python openstack cli.
func TestOpenStackAdapter_AuthenticationTest(t *testing.T) {
	if val, ok := os.LookupEnv("CI_INTEGRATION_OPENSTACK_CLI"); !ok || val != "true" {
		t.Skipf("CI_INTEGRATION_OPENSTACK_CLI not true, skip this integration test")
		return
	}
	type args struct {
		OpenStackEnv types.Environment
	}
	tests := []struct {
		name    string
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "non-existing app cred ID",
			args: args{
				OpenStackEnv: map[string]string{
					"OS_IDENTITY_API_VERSION":          "3",
					"OS_AUTH_URL":                      "https://js2.jetstream-cloud.org:5000/v3/",
					"OS_AUTH_TYPE":                     "v3applicationcredential",
					"OS_APPLICATION_CREDENTIAL_ID":     "foooooooooooo",
					"OS_APPLICATION_CREDENTIAL_SECRET": "barrrrrrrrr",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				assert.Equal(t, err, types.ErrApplicationCredentialNotFound)
				return false
			},
		},
		{
			name: "non-existing app cred name, no username",
			args: args{
				OpenStackEnv: map[string]string{
					"OS_IDENTITY_API_VERSION":          "3",
					"OS_AUTH_URL":                      "https://js2.jetstream-cloud.org:5000/v3/",
					"OS_AUTH_TYPE":                     "v3applicationcredential",
					"OS_APPLICATION_CREDENTIAL_NAME":   "foooooooooooo",
					"OS_APPLICATION_CREDENTIAL_SECRET": "barrrrrrrrr",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "Expecting to find user in application credential")
				return true
			},
		},
		{
			name: "non-existing app cred name, username, no domain",
			args: args{
				OpenStackEnv: map[string]string{
					"OS_IDENTITY_API_VERSION":          "3",
					"OS_AUTH_URL":                      "https://js2.jetstream-cloud.org:5000/v3/",
					"OS_AUTH_TYPE":                     "v3applicationcredential",
					"OS_USERNAME":                      "foobar",
					"OS_APPLICATION_CREDENTIAL_NAME":   "foooooooooooo",
					"OS_APPLICATION_CREDENTIAL_SECRET": "barrrrrrrrr",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "You have provided a username. In the V3 identity API a username is only unique within a domain so you must also provide either a user_domain_id or user_domain_name")
				return true
			},
		},
		{
			name: "non-existing app cred name, username, domain",
			args: args{
				OpenStackEnv: map[string]string{
					"OS_IDENTITY_API_VERSION":          "3",
					"OS_AUTH_URL":                      "https://js2.jetstream-cloud.org:5000/v3/",
					"OS_AUTH_TYPE":                     "v3applicationcredential",
					"OS_USERNAME":                      "foobar",
					"OS_USER_DOMAIN_NAME":              "foobar_domain",
					"OS_APPLICATION_CREDENTIAL_NAME":   "foooooooooooo",
					"OS_APPLICATION_CREDENTIAL_SECRET": "barrrrrrrrr",
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				assert.Error(t, err)
				assert.Equal(t, err, types.ErrAuthentication)
				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			osAdapter := NewOpenStackAdapter(time.Second, nil)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
			defer cancel()
			credCtx := types.WithCredential(ctx, &types.Credential{
				ID:           "",
				CreatedAt:    time.Time{},
				UpdatedAt:    time.Time{},
				OpenStackEnv: tt.args.OpenStackEnv,
			})
			tt.wantErr(t, osAdapter.AuthenticationTest(credCtx), fmt.Sprintf("AuthenticationTest(%v)", credCtx))
		})
	}
}

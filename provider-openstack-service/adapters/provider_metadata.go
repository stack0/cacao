package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderMetadataClient ...
type ProviderMetadataClient struct {
	NATS messaging.NatsConfig
	STAN messaging.StanConfig
}

// NewProviderMetadataClient ...
func NewProviderMetadataClient(NATS messaging.NatsConfig, STAN messaging.StanConfig) *ProviderMetadataClient {
	return &ProviderMetadataClient{
		NATS: NATS,
		STAN: STAN,
	}
}

// GetMetadata fetches metadata for a provider
func (svc ProviderMetadataClient) GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error) {
	metadataSvcClient, err := service.NewNatsProviderClient(ctx, actor, emulator, svc.NATS, svc.STAN)
	if err != nil {
		return nil, err
	}
	provider, err := metadataSvcClient.Get(providerID)
	if err != nil {
		return nil, err
	}
	return provider.GetMetadata(), nil
}

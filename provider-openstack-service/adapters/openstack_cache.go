package adapters

import (
	"context"
	"fmt"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"time"
)

// the caching layer for cli operations.
// cache is keyed with prefix, username, and credential ID. Using token auth skip the cache.
// cache invalidation is done via tags.
type openstackCache struct {
	baseEnv types.Environment
	cache   cache.CacheInterface
	ttl     time.Duration
}

// do the operation if no cache is found (or if cache is skipped)
func (o *openstackCache) doIfNoCache(ctx context.Context, logger *log.Entry, cachePrefix types.CachePrefix, do func(context.Context, types.Environment) ([]byte, error), force bool) ([]byte, error) {
	var (
		outputStr string
		err       error
		credID    string
	)
	o.invalidateCacheIfForced(ctx, logger, cachePrefix, force)

	credID = types.CredentialIDFromCtx(ctx)
	if o.usingTokenAuth(credID) {
		// if the operation does not use credential id but other ways of specifying credential (e.g. token), then ignore cache, since cache key cannot be computed with empty credential ID
		// TODO consider using hash of Environment from context (which contains the credential) as part of cache key
		logger.Trace("token auth, skip cache")
		return do(ctx, o.baseEnv)
	}
	username := types.UsernameFromCtx(ctx)
	cacheKey := GetCacheKey(cachePrefix, username, credID)
	if !force {
		var value interface{}
		value, err = o.cache.Get(cacheKey)
		if err == nil {
			// cache hit
			outputStr = value.(string)
			return []byte(outputStr), nil
		}
	}

	logger.WithError(err).Debug("cache miss")
	outputBytes, err2 := do(ctx, o.baseEnv)
	if err2 != nil {
		return nil, err2
	}
	outputStr = string(outputBytes)
	err = o.cache.Set(cacheKey, outputStr, &store.Options{
		Cost:       int64(len(outputBytes)),
		Expiration: o.ttl,
		Tags:       GetCacheTags(cachePrefix, username, credID),
	})
	if err != nil {
		logger.WithError(err).Error("fail to set cache")
	}
	return outputBytes, nil
}

// do the operation if no cache is found (or if cache is skipped). the operation is regional specific, meaning that the
// result for different regions are cached separately.
func (o *openstackCache) doRegionalOperationIfNoCache(ctx context.Context, logger *log.Entry, cachePrefix types.CachePrefix, region string, do func(context.Context, types.Environment) ([]byte, error), force bool) ([]byte, error) {
	var (
		outputStr string
		err       error
		credID    string
	)
	o.invalidateCacheIfForced(ctx, logger, cachePrefix, force)

	credID = types.CredentialIDFromCtx(ctx)
	if o.usingTokenAuth(credID) {
		// if the operation does not use credential id but other ways of specifying credential (e.g. token), then ignore cache, since cache key cannot be computed with empty credential ID
		// TODO consider using hash of Environment from context (which contains the credential) as part of cache key
		logger.Trace("token auth, skip cache")
		return do(ctx, o.baseEnv)
	}
	username := types.UsernameFromCtx(ctx)
	cacheKey := GetCacheKeyRegional(cachePrefix, username, credID, region)
	if !force {
		var value interface{}
		value, err = o.cache.Get(cacheKey)
		if err == nil {
			// cache hit
			outputStr = value.(string)
			return []byte(outputStr), nil
		}
	}

	logger.WithError(err).Debug("cache miss")
	outputBytes, err2 := do(ctx, o.baseEnv)
	if err2 != nil {
		return nil, err2
	}
	outputStr = string(outputBytes)
	err = o.cache.Set(cacheKey, outputStr, &store.Options{
		Cost:       int64(len(outputBytes)),
		Expiration: o.ttl,
		Tags:       GetCacheTags(cachePrefix, username, credID),
	})
	if err != nil {
		logger.WithError(err).Error("fail to set cache")
	}
	return outputBytes, nil
}

func (o *openstackCache) invalidateCacheIfForced(ctx context.Context, logger *log.Entry, cachePrefix types.CachePrefix, force bool) {
	username := types.UsernameFromCtx(ctx)
	if force {
		logger2 := logger.WithFields(log.Fields{
			"prefix":   cachePrefix,
			"username": username,
		})
		// invalidate all cache entries w/ the same prefix for user
		err := o.invalidateCache(cachePrefix, username)
		if err != nil {
			// log and proceed
			logger2.WithError(err).Errorf("fail to invalidate cache")
		} else {
			logger2.Debug("cache invalidated")
		}
	}
}

func (o *openstackCache) usingTokenAuth(credID string) bool {
	return len(credID) == 0
}

func (o *openstackCache) invalidateCache(prefix types.CachePrefix, username string) error {
	return o.cache.Invalidate(store.InvalidateOptions{Tags: []string{
		getPrefixCacheTag(prefix),
		getUsernameCacheTag(username),
	}})
}

// GetCacheKey accepts a prefix and generates a key that can be used to retrieve a cached value for non-regional
// operation. By convention, this prefix should match the function which is calling GetCacheKey.
func GetCacheKey(prefix types.CachePrefix, username string, credID string) string {
	return fmt.Sprintf("%s.%s.%s", prefix, username, credID)
}

// GetCacheKeyRegional accepts a prefix and generates a key that can be used to retrieve a cached value for a regional operation.
// By convention, this prefix should match the function which is calling GetCacheKeyRegional.
func GetCacheKeyRegional(prefix types.CachePrefix, username string, credID string, region string) string {
	return fmt.Sprintf("%s.%s.%s.%s", prefix, username, credID, region)
}

// GetCacheTags returns the cache tags for a cached value. The tags are used for invalidate values.
func GetCacheTags(prefix types.CachePrefix, username string, credID string) []string {
	return []string{getPrefixCacheTag(prefix), getUsernameCacheTag(username), getCredIDCacheTag(credID)}
}

func getPrefixCacheTag(prefix types.CachePrefix) string {
	return string(prefix)
}

func getUsernameCacheTag(username string) string {
	// add a prefix to prevent collision with other tag, since username is user supplied
	return "username-" + username
}

func getCredIDCacheTag(credID string) string {
	// add a prefix to prevent collision with other tag, since cred ID is user supplied
	return "credid-" + credID
}

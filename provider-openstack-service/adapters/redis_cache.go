package adapters

import (
	"context"
	"strconv"
	"time"

	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// This only exists because the RedisStore in gocache doesn't correctly
// implement the store.StoreInterface. Hopefully we'll be able to nuke
//  it after a future update.

// NewRedisCacheManager ...
func NewRedisCacheManager(config types.Configuration) *cache.ChainCache {
	dbNum, err := strconv.ParseInt(config.RedisDB, 10, 32)
	if err != nil {
		log.WithError(err).Fatal("fail to parse redis db number")
	}

	redisStore := &ProviderRedisStore{
		store: store.NewRedis(redis.NewClient(&redis.Options{
			Addr:     config.RedisAddress,
			Password: config.RedisPassword,
			DB:       int(dbNum),
		}), &store.Options{
			Expiration: time.Second * time.Duration(config.CacheTTL),
		}),
	}

	// We'll use a chain here since we might want to add multiple caching tiers later.
	return cache.NewChain(
		cache.New(redisStore),
	)
}

// ProviderRedisStore fixes an issue in the store.RedisStore compliance
// with the store.StoreInterface.
type ProviderRedisStore struct {
	store *store.RedisStore
}

// Get complies with the store.StoreInterface.
func (m *ProviderRedisStore) Get(key interface{}) (interface{}, error) {
	return m.store.Get(context.Background(), key)
}

// GetWithTTL complies with the store.StoreInterface.
func (m *ProviderRedisStore) GetWithTTL(key interface{}) (interface{}, time.Duration, error) {
	return m.store.GetWithTTL(context.Background(), key)
}

// Set complies with the store.StoreInterface.
func (m *ProviderRedisStore) Set(key interface{}, value interface{}, options *store.Options) error {
	return m.store.Set(context.Background(), key, value, options)
}

// Delete complies with the store.StoreInterface.
func (m *ProviderRedisStore) Delete(key interface{}) error {
	return m.store.Delete(context.Background(), key)
}

// Invalidate complies with the store.StoreInterface.
func (m *ProviderRedisStore) Invalidate(options store.InvalidateOptions) error {
	return m.store.Invalidate(context.Background(), options)
}

// Clear complies with the store.StoreInterface.
func (m *ProviderRedisStore) Clear() error {
	return m.store.Clear(context.Background())
}

// GetType complies with the store.StoreInterface.
func (m *ProviderRedisStore) GetType() string {
	return m.store.GetType()
}

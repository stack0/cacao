# provider-openstack-service

This service provides access to some OpenStack operations.

## Queries
| query subject         | description                                                                              |
|-----------------------|------------------------------------------------------------------------------------------|
| "appcredentials.list" |                                                                                          |
| "appcredentials.get"  |                                                                                          |
| "authentication.test" | test if an OpenStack credential works (able to authenticate with the OpenStack provider) |
| "images.list"         | List available images in the OpenStack cluster                                           |
| "images.get"          | Get the details for a single image, denoted by its UUID within  the OpenStack cluster    |
| "flavors.list"        | List the available flavors in the OpenStack cluster                                      |
| "flavors.get"         | Get the details for a single flavor, denoted by its UUID within  the OpenStack cluster   |
| "projects.list"       |                                                                                          |
| "projects.get"        |                                                                                          |
| "regions.list"        |                                                                                          |
| "catalog.list"        |                                                                                          |
| "token.get"           | issue a new token                                                                        |

## Events
| event type                                                          | description                                                                       |
|---------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| org.cyverse.events.OpenStackApplicationCredentialsCreationRequested | create a new application credential, credential is saved as a credential in CACAO |
| org.cyverse.events.OpenStackApplicationCredentialsDeletionRequested | delete a application credential pointed by a CACAO credential                     |

## Building

To build this microservice, you'll need to have a reasonably up-to-date version
of Go installed. Then run the following in the top-level directory containing
the service code:

```
go build
```

The service should build with that single step. If that is not the case, please
file an issue.

This service uses Go modules.

## Configuration

The service is configured through a set of environment variables. Here is the
important subset of the variables:

| env name                      | description                                                                              | default          |
|-------------------------------|------------------------------------------------------------------------------------------|------------------|
| `DEFAULT_CHANNEL_BUFFER_SIZE` | default channel buffer size, the number of messages(event & query) the is kept in buffer | 1                |
| `QUERY_WORKER_COUNT`          | query worker count (number of go routine listens for query request)                      | 10               |
| `POD_NAME`                    | name of the pod, used as NATS client ID if set                                           |
| `LOG_LEVEL`                   | log level                                                                                | debug            |
| `REDIS_ADDRESS`               | address of the redis cache                                                               | redis:6379       |
| `REDIS_PASSWORD`              |                                                                                          | ""               |
| `REDIS_DB`                    | database number, must be parsable as int                                                 | "0"              |
| `CACHE_TTL`                   | TTL for cache entries for result of operations                                           |
| `NATS_URL`                    | -                                                                                        | nats://nats:4222 |
| `NATS_QUEUE_GROUP`            | -                                                                                        |                  |
| `NATS_WILDCARD_SUBJECT`       | Should be set to  `cacao.openstack.>`                                                    | cyverse.>        |
| `NATS_CLIENT_ID`              | -                                                                                        |                  |
| `NATS_MAX_RECONNECTS`         | -                                                                                        | -1               |
| `NATS_RECONNECT_WAIT`         | -                                                                                        | -1               |
| `NATS_REQUEST_TIMEOUT`        | -                                                                                        | -1               |


## Credentials Access

Openstack operations requires authentication. The credential can be specified in 2 ways in the request:
- OpenStack token
- Credential ID

OpenStack token can be project scoped or unscoped. And the token (string) is included in the request.

Credential ID refers to the ID of a credential object stored in credential service. Its value field must be a JSON string similar to the following examples:

example username/password credential:
```json
{
	"OS_AUTH_URL":             "https://example.com:5000/v3",
	"OS_PROJECT_ID":           "replaceme",
	"OS_PROJECT_NAME":         "replaceme",
	"OS_PROJECT_DOMAIN_ID":    "replaceme",
	"OS_USER_DOMAIN_NAME":     "replaceme",
	"OS_USERNAME":             "replaceme",
	"OS_PASSWORD":             "replaceme",
	"OS_REGION_NAME":          "replaceme",
	"OS_INTERFACE":            "public",
	"OS_IDENTITY_API_VERSION": "3"
}
```

example application credential:
```json
{
	"OS_AUTH_URL":                      "https://example.com:5000/v3",
	"OS_REGION_NAME":                   "replaceme",
	"OS_INTERFACE":                     "public",
	"OS_IDENTITY_API_VERSION":          "3",
	"OS_APPLICATION_CREDENTIAL_ID":     "replaceme",
    "OS_APPLICATION_CREDENTIAL_SECRET": "replaceme"
}
```

package main

import (
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-openstack-service/adapters"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

var config types.Configuration

func init() {
	var err error

	if err = envconfig.Process("", &config); err != nil {
		log.Fatal(err.Error())
	}
	logLevel, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)

	if err = config.Validate(); err != nil {
		log.Fatal(err)
	}
	config.NATS.WildcardSubject = "cyverse.providers.openstack.>"
}

func main() {
	osAdapter := adapters.NewOpenStackAdapter(time.Second*time.Duration(config.CacheTTL), adapters.NewRedisCacheManager(config))

	var osQueryAdapter adapters.QueryAdapter
	osQueryAdapter.Init(config)

	credMS := adapters.NewCredentialMicroservice(config.NATS, config.STAN)

	var osEventAdapter adapters.EventAdapter
	osEventAdapter.Init(config)

	providerMS := adapters.NewProviderMetadataClient(config.NATS, config.STAN)

	d := domain.NewOpenStackDomain(&config, &osEventAdapter, &osQueryAdapter, &osEventAdapter, osAdapter, credMS, providerMS, &adapters.UTCTime{})
	d.Start()
}

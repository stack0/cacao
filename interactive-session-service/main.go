package main

import (
	"sync"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao/interactive-session-service/adapters"
	"gitlab.com/cyverse/cacao/interactive-session-service/domain"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

func main() {
	var envConfig types.EnvConfig
	err := envconfig.Process("", &envConfig)
	if err != nil {
		log.Fatal(err.Error())
	}
	var config types.Config
	config, err = types.FromEnvConfig(envConfig)
	if err != nil {
		log.Fatal(err.Error())
	}

	// create an initial Domain object
	var dmain domain.Domain
	dmain.Init(&config)
	// init port implementations
	queryInImpl := &domain.QueryPortImpl{}
	queryInImpl.Init(&config)
	dmain.QueryIn = queryInImpl

	eventInImpl := &domain.EventPortImpl{}
	eventInImpl.Init(&config)
	dmain.EventIn = eventInImpl

	// add and initialize the storage adapter
	mongoAdapter := &adapters.MongoAdapter{}
	mongoAdapter.Init(&config)
	dmain.Storage = mongoAdapter

	// add and initialize the session adapter
	guacamoleAdapter := &adapters.GuacamoleAdapter{}
	guacamoleAdapter.Init(&config)
	dmain.SessionSetup = guacamoleAdapter

	// add and initialize the query adapter
	queryAdapter := &adapters.QueryAdapter{}
	queryAdapter.Init(&config)
	queryAdapter.IncomingPort = queryInImpl

	eventAdapter := &adapters.EventAdapter{}
	eventAdapter.Init(&config)
	eventAdapter.IncomingPort = eventInImpl

	dmain.EventOut = eventAdapter

	// start
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		dmain.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		queryAdapter.Start()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		eventAdapter.Start()
	}()

	wg.Wait()

	// finalize
	eventAdapter.Finalize()
	queryAdapter.Finalize()

	eventInImpl.Finalize()
	queryInImpl.Finalize()

	dmain.Finalize()
}

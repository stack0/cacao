package domain

import (
	"time"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// EventPortImpl implements IncomingEventPort
type EventPortImpl struct {
	Config     *types.Config
	Channel    chan types.InteractiveSessionChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *EventPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *EventPortImpl) Finalize() {
}

// InitChannel ...
func (impl *EventPortImpl) InitChannel(channel chan types.InteractiveSessionChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *EventPortImpl) Start() {

}

// Create creates an interactive session
func (impl *EventPortImpl) Create(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(interactiveSession.ID) == 0 {
		interactiveSession.ID = cacao_common_service.NewInteractiveSessionID()
	}

	if len(interactiveSession.CloudID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: cloud id is empty")
	}

	if len(interactiveSession.InstanceID) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance id is empty")
	}

	if len(interactiveSession.Protocol) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: protocol is empty")
	}

	if len(interactiveSession.InstanceAddress) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}

	if len(interactiveSession.InstanceAdminUsername) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance admin username is empty")
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	interactiveSessionRequest := types.InteractiveSessionChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.InteractiveSession{
			ID:                    interactiveSession.ID,
			Owner:                 actor,
			InstanceID:            interactiveSession.InstanceID,
			InstanceAddress:       interactiveSession.InstanceAddress,
			InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
			CloudID:               interactiveSession.CloudID,
			Protocol:              interactiveSession.Protocol,
			RedirectURL:           interactiveSession.RedirectURL,
			State:                 interactiveSession.State,
			CreatedAt:             now,
			UpdatedAt:             now,
		},
		Operation:     string(cacao_common_service.InteractiveSessionCreateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- interactiveSessionRequest

	// receive response
	interactiveSessionResponse := <-responseChannel
	return interactiveSessionResponse.Error
}

// Deactivate deactivates an interactive session
func (impl *EventPortImpl) Deactivate(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	if len(actor) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(interactiveSession.ID) == 0 {
		interactiveSession.ID = cacao_common_service.NewInteractiveSessionID()
	}

	now := time.Now().UTC()

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	interactiveSessionRequest := types.InteractiveSessionChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.InteractiveSession{
			ID:        interactiveSession.ID,
			Owner:     actor,
			State:     cacao_common_service.InteractiveSessionStateInactive,
			UpdatedAt: now,
		},
		Operation:     string(cacao_common_service.InteractiveSessionDeactivateRequestedEvent),
		TransactionID: transactionID,
		Response:      responseChannel,
	}

	// request to model
	impl.Channel <- interactiveSessionRequest

	// receive response
	interactiveSessionResponse := <-responseChannel
	return interactiveSessionResponse.Error
}

package domain

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Config     *types.Config
	Channel    chan types.InteractiveSessionChannelRequest
	DomainImpl *Domain
}

// Init ...
func (impl *QueryPortImpl) Init(config *types.Config) {
	impl.Config = config
}

// Finalize ...
func (impl *QueryPortImpl) Finalize() {
}

// InitChannel ...
func (impl *QueryPortImpl) InitChannel(channel chan types.InteractiveSessionChannelRequest) {
	impl.Channel = channel
}

// Start ...
func (impl *QueryPortImpl) Start() {

}

// List retrieves active/creating interactive sessions of the user
func (impl *QueryPortImpl) List(actor string, emulator string) ([]types.InteractiveSession, error) {
	if len(actor) == 0 {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	// request to model
	impl.Channel <- types.InteractiveSessionChannelRequest{
		Actor:     actor,
		Emulator:  emulator,
		Data:      types.InteractiveSession{},
		Operation: string(cacao_common_service.InteractiveSessionListQueryOp),
		Response:  responseChannel,
	}

	// receive response
	interactiveSessionResponse := <-responseChannel

	if interactiveSessionResponse.Error != nil {
		return nil, interactiveSessionResponse.Error
	}

	interactiveSessions, ok := interactiveSessionResponse.Data.([]types.InteractiveSession)
	if !ok {
		return nil, cacao_common_service.NewCacaoMarshalError("unable to convert response data into InteractiveSession array")
	}

	return interactiveSessions, nil
}

// Get retrieves the interactive session
func (impl *QueryPortImpl) Get(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(interactiveSessionID) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: interactive session ID is empty")
	}

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	// request to model
	impl.Channel <- types.InteractiveSessionChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.InteractiveSession{
			ID: interactiveSessionID,
		},
		Operation: string(cacao_common_service.InteractiveSessionGetQueryOp),
		Response:  responseChannel,
	}

	// receive response
	interactiveSessionResponse := <-responseChannel

	if interactiveSessionResponse.Error != nil {
		return types.InteractiveSession{}, interactiveSessionResponse.Error
	}

	interactiveSession, ok := interactiveSessionResponse.Data.(types.InteractiveSession)
	if !ok {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into InteractiveSession")
	}

	return interactiveSession, nil
}

// GetByInstanceID retrieves the interactive session by InstanceID
func (impl *QueryPortImpl) GetByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(instanceID) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance ID is empty")
	}

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	// request to model
	impl.Channel <- types.InteractiveSessionChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.InteractiveSession{
			InstanceID: instanceID,
		},
		Operation: string(cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp),
		Response:  responseChannel,
	}

	// receive response
	interactiveSessionResponse := <-responseChannel

	if interactiveSessionResponse.Error != nil {
		return types.InteractiveSession{}, interactiveSessionResponse.Error
	}

	interactiveSession, ok := interactiveSessionResponse.Data.(types.InteractiveSession)
	if !ok {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into InteractiveSession")
	}

	return interactiveSession, nil
}

// GetByInstanceAddress retrieves the interactive session by Instance Address
func (impl *QueryPortImpl) GetByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
	if len(actor) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
	}

	if len(instanceAddress) == 0 {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	// request to model
	impl.Channel <- types.InteractiveSessionChannelRequest{
		Actor:    actor,
		Emulator: emulator,
		Data: types.InteractiveSession{
			InstanceAddress: instanceAddress,
		},
		Operation: string(cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp),
		Response:  responseChannel,
	}

	// receive response
	interactiveSessionResponse := <-responseChannel

	if interactiveSessionResponse.Error != nil {
		return types.InteractiveSession{}, interactiveSessionResponse.Error
	}

	interactiveSession, ok := interactiveSessionResponse.Data.(types.InteractiveSession)
	if !ok {
		return types.InteractiveSession{}, cacao_common_service.NewCacaoMarshalError("unable to convert response data into InteractiveSession")
	}

	return interactiveSession, nil
}

// CheckPrerequisites checks if prerequisites for the interactive session are met
func (impl *QueryPortImpl) CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	if len(protocol) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: protocol is empty")
	}

	if len(instanceAddress) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance address is empty")
	}

	if len(instanceAdminUsername) == 0 {
		return false, cacao_common_service.NewCacaoInvalidParameterError("input validation error: instance admin username is empty")
	}

	responseChannel := make(chan types.InteractiveSessionChannelResponse)

	// request to model
	impl.Channel <- types.InteractiveSessionChannelRequest{
		Data: types.InteractiveSession{
			InstanceAddress:       instanceAddress,
			InstanceAdminUsername: instanceAdminUsername,
			Protocol:              protocol,
		},
		Operation: string(cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp),
		Response:  responseChannel,
	}

	// receive response
	interactiveSessionResponse := <-responseChannel

	if interactiveSessionResponse.Error != nil {
		return false, interactiveSessionResponse.Error
	}

	check, ok := interactiveSessionResponse.Data.(bool)
	if !ok {
		return false, cacao_common_service.NewCacaoMarshalError("unable to convert response data into bool")
	}

	return check, nil
}

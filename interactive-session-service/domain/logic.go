package domain

import (
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// ListInteractiveSessions returns interactive sessions owned by a user
func (d *Domain) ListInteractiveSessions(actor string, emulator string) ([]types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.ListInteractiveSessions",
	})

	logger.Info("List interactive sessions")

	interactiveSessions, err := d.Storage.List(actor)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return interactiveSessions, nil
}

// GetInteractiveSession returns the interactive session with the ID
func (d *Domain) GetInteractiveSession(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.GetInteractiveSession",
	})

	logger.Infof("Get an interactive session %s", interactiveSessionID.String())

	interactiveSession, err := d.Storage.Get(actor, interactiveSessionID)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// GetInteractiveSessionByInstanceID returns the interactive session with the InstanceID
func (d *Domain) GetInteractiveSessionByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.GetInteractiveSessionByInstanceID",
	})

	logger.Infof("Get an interactive session with instanceID %s", instanceID)

	interactiveSession, err := d.Storage.GetByInstanceID(actor, instanceID)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// GetInteractiveSessionByInstanceAddress returns the interactive session with the Instance Address
func (d *Domain) GetInteractiveSessionByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.GetInteractiveSessionByInstanceAddress",
	})

	logger.Infof("Get an interactive session with instance address %s", instanceAddress)

	interactiveSession, err := d.Storage.GetByInstanceAddress(actor, instanceAddress)
	if err != nil {
		logger.Error(err)
		return types.InteractiveSession{}, err
	}

	return interactiveSession, nil
}

// CheckPrerequisitesForInteractiveSession checks if prerequisites for setting up given protocol
func (d *Domain) CheckPrerequisitesForInteractiveSession(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "Domain.CheckPrerequisitesForInteractiveSession",
	})

	logger.Infof("Check prerequisites for setting up %s interactive session on instance address %s, user %s", protocol, instanceAddress, instanceAdminUsername)

	check, err := d.SessionSetup.CheckPrerequisites(protocol, instanceAddress, instanceAdminUsername)
	if err != nil {
		logger.Error(err)
		return false, err
	}

	return check, err
}

// CreateInteractiveSession creates an interactive session
func (d *Domain) CreateInteractiveSession(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "domain.CreateInteractiveSession",
	})

	logger.Infof("Create an interactive session %s for instanceID %s with protocol %s", interactiveSession.ID.String(), interactiveSession.InstanceID, interactiveSession.Protocol)

	// deactivate all existing interactive sessions with the same instance id
	_, err := d.Storage.DeactivateAllByInstanceID(interactiveSession.InstanceID)
	if err != nil {
		createFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: interactiveSession.InstanceID,
			Protocol:   interactiveSession.Protocol,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// put creating state
	nowTime := time.Now().UTC()

	interactiveSession.Owner = actor
	interactiveSession.RedirectURL = ""
	interactiveSession.State = cacao_common_service.InteractiveSessionStateCreating
	interactiveSession.CreatedAt = nowTime
	interactiveSession.UpdatedAt = nowTime

	err = d.Storage.Create(interactiveSession)
	if err != nil {
		createFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: interactiveSession.InstanceID,
			Protocol:   interactiveSession.Protocol,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// setup
	redirectURL, err := d.SessionSetup.Setup(actor, interactiveSession.CloudID, interactiveSession.Protocol, interactiveSession.InstanceAddress, interactiveSession.InstanceAdminUsername)
	if err != nil {
		// update record, change state to failed
		nowTime = time.Now().UTC()
		interactiveSession.State = cacao_common_service.InteractiveSessionStateFailed
		interactiveSession.UpdatedAt = nowTime

		d.Storage.Update(interactiveSession, []string{"state", "updated_at"})

		createFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: interactiveSession.InstanceID,
			Protocol:   interactiveSession.Protocol,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// update record, change state to active
	// fill missing fields
	nowTime = time.Now().UTC()

	interactiveSession.RedirectURL = redirectURL
	interactiveSession.State = cacao_common_service.InteractiveSessionStateActive
	interactiveSession.UpdatedAt = nowTime

	err = d.Storage.Update(interactiveSession, []string{"redirect_url", "state", "updated_at"})
	if err != nil {
		createFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: interactiveSession.InstanceID,
			Protocol:   interactiveSession.Protocol,
		}

		err2 := d.EventOut.CreateFailed(actor, emulator, createFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a CreateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	createdEvent := types.InteractiveSession{
		ID:         interactiveSession.ID,
		Owner:      actor,
		InstanceID: interactiveSession.InstanceID,
		Protocol:   interactiveSession.Protocol,
		CreatedAt:  interactiveSession.CreatedAt,
		UpdatedAt:  interactiveSession.UpdatedAt,
	}

	err = d.EventOut.Created(actor, emulator, createdEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Created event")
	}

	return nil
}

// DeactivateInteractiveSession deactivates an interactive session
func (d *Domain) DeactivateInteractiveSession(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "domain.DeactivateInteractiveSession",
	})

	logger.Infof("Deactivate an interactive session %s", interactiveSession.ID.String())

	existingInteractiveSession, err := d.Storage.Get(actor, interactiveSession.ID)
	if err != nil {
		deactivateFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: interactiveSession.InstanceID,
		}

		err2 := d.EventOut.DeactivateFailed(actor, emulator, deactivateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeactivateFailed event")
		}

		logger.Error(err)
		return err
	}

	nowTime := time.Now().UTC()

	interactiveSession.Owner = actor
	interactiveSession.RedirectURL = ""
	interactiveSession.State = cacao_common_service.InteractiveSessionStateInactive
	interactiveSession.UpdatedAt = nowTime

	err = d.Storage.Update(interactiveSession, []string{"redirect_url", "state", "updated_at"})
	if err != nil {
		deactivateFailedEvent := types.InteractiveSession{
			ID:         interactiveSession.ID,
			Owner:      actor,
			InstanceID: existingInteractiveSession.InstanceID,
			Protocol:   existingInteractiveSession.Protocol,
		}

		err2 := d.EventOut.DeactivateFailed(actor, emulator, deactivateFailedEvent, err, transactionID)
		if err2 != nil {
			logger.WithError(err2).Errorf("failed to send a DeactivateFailed event")
		}

		logger.Error(err)
		return err
	}

	// output event
	deactivatedEvent := types.InteractiveSession{
		ID:         interactiveSession.ID,
		Owner:      actor,
		InstanceID: existingInteractiveSession.InstanceID,
		Protocol:   existingInteractiveSession.Protocol,
		CreatedAt:  existingInteractiveSession.CreatedAt,
		UpdatedAt:  interactiveSession.UpdatedAt,
	}

	err = d.EventOut.Deactivated(actor, emulator, deactivatedEvent, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to send a Deactivated event")
	}

	return nil
}

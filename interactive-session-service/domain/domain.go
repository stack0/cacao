package domain

import (
	"fmt"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	Config       *types.Config
	Storage      ports.PersistentStoragePort
	QueryIn      ports.IncomingQueryPort
	EventIn      ports.IncomingEventPort
	EventOut     ports.OutgoingEventPort
	SessionSetup ports.SessionSetupPort
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.Config = config
}

// Finalize ...
func (d *Domain) Finalize() {
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start() {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	wg.Add(1)
	qchannel := make(chan types.InteractiveSessionChannelRequest, types.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(qchannel)
	go func() {
		defer wg.Done()
		d.QueryIn.Start()
	}()

	// start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(qchannel, &wg)

	wg.Add(1)
	echannel := make(chan types.InteractiveSessionChannelRequest, types.DefaultChannelBufferSize)
	d.EventIn.InitChannel(echannel)
	go func() {
		defer wg.Done()
		d.EventIn.Start()
	}()

	// start the domain's event worker
	wg.Add(1)
	go d.processEventWorker(echannel, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(qchannel chan types.InteractiveSessionChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "domain.processQueryWorker",
	})

	defer wg.Done()

	for request := range qchannel {
		logger.Debugf("received a query - %s", request.Operation)

		interactiveSessionRequest, ok := request.Data.(types.InteractiveSession)
		if !ok {
			request.Response <- types.InteractiveSessionChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into interactive session"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.InteractiveSessionListQueryOp):
			interactiveSessions, err := d.ListInteractiveSessions(request.Actor, request.Emulator)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  interactiveSessions,
					Error: err,
				}
			}
		case string(cacao_common_service.InteractiveSessionGetQueryOp):
			interactiveSession, err := d.GetInteractiveSession(request.Actor, request.Emulator, interactiveSessionRequest.ID)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  interactiveSession,
					Error: err,
				}
			}
		case string(cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp):
			interactiveSession, err := d.GetInteractiveSessionByInstanceID(request.Actor, request.Emulator, interactiveSessionRequest.InstanceID)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  interactiveSession,
					Error: err,
				}
			}
		case string(cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp):
			interactiveSession, err := d.GetInteractiveSessionByInstanceAddress(request.Actor, request.Emulator, interactiveSessionRequest.InstanceAddress)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  interactiveSession,
					Error: err,
				}
			}
		case string(cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp):
			check, err := d.CheckPrerequisitesForInteractiveSession(interactiveSessionRequest.Protocol, interactiveSessionRequest.InstanceAddress, interactiveSessionRequest.InstanceAdminUsername)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  check,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

func (d *Domain) processEventWorker(echannel chan types.InteractiveSessionChannelRequest, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.domain",
		"function": "domain.processEventWorker",
	})

	defer wg.Done()

	for request := range echannel {
		logger.Debugf("received an event - %s", request.Operation)

		interactiveSessionRequest, ok := request.Data.(types.InteractiveSession)
		if !ok {
			request.Response <- types.InteractiveSessionChannelResponse{
				Data:  nil,
				Error: cacao_common_service.NewCacaoMarshalError("unable to convert request data into interactive session"),
			}
			return
		}

		switch request.Operation {
		case string(cacao_common_service.InteractiveSessionCreateRequestedEvent):
			err := d.CreateInteractiveSession(request.Actor, request.Emulator, interactiveSessionRequest, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		case string(cacao_common_service.InteractiveSessionDeactivateRequestedEvent):
			err := d.DeactivateInteractiveSession(request.Actor, request.Emulator, interactiveSessionRequest, request.TransactionID)
			if request.Response != nil {
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  nil,
					Error: err,
				}
			}
		default:
			if request.Response != nil {
				errorMessage := fmt.Sprintf("unhandled query : %s", request.Operation)
				request.Response <- types.InteractiveSessionChannelResponse{
					Data:  nil,
					Error: cacao_common_service.NewCacaoNotImplementedError(errorMessage),
				}
			}
		}
	}
}

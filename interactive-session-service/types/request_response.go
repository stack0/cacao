package types

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
)

// InteractiveSessionChannelRequest is a request struct used between adapters and domain
type InteractiveSessionChannelRequest struct {
	Actor         string
	Emulator      string
	Data          interface{}
	Operation     string
	TransactionID cacao_common.TransactionID
	Response      chan InteractiveSessionChannelResponse // channel created by adapter(or other caller) and will receive the response
}

// InteractiveSessionChannelResponse is a response struct used between adapters and domain
type InteractiveSessionChannelResponse struct {
	Data  interface{}
	Error error
}

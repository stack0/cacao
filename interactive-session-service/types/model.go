package types

import (
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ConvertFromModel converts InteractiveSessionModel to InteractiveSession
func ConvertFromModel(model cacao_common_service.InteractiveSessionModel) InteractiveSession {
	interactiveSession := InteractiveSession{
		ID:                    model.ID,
		Owner:                 model.Owner,
		InstanceID:            model.InstanceID,
		InstanceAddress:       model.InstanceAddress,
		InstanceAdminUsername: model.InstanceAdminUsername,
		CloudID:               model.CloudID,
		Protocol:              model.Protocol,
		RedirectURL:           model.RedirectURL,
		State:                 model.State,
		CreatedAt:             model.CreatedAt,
		UpdatedAt:             model.UpdatedAt,
	}

	return interactiveSession
}

// ConvertToModel converts InteractiveSession to InteractiveSessionModel
func ConvertToModel(session cacao_common_service.Session, interactiveSession InteractiveSession) cacao_common_service.InteractiveSessionModel {
	return cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    session.SessionActor,
			SessionEmulator: session.SessionEmulator,
			ErrorType:       session.ErrorType,
			ErrorMessage:    session.ErrorMessage,
			ServiceError:    session.ServiceError,
		},
		ID:                    interactiveSession.ID,
		Owner:                 interactiveSession.Owner,
		InstanceID:            interactiveSession.InstanceID,
		InstanceAddress:       interactiveSession.InstanceAddress,
		InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
		CloudID:               interactiveSession.CloudID,
		Protocol:              interactiveSession.Protocol,
		RedirectURL:           interactiveSession.RedirectURL,
		State:                 interactiveSession.State,
		CreatedAt:             interactiveSession.CreatedAt,
		UpdatedAt:             interactiveSession.UpdatedAt,
	}
}

// ConvertToListItemModel converts InteractiveSession to InteractiveSessionListItemModel
func ConvertToListItemModel(interactiveSession InteractiveSession) cacao_common_service.InteractiveSessionListItemModel {
	return cacao_common_service.InteractiveSessionListItemModel{
		ID:                    interactiveSession.ID,
		Owner:                 interactiveSession.Owner,
		InstanceID:            interactiveSession.InstanceID,
		InstanceAddress:       interactiveSession.InstanceAddress,
		InstanceAdminUsername: interactiveSession.InstanceAdminUsername,
		CloudID:               interactiveSession.CloudID,
		Protocol:              interactiveSession.Protocol,
		RedirectURL:           interactiveSession.RedirectURL,
		State:                 interactiveSession.State,
		CreatedAt:             interactiveSession.CreatedAt,
		UpdatedAt:             interactiveSession.UpdatedAt,
	}
}

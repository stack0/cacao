package types

import (
	"crypto/ed25519"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"golang.org/x/crypto/ssh"
	"reflect"
)

// EnvConfig are configuration that can be set via environment variables
type EnvConfig struct {
	// NATS
	NatsConfig cacao_common_messaging.NatsConfig
	StanConfig cacao_common_messaging.StanConfig

	// MongoDB
	MongoDBConfig cacao_common_db.MongoDBConfig

	GuacamoleForceHTTP bool `envconfig:"ISESSION_GUAC_FORCE_HTTP"`

	MasterSSHKey                 string `envconfig:"ISESSION_MASTER_SSH_KEY" required:"true"`
	GuacamoleServersEnv          string `envconfig:"ISESSION_GUAC_SERVERS" required:"true"`
	GuacamoleEncryptionKeyBase64 string `envconfig:"ISESSION_GUAC_ENCRYPTION_KEY" required:"true"`
	GuacamoleLiteAuthKey         string `envconfig:"ISESSION_GUAC_AUTH_KEY" required:"true"`
	VNCDefaultPassword           string `envconfig:"ISESSION_VNC_DEFAULT_PASSWORD"`
}

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	// NATS
	NatsConfig cacao_common_messaging.NatsConfig
	StanConfig cacao_common_messaging.StanConfig

	// MongoDB
	MongoDBConfig                           cacao_common_db.MongoDBConfig
	InteractiveSessionMongoDBCollectionName string

	// Guacamole
	GuacamoleForceHTTP     bool
	MasterSSHKey           string
	GuacamoleServers       []GuacamoleServer
	GuacamoleEncryptionKey []byte
	GuacamoleLiteAuthKey   string
	VNCDefaultPassword     string
}

// FromEnvConfig creates a Config object from EnvConfig. This will perform validation on EnvConfig and populate default values.
func FromEnvConfig(envConfig EnvConfig) (Config, error) {
	err := validateEnvConfig(envConfig)
	if err != nil {
		return Config{}, err
	}

	guacServers, err := loadGuacamoleServers(envConfig.GuacamoleServersEnv)
	if err != nil {
		return Config{}, err
	}

	encryptionKey, err := parseGuacamoleEncryptionKey(envConfig)
	if err != nil {
		return Config{}, err
	}

	config := Config{
		NatsConfig:                              envConfig.NatsConfig,
		StanConfig:                              envConfig.StanConfig,
		MongoDBConfig:                           envConfig.MongoDBConfig,
		InteractiveSessionMongoDBCollectionName: DefaultInteractiveSessionMongoDBCollectionName,
		GuacamoleServers:                        guacServers,
		GuacamoleForceHTTP:                      envConfig.GuacamoleForceHTTP,
		MasterSSHKey:                            envConfig.MasterSSHKey,
		GuacamoleEncryptionKey:                  encryptionKey,
		GuacamoleLiteAuthKey:                    envConfig.GuacamoleLiteAuthKey,
		VNCDefaultPassword:                      envConfig.VNCDefaultPassword,
	}
	// This is to overwrite default cluster id
	config.StanConfig.ClusterID = DefaultNatsClusterID
	// This is to overwrite default nats subject
	config.NatsConfig.WildcardSubject = DefaultNatsWildcardSubject

	// other empty parameters will be filled with defaults
	config.processDefaults()
	return config, nil
}

// processDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) processDefaults() {
	// NATS
	if c.NatsConfig.URL == "" {
		c.NatsConfig.URL = DefaultNatsURL
	}

	if c.NatsConfig.QueueGroup == "" {
		c.NatsConfig.QueueGroup = DefaultNatsQueueGroup
	}

	if c.NatsConfig.WildcardSubject == "" {
		c.NatsConfig.WildcardSubject = DefaultNatsWildcardSubject
	}

	if c.NatsConfig.ClientID == "" {
		c.NatsConfig.ClientID = DefaultNatsClientID
	}

	if c.NatsConfig.MaxReconnects <= 0 {
		c.NatsConfig.MaxReconnects = DefaultNatsMaxReconnect
	}

	if c.NatsConfig.ReconnectWait <= 0 {
		c.NatsConfig.ReconnectWait = DefaultNatsReconnectWait
	}

	if c.NatsConfig.RequestTimeout <= 0 {
		c.NatsConfig.RequestTimeout = DefaultNatsRequestTimeout
	}

	if c.StanConfig.ClusterID == "" {
		c.StanConfig.ClusterID = DefaultNatsClusterID
	}

	if c.StanConfig.DurableName == "" {
		c.StanConfig.DurableName = DefaultNatsDurableName
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}
}

func validateEnvConfig(config EnvConfig) error {
	if len(config.MasterSSHKey) == 0 {
		return errors.New("master SSH Key is not provided")
	}
	err := validatePrivateSSHKey(config.MasterSSHKey)
	if err != nil {
		return err
	}

	if len(config.GuacamoleServersEnv) == 0 {
		return errors.New("guacamole Servers are not provided")
	}

	if len(config.GuacamoleEncryptionKeyBase64) == 0 {
		return errors.New("guacamole Encryption Key is not provided")
	}

	if len(config.GuacamoleLiteAuthKey) == 0 {
		return errors.New("guacamole Lite Auth Key is not provided")
	}

	if len(config.VNCDefaultPassword) == 0 {
		return errors.New("VNC Default Password is not provided")
	}
	return nil
}

// loads guacamole server information from environmental variables
func loadGuacamoleServers(GuacamoleServersEnv string) ([]GuacamoleServer, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.types",
		"function": "loadGuacamoleServers",
	})

	var guacamoleServers []GuacamoleServer

	// Guacamole
	if len(GuacamoleServersEnv) == 0 {
		errorMessage := "failed to get guacamole servers from environment variables - value is empty"
		logger.Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	var arr [][]string
	err := json.Unmarshal([]byte(GuacamoleServersEnv), &arr)
	if err != nil {
		errorMessage := fmt.Sprintf("failed to get guacamole servers from environment variables - %s", GuacamoleServersEnv)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	for _, server := range arr {
		if len(server) != 2 {
			errorMessage := fmt.Sprintf("failed to parse guacamole servers from environment variables, item length must be 2 - %s", GuacamoleServersEnv)
			logger.WithError(err).Error(errorMessage)
			return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
		}

		guacServer := GuacamoleServer{
			CloudID: server[0],
			Address: server[1],
		}

		guacamoleServers = append(guacamoleServers, guacServer)
	}
	if len(guacamoleServers) == 0 {
		log.Warn("No Guacamole Server is provided")
	}
	return guacamoleServers, nil
}

func parseGuacamoleEncryptionKey(config EnvConfig) ([]byte, error) {
	encryptionKey, err := base64.StdEncoding.DecodeString(config.GuacamoleEncryptionKeyBase64)
	if err != nil {
		return nil, fmt.Errorf("fail to base64 decode guacamole encryption key")
	}
	if len(encryptionKey) != 32 {
		return nil, fmt.Errorf("guacamole encryption key must be 256 bits")
	}
	return encryptionKey, nil
}

func validatePrivateSSHKey(sshKey string) error {
	parsedKey, err := ssh.ParseRawPrivateKey([]byte(sshKey))
	if err != nil {
		return fmt.Errorf("fail to parse private ssh key, %w", err)
	}
	switch key := parsedKey.(type) {
	case *rsa.PrivateKey:
		if key.Size() < 4096/8 {
			return fmt.Errorf("RSA ssh key is too weak, needs at least 4096 bits")
		}
	case *ed25519.PrivateKey:
	default:
		return fmt.Errorf("unsupported ssh key format, %s", reflect.TypeOf(parsedKey))
	}
	return nil
}

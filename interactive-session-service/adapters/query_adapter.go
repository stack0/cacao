package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingQueryPort
	// internal
	Connection     cacao_common_messaging.QueryEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *QueryAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.Init",
	})

	logger.Info("initializing QueryAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *QueryAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.Finalize",
	})

	logger.Info("finalizing QueryAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *QueryAdapter) getEventHandlerMapping() []cacao_common_messaging.QueryEventHandlerMapping {
	return []cacao_common_messaging.QueryEventHandlerMapping{
		{
			Subject:      cacao_common_service.InteractiveSessionListQueryOp,
			EventHandler: adapter.handleInteractiveSessionListQuery,
		},
		{
			Subject:      cacao_common_service.InteractiveSessionGetQueryOp,
			EventHandler: adapter.handleInteractiveSessionGetQuery,
		},
		{
			Subject:      cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp,
			EventHandler: adapter.handleInteractiveSessionGetByInstanceIDQuery,
		},
		{
			Subject:      cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp,
			EventHandler: adapter.handleInteractiveSessionGetByInstanceAddressQuery,
		},
		{
			Subject:      cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp,
			EventHandler: adapter.handleInteractiveSessionCheckPrerequisitesQuery,
		},
		{
			Subject:      cacao_common.QueryOp(""),
			EventHandler: adapter.handleDefaultQuery,
		},
	}
}

// Start starts the adapter
func (adapter *QueryAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-query" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.ConnectNatsForService(&natsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *QueryAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.StartMock",
	})

	logger.Info("starting QueryAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	natsConn, err := cacao_common_messaging.CreateMockNatsConnection(&adapter.Config.NatsConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS")
	}

	adapter.Connection = natsConn

	defer natsConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *QueryAdapter) handleInteractiveSessionListQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionListQuery",
	})

	var listRequest cacao_common_service.Session
	var listResponse cacao_common_service.InteractiveSessionListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	listResult, err := adapter.IncomingPort.List(listRequest.GetSessionActor(), listRequest.GetSessionEmulator())
	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.InteractiveSessionListItemModel{}
		for _, interactiveSession := range listResult {
			model := types.ConvertToListItemModel(interactiveSession)
			models = append(models, model)
		}

		listResponse.InteractiveSessions = models
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetQuery",
	})

	var getRequest cacao_common_service.InteractiveSessionModel
	var getResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.Get(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.GetID())
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		getResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getResponse.Session = cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getResponse.ServiceError = cerr.GetBase()
		} else {
			getResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetByInstanceIDQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetByInstanceIDQuery",
	})

	var getByInstanceIDRequest cacao_common_service.InteractiveSessionModel
	var getByInstanceIDResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getByInstanceIDRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getByInstanceIDResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getByInstanceIDResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.GetByInstanceID(getByInstanceIDRequest.GetSessionActor(), getByInstanceIDRequest.GetSessionEmulator(), getByInstanceIDRequest.GetInstanceID())
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getByInstanceIDRequest.GetSessionActor(),
			SessionEmulator: getByInstanceIDRequest.GetSessionEmulator(),
		}

		getByInstanceIDResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getByInstanceIDResponse.Session = cacao_common_service.Session{
			SessionActor:    getByInstanceIDRequest.GetSessionActor(),
			SessionEmulator: getByInstanceIDRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getByInstanceIDResponse.ServiceError = cerr.GetBase()
		} else {
			getByInstanceIDResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getByInstanceIDResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetByInstanceAddressQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetByInstanceAddressQuery",
	})

	var getByInstanceAddressRequest cacao_common_service.InteractiveSessionModel
	var getByInstanceAddressResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getByInstanceAddressRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getByInstanceAddressResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getByInstanceAddressResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.IncomingPort.GetByInstanceAddress(getByInstanceAddressRequest.GetSessionActor(), getByInstanceAddressRequest.GetSessionEmulator(), getByInstanceAddressRequest.GetInstanceAddress())
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getByInstanceAddressRequest.GetSessionActor(),
			SessionEmulator: getByInstanceAddressRequest.GetSessionEmulator(),
		}

		getByInstanceAddressResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getByInstanceAddressResponse.Session = cacao_common_service.Session{
			SessionActor:    getByInstanceAddressRequest.GetSessionActor(),
			SessionEmulator: getByInstanceAddressRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getByInstanceAddressResponse.ServiceError = cerr.GetBase()
		} else {
			getByInstanceAddressResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getByInstanceAddressResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionCheckPrerequisitesQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionCheckPrerequisitesQuery",
	})

	var checkPrerequisitesRequest cacao_common_service.InteractiveSessionModel
	var checkPrerequisitesResponse cacao_common_service.Session
	err := json.Unmarshal(jsonData, &checkPrerequisitesRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(checkPrerequisitesResponse)
		if err2 != nil {
			errorMessage := "unable to marshal Session into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	checkResult, err := adapter.IncomingPort.CheckPrerequisites(checkPrerequisitesRequest.GetProtocol(), checkPrerequisitesRequest.GetInstanceAddress(), checkPrerequisitesRequest.GetInstanceAdminUsername())
	if err == nil {
		checkPrerequisitesResponse = cacao_common_service.Session{
			SessionActor:    checkPrerequisitesRequest.GetSessionActor(),
			SessionEmulator: checkPrerequisitesRequest.GetSessionEmulator(),
		}

		if !checkResult {
			checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoGeneralError("Prerequisites are not met").GetBase()
		}
	} else {
		logger.Error(err)

		checkPrerequisitesResponse = cacao_common_service.Session{
			SessionActor:    checkPrerequisitesRequest.GetSessionActor(),
			SessionEmulator: checkPrerequisitesRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			checkPrerequisitesResponse.ServiceError = cerr.GetBase()
		} else {
			checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(checkPrerequisitesResponse)
	if err != nil {
		errorMessage := "unable to marshal Session into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleDefaultQuery(subject cacao_common.QueryOp, transactionID cacao_common.TransactionID, jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleDefaultQuery",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)

	errResponse := cacao_common_service.InteractiveSessionModel{}
	errMessage := fmt.Sprintf("request %s is not handled", subject)
	errResponse.ServiceError = cacao_common_service.NewCacaoNotImplementedError(errMessage).GetBase()

	resBytes, err := json.Marshal(errResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

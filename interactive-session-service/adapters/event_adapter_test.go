package adapters

import (
	"encoding/json"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// generate a config for unit tests
func testConfig() types.Config {
	var envConf types.EnvConfig
	var config types.Config
	err := envconfig.Process("", &envConf.NatsConfig)
	if err != nil {
		panic(err)
	}
	err = envconfig.Process("", &envConf.StanConfig)
	if err != nil {
		panic(err)
	}
	err = envconfig.Process("", &envConf.MongoDBConfig)
	if err != nil {
		panic(err)
	}
	envConf.NatsConfig.WildcardSubject = types.DefaultNatsWildcardSubject
	envConf.NatsConfig.ClientID = "foobar"
	config = types.Config{
		NatsConfig:                              envConf.NatsConfig,
		StanConfig:                              envConf.StanConfig,
		MongoDBConfig:                           envConf.MongoDBConfig,
		InteractiveSessionMongoDBCollectionName: "foobar",
	}
	return config
}

func createTestEventInPort() *ports.MockIncomingEventPort {
	var config = testConfig()

	eventInImpl := &ports.MockIncomingEventPort{}
	eventInImpl.Init(&config)

	return eventInImpl
}

func createTestEventAdapter(eventInImpl ports.IncomingEventPort) *EventAdapter {
	var config = testConfig()

	eventAdapter := &EventAdapter{}
	eventAdapter.Init(&config)

	eventAdapter.IncomingPort = eventInImpl

	go eventAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return eventAdapter
}

func TestInitEventAdapter(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)
	assert.NotNil(t, eventAdapter)
	assert.NotEmpty(t, eventAdapter.Connection)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestInteractiveSessionCreateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                    testInteractiveSessionID,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSession.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.InteractiveSessionCreateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestInteractiveSessionCreateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID:                    testInteractiveSessionID,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
	}

	eventInImpl.SetCreateHandler(func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSession.ID)
		return fmt.Errorf("unable to create an interactive session")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.InteractiveSessionCreateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestInteractiveSessionDeactivateEvent(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}

	eventInImpl.SetDeactivateHandler(func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSession.ID)
		return nil
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.InteractiveSessionDeactivateRequestedEvent, eventData)
	assert.NoError(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestInteractiveSessionDeactivateEventFailed(t *testing.T) {
	eventInImpl := createTestEventInPort()
	eventAdapter := createTestEventAdapter(eventInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}

	eventInImpl.SetDeactivateHandler(func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSession.ID)
		return fmt.Errorf("unable to deactivate an interactive session")
	})

	err := eventAdapter.Connection.Publish(cacao_common_service.InteractiveSessionDeactivateRequestedEvent, eventData)
	assert.Error(t, err)

	eventAdapter.Finalize()
	eventInImpl.Finalize()
}

func TestInteractiveSessionCreatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := types.InteractiveSession{
		ID:         testInteractiveSessionID,
		Owner:      testUser,
		InstanceID: "test_instanceID",
		Protocol:   "vnc",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.InteractiveSessionCreatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.InteractiveSessionCreatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.InteractiveSessionModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testInteractiveSessionID, response.ID)
		return nil
	})

	err := eventAdapter.Created(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestInteractiveSessionCreateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := types.InteractiveSession{
		ID:         testInteractiveSessionID,
		Owner:      testUser,
		InstanceID: "test_instanceID",
		Protocol:   "vnc",
	}

	testErrorMessage := "interactive session is not created"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.InteractiveSessionCreateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.InteractiveSessionCreateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.InteractiveSessionModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testInteractiveSessionID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.CreateFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestInteractiveSessionDeactivatedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := types.InteractiveSession{
		ID:         testInteractiveSessionID,
		Owner:      testUser,
		InstanceID: "test_instanceID",
		Protocol:   "vnc",
	}

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.InteractiveSessionDeactivatedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.InteractiveSessionDeactivatedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.InteractiveSessionModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testInteractiveSessionID, response.ID)
		return nil
	})

	err := eventAdapter.Deactivated(testUser, testUser, eventData, testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

func TestInteractiveSessionDeactivateFailedEvent(t *testing.T) {
	eventAdapter := createTestEventAdapter(nil)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	eventData := types.InteractiveSession{
		ID:         testInteractiveSessionID,
		Owner:      testUser,
		InstanceID: "test_instanceID",
		Protocol:   "vnc",
	}

	testErrorMessage := "interactive session is not created"

	testTransactionID := cacao_common_messaging.NewTransactionID()

	eventAdapter.Connection.AddEventHandler(cacao_common_service.InteractiveSessionDeactivateFailedEvent, func(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
		assert.Equal(t, subject, cacao_common_service.InteractiveSessionDeactivateFailedEvent)
		assert.NotEmpty(t, jsonData)
		assert.Equal(t, transactionID, testTransactionID)

		var response cacao_common_service.InteractiveSessionModel
		err := json.Unmarshal(jsonData, &response)
		assert.NoError(t, err)

		assert.Equal(t, testUser, response.GetSessionActor())
		assert.Equal(t, testUser, response.GetSessionEmulator())
		assert.Equal(t, testInteractiveSessionID, response.ID)
		assert.Equal(t, testErrorMessage, response.GetServiceError().ContextualError())
		return nil
	})

	err := eventAdapter.DeactivateFailed(testUser, testUser, eventData, fmt.Errorf(testErrorMessage), testTransactionID)
	assert.NoError(t, err)

	eventAdapter.Finalize()
}

package adapters

import (
	"encoding/json"
	"fmt"
	"sync"

	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_messaging "gitlab.com/cyverse/cacao-common/messaging"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	Config       *types.Config
	IncomingPort ports.IncomingEventPort
	// internal
	Connection     cacao_common_messaging.StreamingEventService
	EventWaitGroup sync.WaitGroup
}

// Init initializes the adapter
func (adapter *EventAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Init",
	})

	logger.Info("initializing EventAdapter")

	adapter.Config = config

	// Use a WaitGroup to wait for a message to arrive
	adapter.EventWaitGroup = sync.WaitGroup{}
	adapter.EventWaitGroup.Add(1)
}

// Finalize finalizes the adapter
func (adapter *EventAdapter) Finalize() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Finalize",
	})

	logger.Info("finalizing EventAdapter")

	adapter.EventWaitGroup.Done()
}

func (adapter *EventAdapter) getEventHandlerMapping() []cacao_common_messaging.StreamingEventHandlerMapping {
	return []cacao_common_messaging.StreamingEventHandlerMapping{
		{
			Subject:      cacao_common_service.InteractiveSessionCreateRequestedEvent,
			EventHandler: adapter.handleInteractiveSessionCreateRequest,
		},
		{
			Subject:      cacao_common_service.InteractiveSessionDeactivateRequestedEvent,
			EventHandler: adapter.handleInteractiveSessionDeactivateRequest,
		},
		{
			Subject:      cacao_common.EventType(""),
			EventHandler: adapter.handleDefaultEvent,
		},
	}
}

// Start starts the adapter
func (adapter *EventAdapter) Start() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")

	natsConfig := adapter.Config.NatsConfig
	natsConfig.ClientID = natsConfig.ClientID + "-event" + xid.New().String()

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.ConnectStanForService(&natsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

// StartMock starts the adapter
func (adapter *EventAdapter) StartMock() {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.StartMock",
	})

	logger.Info("starting EventAdapter")

	eventHandlerMappings := adapter.getEventHandlerMapping()

	stanConn, err := cacao_common_messaging.CreateMockStanConnection(&adapter.Config.NatsConfig, &adapter.Config.StanConfig, eventHandlerMappings)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to NATS Streaming")
	}

	adapter.Connection = stanConn

	defer stanConn.Disconnect()

	// Wait for a message to come in
	adapter.EventWaitGroup.Wait()

	adapter.Connection = nil
}

func (adapter *EventAdapter) handleInteractiveSessionCreateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.handleInteractiveSessionCreateRequest",
	})

	var createRequest cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &createRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	interactiveSession := types.ConvertFromModel(createRequest)

	err = adapter.IncomingPort.Create(createRequest.GetSessionActor(), createRequest.GetSessionEmulator(), interactiveSession, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (adapter *EventAdapter) handleInteractiveSessionDeactivateRequest(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.handleInteractiveSessionDeactivateRequest",
	})

	var deactivateRequest cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &deactivateRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		return fmt.Errorf(errorMessage)
	}

	interactiveSession := types.ConvertFromModel(deactivateRequest)

	err = adapter.IncomingPort.Deactivate(deactivateRequest.GetSessionActor(), deactivateRequest.GetSessionEmulator(), interactiveSession, transactionID)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

// Implement OutgoingEventPort

// Created ...
func (adapter *EventAdapter) Created(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Created",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, interactiveSession)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.InteractiveSessionCreatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionCreatedEvent)
	}

	return nil
}

// CreateFailed ...
func (adapter *EventAdapter) CreateFailed(actor string, emulator string, interactiveSession types.InteractiveSession, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       creationError.Error(),
		ErrorMessage:    creationError.Error(),
	}

	if cerr, ok := creationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(creationError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, interactiveSession)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.InteractiveSessionCreateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionCreateFailedEvent)
	}

	return nil
}

// Deactivated ...
func (adapter *EventAdapter) Deactivated(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.Deactivated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       "",
		ErrorMessage:    "",
	}

	model := types.ConvertToModel(session, interactiveSession)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.InteractiveSessionDeactivatedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionDeactivatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionDeactivatedEvent)
	}

	return nil
}

// DeactivateFailed ...
func (adapter *EventAdapter) DeactivateFailed(actor string, emulator string, interactiveSession types.InteractiveSession, deactivationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.DeactivateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
		ErrorType:       deactivationError.Error(),
		ErrorMessage:    deactivationError.Error(),
	}

	if cerr, ok := deactivationError.(cacao_common_service.CacaoError); ok {
		session.ServiceError = cerr.GetBase()
	} else {
		session.ServiceError = cacao_common_service.NewCacaoGeneralError(deactivationError.Error()).GetBase()
	}

	model := types.ConvertToModel(session, interactiveSession)

	err := adapter.Connection.PublishWithTransactionID(cacao_common_service.InteractiveSessionDeactivateFailedEvent, model, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionDeactivateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.InteractiveSessionDeactivateFailedEvent)
	}

	return nil
}

func (adapter *EventAdapter) handleDefaultEvent(subject cacao_common.EventType, transactionID cacao_common.TransactionID, jsonData []byte) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "EventAdapter.handleDefaultEvent",
	})

	logger.Tracef("received an unhandled event %s, TransactionID %s", subject, transactionID)
	return nil
}

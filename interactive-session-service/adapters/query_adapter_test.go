package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

func createTestQueryPort() *ports.MockIncomingQueryPort {
	var config = testConfig()

	queryInImpl := &ports.MockIncomingQueryPort{}
	queryInImpl.Init(&config)

	return queryInImpl
}

func createTestQueryAdapter(queryInImpl ports.IncomingQueryPort) *QueryAdapter {
	var config = testConfig()

	queryAdapter := &QueryAdapter{}
	queryAdapter.Init(&config)

	queryAdapter.IncomingPort = queryInImpl

	go queryAdapter.StartMock()

	time.Sleep(100 * time.Millisecond)
	return queryAdapter
}

func TestInitQueryAdapter(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)
	assert.NotNil(t, queryAdapter)
	assert.NotEmpty(t, queryAdapter.Connection)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionListQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	testTime := time.Now().UTC()

	expectedResults := []types.InteractiveSession{
		{
			ID:                    "0001",
			Owner:                 testUser,
			InstanceID:            "test_instance1",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud1",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
		{
			ID:                    "0002",
			Owner:                 testUser,
			InstanceID:            "test_instance2",
			InstanceAddress:       "127.0.0.1",
			InstanceAdminUsername: "ubuntu",
			CloudID:               "test_cloud2",
			Protocol:              "vnc",
			RedirectURL:           "",
			State:                 cacao_common_service.InteractiveSessionStateActive,
			CreatedAt:             testTime,
			UpdatedAt:             testTime,
		},
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return expectedResults, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result)
	assert.NoError(t, result.GetServiceError())
	assert.EqualValues(t, len(expectedResults), len(result.GetInteractiveSessions()))

	firstInteractiveSession := result.GetInteractiveSessions()[0]
	assert.EqualValues(t, expectedResults[0].ID, firstInteractiveSession.GetID())
	assert.EqualValues(t, expectedResults[0].InstanceID, firstInteractiveSession.GetInstanceID())
	assert.EqualValues(t, expectedResults[0].InstanceAddress, firstInteractiveSession.GetInstanceAddress())
	assert.EqualValues(t, expectedResults[0].InstanceAdminUsername, firstInteractiveSession.GetInstanceAdminUsername())
	assert.EqualValues(t, expectedResults[0].CloudID, firstInteractiveSession.GetCloudID())
	assert.EqualValues(t, expectedResults[0].Protocol, firstInteractiveSession.GetProtocol())
	assert.EqualValues(t, expectedResults[0].State, firstInteractiveSession.GetState())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionListQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"

	queryData := cacao_common_service.Session{
		SessionActor:    testUser,
		SessionEmulator: testUser,
	}

	queryInImpl.SetListHandler(func(actor string, emulator string) ([]types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		return nil, fmt.Errorf("unable to list interactive sessions")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionListQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionListModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.InteractiveSessions)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instance1",
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSessionID)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		ID: testInteractiveSessionID,
	}

	queryInImpl.SetGetHandler(func(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInteractiveSessionID, interactiveSessionID)
		return types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetByInstanceIDQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceID := "test_instanceID1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceID: testInstanceID,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            testInstanceID,
		InstanceAddress:       "127.0.0.1",
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}

	queryInImpl.SetGetByInstanceIDHandler(func(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInstanceID, instanceID)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetByInstanceIDQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInstanceID := "test_instanceID1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceID: testInstanceID,
	}

	queryInImpl.SetGetByInstanceIDHandler(func(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInstanceID, instanceID)
		return types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session by InstanceID")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetByInstanceAddressQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInteractiveSessionID := cacao_common_service.NewInteractiveSessionID()
	testInstanceAddress := "127.0.0.1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress: testInstanceAddress,
	}

	testTime := time.Now().UTC()

	expectedResult := types.InteractiveSession{
		ID:                    testInteractiveSessionID,
		Owner:                 testUser,
		InstanceID:            "test_instanceID1",
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: "ubuntu",
		CloudID:               "test_cloud1",
		Protocol:              "vnc",
		RedirectURL:           "",
		State:                 cacao_common_service.InteractiveSessionStateActive,
		CreatedAt:             testTime,
		UpdatedAt:             testTime,
	}

	queryInImpl.SetGetByInstanceAddressHandler(func(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInstanceAddress, instanceAddress)
		return expectedResult, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NotEmpty(t, result.ID)
	assert.NoError(t, result.GetServiceError())

	assert.Equal(t, expectedResult.ID, result.ID)
	assert.Equal(t, expectedResult.InstanceID, result.InstanceID)
	assert.Equal(t, expectedResult.InstanceAddress, result.InstanceAddress)
	assert.Equal(t, expectedResult.InstanceAdminUsername, result.InstanceAdminUsername)
	assert.Equal(t, expectedResult.CloudID, result.CloudID)
	assert.Equal(t, expectedResult.Protocol, result.Protocol)
	assert.Equal(t, expectedResult.State, result.State)

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionGetByInstanceAddressQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress: testInstanceAddress,
	}

	queryInImpl.SetGetByInstanceAddressHandler(func(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
		assert.Equal(t, testUser, actor)
		assert.Equal(t, testUser, emulator)
		assert.Equal(t, testInstanceAddress, instanceAddress)
		return types.InteractiveSession{}, fmt.Errorf("unable to get an interactive session by Instance Address")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.InteractiveSessionModel

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Empty(t, result.ID)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionCheckPrerequisitesQuery(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"
	testInstanceAdminUsername := "ubuntu"
	testProtocol := cacao_common_service.InteractiveSessionProtocolSSH

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: testInstanceAdminUsername,
		Protocol:              testProtocol,
	}

	queryInImpl.SetCheckPrerequisitesHandler(func(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
		assert.Equal(t, testProtocol, protocol)
		assert.Equal(t, testInstanceAddress, instanceAddress)
		assert.Equal(t, testInstanceAdminUsername, instanceAdminUsername)
		return true, nil
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.Session

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.NoError(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

func TestInteractiveSessionCheckPrerequisitesQueryFailed(t *testing.T) {
	queryInImpl := createTestQueryPort()
	queryAdapter := createTestQueryAdapter(queryInImpl)

	testUser := "test_user1"
	testInstanceAddress := "127.0.0.1"
	testInstanceAdminUsername := "ubuntu"
	testProtocol := cacao_common_service.InteractiveSessionProtocolSSH

	queryData := cacao_common_service.InteractiveSessionModel{
		Session: cacao_common_service.Session{
			SessionActor:    testUser,
			SessionEmulator: testUser,
		},
		InstanceAddress:       testInstanceAddress,
		InstanceAdminUsername: testInstanceAdminUsername,
		Protocol:              testProtocol,
	}

	queryInImpl.SetCheckPrerequisitesHandler(func(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
		assert.Equal(t, testProtocol, protocol)
		assert.Equal(t, testInstanceAddress, instanceAddress)
		assert.Equal(t, testInstanceAdminUsername, instanceAdminUsername)
		return false, fmt.Errorf("prerequisites are not met")
	})

	responseData, err := queryAdapter.Connection.Request(context.Background(), cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp, queryData)
	assert.NoError(t, err)
	assert.NotEmpty(t, responseData)

	var result cacao_common_service.Session

	err = json.Unmarshal(responseData, &result)
	assert.NoError(t, err)
	assert.Error(t, result.GetServiceError())

	queryAdapter.Finalize()
	queryInImpl.Finalize()
}

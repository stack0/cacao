package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// CreateHandler is a handler for Create event
type CreateHandler func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error

// DeactivateHandler is a handler for Deactivate event
type DeactivateHandler func(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error

// MockIncomingEventPort is a mock implementation of IncomingEventPort
type MockIncomingEventPort struct {
	Config            *types.Config
	CreateHandler     CreateHandler
	DeactivateHandler DeactivateHandler
}

// Init inits the port
func (port *MockIncomingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingEventPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingEventPort) InitChannel(channel chan types.InteractiveSessionChannelRequest) {}

// Start starts the port
func (port *MockIncomingEventPort) Start() {}

// SetCreateHandler sets a handler for Create event
func (port *MockIncomingEventPort) SetCreateHandler(createHandler CreateHandler) {
	port.CreateHandler = createHandler
}

// SetDeactivateHandler sets a handler for Deactivate event
func (port *MockIncomingEventPort) SetDeactivateHandler(deactivateHandler DeactivateHandler) {
	port.DeactivateHandler = deactivateHandler
}

// Create creates an interactive session
func (port *MockIncomingEventPort) Create(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	return port.CreateHandler(actor, emulator, interactiveSession, transactionID)
}

// Deactivate deactivates an interactive session
func (port *MockIncomingEventPort) Deactivate(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error {
	return port.DeactivateHandler(actor, emulator, interactiveSession, transactionID)
}

package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// CreatedHandler is a handler for Created event
type CreatedHandler func(actor string, emulator string, interactiveSession types.InteractiveSession, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// CreateFailedHandler is a handler for CreateFailed event
type CreateFailedHandler func(actor string, emulator string, interactiveSession types.InteractiveSession, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error

// MockOutgoingEventPort is a mock implementation of OutgoingEventPort
type MockOutgoingEventPort struct {
	Config              *types.Config
	CreatedHandler      CreatedHandler
	CreateFailedHandler CreateFailedHandler
}

// Init inits the port
func (port *MockOutgoingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockOutgoingEventPort) Finalize() {}

// SetCreatedHandler sets a handler for Created event
func (port *MockOutgoingEventPort) SetCreatedHandler(createdHandler CreatedHandler) {
	port.CreatedHandler = createdHandler
}

// SetCreateFailedHandler sets a handler for CreateFailed event
func (port *MockOutgoingEventPort) SetCreateFailedHandler(createFailedHandler CreateFailedHandler) {
	port.CreateFailedHandler = createFailedHandler
}

// Created creates an event for interactive session creation
func (port *MockOutgoingEventPort) Created(actor string, emulator string, interactiveSession types.InteractiveSession, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CreatedHandler(actor, emulator, interactiveSession, status, transactionID)
}

// CreateFailed creates an event for interactive session creation failure
func (port *MockOutgoingEventPort) CreateFailed(actor string, emulator string, interactiveSession types.InteractiveSession, status cacao_common.HTTPStatus, transactionID cacao_common.TransactionID) error {
	return port.CreateFailedHandler(actor, emulator, interactiveSession, status, transactionID)
}

package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// ListHandler is a handler for List query
type ListHandler func(actor string, emulator string) ([]types.InteractiveSession, error)

// GetHandler is a handler for Get query
type GetHandler func(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error)

// GetByInstanceIDHandler is a handler for GetByInstanceID query
type GetByInstanceIDHandler func(actor string, emulator string, instanceID string) (types.InteractiveSession, error)

// GetByInstanceAddressHandler is a handler for GetByInstanceAddress query
type GetByInstanceAddressHandler func(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error)

// CheckPrerequisitesHandler is a handler for CheckPrerequisitesHandler query
type CheckPrerequisitesHandler func(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)

// MockIncomingQueryPort is a mock implementation of IncomingQueryPort
type MockIncomingQueryPort struct {
	Config                      *types.Config
	ListHandler                 ListHandler
	GetHandler                  GetHandler
	GetByInstanceIDHandler      GetByInstanceIDHandler
	GetByInstanceAddressHandler GetByInstanceAddressHandler
	CheckPrerequisitesHandler   CheckPrerequisitesHandler
}

// Init inits the port
func (port *MockIncomingQueryPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingQueryPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingQueryPort) InitChannel(channel chan types.InteractiveSessionChannelRequest) {}

// Start starts the port
func (port *MockIncomingQueryPort) Start() {}

// SetListHandler sets a handler for List query
func (port *MockIncomingQueryPort) SetListHandler(listHandler ListHandler) {
	port.ListHandler = listHandler
}

// SetGetHandler sets a handler for Get query
func (port *MockIncomingQueryPort) SetGetHandler(getHandler GetHandler) {
	port.GetHandler = getHandler
}

// SetGetByInstanceIDHandler sets a handler for GetByInstanceID query
func (port *MockIncomingQueryPort) SetGetByInstanceIDHandler(getByInstanceIDHandler GetByInstanceIDHandler) {
	port.GetByInstanceIDHandler = getByInstanceIDHandler
}

// SetGetByInstanceAddressHandler sets a handler for GetByInstanceAddress query
func (port *MockIncomingQueryPort) SetGetByInstanceAddressHandler(getByInstanceAddressHandler GetByInstanceAddressHandler) {
	port.GetByInstanceAddressHandler = getByInstanceAddressHandler
}

// SetCheckPrerequisitesHandler sets a handler for CheckPrerequisites query
func (port *MockIncomingQueryPort) SetCheckPrerequisitesHandler(checkPrerequisitesHandler CheckPrerequisitesHandler) {
	port.CheckPrerequisitesHandler = checkPrerequisitesHandler
}

// List lists interactive sessions
func (port *MockIncomingQueryPort) List(actor string, emulator string) ([]types.InteractiveSession, error) {
	return port.ListHandler(actor, emulator)
}

// Get returns an interactive session
func (port *MockIncomingQueryPort) Get(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	return port.GetHandler(actor, emulator, interactiveSessionID)
}

// GetByInstanceID returns an interactive session by InstanceID
func (port *MockIncomingQueryPort) GetByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error) {
	return port.GetByInstanceIDHandler(actor, emulator, instanceID)
}

// GetByInstanceAddress returns an interactive session by Instance Address
func (port *MockIncomingQueryPort) GetByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error) {
	return port.GetByInstanceAddressHandler(actor, emulator, instanceAddress)
}

// CheckPrerequisites checks if prerequisites for the interactive session are met
func (port *MockIncomingQueryPort) CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error) {
	return port.CheckPrerequisitesHandler(protocol, instanceAddress, instanceAdminUsername)
}

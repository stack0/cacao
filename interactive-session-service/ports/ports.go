package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// Port is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type Port interface {
	Init(config *types.Config)
	Finalize()
}

// AsyncPort ...
type AsyncPort interface {
	Port
	InitChannel(channel chan types.InteractiveSessionChannelRequest)
	Start()
}

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	AsyncPort
	List(actor string, emulator string) ([]types.InteractiveSession, error)
	Get(actor string, emulator string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error)
	GetByInstanceID(actor string, emulator string, instanceID string) (types.InteractiveSession, error)
	GetByInstanceAddress(actor string, emulator string, instanceAddress string) (types.InteractiveSession, error)
	CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	AsyncPort
	Create(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error
	Deactivate(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error
}

// OutgoingEventPort is an outgoing event port
type OutgoingEventPort interface {
	Port
	Created(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error
	CreateFailed(actor string, emulator string, interactiveSession types.InteractiveSession, creationError error, transactionID cacao_common.TransactionID) error
	Deactivated(actor string, emulator string, interactiveSession types.InteractiveSession, transactionID cacao_common.TransactionID) error
	DeactivateFailed(actor string, emulator string, interactiveSession types.InteractiveSession, deactivationError error, transactionID cacao_common.TransactionID) error
}

// SessionSetupPort is an interface for setting up sessions for VNC or SSH on instances
type SessionSetupPort interface {
	Port
	Setup(user string, cloudID string, protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (string, error)
	CheckPrerequisites(protocol cacao_common_service.InteractiveSessionProtocol, instanceAddress string, instanceAdminUsername string) (bool, error)
}

// PersistentStoragePort is an interface for Perstistent Storage.
type PersistentStoragePort interface {
	Port
	List(user string) ([]types.InteractiveSession, error)
	Get(user string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error)
	GetByInstanceID(user string, instanceID string) (types.InteractiveSession, error)
	GetByInstanceAddress(user string, instanceAddress string) (types.InteractiveSession, error)
	Create(interactiveSession types.InteractiveSession) error
	Update(interactiveSession types.InteractiveSession, updateFieldNames []string) error
	DeactivateAllByInstanceID(instanceID string) (int64, error)
}

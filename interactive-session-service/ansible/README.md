This folder contains  ansible for the interactive sessions

`playbook-ssh-setup.yml` makes the following assumptions

* USERNAME has already been created and can already deposit the ssh key into username

Example execution:

ansible-playbook --private-key someprivate -i '1.2.3.4,' -e USERNAME=testuser -e PUBLIC_KEY_PATH=/path/to/localdir/somekey.pub `playbook-ssh-setup.yml`

Outstanding question

* Should we assume file or environment variable to contain public key (environment variable may be better)

| Variable                | Required | Default                    | Choices  | Comments                                   |
|-------------------------|----------|----------------------------|----------|--------------------------------------------|
| USERNAME                | yes      | no default                 |          | Username that EXTERNAL_HOST can ssh as     |
| PUBIC_KEY_PATH          | yes      | no default                 |          | Location of generated key pair             |

--------------------------
`playbook-vnc-setup.yml` makes the following assumptions

* USERNAME has already been created and can already deposit the ssh key into username
* GUACAMOLE_SERVER_IP is the public ip that the vm should authorize for vnc connections

Example execution:

ansible-playbook --private-key someprivate -i '1.2.3.4,' -e USERNAME=testuser -e GUACAMOLE_SERVER_IP=w.x.y.z `playbook-vnc-setup.yml`

Outstanding question

* Should we assume file or environment variable to contain public key (environment variable may be better)

| Variable                | Required | Default                    | Choices  | Comments                                   |
|-------------------------|----------|----------------------------|----------|--------------------------------------------|
| USERNAME                | yes      | no default                 |          | Username that EXTERNAL_HOST can ssh as     |
| GUACAMOLE_SERVER_IP     | yes      | no default                 |          | guacamole server ip                        |
| VNC_DISPLAY_NUMBER      | no       | 5                          |          | display number translates to port 5900+display number (e.g. 5 = port 5905)  |